export * from './routes/base/institutes.base.route';

/**
 * institue
 */
export * from './routes/institute.route';
export * from './controllers/institutes.controller';
export * from './models/institute.model';
export * from './models/schema/institute';

/**
 * faculty
 */
export * from './routes/faculties.route';
export * from './controllers/faculties.controller';
export * from './models/faculty.model';
export * from './models/schema/faculty';
export * from './models/faculty.model';
export * from './models/schema/faculty';

/**
 * Program
 */
export * from './routes/programs.route';
export * from './controllers/programs.controller';
export * from './models/program.model';
export * from './models/schema/program';

/**
 * Education Level
 */
export * from './routes/education-levels.routes';
export * from './controllers/education-levels.controller';
export * from './models/education-level.model';
export * from './models/schema/education-level';

/**
 * Program Details
 */
export * from './routes/program-details.route';
export * from './controllers/program-details.controller';
export * from './models/program-detail.model';
export * from './models/schema/program-detail';

/**
 * Institute Type
 */
export * from './routes/institute-types.routes';
export * from './controllers/institute-types.controller';
export * from './models/institute-type.model';
export * from './models/schema/institute-type';

/**
 * License Fee
 */
export * from './routes/license-fees.route';
export * from './controllers/license-fees.controller';
export * from './models/license-fee.model';
export * from './models/schema/license-fee';

/**
 * Course
 */
export * from './routes/courses.routes';
export * from './controllers/courses.controller';
export * from './models/course.model';
export * from './models/schema/course';

/**
 * Classes
 */
export * from './routes/classes.routes';
export * from './controllers/classes.controller';
export * from './models/classes.model';
export * from './models/schema/classes';

/**
 * Academic Calendar Program Detail
 */
export * from './routes/academic-calendar-program-details.routes';
export * from './controllers/academic-calendar-program-details.controller';
export * from './models/academic-calendar-program-detail.model';
export * from './models/schema/academic-calendar-program-detail';

/**
 * Faculty Program Detail
 */
export * from './routes/faculty-program-details.routes';
export * from './controllers/faculty-program-details.controller';
export * from './models/faculty-program-detail.model';
export * from './models/schema/faculty-program-detail';

/** 
 * Academic Calendar
 */
export * from './routes/academic-calendars.routes';
export * from './controllers/academic-calendars.controller';
export * from './models/academic-calendar.model';
export * from './models/schema/academic-calendar';

/**
 * RoadMap
 */
export * from './routes/roadmaps.routes';
export * from './controllers/roadmaps.controller';
export * from './models/roadmap.model';
export * from './models/schema/roadmap';

/**
 * RoadMap Pre Requisite Course
 */
export * from './routes/roadmap-pre-requisite-courses.routes';
export * from './controllers/roadmap-pre-requisite-courses.controller';
export * from './models/roadmap-pre-requisite-course.model';
export * from './models/schema/roadmap-pre-requisite-course';
