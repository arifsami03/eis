import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Program, Faculty } from '../';
import { EducationLevelModel } from '../'
import { Sequelize } from 'sequelize-typescript';

export class ProgramModel extends BaseModel {
  constructor() {
    super(Program);
  }
  /**
   * it will find all programs
   */
  index() {
    this.openConnection();
    var programs = [];
    return this.findAllByConditions(['id', 'name', 'educationLevelId'],
      { educationLevelId: { [Sequelize.Op.ne]: null } }).then(res => {
        return Promise.each(res, (resItem) => {

          return new EducationLevelModel().findByCondition([ 'education'], { id: resItem['educationLevelId'] }).then(finalRes => {

            programs.push({
              id: resItem['id'],
              name: resItem['name'],
              educationLevel: finalRes
            });
          })

        }).then(()=>{
          return programs;
        })
      });

   
  }

  find(id) {
    //this.openConnection();
    return this.findByCondition(['id', 'name',  'educationLevelId'],{ id: id });
  }

  getPrograms(id) {
    return this.findAllByConditions(['id', 'name'], { facultyId: id });
  }
  getProgramsList(){
    return this.findAll(['id', 'name']);
  }

  // /**
  //  * it will one all program
  //  */
  // findOneProgram(id) {
  //   this.openConnection();
  //   return this.sequelizeModel.findById(id);
  // }

  // /**
  //  * it will create new program
  //  */
  // create(item) {
  //   this.openConnection();
  //   let program = item;
  //   return this.sequelizeModel.create(program);
  // }

  // /**
  //  * it will update existing program
  //  */
  // update(id, item) {
  //   this.openConnection();
  //   const program = item;
  //   return this.sequelizeModel.findById(id).then(_program => {
  //     if (_program) {
  //       return this.sequelizeModel.update(program, {
  //         where: { id: _program.id },
  //         validate: true
  //       });
  //     }
  //   });
  // }
}
