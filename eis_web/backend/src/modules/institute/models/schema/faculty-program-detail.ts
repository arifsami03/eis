import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { Faculty, ProgramDetail } from '../..';

@Table({ timestamps: true })
export class FacultyProgramDetail extends Model<FacultyProgramDetail> {

  @ForeignKey(() => Faculty)
  @Column
  facultyId: string;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Faculty, {
    foreignKey: 'facultyId'
  })
  faculty: Faculty;

  @BelongsTo(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
