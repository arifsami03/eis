import { Table, Column, Model, ForeignKey, BelongsTo, HasMany } from 'sequelize-typescript';
import { ProgramDetail, AcademicCalendar, Classes, Course, RoadMapPreRequisiteCourse } from '../..';

@Table({ timestamps: true })
export class RoadMap extends Model<RoadMap> {
  @Column title: string;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @ForeignKey(() => AcademicCalendar)
  @Column
  academicCalendarId: number;

  @ForeignKey(() => Course)
  @Column
  courseId: number;

  @ForeignKey(() => Classes)
  @Column
  classId: number;

  @Column courseCode: number;

  @Column natureOfCourse: string;

  @Column courseType: string;

  @Column courseOrder: number;

  @Column creditHours: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * Has Many Relationships
   */
  @HasMany(() => RoadMapPreRequisiteCourse, {
    constraints: false,
    foreignKey: 'courseId'
  })
  preReqCoursesList: RoadMapPreRequisiteCourse[];
}
