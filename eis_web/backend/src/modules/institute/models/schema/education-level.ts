import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { InstituteType } from '../..';

@Table({ timestamps: true })
export class EducationLevel extends Model<EducationLevel> {
  @Column education: string;

  @ForeignKey(() => InstituteType)
  @Column instituteTypeId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => InstituteType, {
    foreignKey: 'instituteTypeId'
  })
  instituteType: InstituteType;
}
