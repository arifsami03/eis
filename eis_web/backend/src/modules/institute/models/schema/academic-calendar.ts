import { Table, Column, Model, HasMany, BelongsToMany } from 'sequelize-typescript';
import { AcademicCalendarProgramDetail, ProgramDetail } from '../..';

@Table({ timestamps: true })
export class AcademicCalendar extends Model<AcademicCalendar> {
  @Column title: string;
  @Column abbreviation: string;
  @Column description: string;
  @Column createdBy: number;
  @Column updatedBy: number;

  /**
   * Has Many Relationships
   */
  @HasMany(() => AcademicCalendarProgramDetail, { foreignKey: 'academicCalendarId' })
  academicCalendarProgramDetails: AcademicCalendarProgramDetail[];
}
