import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { FacultyProgramDetail } from '../..';

@Table({ timestamps: true })
export class Faculty extends Model<Faculty> {
  @Column name: string;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => FacultyProgramDetail, { foreignKey: 'facultyId' })
  facultyProgramDetails: FacultyProgramDetail[];
}
