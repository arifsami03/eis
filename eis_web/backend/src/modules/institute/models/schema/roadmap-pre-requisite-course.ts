import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';
import { RoadMap, Course } from '../..';

@Table({ timestamps: true })
export class RoadMapPreRequisiteCourse extends Model<RoadMapPreRequisiteCourse> {
  @ForeignKey(() => RoadMap)
  @Column
  roadMapId: number;

  @ForeignKey(() => Course)
  @Column
  courseId: number;
}
