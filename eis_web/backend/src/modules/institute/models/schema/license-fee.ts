import { Table, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript';

import { EducationLevel } from './education-level'

@Table({ timestamps: true })
export class LicenseFee extends Model<LicenseFee> {

  @ForeignKey(() => EducationLevel)
  @Column educationLevelId: number;

  @Column amount: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => EducationLevel, {
    foreignKey: 'educationLevelId'
  })
  educationLevel: EducationLevel;
}
