import { Table, Column, Model, ForeignKey, BelongsTo, BelongsToMany } from 'sequelize-typescript';
import { Program } from '../..';

@Table({ timestamps: true })
export class ProgramDetail extends Model<ProgramDetail> {
  @Column name: string;

  @ForeignKey(() => Program)
  @Column
  programId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => Program, {
    foreignKey: 'programId'
  })
  program: Program;
}
