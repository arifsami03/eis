import { Table, Column, Model, ForeignKey, HasMany, BelongsTo, HasOne } from 'sequelize-typescript';
import { AcademicCalendar, ProgramDetail } from '../..';

@Table({ timestamps: true })
export class AcademicCalendarProgramDetail extends Model<AcademicCalendarProgramDetail> {
  @ForeignKey(() => AcademicCalendar)
  @Column
  academicCalendarId: number;

  @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column createdBy: number;
  @Column updatedBy: number;

  @BelongsTo(() => ProgramDetail, { foreignKey: 'programDetailId' })
  programDetail: ProgramDetail;
}
