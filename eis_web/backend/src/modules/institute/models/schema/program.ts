import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
import { EducationLevel } from './education-level';

@Table({ timestamps: true })
export class Program extends Model<Program> {
  @Column name: string;

  @ForeignKey(() => EducationLevel)
  @Column educationLevelId:number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => EducationLevel, {
    foreignKey: 'educationLevelId'
  })
  educationLevel: EducationLevel;
}
