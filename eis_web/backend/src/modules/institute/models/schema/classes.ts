import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
import { ProgramDetail } from './program-detail';

@Table({ timestamps: true })
export class Classes extends Model<Classes> {
  @Column name: string;

  @ForeignKey(() => ProgramDetail)
  @Column programDetailId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @BelongsTo(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
