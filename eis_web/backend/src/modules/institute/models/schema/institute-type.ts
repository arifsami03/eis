import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class InstituteType extends Model<InstituteType> {
  @Column name: string;
  @Column createdBy: number;
  @Column updatedBy: number;
}
