import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Course extends Model<Course> {
  @Column title: string;
  @Column abbreviation: string;
  @Column description: string;
  @Column createdBy: number;
  @Column updatedBy: number;
}
