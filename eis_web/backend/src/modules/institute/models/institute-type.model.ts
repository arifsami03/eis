import { BaseModel } from '../../base';
import { InstituteType } from '../';

export class InstituteTypeModel extends BaseModel {
  constructor() {
    super(InstituteType);
  }
}
