import { BaseModel } from '../../base';
import { EducationLevel, InstituteType } from '../';

export class EducationLevelModel extends BaseModel {
  constructor() {
    super(EducationLevel);
  }



  /**
  * it will find all education levels
  */
  findAll() {
    return EducationLevel.findAll({
      attributes: ['id', 'education', 'instituteTypeId'],
      include: [
        {
          model: InstituteType,
          as: 'instituteType',
          attributes: ['id', 'name']
        }
      ]
    });
  }
}
