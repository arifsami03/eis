import { BaseModel } from '../../base';
import { RoadMapPreRequisiteCourse, InstituteType } from '../';

export class RoadMapPreRequisiteCourseModel extends BaseModel {
  constructor() {
    super(RoadMapPreRequisiteCourse);
  }
}
