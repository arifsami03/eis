import { BaseModel } from '../../base';
import { Course } from '../';
import { Sequelize } from 'sequelize-typescript';

export class CourseModel extends BaseModel {
  constructor() {
    super(Course);
  }
}
