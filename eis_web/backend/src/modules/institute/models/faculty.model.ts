import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Faculty, FacultyProgramDetail, ProgramDetail } from '../';
import { Sequelize } from 'sequelize-typescript';
import { FacultyProgramDetailModel, ProgramDetailModel } from '../';

export class FacultyModel extends BaseModel {
  constructor() {
    super(Faculty);
  }

  getFaculties(ids) {

    return this.findAllByConditions(['id', 'name'], { id: { [Sequelize.Op.notIn]: ids } });
  }
  getFacultiesWithProgramDetails() {

    let campFacProDetResult = [];
    return this.findAll(['id', 'name']).then(res => {
      return Promise.each(res, (FacItem) => {

        let programDetails = [];

        return new FacultyProgramDetailModel().findAllByConditions(['id', 'facultyId', 'programDetailId'], { facultyId: FacItem['id'] }).then(progRes => {

          return Promise.each(progRes, (progResItem) => {
            return new ProgramDetailModel().findByCondition(['id', 'name'], { id: progResItem['programDetailId'] }).then(finalRes => {

              programDetails.push(finalRes);
            })

          }).then(() => {
            campFacProDetResult.push({
              facultyId: FacItem['id'],
              facultyName: FacItem['name'],
              programDetails: programDetails
            });
          })

        })

      })

    }).then(() => {
      return campFacProDetResult;
    })
  }
}
