import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { ProgramModel, EducationLevelModel } from '../'
import { Sequelize } from 'sequelize-typescript';
import { ProgramDetail } from './schema/program-detail';
import { FacultyProgramDetailModel } from './../';

export class ProgramDetailModel extends BaseModel {
  constructor() {
    super(ProgramDetail);
  }
  /**
   * it will find all programs
   */
  index() {
    this.openConnection();
    var programDetails = [];
    return this.findAll(['id', 'name', 'programId']).then(res => {
      return Promise.each(res, (resItem) => {

        return new ProgramModel().findByCondition(['name'], { id: resItem['programId'] }).then(finalRes => {

          programDetails.push({
            id: resItem['id'],
            name: resItem['name'],
            program: finalRes
          });
        })

      }).then(() => {
        return programDetails;
      })
    });


  }

  find(id) {
    //this.openConnection();
    return this.findByCondition(['id', 'name', 'programId'], { id: id }).then(res => {
      return res;
    });
  }

  getPrograms(id) {
    var programDetails = [];
    return new ProgramModel().findAllByConditions(['id'], { facultyId: id }).then(res => {
      return Promise.each(res, (resItem) => {
        return this.findByCondition(['id', 'name'], { programId: resItem['id'] }).then(finalRes => {

          programDetails.push({
            id: finalRes['id'],
            name: finalRes['name'],
          });
        })

      }).then(() => {
        return programDetails;
      })
    })
    //    return this.findAllByConditions(['id', 'name'], { facultyId: id });
  }

  getFacultyProgramDetails(id) {
    var programDetails = [];
    return new FacultyProgramDetailModel().findAllByConditions(['id', 'facultyId', 'programDetailId'], { facultyId: id }).then(res => {
      return Promise.each(res, (resItem) => {
        return this.findByCondition(['id', 'name'], { id: resItem['programDetailId'] }).then(finalRes => {

          programDetails.push({
            id: finalRes['id'],
            name: finalRes['name'],
          });
        })

      }).then(() => {
        return programDetails;
      })
    })
    //    return this.findAllByConditions(['id', 'name'], { facultyId: id });
  }
  getProgramDetails() {
    var programDetailsList = [];
    return this.findAll(['id', 'name', 'programId']).then(programDetails => {

      return Promise.each(programDetails, (programDetailItem) => {

        return new ProgramModel().findByCondition(['name', 'educationLevelId'], { id: programDetailItem['programId'] }).then(program => {
          return new EducationLevelModel().findByCondition(['education'], { id: program['educationLevelId'] }).then(educationLevel => {
            programDetailsList.push({
              id: programDetailItem['id'],
              name: educationLevel['education'] + '-' + program['name'] + '-' + programDetailItem['name'],
            })

          })
        })
      }).then(() => {
        return programDetailsList;
      })

    })
  }

}
