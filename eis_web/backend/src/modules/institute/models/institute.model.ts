import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { Institute } from '../';
import { MetaConf } from '../../configuration/models/schema/meta-conf';
import { MetaConfModel } from '../../configuration';
import { Promise } from 'bluebird';


export class InstituteModel extends BaseModel {
  constructor() {
    super(Institute);
  }

  public findInstitute() {

    let mcModel = new MetaConfModel();

    return super.findByCondition([
      'id', 'name', 'logo', 'abbreviation', 'website', 'officeEmail', 'headOfficeLocation', 'latitude', 'longitude',
      'establishedScince', 'focalPerson', 'Description'], {}).then((data) => {

        let result = data;
        result['dataValues']['contactNumbers'] = [];
        result['dataValues']['affiliation'] = [];


        return mcModel.findAllByConditions(
          ['id', 'metaKey', 'metaValue'],
          { [Sequelize.Op.or]: [{ metaKey: 'instituteContact' }, { metaKey: 'instituteAffiliation' }] }
        ).then((metaData) => {
          metaData.forEach(meta => {
            if (meta.metaKey == 'instituteContact') {
              result['dataValues']['contactNumbers'].push({ id: meta.id, contact: meta.metaValue });
            }
            else if (meta.metaKey == 'instituteAffiliation') {
              result['dataValues']['affiliation'].push({ id: meta.id, affiliationa: meta.metaValue });
            }
          });
          return result;
        })

      });

  }

  public updateInstitute(id, item) {

    return this.update(id, item['institute']).then(result => {

      return this.createContacts(item);

    }).then(res => {

      return this.createAffiliations(item);

    })
      .then(result => {

        return this.updateContacts(item);

      })
      .then(res => {

        return this.updateAffiliations(item);

      })
      .then(res => {

        return this.deleteContacts(item);

      })
      .then(res => {

        return this.deleteAffiliations(item);

      })
  }

  /**
   * Function to add bulk of contacts in meta Configuration
   * @param item post Data of Institute
   */
  private createContacts(item) {

    let mcModel = new MetaConfModel();

    var conatacts = [];

    for (var i = 0; i < item['institute']['contactNumbers'].length; i++) {

      if (item['institute']['contactNumbers'][i].contact != '' && item['institute']['contactNumbers'][i].id == '') {
        conatacts.push({
          metaKey: 'instituteContact',
          metaValue: item['institute']['contactNumbers'][i].contact
        })
      }
    }
    // return MetaConf.bulkCreate(conatacts);
    return mcModel.sequelizeModel.bulkCreate(conatacts);
  }


  /**
   * Function to add bulk of Affiliations in meta Configuration
   * @param item post Data of Institute
   */
  private createAffiliations(item) {

    let mcModel = new MetaConfModel();
    var affiliations = [];

    for (var j = 0; j < item['institute']['affiliation'].length; j++) {
      if (item['institute']['affiliation'][j].affiliationa != '' && item['institute']['affiliation'][j].id == '') {
        affiliations.push({
          metaKey: 'instituteAffiliation',
          metaValue: item['institute']['affiliation'][j].affiliationa
        })
      }
    }

    // return MetaConf.bulkCreate(affiliations);
    return mcModel.sequelizeModel.bulkCreate(affiliations);
  }

  /**
   * Function to delete Contacts from meta configuration
   * @param item post Data of Institute
   */
  private deleteContacts(item) {

    return new Promise((resolve, reject) => {

      let mcModel = new MetaConfModel();

      if (item['deletedContactIds'].length != 0) {

        // return MetaConf.destroy({ where: { id: item['deletedContactIds'] } });
        return mcModel.deleteByConditions({ id: item['deletedContactIds'] }).then(res => {
          resolve(res);
        });

      }
      else {
        resolve(true);
      }
    })
  }

  /**
   * Function to delete Affiliations from meta configuration
   * @param item  post Data of Institute
   */
  private deleteAffiliations(item) {

    return new Promise((resolve, reject) => {

      let mcModel = new MetaConfModel();

      if (item['deletedAffiliationIds'].length != 0) {

        // return MetaConf.destroy({ where: { id: item['deletedAffiliationIds'] } });

        return mcModel.deleteByConditions({ id: item['deletedAffiliationIds'] }).then(res => {
          resolve(res);
        });
      }
      else {
        resolve(true);
      }
    })
  }

  /**
   * update Contacts of Institute
   * @param item Post Data of Institute
   */
  private updateContacts(item) {

    let mcModel = new MetaConfModel();

    return Promise.each(item['institute']['contactNumbers'], (contactItem) => {

      if (contactItem['id'] != '') {
        return mcModel.update(contactItem['id'], { metaValue: contactItem['contact'] }).then(res => {
        });
      }

    })

  }

  /**
   * Function to Update Institute
   * @param item Post Data of Institute
   */
  private updateAffiliations(item) {

    let mcModel = new MetaConfModel();

    return Promise.each(item['institute']['affiliation'], (affiliationItem) => {

      if (affiliationItem['id'] != '') {
        return mcModel.update(affiliationItem['id'], { metaValue: affiliationItem['affiliationa'] }).then(res => {
        });
      }

    })

  }

}
