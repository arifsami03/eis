import { AcademicCalendarProgramDetailModel } from './academic-calendar-program-detail.model';
import { Promise } from 'bluebird';
import { BaseModel } from '../../base';
import { AcademicCalendar, AcademicCalendarProgramDetail, ProgramDetail, Program } from '../';

export class AcademicCalendarModel extends BaseModel {
  constructor() {
    super(AcademicCalendar);
  }

  /**
   * Creating program details against academic calendar
   * @param item
   */
  createWithProgramDetails(item) {
    let academisCalendarItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      description: item['description']
    };

    return super.create(academisCalendarItem).then(acResult => {
      if (acResult) {
        let acProgramDetailModel = new AcademicCalendarProgramDetailModel();

        return Promise.each(item['programDetailId'], pdId => {
          let acpdItem = {
            academicCalendarId: acResult['id'],
            programDetailId: pdId
          };

          return acProgramDetailModel.create(acpdItem);
        }).then(() => {
          return acResult;
        });
      }
    });
  }

  /**
   * Update program details against academic calendar
   * @param _id academic calendar id
   * @param item
   */
  updateWithProgramDetails(_id, item) {
    let academisCalendarItem = {
      title: item['title'],
      abbreviation: item['abbreviation'],
      description: item['description']
    };
    return this.sequelizeModel
      .find({
        where: { id: _id },
        include: [
          {
            model: AcademicCalendarProgramDetail,
            as: 'academicCalendarProgramDetails',
            attributes: ['programDetailId']
          }
        ]
      })
      .then(_acResult => {
        let existingProgramDetailIds = _acResult['academicCalendarProgramDetails'];
        let inComingProgramDetailIds: number[] = item['programDetailId'];

        // Matching and adding new one (programDetailId)
        inComingProgramDetailIds.forEach(InComingPdi => {
          let exist: boolean = false;
          // Matching
          existingProgramDetailIds.forEach(existingPdi => {
            if (InComingPdi === existingPdi['dataValues']['programDetailId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            let acpdItem = {
              academicCalendarId: _acResult['id'],
              programDetailId: InComingPdi
            };
            return new AcademicCalendarProgramDetailModel().create(acpdItem).then(acpd_result => {
              return acpd_result;
            });
          }
        });

        // Matching and deleteing previous one  (programDetailId)
        existingProgramDetailIds.forEach(existingPdi => {
          let exist: boolean = false;
          // Matching
          inComingProgramDetailIds.forEach(InComingPdi => {
            if (InComingPdi === existingPdi['dataValues']['programDetailId']) {
              exist = true;
            }
          });
          if (!exist) {
            // Adding
            return new AcademicCalendarProgramDetailModel().deleteByConditions({ programDetailId: existingPdi['dataValues']['programDetailId'] }).then(acpd_result => {
              return acpd_result;
            });
          }
        });
        return _acResult;
      });
  }

  /**
   * find data with program details
   * @param _id academic calendar id
   */
  public find(_id) {
    this.openConnection();
    return this.sequelizeModel.find({
      where: { id: _id },
      include: [
        {
          model: AcademicCalendarProgramDetail,
          as: 'academicCalendarProgramDetails',
          include: [
            {
              model: ProgramDetail,
              as: 'programDetail',
              attributes: ['id', 'name'],
              include: [
                {
                  model: Program,
                  as: 'program',
                  attributes: ['id', 'name']
                }
              ]
            }
          ]
        }
      ]
    });
  }
}
