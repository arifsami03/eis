import { BaseModel } from '../../base';
import { AcademicCalendarProgramDetail } from '../';

export class AcademicCalendarProgramDetailModel extends BaseModel {
  constructor() {
    super(AcademicCalendarProgramDetail);
  }
}
