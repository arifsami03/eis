import { BaseModel } from '../../base';
import { RoadMap, InstituteType, RoadMapPreRequisiteCourseModel } from '../';
import { Promise } from 'bluebird';

export class RoadMapModel extends BaseModel {
  constructor() {
    super(RoadMap);
  }
  /**
   * Creating program details against academic calendar
   * @param item
   */
  createWithPreRequisiteCourse(item) {
    let roadMapItem = {
      academicCalendarId: item['academicCalendarId'],
      classId: item['classId'],
      courseCode: item['courseCode'],
      courseId: item['courseId'],
      courseOrder: item['courseOrder'],
      courseType: item['courseType'],
      creditHours: item['creditHours'],
      natureOfCourse: item['natureOfCourse'],
      programDetailId: item['programDetailId'],
      title: item['title']
    };

    return super.create(roadMapItem).then(roadMapResult => {
      if (roadMapResult) {
        let roadMapPreReqModel = new RoadMapPreRequisiteCourseModel();

        return Promise.each(item['preReqCoursesList'], _course => {
          if (_course['courseId']) {
            let roadMapPreReqItem = {
              roadMapId: roadMapResult['id'],
              courseId: _course['courseId']
            };

            return roadMapPreReqModel.create(roadMapPreReqItem);
          }
        }).then(() => {
          return roadMapResult;
        });
      }
    });
  }
  /**
   * Update program details against academic calendar
   * @param _id academic calendar id
   * @param item
   */
  updateWithPreRequisiteCourse(_id, item) {
    let roadMapItem = {
      academicCalendarId: item['academicCalendarId'],
      classId: item['classId'],
      courseCode: item['courseCode'],
      courseId: item['courseId'],
      courseOrder: item['courseOrder'],
      courseType: item['courseType'],
      creditHours: item['creditHours'],
      natureOfCourse: item['natureOfCourse'],
      programDetailId: item['programDetailId'],
      title: item['title']
    };
    let existingCourseIds: any[] = [];
    let inComingCourseIds: number[] = item['preReqCoursesList'];
    return new RoadMapPreRequisiteCourseModel().findAll(null, { roadMapId: _id }).then(preReqResult => {
      if (preReqResult) {
        preReqResult.forEach(element => {
          existingCourseIds.push({ courseId: element['courseId'] });
        });
      }

      // Matching and adding new one (programDetailId)
      inComingCourseIds.forEach(InComingCId => {
        let exist: boolean = false;
        // Matching
        existingCourseIds.forEach(existingCId => {
          if (InComingCId['courseId'] === existingCId['courseId']) {
            exist = true;
          }
        });
        if (!exist) {
          // Adding
          let roadMapPreReqItem = {
            roadMapId: _id,
            courseId: InComingCId['courseId']
          };
          return new RoadMapPreRequisiteCourseModel().create(roadMapPreReqItem).then(roadMapPreReq_result => {
            return roadMapPreReq_result;
          });
        }
      });

      // Matching and deleteing previous one  (programDetailId)
      existingCourseIds.forEach(existingCId => {
        let exist: boolean = false;
        // Matching
        inComingCourseIds.forEach(InComingCId => {
          if (InComingCId['courseId'] === existingCId['courseId']) {
            exist = true;
          }
        });
        if (!exist) {
          // Deleting
          return new RoadMapPreRequisiteCourseModel()
            .deleteByConditions({ courseId: existingCId['courseId'] })
            .then(roadMapPreReq_result => {
              return roadMapPreReq_result;
            });
        }
      });
      return preReqResult;
    });
  }
  public find(_id) {
    let preReqCoursesList: any[] = [];
    this.openConnection();
    return this.sequelizeModel
      .find({
        where: { id: _id }
      })
      .then(roadMapResult => {
        return new RoadMapPreRequisiteCourseModel()
          .findAll(null, { roadMapId: roadMapResult['id'] })
          .then(preReqResult => {
            if (preReqResult) {
              preReqResult.forEach(element => {
                preReqCoursesList.push({ courseId: element['courseId'] });
              });
            }
            roadMapResult['dataValues']['preReqCoursesList'] = preReqCoursesList;
            return roadMapResult;
          });
      });
  }
}
