import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { Classes } from '../';

import { ProgramDetailModel, ProgramModel, EducationLevelModel } from '../'
export class ClassesModel extends BaseModel {
  constructor() {
    super(Classes);
  }

  getClasses() {

    var classesList = [];
    return this.findAll(['id', 'name', 'programDetailId']).then(classes => {
     
      return Promise.each(classes, (classItem) => {
        
      return new ProgramDetailModel().findByCondition(['name', 'programId'], { id: classItem['programDetailId'] }).then(programDetail => {
        return new ProgramModel().findByCondition(['name', 'educationLevelId'], { id: programDetail['programId'] }).then(program => {
          return new EducationLevelModel().findByCondition(['education'], { id: program['educationLevelId'] }).then(educationLevel => {
            classesList.push({
              id : classItem['id'],
              name:classItem['name'],
              programDetail:programDetail['name'],
              program:program['name'],
              educationLevel:educationLevel['education'],

            })
          })
        })
      })
    }).then(()=>{
      return classesList;
    })

    })
  }
}
