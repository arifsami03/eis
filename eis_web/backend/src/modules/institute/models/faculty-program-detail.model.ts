import { Promise, resolve } from 'bluebird';
import { BaseModel } from '../../base';
import { FacultyProgramDetail, ProgramDetail, Program, ProgramDetailModel } from '../';

export class FacultyProgramDetailModel extends BaseModel {
  constructor() {
    super(FacultyProgramDetail);
  }

  findAllByConditions(attributes, conditions) {
    return FacultyProgramDetail.findAll({
      attributes: attributes, where: conditions,
      include: [{
        model: ProgramDetail, as: 'programDetail', attributes: ['id', 'name'],
        include: [{
          model: Program, as: 'program', attributes: ['id', 'name']
        }]
      }]
    });
  }

  findAllProgramDetails() {

    return ProgramDetail.findAll({
      attributes: ['id', 'name', 'programId'],
      include: [
        {
          model: Program,
          as: 'program',
          attributes: ['id', 'name']
        }
      ]
    });
  }

  modifyProgramDetails(id, item) {
    // if insertedIds are received then put them in createArray for bulkCreate
    let createArray = [];
    if (item['insertedIds'].length > 0) {
      item['insertedIds'].forEach(insertId => {
        createArray.push({ facultyId: id, programDetailId: insertId });
      });
    }

    return new Promise((resolve, reject) => {
      FacultyProgramDetail.bulkCreate(createArray).then(() => {
        if (item['deletedIds'].length > 0) {
          return FacultyProgramDetail.destroy({ where: { facultyId: id, programDetailId: item['deletedIds'] } }).then(result => {
          });
        }
      });
      return resolve(true);
    });
  }
}
