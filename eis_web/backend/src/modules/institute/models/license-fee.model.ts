import { BaseModel } from '../../base';
import { Promise } from 'bluebird';
import { LicenseFee, EducationLevel } from '../';
import { Sequelize } from 'sequelize-typescript';

export class LicenseFeeModel extends BaseModel {
  constructor() {
    super(LicenseFee);
  }

  find(id, attributes) {
    return this.sequelizeModel.findOne({
      attributes: attributes,
      where: { id: id },
      include: [
        {
          model: EducationLevel,
          as: 'educationLevel',
          attributes: ['id', 'education']
        }
      ]
    });
  }

  getAllLicenseFees(attributes) {
    return this.sequelizeModel.findAll({
      attributes: attributes,
      include: [
        {
          model: EducationLevel,
          as: 'educationLevel',
          attributes: ['id', 'education']
        }
      ]
    });
  }
}

// find(id, attributes) {
//   return this.sequelizeModel.find(id, attributes).
//     then(result => {
//       let edId = result.educationLevelId;
//       this.sequelizeModel.find(edId, ['id', 'education']);
//     });

// }