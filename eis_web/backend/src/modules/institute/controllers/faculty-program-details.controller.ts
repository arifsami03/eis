import * as express from 'express';
import { Request, Response } from 'express';
import { ErrorHandler } from '../../base/conf/error-handler';
import { FacultyProgramDetailModel } from '../';

export class FacultyProgramDetailsController {
  constructor() { }

  /**
   * Find All record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new FacultyProgramDetailModel()
      .findAllByConditions(['id', 'facultyId', 'programDetailId'], { facultyId: id })
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Find All program details for faculty
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAllProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {

    new FacultyProgramDetailModel()
      .findAllProgramDetails()
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;

    new FacultyProgramDetailModel()
      .modifyProgramDetails(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
