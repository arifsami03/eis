import { Request, Response } from 'express';
import * as express from "express";
import { ErrorHandler } from "../../base/conf/error-handler";

import { ProgramModel } from '../';

export class ProgramsController {
  constructor() {}

  /**
   * it will get all Programs
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new ProgramModel()
      .index()
      .then(programs => {
       // throw('mesam raza kazmi');
        res.send(programs);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
      
  }

  /**
   * it will get one Programs
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramModel()
      .find(id)
      .then(program => {
        res.send({
          data: program
        });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will create new Programs
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramModel()
      .create(req.body)
      .then(program => {
        res.send({
          data: program
        });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will delete Programs
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new ProgramModel()
      .delete(id)
      .then(program => {
        res.send(program);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * it will update Programs
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new ProgramModel()
      .update(id, item)
      .then(result => {
       // throw('Farasat Hussain Kazmi');
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  getPrograms(req: express.Request, res: express.Response, next: express.NextFunction){
    new ProgramModel()
    .getPrograms(req.params.id)
    .then(programs => {
      res.send(programs);
    })
    .catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
  getProgramsList(req: express.Request, res: express.Response, next: express.NextFunction){
    new ProgramModel()
    .getProgramsList()
    .then(programs => {
      res.send(programs);
    })
    .catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
}
