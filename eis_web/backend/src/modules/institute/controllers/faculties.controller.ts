import * as express from "express";
import { Request, Response } from 'express';

import { ErrorHandler } from "../../base/conf/error-handler";
import { FacultyModel } from '../';

export class FacultiesController {
  constructor() { }

  /**
   * Get All Recrods
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FacultyModel()
      .findAll(['id', 'name'], null)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
     * Get All Recrods with Programs
     * 
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     */
  getFacultiesWithProgramDetails(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FacultyModel()
      .getFacultiesWithProgramDetails()
      .then(faculties => {
        res.send(faculties);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }
  /**
     * Get All Recrods
     * 
     * @param req express.Request
     * @param res express.Response
     * @param next express.NextFunction
     */
  getFaculties(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FacultyModel()
      .getFaculties(req.body)
      .then(faculties => {
        res.send(faculties);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * Find One Record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FacultyModel()
      .find(id, ['id', 'name'])
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new FacultyModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new FacultyModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new FacultyModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
