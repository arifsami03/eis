import * as express from 'express';
import { Request, Response } from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
import { CourseModel } from '../';

export class CoursesController {
  constructor() {}

  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CourseModel()
      .findAll(['id', 'title', 'abbreviation', 'description'], null)
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Get All Recrods
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CourseModel()
      .findAll(['id', 'title', 'abbreviation'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
  /**
   * Find One Record
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CourseModel()
      .find(id, ['id', 'title', 'abbreviation', 'description'])
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    new CourseModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    new CourseModel()
      .delete(id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    let id = req.params.id;
    new CourseModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
