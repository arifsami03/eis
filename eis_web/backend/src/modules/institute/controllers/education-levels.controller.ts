import * as express from "express";
import { Request, Response } from 'express';
import { ErrorHandler } from "../../base/conf/error-handler";
import { EducationLevelModel } from '../';


export class EducationLevelsController {
  constructor() { }

  /**
   * Get All Recrods
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {

    new EducationLevelModel().findAll().then(result => {

      res.send(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find single record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new EducationLevelModel().find(id).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Create record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {

    new EducationLevelModel().create(req.body).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Delete one record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.id;

    new EducationLevelModel().delete(id).then(result => {
      res.json(result);
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update one record
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let item = req.body;
    let id = req.params.id;

    new EducationLevelModel().update(id, item).then(result => {

      res.json(result);

    }).catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }

  /**
   * Find List
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    //TODO:low: university portal is hardcoded it should not
    new EducationLevelModel()
      .findAllByConditions(['id', 'education'], null)
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
