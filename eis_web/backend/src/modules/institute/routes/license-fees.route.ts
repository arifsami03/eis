import { Router } from 'express';
import { LicenseFeesController } from '../index';

export class LicenseFeesRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new LicenseFeesController();

    this.router.route('/institute/licenseFees/index').get(controller.index);

    this.router.route('/institute/licenseFees/find/:id').get(controller.find);

    this.router.route('/institute/licenseFees/create').post(controller.create);

    this.router.route('/institute/licenseFees/delete/:id').delete(controller.delete);

    this.router.route('/institute/licenseFees/update/:id').put(controller.update);
  }
}
