import { Router } from 'express';

import { CoursesController } from '../index';

export class CoursesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new CoursesController();

    this.router.route('/institute/courses/index').get(controller.index);
    this.router.route('/institute/courses/findAttributesList').get(controller.findAttributesList);
    this.router.route('/institute/courses/find/:id').get(controller.find);
    this.router.route('/institute/courses/create').post(controller.create);
    this.router.route('/institute/courses/update/:id').put(controller.update);
    this.router.route('/institute/courses/delete/:id').delete(controller.delete);
  }
}
