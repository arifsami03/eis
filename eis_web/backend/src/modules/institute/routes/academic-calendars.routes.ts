import { Router } from 'express';

import { AcademicCalendarsController } from '../index';

export class AcademicCalendarsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AcademicCalendarsController();

    this.router.route('/institute/academicCalendars/index').get(controller.index);
    this.router.route('/institute/academicCalendars/findAttributesList').get(controller.findAttributesList);
    this.router.route('/institute/academicCalendars/find/:id').get(controller.find);
    this.router.route('/institute/academicCalendars/create').post(controller.create);
    this.router.route('/institute/academicCalendars/update/:id').put(controller.update);
    this.router.route('/institute/academicCalendars/delete/:id').delete(controller.delete);
  }
}
