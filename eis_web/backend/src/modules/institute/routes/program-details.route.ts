import { Router, Request, Response } from 'express';
import { ProgramDetailsController } from '../index';

export class ProgramDetailsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let programsController = new ProgramDetailsController();
    this.router.route('/institute/programDetails/index').get(programsController.index);

    this.router.route('/institute/programDetails/findAttributesList').get(programsController.index);

    this.router.route('/institute/programDetails/getProgramDetails').get(programsController.getProgramDetails);

    this.router.route('/institute/programDetails/getPrograms/:id').get(programsController.getPrograms);
    this.router.route('/institute/programDetails/getFacultyProgramDetails/:id').get(programsController.getFacultyProgramDetails);

    this.router.route('/institute/programDetails/find/:id').get(programsController.find);

    this.router.route('/institute/programDetails/create').post(programsController.create);

    this.router.route('/institute/programDetails/delete/:id').delete(programsController.delete);

    this.router.route('/institute/programDetails/update/:id').put(programsController.update);
  }
}
