import { NextFunction, Request, Response, Router } from 'express';
import {
  InstituteRoute,
  ProgramsRoute,
  FacultiesRoute,
  FacultyProgramDetailsRoute,
  EducationLevelsRoute,
  ProgramDetailsRoute,
  InstituteTypesRoute,
  ClassesRoute,
  AcademicCalendarsRoute,
  AcademicCalendarProgramDetailsRoute,
  LicenseFeesRoute,
  CoursesRoute,
  RoadMapsRoute,
  RoadMapPreRequisiteCoursesRoute
} from '../../index';

/**
 *
 *
 * @class InstituteEditRoute
 */
export class InstitutesBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new InstituteRoute(this.router);
    new FacultiesRoute(this.router);
    new FacultyProgramDetailsRoute(this.router);
    new ProgramsRoute(this.router);
    new EducationLevelsRoute(this.router);
    new ProgramDetailsRoute(this.router);
    new InstituteTypesRoute(this.router);
    new LicenseFeesRoute(this.router);
    new CoursesRoute(this.router);
    new ClassesRoute(this.router);
    new AcademicCalendarsRoute(this.router);
    new AcademicCalendarProgramDetailsRoute(this.router);
    new RoadMapsRoute(this.router);
    new RoadMapPreRequisiteCoursesRoute(this.router);
  }
}
