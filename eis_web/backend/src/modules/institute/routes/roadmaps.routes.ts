import { Router } from 'express';

import { RoadMapsController } from '../index';

export class RoadMapsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new RoadMapsController();

    this.router.route('/institute/roadMaps/index').get(controller.index);
    this.router.route('/institute/roadMaps/find/:id').get(controller.find);
    this.router.route('/institute/roadMaps/create').post(controller.create);
    this.router.route('/institute/roadMaps/update/:id').put(controller.update);
    this.router.route('/institute/roadMaps/delete/:id').delete(controller.delete);
    this.router.route('/institute/roadMaps/findAttributesList').get(controller.findAttributesList);
  }
}
