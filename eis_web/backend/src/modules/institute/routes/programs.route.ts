import { Router, Request, Response } from 'express';
import { ProgramsController } from '../index';

export class ProgramsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let programsController = new ProgramsController();

    this.router.route('/institute/programs/index').get(programsController.index);
    this.router.route('/institute/programs/getPrograms/:id').get(programsController.getPrograms);

    this.router.route('/institute/programs/find/:id').get(programsController.find);

    this.router.route('/institute/programs/create').post(programsController.create);

    this.router.route('/institute/programs/delete/:id').delete(programsController.delete);

    this.router.route('/institute/programs/update/:id').put(programsController.update);

    this.router.route('/institute/programs/getProgramsList').get(programsController.getProgramsList);
    
  }
}
