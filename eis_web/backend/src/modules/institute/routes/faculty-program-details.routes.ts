import { Router } from 'express';

import { FacultyProgramDetailsController } from '../index';

export class FacultyProgramDetailsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FacultyProgramDetailsController();

    this.router.route('/institute/facultyProgramDetails/index/:id').get(controller.index);
    this.router.route('/institute/facultyProgramDetails/findAllProgramDetails').get(controller.findAllProgramDetails);
    this.router.route('/institute/facultyProgramDetails/update/:id').put(controller.update);
  }
}
