import { Router } from 'express';

import { RoadMapPreRequisiteCoursesController } from '../index';

export class RoadMapPreRequisiteCoursesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new RoadMapPreRequisiteCoursesController();

    this.router.route('/institute/roadMapPreReuisiteCourses/index').get(controller.index);
    this.router.route('/institute/roadMapPreReuisiteCourses/find/:id').get(controller.find);
    this.router.route('/institute/roadMapPreReuisiteCourses/create').post(controller.create);
    this.router.route('/institute/roadMapPreReuisiteCourses/update/:id').put(controller.update);
    this.router.route('/institute/roadMapPreReuisiteCourses/delete/:id').delete(controller.delete);
  }
}
