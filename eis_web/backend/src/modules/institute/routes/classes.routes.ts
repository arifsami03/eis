import { Router } from 'express';

import { ClassesController } from '../index';

export class ClassesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ClassesController();

    this.router.route('/institute/classes/index').get(controller.index);
    this.router.route('/institute/classes/findAttributesList').get(controller.findAttributesList);
    this.router.route('/institute/classes/find/:id').get(controller.find);
    this.router.route('/institute/classes/create').post(controller.create);
    this.router.route('/institute/classes/update/:id').put(controller.update);
    this.router.route('/institute/classes/delete/:id').delete(controller.delete);
  }
}
