import { Router } from 'express';

import { AcademicCalendarProgramDetailsController } from '../index';

export class AcademicCalendarProgramDetailsRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new AcademicCalendarProgramDetailsController();

    this.router.route('/institute/academicCalendarProgramDetails/index').get(controller.index);
    this.router.route('/institute/academicCalendarProgramDetails/find/:id').get(controller.find);
    this.router.route('/institute/academicCalendarProgramDetails/create').post(controller.create);
    this.router.route('/institute/academicCalendarProgramDetails/update/:id').put(controller.update);
    this.router.route('/institute/academicCalendarProgramDetails/delete/:id').delete(controller.delete);
  }
}
