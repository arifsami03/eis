import { Router } from 'express';
import { FacultiesController } from '../index';

export class FacultiesRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new FacultiesController();

    this.router.route('/institute/faculties/index').get(controller.index);
    this.router.route('/institute/faculties/find/:id').get(controller.find);
    this.router.route('/institute/faculties/create').post(controller.create);
    this.router.route('/institute/faculties/update/:id').put(controller.update);
    this.router.route('/institute/faculties/delete/:id').delete(controller.delete);
    this.router.route('/institute/faculties/getFacultiesWithProgramDetails').get(controller.getFacultiesWithProgramDetails);
    this.router.route('/institute/faculties/getFaculties').post(controller.getFaculties);
  }
}
