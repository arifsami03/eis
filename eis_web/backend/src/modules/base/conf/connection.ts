import { Sequelize } from 'sequelize-typescript';

import {
  AppFeature, FeaturePermission, Group, RoleAssignment, Role, UserGroup, User, RoleWFStateFlow
} from '../../security';
import {
  MetaConf, Bank, OutGoingMailServer, EmailTemplate, WorkFlow, WFState, WFStateFlow, EvaluationSheet, ESSection
} from '../../configuration';
import {
  Campus, CampusOwner, CampusAvailableBuilding, CampusOwnerReference, CampusTargettedLocations, CampusAffiliation,
  CampusAddress, Remittance, CampFaculty, CFProgramDetail
} from '../../campus';

import {
  Institute, Program, Faculty, EducationLevel, ProgramDetail, FacultyProgramDetail, InstituteType, Classes, LicenseFee,
  Course, AcademicCalendar, AcademicCalendarProgramDetail, RoadMap, RoadMapPreRequisiteCourse
} from '../../institute';

import { CONNECTION_STRING } from './connection_string';
import { F_OK } from 'constants';

export class Connection {
  sequelize: Sequelize;
  constructor() { }
  public createConnection(): Sequelize {
    /** Instantiating Sequelize instance for creating connection */
    this.sequelize = new Sequelize(CONNECTION_STRING);

    this.sequelize
      .authenticate()
      .then(() => {
        // console.log('Connection has been established successfully.');
      })
      .catch(err => {
        // console.error('Unable to connect to the database:', err);
      });
    this.sequelize.addModels([
      AppFeature,
      FeaturePermission,
      Group,
      RoleAssignment,
      Role,
      UserGroup,
      User,
      MetaConf,
      Campus,
      CampusOwner,
      CampusAvailableBuilding,
      CampusOwnerReference,
      CampusTargettedLocations,
      Institute,
      Program,
      Faculty,
      FacultyProgramDetail,
      CampusAffiliation,
      CampusAddress,
      Remittance,
      CampFaculty,
      CFProgramDetail,
      Bank,
      EducationLevel,
      ProgramDetail,
      InstituteType,
      OutGoingMailServer,
      EmailTemplate,
      LicenseFee,
      Course,
      Classes,
      AcademicCalendar,
      AcademicCalendarProgramDetail,
      WorkFlow,
      WFState,
      WFStateFlow,
      EvaluationSheet,
      ESSection,
      RoadMap,
      RoadMapPreRequisiteCourse
    ]);
    return this.sequelize;
  }
}
