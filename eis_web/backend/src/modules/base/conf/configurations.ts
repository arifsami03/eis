export const CONFIGURATIONS = {
  SECRET: 'eis_secret',

  PUBLIC_URLS: [
    '/security/users/getPortal',
    '/security/users/login',
    '/tables',
    '/configuration/metaconf/city',
    '/campus/preRegistration',
    '/shared/eligibility-criteria/download',
    '/campus/preRegistration/validateUniqueEmail',
    '/configuration/banks/findAttributesList'
  ]
};
