// import * as express from "express";

export class ErrorHandler {

  static invalidLogin = { error: true, status: 404, message: "Invalid username or password." };
  static invalidToken = { error: true, status: 400, message: "Invalid token or token has been expired." }
  static notPermitted = { error: true, status: 403, message: "You are not authorized for this action." }
  static recordNotFound = { error: true, status: 400, message: "Record not found." }
  static missingRequiredHeader = { error: true, status: 400, message: "A required HTTP header was not specified." }
  static missingRequiredQueryParameter = { error: true, status: 400, message: "A required query parameter was not specified for this request." }
  static invalidEmail = { error: true, status: 400, message: "The given email address is not valid." }
  static invalidPassword = { error: true, status: 400, message: "The given password is not valid." }
  static authenticationFail = { error: true, status: 400, message: "The given email or password is not valid." }
  static unAuthorized = { error: true, status: 401, message: "You are not authorized to access this." }
  static forbidden = { error: true, status: 403, message: "You are forbidden to access this." }
  static accountIsDisabled = { error: true, status: 403, message: "The specified account is disabled." }
  static accountAlreadyExists = { error: true, status: 409, message: "The specified account already exists." }
  static resourceAlreadyExists = { error: true, status: 409, message: "The specified resource already exists." }
  static notFound = { error: true, status: 404, message: "The specified resource does not exist." }
  static inActiveUser = { error: true, status: 403, message: "The specified user is Inactive." }
  static internalServerError = { error: true, status: 500, message: "The server encountered an internal error. Please retry the request." }


  static send(err, res, next) {
    return res.status(err['status']).send(err);
  }

  static sendServerError(err, res, next) {
    console.log('Server Error: ' + err);
    return res.status(ErrorHandler.internalServerError.status).send(ErrorHandler.internalServerError);
  }

  static sendAuthorizationError(err, res, next) {
    return res.status(err.status).send(err);
  }

};
