import { Router } from 'express';

import { ConfigurationBaseRoute } from './../../configuration';
import { InstitutesBaseRoute } from './../../institute';
import { SecurityBaseRoute } from './../../security';
import { CampusBaseRoute, CampusInfoRoute } from './../../campus';
import { DownloadPdfRoute } from '../';

/**
 * / route
 *
 * @class BaseRoute
 */
export class BaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new ConfigurationBaseRoute(this.router);
    new InstitutesBaseRoute(this.router);
    new SecurityBaseRoute(this.router);
    new CampusBaseRoute(this.router);
    new DownloadPdfRoute(this.router);
  }
}
