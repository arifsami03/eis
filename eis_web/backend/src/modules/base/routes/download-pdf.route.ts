import { Request, Response, Router } from 'express';
import { DownloadPdfController } from '../controllers/download-pdf.controller';

/**
 * / route
 *
 * @class CampusInfoRoute
 */
export class DownloadPdfRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusInfoRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class Download Pdf Route
   * @method create
   *
   */
  public create() {
    let controller = new DownloadPdfController();
    this.router
      .route('/shared/eligibility-criteria/download')
      .get(controller.download);
  }
}
