import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import { Connection } from '../';

export class BaseModel {
  public sequelizeModel;
  protected connection;

  constructor(model) {
    this.sequelizeModel = model;
    this.openConnection();
  }

  protected openConnection() {
    this.connection = new Connection().createConnection();
  }

  protected closeConnection() {
    this.connection.close();
  }

  /**
   * Find single record by id
   * @param id
   */
  find(id, attributes?) {
    return this.findByCondition(attributes, { id: id });
  }

  /**
   * Find single record by specified condition
   * @param attributes
   */
  findByCondition(attributes, conditions) {
    // this.openConnection();
    return this.sequelizeModel.findOne(
      this.sequelizeQueryBuilder(attributes, conditions)
    );
  }

  /**
   * Find all records with specified attributes
   * @param attributes
   */
  findAll(attributes?, conditions?) {
    return this.findAllByConditions(attributes, conditions);
  }

  /**
   * Find all records with specified attributes and conditions
   * @param attributes
   */
  findAllByConditions(attributes, conditions) {
    // this.openConnection();
    return this.sequelizeModel.findAll(
      this.sequelizeQueryBuilder(attributes, conditions)
    );
  }

  /**
   * Update a record for given id
   * @param item
   * @param id
   */
  update(id, item) {
    // this.openConnection();
    return this.sequelizeModel
      .update(item, {
        where: { id: id }
      })
      .then(item => {
        // check if record updated: Sequelize return [1] if record updated and return [0] if record not updated
        if (item.includes(1)) {
          return {
            success: true,
            message: 'Record has been udpated successfully'
          };
        } else {
          return { error: true, message: 'An error has been occured' };
        }
      });
  }

  /**
   * Create a new record
   * @param item
   */
  create(item) {
    // this.openConnection();

    // By default when user is creating we are going to set updated by too
    item.updatedBy = item.createdBy;

    return this.sequelizeModel.create(item);
  }

  /**
   * Count all records
   */
  count() {
    // return this.countByCondition(null);
    // this.openConnection();
    return this.sequelizeModel.count();
  }

  /**
   * Count all records by some condition
   //  */
  // countByCondition(condition) {
  //   this.openConnection();
  //   return this.sequelizeModel.count(this.sequelizeQueryBuilder(condition));
  // }

  /**
   * Delete a record against an id
   * @param id
   */
  delete(id) {
    return this.deleteByConditions({ id: id });
  }

  /**
   * Delete a record by conditions
   * @param conditions
   */
  deleteByConditions(conditions) {
    // this.openConnection();
    return this.sequelizeModel.destroy(
      this.sequelizeQueryBuilder(null, conditions)
    );
  }

  /**
   * To prepare the sequelize query.
   *
   * @param attributes any
   * @param condition any
   */
  protected sequelizeQueryBuilder(attributes?, condition?) {
    let obj = {};

    if (attributes) {
      obj['attributes'] = attributes;
    }

    if (condition) {
      obj['where'] = condition;
    }

    return obj;
  }
}
