export const USER = 'user';
export const GROUP = 'group';
export const ROLE = 'role';
export const ACTIVE = 'true';
export const INACTIVE = 'false';
