import * as express from 'express';

import { AppFeatureModel } from '../';

export class AppFeaturesController {
  constructor() {}

  /**
   * findAll - retrieve list of all AppFeatures
   *
   * @param req route
   * @param res [{AppFeatures}... | error]
   */
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new AppFeatureModel()
      .findAll(['id', 'parentId', 'title'])
      .then(_appFeatures => {
        res.send(_appFeatures);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * findAllByConditions - retrieve list of all AppFeatures with specified conditions
   *
   * @param req route
   * @param res [{AppFeatures}... | error]
   */
  findAllParents(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new AppFeatureModel()
      .findAllByConditions(['id', 'title'], { parentId: null })
      .then(_appFeatures => {
        res.send(_appFeatures);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * findAllByConditions - retrieve list of all AppFeatures with specified conditions
   *
   * @param req route
   * @param res [{AppFeatures}... | error]
   */
  findAllchildren(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new AppFeatureModel()
      .find(['id', 'title'], { id: id })
      .then(_appFeature => {
        res.send(_appFeature);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * update - update a role
   * @param req params: id, {Role}
   * @param res { success | error}
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;

    new AppFeatureModel()
      .update(id, item)
      .then(_result => {
        res.send(_result);
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * create - inserting single AppFeature into database
   *
   * @param req {AppFeature}
   * @param res {true | false }
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;

    new AppFeatureModel()
      .create(item)
      .then(() => {
        res.send({
          success: true,
          message: 'App Feature has been added successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: 'App Feature did not added' });
      });
  }

  /**
   * count - count all AppFeatures in database
   *
   * @param req route
   * @param res {number | error}
   */
  count(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new AppFeatureModel()
      .count()
      .then(_count => {
        res.send(_count.toString());
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * delete - deleting single AppFeature from database
   *
   * @param req route
   * @param res {success | error}
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new AppFeatureModel()
      .delete(req.params.id)
      .then(_count => {
        res.send({
          success: true,
          message: 'App Feature has been deleted successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }
}
