// import * as express from 'express';
// /**
//  * Importing related Models
//  */
// import { FeaturePermissionModel, UserModel, USER, GROUP, ROLE } from '../';

// export class FeaturePermissionsController {
//   constructor() { }

//   /**
//    * grant permission to user
//    *
//    * @param req
//    * @param res
//    */
//   grantPermissionToUser(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = GROUP;
//     item.parentId = item.userId;
//     new FeaturePermissionModel()
//       .grant(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been granted successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * grant permission to group
//    *
//    * @param req
//    * @param res
//    */
//   grantPermissionToGroup(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = GROUP;
//     item.parentId = item.groupId;
//     new FeaturePermissionModel()
//       .grant(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been granted successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * grant permission to role
//    *
//    * @param req
//    * @param res
//    */
//   grantPermissionToRole(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = ROLE;
//     item.parentId = item.roleId;
//     new FeaturePermissionModel()
//       .grant(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been granted successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * modify role permissions
//    *
//    * @param req
//    * @param res
//    */
//   modifyRolePermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
//     let id = req.params.id;
//     let item = req.body;
//     // item.parent = ROLE;
//     // item.parentId = item.roleId;
//     new FeaturePermissionModel().modifyRolePermissions(id, item).then(() => {
//       res.send({
//         success: true,
//         message: 'Permission has been granted successfully'
//       });
//     })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * modify group permissions
//    *
//    * @param req
//    * @param res
//    */
//   modifyGroupPermissions(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let id = req.params.id;
//     let item = req.body;
//     // item.parent = ROLE;
//     // item.parentId = item.roleId;
//     new FeaturePermissionModel()
//       .modifyGroupPermissions(id, item, [
//         'id',
//         'featureId',
//         'parent',
//         'parentId',
//         'status'
//       ])
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been granted successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * modify user permissions
//    *
//    * @param req
//    * @param res
//    */
//   modifyUserPermissions(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let id = req.params.id;
//     let item = req.body;
//     // item.parent = ROLE;
//     // item.parentId = item.roleId;
//     new FeaturePermissionModel()
//       .modifyUserPermissions(id, item, [
//         'id',
//         'featureId',
//         'parent',
//         'parentId',
//         'status'
//       ])
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been granted successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * revoke permission from user
//    *
//    * @param req
//    * @param res
//    */
//   revokePermissionFromUser(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = USER;
//     item.parentId = item.userId;
//     new FeaturePermissionModel()
//       .revoke(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been revoked successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * revoke permission from group
//    *
//    * @param req
//    * @param res
//    */
//   revokePermissionFromGroup(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = GROUP;
//     item.parentId = item.groupId;
//     new FeaturePermissionModel()
//       .revoke(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been revoked successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }

//   /**
//    * revoke permission from role
//    *
//    * @param req
//    * @param res
//    */
//   revokePermissionFromRole(
//     req: express.Request,
//     res: express.Response,
//     next: express.NextFunction
//   ) {
//     let item = req.body;
//     item.parent = ROLE;
//     item.parentId = item.roleId;
//     new FeaturePermissionModel()
//       .revoke(item)
//       .then(() => {
//         res.send({
//           success: true,
//           message: 'Permission has been revoked successfully'
//         });
//       })
//       .catch(_error => {
//         res.send({ error: true, message: 'Something went wrong' });
//       });
//   }
// }
