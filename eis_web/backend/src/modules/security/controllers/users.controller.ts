import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';

/**
 * Importing related Models
 */
import { UserModel } from '../';

export class UsersController {
  constructor() { }

  /**
   * Login
   *
   * @param req {User}
   */
  login(req: express.Request, res: express.Response, next: express.NextFunction) {
    new UserModel()
      .login(req.body)
      .then(result => {
        if (result && !result['error']) {
          res.json(result);
        } else {
          ErrorHandler.send(result, res, next);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Index
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(req: express.Request, res: express.Response, next: express.NextFunction) {
    new UserModel()
      .findAllByConditions(['id', 'username'], { portal: 'university' })
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * find One
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    new UserModel()
      .find(id, ['id', 'username', 'isSuperUser', 'isActive', 'portal'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * find One
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getPortal(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.body.id;

    new UserModel()
      .find(id, ['portal'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }
  /**
   * find One
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findAttributesList(req: express.Request, res: express.Response, next: express.NextFunction) {
    //TODO:low: university portal is hardcoded it should not
    new UserModel()
      .findAllByConditions(['id', 'username'], { portal: 'university' })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;

    // Only update username and isActive i.e restrict to update password here
    let item = { username: req.body.username, isActive: req.body.isActive };

    new UserModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(req: express.Request, res: express.Response, next: express.NextFunction) {
    let item = req.body;
    // set default password and portal for new user
    item.portal = 'university';  //TODO: default university should come from some proper location
    item.isSuperUser = false;    //TODO: implement proper way to set superuser false
    new UserModel()
      .create(item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Delete
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(req: express.Request, res: express.Response, next: express.NextFunction) {
    new UserModel()
      .delete(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllGroups - retrieve list of groups that are not assigned to user
   *
   * @param req route
   * @param res [{User}... | error]
   */
  findAllGroups(req: express.Request, res: express.Response, next: express.NextFunction) {
    let userId = req.params.userId;

    new UserModel()
      .findAllGroups(userId, ['id', 'name'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllRoles - retrieve list of roles that are not assigned to user
   *
   * @param req route
   * @param res [{User}... | error]
   */
  findAllRoles(req: express.Request, res: express.Response, next: express.NextFunction) {
    let userId = req.params.userId;

    new UserModel()
      .findAllRoles(userId, ['id', 'name'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllPermissions - retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
    let userId = req.params.userId;

    new UserModel()
      .findAllPermissions(userId, ['id'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
