import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';

/**
 * Importing related Models
 */
import { GroupModel } from '../';

export class GroupsController {
  constructor() { }

  /**
   * index - retrieve list of all Groups
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new GroupModel()
      .findAll(['id', 'name'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * find - retrieve single Group
   *
   * @param req id
   * @param res {Group}
   */
  find(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    new GroupModel()
      .find(id, ['id', 'name'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * update - update a group by group id
   * @param req params: id, {Group}
   * @param res { success | error}
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;

    new GroupModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * create - inserting single Group into database
   *
   * @param req {Group}
   * @param res {true | false }
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;
    new GroupModel()
      .create(item)
      .then(result => {

        res.json(result);

      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * delete - deletes a Group its relation with Users and Roles and its permissions
   *
   * @param req route
   * @param res {success | error}
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new GroupModel()
      .delete(req.params.id)
      .then(result => {
        res.send({ success: 'Group has been deleted successfully' });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllUsers - retrieve list of users that are not assigned to group
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  findAllUsers(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let groupId = req.params.groupId;

    new GroupModel()
      .findAllUsers(groupId, ['id', 'username'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }


  /**
   * findAllRoles - retrieve list of roles that are not assigned to group
   *
   * @param req route
   * @param res [{Group}... | error]
   */
  findAllRoles(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let groupId = req.params.groupId;

    new GroupModel()
      .findAllRoles(groupId, ['id', 'name'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllPermissions - retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllPermissions(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let groupId = req.params.groupId;

    new GroupModel()
      .findAllPermissions(groupId, ['id', 'title'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

}
