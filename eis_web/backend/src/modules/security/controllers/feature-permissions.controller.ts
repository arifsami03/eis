import * as express from 'express';
/**
 * Importing related Models
 */
import { FeaturePermissionModel, UserModel, USER, GROUP, ROLE } from '../';

export class FeaturePermissionsController {
  constructor() { }

  /**
   * Update Role Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateRolePermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    let item = req.body;
    
    new FeaturePermissionModel().updatePermissions('role', id, item).then(() => {
      res.send({
        success: true,
        message: 'Permission has been granted successfully'
      });
    })
      .catch(_error => {
        res.send({ error: true, message: 'Something went wrong' });
      });
  }

  /**
   * Update Group Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateGroupPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    let item = req.body;
    
    new FeaturePermissionModel().updatePermissions('group', id, item).then(() => {
      res.send({
        success: true,
        message: 'Permission has been granted successfully'
      });
    })
      .catch(_error => {
        res.send({ error: true, message: 'Something went wrong' });
      });
  }

  /**
   * Update User Permissions
   * 
   * @param req 
   * @param res 
   * @param next 
   */
  updateUserPermissions(req: express.Request, res: express.Response, next: express.NextFunction) {
    let id = req.params.id;
    let item = req.body;
    
    new FeaturePermissionModel().updatePermissions('user', id, item).then(() => {
      res.send({
        success: true,
        message: 'Permission has been granted successfully'
      });
    })
      .catch(_error => {
        res.send({ error: true, message: 'Something went wrong' });
      });
  }

}
