import * as express from 'express';

import { ErrorHandler } from '../../base/conf/error-handler';
/**
 * Importing related Models
 */
import { RoleModel, RoleAssignmentModel, USER, GROUP } from '../';

export class RolesController {
  constructor() { }

  /**
   * retrieve list of all Roles
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleModel()
      .findAll(['id', 'name'])
      .then(result => {
        res.send(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * retrieve single Role with its association
   *
   * @param req id
   * @param res {Group}
   */
  find(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new RoleModel()
      .find(id, ['id', 'name', 'description'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * update a role
   * @param req params: id, {Role}
   * @param res { success | error}
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;
    console.log('item: ', item);
    new RoleModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * inserting single Role into database
   *
   * @param req {Role}
   * @param res {true | false }
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleModel()
      .create(req.body)
      .then(result => {

        res.json(result);

      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * deletes single Role, its relation with Users and Groups and its Permissions
   *
   * @param req route
   * @param res {success | error}
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleModel()
      .delete(req.params.id)
      .then(result => {
        res.send({ success: 'Role has been deleted successfully' });
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllUsers - retrieve list of users that are not assigned to role
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllUsers(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let roleId = req.params.roleId;

    new RoleModel()
      .findAllUsers(roleId, ['id', 'username'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllPermissions - retrieve list of all permissions
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllFeatures(req: express.Request, res: express.Response, next: express.NextFunction) {
    let roleId = req.params.roleId;

    new RoleModel().findAllFeatures(roleId).then(result => {
      if (result) {
        res.json(result);
      } else {
        res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
      }
    })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * findAllGroups - retrieve list of groups that are not assigned to role
   *
   * @param req route
   * @param res [{Role}... | error]
   */
  findAllGroups(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let roleId = req.params.roleId;

    new RoleModel()
      .findAllGroups(roleId, ['id', 'name'])
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }
}
