import * as express from 'express';
import { UserGroupModel, UserModel } from '../';

export class UserGroupsController {
  constructor() {}

  /**
   * assign a group to user
   *
   * @param req {userId, groupId, createdBy}
   * @param res {true | false }
   */
  assign(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;

    new UserGroupModel()
      .create(item)
      .then(() => {
        res.send({
          success: true,
          message: 'User has been added to group successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: 'User did not added to group' });
      });
  }

  /**
   * delete a record
   *
   * @param req route
   * @param res {success | error}
   */
  revoke(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new UserGroupModel()
      .delete(req.params.id)
      .then(() => {
        res.send({
          success: true,
          message: 'Record has been deleted successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * delete a user from group
   *
   * @param req {userId, groupId}
   * @param res {success | error}
   */
  // delete( req: express.Request,     res: express.Response,     next: express.NextFunction) {
  //   let item = req.body;

  //   let conditions = { userId: item.userId, groupId: item.groupId };

  //   new UserGroupModel()
  //     .deleteByConditions(conditions)
  //     .then(_result => {
  //       if (_result) {
  //         res.send({
  //           success: true,
  //           message: 'User has been deleted from group successfully'
  //         });
  //       } else {
  //         res.send({ success: true, message: 'Something went wrong' });
  //       }
  //     })
  //     .catch(_error => {
  //       res.send({ error: true, message: _error });
  //     });
  // }
}
