import * as express from 'express';
/**
 * Importing related Models
 */
import { RoleAssignmentModel, UserModel, USER, GROUP } from '../';

export class RoleAssignmentsController {
  constructor() {}

  /**
   * assign role to user
   *
   * @param req {userId, roleId, createdBy}
   * @param res {success | error }
   */
  assignToUser(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;
    item.parent = USER;
    item.parentId = item.userId;

    new RoleAssignmentModel()
      .create(item)
      .then(() => {
        res.send({
          success: true,
          message: 'Role has been assigned to user successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: 'Something went wrong' });
      });
  }

  /**
   * assign role to group
   *
   * @param req {groupId, roleId, createdBy}
   * @param res {success | error }
   */
  assignToGroup(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;
    item.parent = GROUP;
    item.parentId = item.groupId;

    new RoleAssignmentModel()
      .create(item)
      .then(_group => {
        res.send({
          success: true,
          message: 'Role has been assigned to group successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: 'Something went wrong' });
      });
  }

  /**
   * delete a record
   *
   * @param req route
   * @param res {success | error}
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RoleAssignmentModel()
      .delete(req.params.id)
      .then(() => {
        res.send({
          success: true,
          message: 'Record has been deleted successfully'
        });
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * revoke role from a user
   *
   * @param req { userId, roleId }
   * @param res {success | error}
   */
  revokeFromUser(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let item = req.body;

    new UserModel();
    let conditions = {
      roleId: item.roleId,
      parent: USER,
      parentId: item.userId
    };
    new RoleAssignmentModel()
      .deleteByConditions(conditions)
      .then(_result => {
        if (_result) {
          res.send({
            success: true,
            message: 'Role has been revoked from user successfully'
          });
        } else {
          res.send({ success: true, message: 'Something went wrong' });
        }
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }

  /**
   * revoke Group from a role
   *
   * @param req { groupId, roleId }
   * @param res {success | error}
   */
  revokeFromGroup(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let conditions = {
      roleId: req.body.roleId,
      parent: GROUP,
      parentId: req.body.groupId
    };
    new RoleAssignmentModel()
      .deleteByConditions(conditions)
      .then(_result => {
        if (_result) {
          res.send({
            success: true,
            message: 'Role has been revoked from group successfully'
          });
        } else {
          res.send({ success: true, message: 'Something went wrong' });
        }
      })
      .catch(_error => {
        res.send({ error: true, message: _error });
      });
  }
}
