/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { AppFeature } from '../';

export class AppFeatureModel extends BaseModel {
  constructor() {
    super(AppFeature);
  }

  // TODO:low implement proper way to find nested chilren of an app-feature if exists
  find(attributes, conditions) {
    this.openConnection();

    return this.sequelizeModel.findOne({
      attributes: attributes,
      where: conditions,
      include: [
        {
          model: AppFeature,
          as: 'children',
          attributes: attributes,
          include: [
            {
              model: AppFeature,
              as: 'children',
              attributes: attributes,
              include: [
                {
                  model: AppFeature,
                  as: 'children',
                  attributes: attributes,
                  include: [
                    {
                      model: AppFeature,
                      as: 'children',
                      attributes: attributes,
                      include: [
                        {
                          model: AppFeature,
                          as: 'children',
                          attributes: attributes
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    });
  }
}
