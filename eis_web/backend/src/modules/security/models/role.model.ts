import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';

import { Role, RoleAssignment, FeaturePermission, AppFeature, Group, User, GROUP, USER, ROLE, AppFeatureModel, FeaturePermissionModel } from '../';

export class RoleModel extends BaseModel {


  private appFeatureModel = new AppFeatureModel();

  /**
   * To find all featureIds in FeaturePermission
   */
  private featureIdsAry: string[] = [];

  constructor() {
    super(Role);
  }

  find(id, attributes?) {
    this.openConnection();

    return this.sequelizeModel.findOne({
      where: { id: id },
      attributes: attributes,
      include: [
        {
          model: RoleAssignment,
          as: 'assignedGroups',
          attributes: ['id', 'roleId', 'parent', 'parentId'],
          include: [
            {
              model: Group,
              as: 'group',
              attributes: ['name']
            }
          ]
        },
        {
          model: RoleAssignment,
          as: 'assignedUsers',
          attributes: ['id', 'roleId', 'parent', 'parentId'],
          include: [
            {
              model: User,
              as: 'user',
              attributes: ['id', 'username']
            }
          ]
        }
      ]
    });
  }

  findAllUsers(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parent', 'parentId'],
      where: {
        roleId: id,
        parent: USER
      }
    }).then(_roleAssignments => {
      // Store group ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.parentId);
      });
      // Select all groups that are not assigned to user yet
      return User.findAll({
        attributes: attributes,
        where: {
          isSuperUser: false,
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllGroups(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parent', 'parentId'],
      where: {
        roleId: id,
        parent: GROUP
      }
    }).then(_roleAssignments => {
      // Store group ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.parentId);
      });
      // Select all groups that are not assigned to user yet
      return Group.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  /**
   * Find All Features and set statuses accordingly assigned permissions.
   * 
   * Find all features and put all level of children on same array "children" if its set to visible (true)
   *    i.e. [{id: 'users.*', title: 'Users', parentId: '', children : [{id: 'users.create', title: 'Create'}, {id: 'users.view', title: 'View'}]}]
   * @param roleId number
   */
  public findAllFeatures(roleId) {

    let appFeatures = [];

    //TODO:high: parentId: null will not work if in db its '' empty instead of null
    return this.appFeatureModel.findAllByConditions(['id', 'parentId', 'title', 'isVisible'], { parentId: null, isVisible: true }).then(featuresAry => {

      return Promise.each(featuresAry, (item) => {

        let childrenArray = [];

        return this.findChildFeatures(item['id'], childrenArray).then(() => {

          let obj = { id: item['id'], title: item['title'], children: childrenArray }

          appFeatures.push(obj);
        })

      })
    }).then(() => {

      // Now we also need a list of Feature permissions which are assigend to role but only those which we fetched in above block ;)
      return new FeaturePermissionModel().findAllByConditions(['featureId', 'status'], { parent: 'role', parentId: roleId, featureId: this.featureIdsAry }).then(fpResult => {

        // Now set ststus true or false.
        return Promise.each(appFeatures, (afItems) => {
          return Promise.each(afItems['children'], (afItem) => {

            let fpObj = fpResult.find((element) => {
              return element['featureId'] == afItem['id'];
            });

            if (fpObj) {
              afItem['status'] = fpObj['status'];
            }
          })
        })

      })

    }).then(() => {
      return appFeatures;
    })
  }


  /**
   * Find child features recursivly
   * 
   * @param parentId string
   * @param childrenAry array
   */
  private findChildFeatures(parentId, childrenAry) {

    return this.appFeatureModel.findAllByConditions(['id', 'parentId', 'title', 'isVisible'], { parentId: parentId, isVisible: true }).then(featuresAry => {

      if (featuresAry && featuresAry.length) {

        return Promise.each(featuresAry, (item) => {

          let obj = { id: item['id'], title: item['title'], }

          this.featureIdsAry.push(item['id']);

          childrenAry.push(obj);

          return this.findChildFeatures(item['id'], childrenAry);
        })

      }
    })
  }

  /**
   * Delete a Role, its relation with User, Group and its Permissions
   * @param id
   */
  delete(id) {
    this.openConnection();
    return this.connection.transaction(t => {
      return Role.destroy({ where: { id: id }, transaction: t }).then(
        result => {
          if (result) {
            return FeaturePermission.destroy({
              where: { parent: 'role', parentId: id },
              transaction: t
            });
          } else {
            throw new Error('Role not found');
          }
        }
      );
    });
  }
}
