import { Promise } from 'bluebird';
/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import {
  FeaturePermission,
  AppFeatureModel,
  ACTIVE,
  INACTIVE,
  ROLE,
  AppFeature,
  GROUP,
  USER
} from '../';

export class FeaturePermissionModel extends BaseModel {
  constructor() {
    super(FeaturePermission);
  }

  modifyRolePermissions(roleId, featurePermissions, attributes) {
    this.openConnection();
    let promises = [];

    featurePermissions.forEach(featurePermission => {
      featurePermission.children.forEach(child => {
        FeaturePermission.findOne({
          attributes: attributes,
          where: {
            featureId: child.id,
            parent: ROLE,
            parentId: roleId
          }
        }).then(_featurePermission => {
          /**
           * UPDATE SCENARIO: check if permission is already assinged and its status is different from requested permission status
           */
          if (_featurePermission && _featurePermission.status != child.status) {
            //
            // find its parents from AppFeature table and update their status
            this.getAppFeatureParents(_featurePermission.featureId).then(
              _parentIds => {
                if (_parentIds.length > 0) {
                  _parentIds.forEach(_parentId => {
                    let updateParentPromise = FeaturePermission.update(
                      { status: child.status },
                      {
                        where: { featureId: _parentId, parent: ROLE }
                      }
                    );
                    promises.push(updateParentPromise);
                  });
                }
              }
            );

            // now update its own status
            let updatePromise = FeaturePermission.update(
              {
                status: child.status
              },
              {
                where: {
                  id: _featurePermission.id
                }
              }
            );
            promises.push(updatePromise);
          } else if (!_featurePermission && child.status === true) {
            // UPDATE SCENARIO ENDS HERE

            /**
             * CREATE SCENARIO: check if permission is not assigned yet and requested permission status is true
             */
            this.getAppFeatureChildren(child.id).then(_childrenIds => {
              if (_childrenIds.length > 0) {
                _childrenIds.forEach(_childrenId => {
                  /**
                   * Check if children item is present and it status is false then update children item status
                   *
                   */
                  this.checkIfRecordExists(_childrenId, roleId).then(
                    _record => {
                      if (_record && _record.status === false) {
                        let updatePromise = FeaturePermission.update(
                          {
                            status: child.status
                          },
                          {
                            where: {
                              id: _record.id
                            }
                          }
                        );
                        promises.push(updatePromise);
                      } else if (!_record) {
                        /**
                         * If children item is not present then insert record
                         */
                        let createChildrenPromise = FeaturePermission.create({
                          featureId: _childrenId,
                          parent: ROLE,
                          parentId: roleId,
                          status: child.status
                        });
                        promises.push(createChildrenPromise);
                      }
                    }
                  );
                });
              }
            });

            // now creates its own entry
            let createPromise = FeaturePermission.create({
              featureId: child.id,
              parent: ROLE,
              parentId: roleId,
              status: child.status
            });
            promises.push(createPromise);
          }
        });
      });
    });

    return Promise.all(promises);
  }

  /**
   * Return parent and grand parent ids in an array i.e find parents up to two level
   * @param featureId
   * @returns [id...]
   */
  private getAppFeatureParents(featureId) {
    return AppFeature.findOne({
      attributes: ['id', 'parentId'],
      where: {
        id: featureId
      },
      include: [
        {
          model: AppFeature,
          as: 'parent',
          attributes: ['id']
        }
      ]
    }).then(_appFeature => {
      let parentIds = [];
      if (_appFeature) {
        parentIds.push(_appFeature.id);
        if (_appFeature.parent) {
          parentIds.push(_appFeature.parent.id);
        }
      }
      return parentIds;
    });
  }

  /**
   * Return children and grand children ids in an array i.e find children up to two level deep
   * @param featureId
   * @returns [id...]
   */
  private getAppFeatureChildren(featureId) {
    return AppFeature.findAll({
      attributes: ['id', 'parentId'],
      where: {
        parentId: featureId
      },
      include: [
        {
          model: AppFeature,
          as: 'children',
          attributes: ['id']
        }
      ]
    }).then(_appFeatures => {
      let childrenIds = [];
      if (_appFeatures.length > 0) {
        _appFeatures.forEach(_appFeature => {
          childrenIds.push(_appFeature.id);
          if (_appFeature.children.length > 0) {
            _appFeature.children.forEach(_child => {
              childrenIds.push(_child.id);
            });
          }
        });
      }
      return childrenIds;
    });
  }

  /**
   * Check if record exists
   * @param featureId
   * @param roleId
   * @return [{FeaturePermission}]
   */
  private checkIfRecordExists(featureId, roleId) {
    return this.findByCondition(
      ['id', 'featureId', 'parent', 'parentId', 'status'],
      { featureId: featureId, parent: ROLE, parentId: roleId }
    );
  }

  modifyGroupPermissions(groupId, featurePermissions, attributes) {
    this.openConnection();

    let promises = [];
    // first loop through retrieved featurePermissions array
    featurePermissions.forEach(_featurePermission => {
      // loop through children of featurePermissions array that are might stored in featurePermission table against a group
      _featurePermission.children.forEach(permission => {
        // find if a permission is stored against group
        FeaturePermission.findOne({
          attributes: attributes,
          where: {
            featureId: permission.id,
            parent: GROUP,
            parentId: groupId
          }
        }).then(_permission => {
          // if permission exists and its status is different form retrieved permission then update it
          if (_permission && _permission.status != permission.status) {
            let updatePromise = FeaturePermission.update(
              {
                status: permission.status
              },
              {
                where: {
                  id: _permission.id
                }
              }
            );
            promises.push(updatePromise);
            // also update chilren if permission exists
            AppFeature.findAll({
              attributes: ['id', 'parentId'],
              where: {
                parentId: _permission.featureId
              }
            }).then(_appFeatures => {
              if (_appFeatures.length > 0) {
                _appFeatures.forEach(_appFeature => {
                  let childUpdatePromise = FeaturePermission.update(
                    {
                      status: permission.status
                    },
                    {
                      where: {
                        featureId: _appFeature.id,
                        parent: GROUP,
                        parentId: groupId
                      }
                    }
                  );
                  promises.push(childUpdatePromise);
                });
              }
            });
          } else if (!_permission && permission.status === true) {
            // if a permission is not exists and retrieved permission status is true then create / assign permission to group
            let createPromise = FeaturePermission.create({
              featureId: permission.id,
              parent: GROUP,
              parentId: groupId,
              status: permission.status
            });
            promises.push(createPromise);
            // also assign permission to childrent of newly assigned permission to group.
            AppFeature.findAll({
              attributes: ['id', 'parentId'],
              where: {
                parentId: permission.id
              }
            }).then(_appFeatures => {
              if (_appFeatures.length > 0) {
                _appFeatures.forEach(_appFeature => {
                  let childCreatePromise = FeaturePermission.create({
                    featureId: _appFeature.id,
                    parent: GROUP,
                    parentId: groupId,
                    status: permission.status
                  });
                  promises.push(childCreatePromise);
                });
              }
            });
          }
        });
      });
    });
    return Promise.all(promises);
  }

  modifyUserPermissions(userId, featurePermissions, attributes) {
    this.openConnection();

    let promises = [];
    // first loop through retrieved featurePermissions array
    featurePermissions.forEach(_featurePermission => {
      // loop through children of featurePermissions array that are might stored in featurePermission table against a user
      _featurePermission.children.forEach(permission => {
        // find if a permission is stored against user
        FeaturePermission.findOne({
          attributes: attributes,
          where: {
            featureId: permission.id,
            parent: USER,
            parentId: userId
          }
        }).then(_permission => {
          // if permission exists and its status is different form retrieved permission then update it
          if (_permission && _permission.status != permission.status) {
            let updatePromise = FeaturePermission.update(
              {
                status: permission.status
              },
              {
                where: {
                  id: _permission.id
                }
              }
            );
            promises.push(updatePromise);
            // also update chilren if permission exists
            AppFeature.findAll({
              attributes: ['id', 'parentId'],
              where: {
                parentId: _permission.featureId
              }
            }).then(_appFeatures => {
              if (_appFeatures.length > 0) {
                _appFeatures.forEach(_appFeature => {
                  let childUpdatePromise = FeaturePermission.update(
                    {
                      status: permission.status
                    },
                    {
                      where: {
                        featureId: _appFeature.id,
                        parent: USER,
                        parentId: userId
                      }
                    }
                  );
                  promises.push(childUpdatePromise);
                });
              }
            });
          } else if (!_permission && permission.status === true) {
            // if a permission is not exists and retrieved permission status is true then create / assign permission to user
            let createPromise = FeaturePermission.create({
              featureId: permission.id,
              parent: USER,
              parentId: userId,
              status: permission.status
            });
            promises.push(createPromise);
            // also assign permission to childrent of newly assigned permission to user.
            AppFeature.findAll({
              attributes: ['id', 'parentId'],
              where: {
                parentId: permission.id
              }
            }).then(_appFeatures => {
              if (_appFeatures.length > 0) {
                _appFeatures.forEach(_appFeature => {
                  let childCreatePromise = FeaturePermission.create({
                    featureId: _appFeature.id,
                    parent: USER,
                    parentId: userId,
                    status: permission.status
                  });
                  promises.push(childCreatePromise);
                });
              }
            });
          }
        });
      });
    });
    return Promise.all(promises);
  }

  grant(item) {
    let attributes = ['id', 'featureId', 'parent', 'parentId', 'status'];
    let conditions = {
      featureId: item.featureId,
      parent: item.parent,
      parentId: item.parentId
    };

    return this.findByCondition(attributes, conditions).then(_item => {
      if (_item) {
        if (_item.status === INACTIVE) {
          item.status = ACTIVE;
          return this.update(_item.id, item);
        }
      } else {
        item.status = ACTIVE;
        return this.create(item);
      }
    });
  }

  revoke(item) {
    let attributes = ['id', 'featureId', 'parent', 'parentId', 'status'];
    let conditions = {
      featureId: item.featureId,
      parent: item.parent,
      parentId: item.parentId
    };
    return this.findByCondition(attributes, conditions).then(_item => {
      if (_item) {
        if (_item.status === ACTIVE) {
          item.status = INACTIVE;
          return this.update(_item.id, item);
        }
      } else {
        item.status = INACTIVE;
        return this.create(item);
      }
    });
  }
}
