/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import {
  Role,
  RoleAssignment,
  FeaturePermission,
  AppFeature,
  Group,
  User,
  GROUP,
  USER,
  ROLE
} from '../';
import { Sequelize } from 'sequelize-typescript';

export class RoleModel extends BaseModel {
  constructor() {
    super(Role);
  }

  findDetail(id, attributes) {
    this.openConnection();

    return this.sequelizeModel.findOne({
      where: { id: id },
      attributes: attributes,
      include: [
        // {
        //   model: FeaturePermission,
        //   as: 'permissions',
        //   attributes: ['id'],
        //   include: [
        //     {
        //       model: AppFeature,
        //       as: 'appFeature',
        //       attributes: ['id', 'type', 'name'],
        //       include: [
        //         {
        //           model: AppFeature,
        //           as: 'children',
        //           attributes: ['id', 'type', 'name']
        //         }
        //       ]
        //     }
        //   ]
        // },
        {
          model: RoleAssignment,
          as: 'assignedGroups',
          attributes: ['id', 'roleId', 'parent', 'parentId'],
          include: [
            {
              model: Group,
              as: 'group',
              attributes: ['name']
            }
          ]
        },
        {
          model: RoleAssignment,
          as: 'assignedUsers',
          attributes: ['id', 'roleId', 'parent', 'parentId'],
          include: [
            {
              model: User,
              as: 'user',
              attributes: ['id', 'username']
            }
          ]
        }
      ]
    });
  }

  findAllUsers(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parent', 'parentId'],
      where: {
        roleId: id,
        parent: USER
      }
    }).then(_roleAssignments => {
      // Store group ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.parentId);
      });
      // Select all groups that are not assigned to user yet
      return User.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllGroups(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parent', 'parentId'],
      where: {
        roleId: id,
        parent: GROUP
      }
    }).then(_roleAssignments => {
      // Store group ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.parentId);
      });
      // Select all groups that are not assigned to user yet
      return Group.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllPermissions(id, attributes) {
    this.openConnection();

    return FeaturePermission.findAll({
      attributes: ['id', 'featureId', 'parent', 'parentId', 'status'],
      where: {
        parentId: id,
        parent: ROLE
      }
    }).then(_featurePermissions => {
      return AppFeature.findAll({
        where: {
          parentId: null,
          isVisible: true
        },
        attributes: ['id', 'parentId', 'title'],
        include: [
          {
            model: AppFeature,
            as: 'children',
            attributes: ['id', 'parentId', 'title']
          }
        ]
      }).then(_appFeatures => {
        let result = _appFeatures;
        _appFeatures.forEach((_appFeature, i) => {
          if (_appFeature.children.length > 0) {
            _appFeature.children.forEach((_children, index) => {
              result[i].children[index]['dataValues']['status'] = false;
              _featurePermissions.forEach(_featurePermission => {
                if (
                  _featurePermission.status == true &&
                  _featurePermission.featureId === result[i].children[index].id
                ) {
                  result[i].children[index]['dataValues']['status'] = true;
                }
              });
            });
          }
        });
        return result;
      });
    });
  }

  /**
   * Delete a Role, its relation with User, Group and its Permissions
   * @param id
   */
  delete(id) {
    this.openConnection();
    return this.connection.transaction(t => {
      return Role.destroy({ where: { id: id }, transaction: t }).then(
        result => {
          if (result) {
            return FeaturePermission.destroy({
              where: { parent: 'role', parentId: id },
              transaction: t
            });
          } else {
            throw new Error('Role not found');
          }
        }
      );
    });
  }
}
