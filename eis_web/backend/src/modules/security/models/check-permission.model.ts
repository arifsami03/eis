import {
  RoleAssignmentModel,
  FeaturePermissionModel,
  AppFeatureModel,
  UserModel
} from '../';

export class CheckPermissionModel {

  /**
   * Check permissions of user
   * @param userId 
   * @param originalUrl 
   * @returns true | false
   */
  public checkPermission(userId, originalUrl) {


    /**
     * Check if user is superUser then bypass RBAC and return true  
     */
    return new UserModel().findByCondition(['id', 'isSuperUser'], { id: userId, isActive: true }).then(_user => {

      if (_user.isSuperUser === true) {

        return true;

      } else {
        /**
          * Check if requested url/route is part of RBAC i.e corresponding permission exists in AppFeature table then proceed to our 
          * RBAC logic otehrwise return true
        **/
        let featureId = this.convertUrlToFeatureId(originalUrl);

        return new AppFeatureModel().findByCondition(['id'], { id: featureId }).then(_appFeature => {
          if (!_appFeature) {

            return true;

          } else {

            // Find roles assigned to user
            return new RoleAssignmentModel().findAllByConditions(['id', 'roleId'], {
              parentId: userId, parent: 'user' //TODO:user keyword should come from some proper location
            }).then(assignedRoles => {

              // Check if no role is assigned to user then return false
              if (assignedRoles.length === 0) {

                return false;

              } else {

                // get all role ids that are assigned to user
                let roleIds = [];

                assignedRoles.forEach(role => {
                  roleIds.push(role.roleId);
                });

                // check if this permission is assigned to any assigned role
                return new FeaturePermissionModel()
                  .findAllByConditions(['id', 'status'], {
                    featureId: featureId,
                    parent: 'role',
                    parentId: roleIds
                  })
                  .then(permissions => {
                    if (permissions.length > 0) {

                      let status: boolean = false;

                      permissions.forEach(permission => {
                        if (permission.status == true) {
                          status = permission.status;
                        }
                      });
                      return status;

                    } else {

                      // If no permission is assigned to any role of user then return false
                      return false;

                    }
                  });
              }

            });

          }
        });
      }
    });
  }

  /**
   * Convert requested url/route to corresponding AppFeatureId
   * @param url 
   */
  private convertUrlToFeatureId(url) {
    let featureId = url.split('/');

    // Controller name must be on index 2 and method name must be on index 3 
    return featureId[2].concat('.', featureId[3]);
  }
}
