import { Sequelize } from 'sequelize-typescript';
import { GROUP } from './../conf/constants';

/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import {
  Group,
  UserGroup,
  FeaturePermission,
  RoleAssignment,
  AppFeature,
  Role,
  User
} from '../';

export class GroupModel extends BaseModel {
  constructor() {
    super(Group);
  }

  findAllUsers(id, attributes) {
    this.openConnection();

    return UserGroup.findAll({
      attributes: ['id', 'userId', 'groupId'],
      where: {
        groupId: id
      }
    }).then(_userGroups => {
      // Store user ids to condition array that are already assigned to group
      let condition = [];
      _userGroups.forEach(_userGroup => {
        condition.push(_userGroup.userId);
      });
      // Select all users that are not assigned to  yet group
      return User.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllRoles(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parentId', 'parent'],
      where: {
        parentId: id,
        parent: GROUP
      }
    }).then(_roleAssignments => {
      // Store role ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.roleId);
      });
      // Select all roles that are not assigned to user yet
      return Role.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }
  findAllPermissions(id, attributes) {
    this.openConnection();

    return FeaturePermission.findAll({
      attributes: ['id', 'featureId', 'parent', 'parentId', 'status'],
      where: {
        parentId: id,
        parent: GROUP
      }
    }).then(_featurePermissions => {
      return AppFeature.findAll({
        where: {
          parentId: null,
          isVisible: true
        },
        attributes: ['id', 'parentId', 'title'],
        include: [
          {
            model: AppFeature,
            as: 'children',
            attributes: ['id', 'parentId', 'title']
          }
        ]
      }).then(_appFeatures => {
        let result = _appFeatures;
        _appFeatures.forEach((_appFeature, i) => {
          if (_appFeature.children.length > 0) {
            _appFeature.children.forEach((_children, index) => {
              result[i].children[index]['dataValues']['status'] = false;
              _featurePermissions.forEach(_featurePermission => {
                if (
                  _featurePermission.status == true &&
                  _featurePermission.featureId === result[i].children[index].id
                ) {
                  result[i].children[index]['dataValues']['status'] = true;
                }
              });
            });
          }
        });
        return result;
      });
    });
  }

  findDetail(id, attributes) {
    this.openConnection();

    return this.sequelizeModel.findOne({
      where: { id: id },
      attributes: attributes,
      include: [
        {
          model: UserGroup,
          as: 'userGroups',
          attributes: ['id', 'userId'],
          include: [
            {
              model: User,
              as: 'user',
              attributes: ['id', 'username']
            }
          ]
        },
        {
          model: RoleAssignment,
          as: 'roleAssignments',
          attributes: ['id'],
          include: [
            {
              model: Role,
              as: 'role',
              attributes: ['id', 'name']
            }
          ]
        },
        {
          model: FeaturePermission,
          as: 'permissions',
          attributes: ['id'],
          include: [
            {
              model: AppFeature,
              as: 'appFeature',
              attributes: ['id', 'title'],
              include: [
                {
                  model: AppFeature,
                  as: 'children',
                  attributes: ['id', 'title']
                }
              ]
            }
          ]
        }
      ]
    });
  }

  /**
   * Delete a User, its relation to Group and Role and its permissions
   * @param id
   */
  delete(id) {
    this.openConnection();
    return this.connection.transaction(t => {
      return Group.destroy({ where: { id: id }, transaction: t }).then(
        result => {
          if (result) {
            return RoleAssignment.destroy({
              where: { parent: 'group', parentId: id },
              transaction: t
            }).then(() => {
              return FeaturePermission.destroy({
                where: { parent: 'group', parentId: id },
                transaction: t
              });
            });
          } else {
            throw new Error('Group not found');
          }
        }
      );
    });
  }
}
