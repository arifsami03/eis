import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';
import * as jwt from 'jsonwebtoken';
import { BaseModel, CONFIGURATIONS } from '../../base';
import {
  User,
  FeaturePermission,
  AppFeature,
  UserGroup,
  Group,
  RoleAssignment,
  Role,
  USER,
  RoleAssignmentModel
} from '../';
import { IUserModel } from './interfaces/IUserModel';
import { ErrorHandler } from '../../base/conf/error-handler';

export class UserModel extends BaseModel {
  constructor() {
    super(User);
  }

  public login(item: IUserModel) {
    return super
      .findByCondition(['id', 'username', 'password', 'portal', 'isActive', 'isSuperUser'], {
        username: item.username
      })
      .then(res => {
        let userRes = <IUserModel>res;

        if (res && userRes.password == item.password && userRes.isActive) {

          let token = jwt.sign(
            {
              id: res['id'],
              username: res['username'],
              iat: Math.floor(Date.now() / 1000) - 30
            },
            CONFIGURATIONS.SECRET
          );
          let result = {
            id: userRes.id,
            username: userRes.username,
            isSuperUser: userRes.isSuperUser,
            token: token,
            permissions: [],
            portal: userRes.portal
          }
          // if user is superuser return with empty permissions
          if (userRes.isSuperUser) {
            return result;
          } else { // else check for user permissions
            return this.findPermissions(userRes.id).then(_permissions => {
              result.permissions = _permissions;
              return result;
            });
          }

        } else {
          return ErrorHandler.invalidLogin;
        }
      });
  }

  private findPermissions(userId: number) {

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parent', 'parentId'],
      where: {
        parentId: userId,
        parent: USER
      },
      include: [
        {
          model: Role,
          as: 'role',
          attributes: ['id', 'name'],
          include: [
            {
              model: FeaturePermission,
              as: 'permissions',
              attributes: ['id', 'featureId', 'status']
            }
          ]
        }
      ]
    }).then(_roleAssignments => {
      let permissions: { name: string; status: boolean }[] = [];

      _roleAssignments.forEach(roleAssignment => {
        roleAssignment.role.permissions.forEach(permission => {
          let pitem = {
            name: permission.featureId.toLowerCase(),
            status: permission.status
          };
          // We need to check if permission is not already added in permissions array.
          let existedItem = permissions.find(o => o['name'] == pitem.name);
          if (!existedItem) {
            permissions.push(pitem);
          } else {
            if (existedItem.status == false) {
              existedItem.status = pitem.status;
            }
          }
        });
      });
      return permissions;
    });
  }

  findAllGroups(id, attributes) {
    this.openConnection();

    return UserGroup.findAll({
      attributes: ['id', 'userId', 'groupId'],
      where: {
        userId: id
      }
    }).then(_userGroups => {
      // Store group ids to condition array that are already assigned to user
      let condition = [];
      _userGroups.forEach(_userGroup => {
        condition.push(_userGroup.groupId);
      });
      // Select all groups that are not assigned to user yet
      return Group.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllRoles(id, attributes) {
    this.openConnection();

    return RoleAssignment.findAll({
      attributes: ['id', 'roleId', 'parentId', 'parent'],
      where: {
        parentId: id,
        parent: USER
      }
    }).then(_roleAssignments => {
      // Store role ids to condition array that are already assigned to user
      let condition = [];
      _roleAssignments.forEach(_roleAssignment => {
        condition.push(_roleAssignment.roleId);
      });
      // Select all roles that are not assigned to user yet
      return Role.findAll({
        attributes: attributes,
        where: {
          id: {
            [Sequelize.Op.notIn]: condition
          }
        }
      });
    });
  }

  findAllPermissions(id, attributes) {
    this.openConnection();

    return FeaturePermission.findAll({
      attributes: ['id', 'featureId', 'parent', 'parentId', 'status'],
      where: {
        parentId: id,
        parent: USER
      }
    }).then(_featurePermissions => {
      return AppFeature.findAll({
        where: {
          parentId: null,
          isVisible: true
        },
        attributes: ['id', 'parentId', 'title'],
        include: [
          {
            model: AppFeature,
            as: 'children',
            attributes: ['id', 'parentId', 'title']
          }
        ]
      }).then(_appFeatures => {
        let result = _appFeatures;
        _appFeatures.forEach((_appFeature, i) => {
          if (_appFeature.children.length > 0) {
            _appFeature.children.forEach((_children, index) => {
              result[i].children[index]['dataValues']['status'] = false;
              _featurePermissions.forEach(_featurePermission => {
                if (
                  _featurePermission.status == true &&
                  _featurePermission.featureId === result[i].children[index].id
                ) {
                  result[i].children[index]['dataValues']['status'] = true;
                }
              });
            });
          }
        });
        return result;
      });
    });
  }

  /**
   * Find User and Roles by id
   * @param id
   * @param attributes
   */
  find(id, attributes?) {

    return super.find(id, attributes).then(_result => {

      let result = _result['dataValues']; // if we want to overwrite the default response of sequelize then we've to do this

      // if user is not superuser return its roles
      if (!result.isSuperUser) {

        result.roleAssignments = [];

        return RoleAssignment.findAll({
          attributes: ['id', 'roleId', 'parent', 'parentId'],
          where: {
            parent: 'user',
            parentId: result.id
          },
          include: [
            {
              model: Role,
              as: 'role',
              attributes: ['id', 'name']
            }
          ]
        }).then(_roleAssignments => {

          result.roleAssignments = _roleAssignments;

          return result;

        });

      } else {

        return result;

      }

    });

  }
  // 
  /**
   * Delete a User, its relation to Group and Role and its permissions
   * @param id
   */
  delete(id) {
    this.openConnection();
    return this.connection.transaction(t => {
      return User.findOne({ where: { id: id }, transaction: t }).then(_user => {
        let id = _user.id;
        return User.destroy({ where: { id: id }, transaction: t }).then(() => {
          return RoleAssignment.destroy({
            where: { parent: 'user', parentId: id },
            transaction: t
          }).then(() => {
            return FeaturePermission.destroy({
              where: { parent: 'user', parentId: id },
              transaction: t
            }).then(() => {
              return {
                success: true,
                message: 'User has bee deleted successfully'
              };
            });
          });
        });
      });
    });
  }
}
