/**
 * Importing related Models
 */
import { BaseModel } from '../../base';
import { RoleWFStateFlow } from '../';
import { Promise } from 'bluebird';
import { WFStateFlowModel, WFStateModel } from '../../configuration'

export class RoleWFStateFlowModel extends BaseModel {
  constructor() {
    super(RoleWFStateFlow);
  }

  list(id: number) {

    return this.findAllByConditions(['id', 'roleId', 'WFStateFlowId'], { roleId: id }).then(roleFlows => {
      var myflows = [];
      return Promise.each(roleFlows, (flowItem) => {

        return new WFStateFlowModel().findByCondition(['fromId', 'toId', 'WFId'], { id: flowItem['WFStateFlowId'] }).then(stateFlow => {
          var flows = [];
          return new WFStateModel().findByCondition(['state'], { id: stateFlow['fromId'] }).then(fromState => {
            return new WFStateModel().findByCondition(['state'], { id: stateFlow['toId'] }).then(result => {
              flows.push({
                from: fromState['state'],
                to: result['state'],
              })
              myflows.push({
                id: flowItem['id'],
                flows: flows,
                WFId: stateFlow['WFId'],

              });
            })

          })

        })

      }).then(() => {

        return myflows;
      });
    })
  }

  create(item) {


    return Promise.each(item.data, (row) => {
      return this.findByCondition(['id'], { roleId: row['roleId'], WFStateFlowId: row['WFStateFlowId'] }).then(res => {
        if (res != undefined) {

        }
        else {
          return super.create(row);
        }

      })


    }).then(() => {
      return this.deleteWorkFlows(item);
    })

  }

  private deleteWorkFlows(item) {
    console.log(item['deletedIds']);
    return  Promise.each(item['deletedIds'], (row) => {



      if (item['deletedIds'].length != 0) {

        return this.deleteByConditions({ roleId: row['roleId'], WFStateFlowId: row['WFStateFlowId'] })

      }

    })
  }

  getWFStateFlowByRole(id: number) {
    return this.findAllByConditions(['WFStateFlowId'], { roleId: id });
  }
}
