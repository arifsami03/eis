import {
  Table,
  Column,
  Model,
  HasMany,
  Default,
  NotEmpty
} from 'sequelize-typescript';
/**
 * Importing related Models
 */
import { UserGroup, RoleAssignment, FeaturePermission } from '../../';

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class Group extends Model<Group> {
  /**
   * Adding Columns and their properties of Group Table
   */
  @Default('')
  @NotEmpty({ msg: 'Name is required!' })
  @Column
  name: string;

  @Default('')
  @NotEmpty({ msg: 'Created by is required!' })
  @Column
  createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relatonship
   */
  @HasMany(() => UserGroup, {
    foreignKey: 'groupId'
  })
  userGroups: UserGroup[];

  @HasMany(() => RoleAssignment, {
    foreignKey: 'parentId',
    constraints: false,
    scope: {
      parent: 'group'
    }
  })
  roleAssignments: RoleAssignment[];

  @HasMany(() => FeaturePermission, {
    foreignKey: 'parentId',
    constraints: false,
    scope: {
      parent: 'group',
      status: true
    }
  })
  permissions: FeaturePermission[];
}
