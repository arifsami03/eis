import {  Table,  Column,  Model,  ForeignKey,  BelongsTo} from 'sequelize-typescript';
/**
 * Importing related Models
 */
import { Role } from '../../';
import { WFStateFlow } from '../../../configuration';

@Table({ timestamps: true }) // for adding createdAt and updatedAt Columns
export class RoleWFStateFlow extends Model<RoleWFStateFlow> {
  /**
   * Adding Columns and their properties of UserGroup Table
   */
  @ForeignKey(() => Role)
  @Column
  roleId: number;

  @ForeignKey(() => WFStateFlow)
  @Column
  WFStateFlowId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

}
