import { Router } from 'express';

import { RoleWFStateFlowsController } from '../';

/**
 * / route
 *
 * @class User
 */
export class RoleWFStateFlowsRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class UserRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class UserRoute
   * @method create
   *
   */
  public create() {
    let controller = new RoleWFStateFlowsController();

    // this.router.route('/security/users/index').get(controller.index);

     this.router.route('/security/roleWFStateFlow/list/:id').get(controller.list);
     this.router.route('/security/roleWFStateFlow/create').post(controller.create);
     this.router.route('/security/roleWFStateFlow/getWFStateFlowByRole/:id').get(controller.getWFStateFlowByRole);

    // this.router.route('/security/users/findAllRoles/:userId').get(controller.findAllRoles);

    // this.router.route('/security/users/findAllPermissions/:userId').get(controller.findAllPermissions);

    // this.router.route('/security/users/findAttributesList').get(controller.findAttributesList);

    // this.router.route('/security/users/update/:id').put(controller.update);

    // this.router.route('/security/users/login').post(controller.login);

    // this.router.route('/security/users/create').post(controller.create);

    // this.router.route('/security/users/delete/:id').delete(controller.delete);

    // // route to get portal
    // this.router.route('/security/users/getPortal').post(controller.getPortal);
  }
}
