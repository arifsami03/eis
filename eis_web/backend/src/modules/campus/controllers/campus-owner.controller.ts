import * as express from "express";
import { Request, Response } from 'express';
import { ErrorHandler } from "../../base/conf/error-handler";

import { CampusOwnerModel } from '../';
import { UserModel } from '../../security';

export class CampusOwnerController {
  constructor() { }



  findPersonalInfo(req: express.Request,
    res: express.Response,
    next: express.NextFunction) {

    new CampusOwnerModel().getCampusOwnerPersonalInfo(req.params.userId)
      .then(result => {
        res.json(result);
      })
      .catch(err => {
        ErrorHandler.sendServerError(err, res, next);
      });
  }

  /**
   * Update
   * 
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(req: express.Request, res: express.Response, next: express.NextFunction) {

    let id = req.params.userId;
    let item = req.body;

    new CampusOwnerModel().updateCampusOwnerPersonalInfo(id, item).then(result => {

      res.json(result);

    })
    .catch(err => {
      ErrorHandler.sendServerError(err, res, next);
    });
  }
}
