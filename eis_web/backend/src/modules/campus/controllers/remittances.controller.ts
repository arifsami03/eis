import * as express from 'express';
import { Request, Response } from 'express';
import { RemittanceModel } from '..';

export class RemittancesController {
  constructor() {}

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  save(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RemittanceModel()
      .save(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
  /**
   * view
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  view(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new RemittanceModel()
      .view(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
}
