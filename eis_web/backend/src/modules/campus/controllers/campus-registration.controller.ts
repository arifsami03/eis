import * as express from 'express';
import { Request, Response } from 'express';

import { CampusModel } from '../';
import { UserModel } from '../../security';

export class CampusRegistrationController {
  constructor() {}

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  preRegistration(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .preRegistration(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  preRegistrationList(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .preRegistrationList(req.params.status)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  preRegistrationDetailOne(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .preRegistrationDetailOne(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  changeStatus(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .changeStatus(req.params.id, { status: req.body.status })
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  submitPostRegistration(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .submitPostRegistration(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  getRegistrationStatus(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .getRegistrationStatus(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  validateUniqueEmail(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new CampusModel()
      .validateUniqueEmail(req.body)
      .then(result => {
        if (result) {
          res.json(true);
        } else {
          res.json(false);
        }
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }
}
