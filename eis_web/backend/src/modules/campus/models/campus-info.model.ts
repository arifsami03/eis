import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { UserModel } from '../../security/index';
import { Promise } from 'bluebird';
import { Campus, CampusAffiliationModel, CampusAddressModel, CampusAvailableBuildingModel, CampFacultyModel, CFProgramDetailModel } from '../';


export class CampusInfoModel extends BaseModel {
  constructor() {
    super(Campus);
  }

  getCampusInfo(id) {

    let returnResult = {};

    return new UserModel().find(id).then(user => {
      let id = user['campusOwnerId']
      return this.findByCondition(
        ['id', 'campusName', 'levelOfEducation', 'tentativeSessionStart', 'campusLocation', 'buildingAvailable', 'website', 'officialEmail', 'establishedSince', 'noOfCampusTransfer', 'applicationType'],
        { campusOwnerId: id }
      ).then((campusResult) => {

        returnResult = campusResult['dataValues'];

        let campAff = new CampusAffiliationModel();

        return campAff.findAllByConditions(['id', 'affiliation'], { campusId: campusResult.id }).then(affResult => {

          returnResult['affiliations'] = affResult;

        })
      }).then(() => {


        let campAddress = new CampusAddressModel();
        return campAddress.findAllByConditions(['id', 'address', 'latitude', 'longitude'], { campusId: returnResult['id'] }).then(camAddressResult => {
          returnResult['addresses'] = camAddressResult;
        });

      }).then(() => {


        let buildingAvialable = new CampusAvailableBuildingModel();

        return buildingAvialable.findByCondition(['id',
          'buildingOwn',
          'rentAgreementUpTo',
          'coverdArea',
          'openArea',
          'totalArea',
          'roomsQuantity',
          'washroomsQuantity',
          'teachingStaffQuantity',
          'labsQuantity',
          'nonTeachingStaffQty',
          'studentsQuantity',
          'playGround',
          'swimmingPool',
          'healthClinic',
          'mosque',
          'cafeteria',
          'transport',
          'library',
          'bankBranch'], { campusId: returnResult['id'] }).then(availableBuilding => {

            if (availableBuilding) {
              // availableBuilding = availableBuilding['dataValues'];
              returnResult['buildingOwn'] = availableBuilding['buildingOwn'];
              returnResult['rentAgreementUpTo'] = availableBuilding['rentAgreementUpTo'];
              returnResult['coverdArea'] = availableBuilding['coverdArea'];
              returnResult['openArea'] = availableBuilding['openArea'];
              returnResult['totalArea'] = availableBuilding['totalArea'];
              returnResult['roomsQuantity'] = availableBuilding['roomsQuantity'];
              returnResult['washroomsQuantity'] = availableBuilding['washroomsQuantity'];
              returnResult['teachingStaffQuantity'] = availableBuilding['teachingStaffQuantity'];
              returnResult['labsQuantity'] = availableBuilding['labsQuantity'];
              returnResult['nonTeachingStaffQty'] = availableBuilding['nonTeachingStaffQty'];
              returnResult['studentsQuantity'] = availableBuilding['studentsQuantity'];
              returnResult['playGround'] = availableBuilding['playGround'];
              returnResult['swimmingPool'] = availableBuilding['swimmingPool'];
              returnResult['healthClinic'] = availableBuilding['healthClinic'];
              returnResult['mosque'] = availableBuilding['mosque'];
              returnResult['cafeteria'] = availableBuilding['cafeteria'];
              returnResult['transport'] = availableBuilding['transport'];
              returnResult['library'] = availableBuilding['library'];
              returnResult['bankBranch'] = availableBuilding['bankBranch'];
            }
          });
      }).then(() => {

        let cFac = new CampFacultyModel;

        let campFacProDetResult = [];

        return cFac.findAllByConditions(['id', 'facultyId'], { campusId: returnResult['id'] }).then(camFacRes => {

          return Promise.each(camFacRes, (camFacItem) => {

            return new CFProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { campusFacultyId: camFacItem['id'] }).then(camProgDetRes => {

              campFacProDetResult.push({
                facultyId: camFacItem['facultyId'],
                programDetails: camProgDetRes
              });
            })

          })

        }).then(() => {
          returnResult['facultyProgramDetails'] = campFacProDetResult;
          return returnResult;
        })


      })
    });

  }

  // getCampusInfo(id) {

  //   let returnResult = {};

  //   return new UserModel().find(id).then(user => {
  //     let id = user['campusOwnerId']
  //     return this.findByCondition(
  //       ['id', 'campusName', 'levelOfEducation', 'tentativeSessionStart', 'campusLocation', 'buildingAvailable', 'website', 'officialEmail', 'establishedSince', 'noOfCampusTransfer', 'applicationType'],
  //       { campusOwnerId: id }
  //     ).then((campusResult) => {

  //       returnResult = campusResult['dataValues'];

  //       let campAff = new CampusAffiliationModel();

  //       return campAff.findAllByConditions(['id', 'affiliation'], { campusId: campusResult.id }).then(affResult => {

  //         returnResult['affiliations'] = affResult;

  //       })
  //     }).then(() => {


  //       let campAddress = new CampusAddressModel();
  //       return campAddress.findAllByConditions(['id', 'address', 'latitude', 'longitude'], { campusId: returnResult['id'] }).then(camAddressResult => {
  //         returnResult['addresses'] = camAddressResult;
  //       });

  //     }).then(() => {


  //       let buildingAvialable = new CampusAvailableBuildingModel();

  //       return buildingAvialable.findByCondition(['id',
  //         'buildingOwn',
  //         'rentAgreementUpTo',
  //         'coverdArea',
  //         'openArea',
  //         'totalArea',
  //         'roomsQuantity',
  //         'washroomsQuantity',
  //         'teachingStaffQuantity',
  //         'labsQuantity',
  //         'nonTeachingStaffQty',
  //         'studentsQuantity',
  //         'playGround',
  //         'swimmingPool',
  //         'healthClinic',
  //         'mosque',
  //         'cafeteria',
  //         'transport',
  //         'library',
  //         'bankBranch'], { campusId: returnResult['id'] }).then(availableBuilding => {

  //           if (availableBuilding) {
  //             // availableBuilding = availableBuilding['dataValues'];
  //             returnResult['buildingOwn'] = availableBuilding['buildingOwn'];
  //             returnResult['rentAgreementUpTo'] = availableBuilding['rentAgreementUpTo'];
  //             returnResult['coverdArea'] = availableBuilding['coverdArea'];
  //             returnResult['openArea'] = availableBuilding['openArea'];
  //             returnResult['totalArea'] = availableBuilding['totalArea'];
  //             returnResult['roomsQuantity'] = availableBuilding['roomsQuantity'];
  //             returnResult['washroomsQuantity'] = availableBuilding['washroomsQuantity'];
  //             returnResult['teachingStaffQuantity'] = availableBuilding['teachingStaffQuantity'];
  //             returnResult['labsQuantity'] = availableBuilding['labsQuantity'];
  //             returnResult['nonTeachingStaffQty'] = availableBuilding['nonTeachingStaffQty'];
  //             returnResult['studentsQuantity'] = availableBuilding['studentsQuantity'];
  //             returnResult['playGround'] = availableBuilding['playGround'];
  //             returnResult['swimmingPool'] = availableBuilding['swimmingPool'];
  //             returnResult['healthClinic'] = availableBuilding['healthClinic'];
  //             returnResult['mosque'] = availableBuilding['mosque'];
  //             returnResult['cafeteria'] = availableBuilding['cafeteria'];
  //             returnResult['transport'] = availableBuilding['transport'];
  //             returnResult['library'] = availableBuilding['library'];
  //             returnResult['bankBranch'] = availableBuilding['bankBranch'];
  //           }
  //         });
  //     }).then(() => {

  //       let cFac = new CampFacultyModel;

  //       let campFacProResult = [];

  //       return cFac.findAllByConditions(['id', 'facultyId'], { campusId: returnResult['id'] }).then(camFacRes => {

  //         return Promise.each(camFacRes, (camFacItem) => {

  //           return new CFProgramDetailModel().findAllByConditions(['id', 'programDetailId'], { campusFacultyId: camFacItem['id'] }).then(camProgRes => {

  //             campFacProResult.push({
  //               facultyId: camFacItem['facultyId'],
  //               programs: camProgRes
  //             });
  //           })

  //         })

  //       }).then(() => {
  //         returnResult['facultyProgram'] = campFacProResult;
  //         return returnResult;
  //       })


  //     })
  //   });

  // }

  updateCampusInfo(id, item) {


    return this.update(id, item).then((result) => {
      return this.createAddresses(id, item).then(res => {

        return this.updateAddresses(item)
          .then(res => {

            return this.createBuildingAvailableDetails(id, item);

          }).then(res => {

            return this.createAffiliations(id, item).then(res => {

              return this.updateAffiliations(item).then(res => {

                return this.deleteAffiliations(item).then(res => {

                  return this.deleteFacultyProgram(id).then(res => {


                    return this.saveFacultyProgram(id, item);
                  })

                })


              })

            })

          })

      })

    })
  }

  private createAddresses(id, item) {
    let cRef = new CampusAddressModel();
    var addresses = [];

    for (var i = 0; i < item['addresses'].length; i++) {

      if ((item['addresses'][i].id == '' || item['addresses'][i].id == null) && item['addresses'][i].address != '' && item['addresses'][i].latitude != '' && item['addresses'][i].longitude != '') {
        addresses.push({
          campusId: id,
          address: item['addresses'][i].address,
          latitude: item['addresses'][i].latitude,
          longitude: item['addresses'][i].longitude,
          buildingAvailableType: item['buildingAvailable']

        })
      }
    }
    return cRef.sequelizeModel.bulkCreate(addresses);
  }

  private updateAddresses(item) {

    let mcModel = new CampusAddressModel();

    return Promise.each(item['addresses'], (addressItem) => {

      if (addressItem['id'] != '') {
        return mcModel.update(addressItem['id'], { address: addressItem['address'], latitude: addressItem['latitude'], longitude: addressItem['longitude'] }).then(res => {
        });
      }

    })

  }
  private createBuildingAvailableDetails(id, item) {
    item['campusId'] = id;

    let cAvailableModel = new CampusAvailableBuildingModel();
    return cAvailableModel.findAllByConditions(['id'], { campusId: id }).then(res => {

      if (res.length != 0) {
        return this.UpdateBuildingAvailable(res[0]['id'], item);
      }
      else {
        return cAvailableModel.create(item);

      }

    }

    )

  }
  private UpdateBuildingAvailable(id, item) {

    let cAvailableModel = new CampusAvailableBuildingModel();
    return cAvailableModel.update(id, item);

  }
  private createAffiliations(id, item) {
    let cAff = new CampusAffiliationModel();
    var affiliations = [];

    for (var i = 0; i < item['affiliations'].length; i++) {

      if (item['affiliations'][i].id == '' && item['affiliations'][i].affiliation != '') {
        affiliations.push({
          campusId: id,
          affiliation: item['affiliations'][i].affiliation,

        })
      }
    }
    return cAff.sequelizeModel.bulkCreate(affiliations);
  }
  private updateAffiliations(item) {

    let cAff = new CampusAffiliationModel();

    return Promise.each(item['affiliations'], (affiliationItem) => {

      if (affiliationItem['id'] != '') {
        return cAff.update(affiliationItem['id'], { affiliation: affiliationItem['affiliation'] }).then(res => {
        });
      }

    })

  }
  private deleteAffiliations(item) {

    return new Promise((resolve, reject) => {

      let cAff = new CampusAffiliationModel();

      if (item['deletedAffiliationIds'].length != 0) {
        var deleteItems = [];
        for (var i = 0; i < item['deletedAffiliationIds'].length; i++) {
          deleteItems.push(item['deletedAffiliationIds'][i].id);
        }


        return cAff.deleteByConditions({ id: deleteItems }).then(res => {
          resolve(res);
        });
      }
      else {
        resolve(true);
      }
    })
  }
  private saveFacultyProgram(id, item) {

    // return new Promise((resolve, reject) => {

    let cFac = new CampFacultyModel();
    let cFacProg = new CFProgramDetailModel


    if (item['facultyProgram'].length != 0) {
      var facultyPrograms = {};

      return Promise.each(item['facultyProgram'], (facultyProgramItem) => {

        return cFac.create({
          campusId: id,
          facultyId: facultyProgramItem['facultyId']
        }).then(res => {
          return Promise.each(facultyProgramItem['programDetails'], programDetails => {
            return cFacProg.create({
              campusFacultyId: res['dataValues']['id'],
              programDetailId: programDetails
            })

          })
        });

      })
      // for (var i = 0; i < item['facultyProgram'].length; i++) {
      //   facultyPrograms = {
      //     campusId: id,
      //     facultyId: item['facultyProgram'][i]['facultyId']
      //   }

      //   return cFac.create(facultyPrograms).then(res => {
      //     //resolve(res);
      //   });
      // }



    }
    else {
      //  resolve(true);
    }
    //})
  }
  deleteFacultyProgram(id) {
    let cFac = new CampFacultyModel();
    let cFacProg = new CFProgramDetailModel();
    return cFac.findAllByConditions(['id'], { campusId: id }).then(res => {

      var deleted = [];
      for (var i = 0; i < res.length; i++) {

        deleted.push(res[i].id)
      }

      return cFacProg.deleteByConditions({ campusFacultyId: deleted }).then(() => {
        return cFac.deleteByConditions({ campusId: id });
      })
    })
  }



}
