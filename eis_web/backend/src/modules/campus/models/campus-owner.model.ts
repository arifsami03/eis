import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { CampusOwner } from '../';
import { UserModel } from '../../security/index';
import { CampusOwnerReferenceModel } from '../models/campus-owner-reference.model';
import { Promise } from 'bluebird';



export class CampusOwnerModel extends BaseModel {
  constructor() {
    super(CampusOwner);
  }

  getCampusOwnerPersonalInfo(id) {
    let cRef = new CampusOwnerReferenceModel();
    return new UserModel().find(id).then(userResult => {
      return super.find(userResult['campusOwnerId']).then((data) => {

        let result = data;
        result['dataValues']['references'] = [];


        return cRef.findAllByConditions(
          ['id', 'fullName', 'mobileNumber', 'address'],
          { [Sequelize.Op.or]: [{ campusOwnerId: userResult['campusOwnerId'] }] }
        ).then((metaData) => {
          metaData.forEach(meta => {
            result['dataValues']['references'].push({ id: meta.id, refName: meta.fullName, refMobileNumber: meta.mobileNumber, refAddress: meta.address });


          });
          return result;
        });
      })
    })
  }

  updateCampusOwnerPersonalInfo(id, item) {
    
    return this.update(id, item['personalInfo']).then((result) => {

      return this.createReferences(id, item['personalInfo']).then(result => {

        return this.updateReferences(item['personalInfo']).then(res => {

          return this.deleteReferences(item)
          // .then(res => {

          //   return this.updateUserEmail(item);

          // })


        })

      })

    })
  }

  private createReferences(id, item) {

    let cRef = new CampusOwnerReferenceModel();
    var references = [];

    for (var i = 0; i < item['references'].length; i++) {

      if (item['references'][i].refName != '' && item['references'][i].refAddress != '' && item['references'][i].refMobileNumber != '' && item['references'][i].id == '') {
        references.push({
          campusOwnerId: id,
          fullName: item['references'][i].refName,
          mobileNumber: item['references'][i].refMobileNumber,
          address: item['references'][i].refAddress
        })
      }
    }
    // return MetaConf.bulkCreate(conatacts);
    return cRef.sequelizeModel.bulkCreate(references);
  }

  private updateReferences(item) {
    let cRef = new CampusOwnerReferenceModel();

    return Promise.each(item['references'], (references) => {

      if (references['id'] != '') {
        return cRef.update(references['id'], { fullName: references['refName'], mobileNumber: references['refMobileNumber'], address: references['refAddress'] }).then(res => {
        });
      }

    })

  }
  private updateUserEmail(item) {
    // let user = new UserModel();
    // return user.update()

    // return Promise.each(item['references'], (references) => {

    //   if (references['id'] != '') {
    //     return cRef.update(references['id'], { fullName: references['refName'], mobileNumber: references['refMobileNumber'], address: references['refAddress'] }).then(res => {
    //     });
    //   }

    // })
  }

  private deleteReferences(item) {

    return new Promise((resolve, reject) => {

      let cRef = new CampusOwnerReferenceModel();

      if (item['deleteRefernceIds'].length != 0) {

        // return MetaConf.destroy({ where: { id: item['deletedContactIds'] } });
        return cRef.deleteByConditions({ id: item['deleteRefernceIds'] }).then(res => {
          resolve(res);
        });

      }
      else {
        resolve(true);
      }
    })
  }


}
