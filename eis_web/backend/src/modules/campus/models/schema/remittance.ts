import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
  HasOne,
  PrimaryKey
} from 'sequelize-typescript';

import { Campus } from './../../';

@Table({ timestamps: true })
export class Remittance extends Model<Remittance> {
  @ForeignKey(() => Campus)
  @PrimaryKey
  @Column
  id: number;

  @Column instrumentCategory: string;

  @Column currentDate: Date;

  @Column bankId: number;

  @Column branchName: string;

  @Column instrumentDate: Date;

  @Column instrumentNumber: string;

  @Column amount: number;

  @Column purpose: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => Campus, {
    foreignKey: 'id'
  })
  campus: Campus;
}
