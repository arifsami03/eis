import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';

import { Campus } from './../../';

@Table({ timestamps: true })
export class CampusTargettedLocations extends Model<CampusTargettedLocations> {
  @ForeignKey(() => Campus)
  @Column
  campusId: number;

  @Column address: string;

  @Column latitude: number;

  @Column longitude: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => Campus, {
    foreignKey: 'campusId'
  })
  campus: Campus;
}
