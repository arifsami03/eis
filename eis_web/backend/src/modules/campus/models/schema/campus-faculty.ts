import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany
} from 'sequelize-typescript';
import { Faculty } from '../../../institute';
import { Campus } from '../../index';

@Table({ timestamps: true })
export class CampFaculty extends Model<CampFaculty> {
  @ForeignKey(() => Campus)  
  @Column campusId: number;

  @ForeignKey(() => Faculty)
  @Column
  facultyId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => Faculty, {
    foreignKey: 'facultyId'
  })
  faculty: Faculty;
}
