import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';
// import { Campus } from './../../';

@Table({ timestamps: true })
export class CampusAvailableBuilding extends Model<CampusAvailableBuilding> {
  // @ForeignKey(() => Campus)
  @Column
  campusId: number;

  @Column buildingOwn: boolean;

  @Column rentAgreementUpTo: string;


  @Column coverdArea: number;

  @Column openArea: number;

  @Column totalArea: number;

  @Column roomsQuantity: number;

  @Column washroomsQuantity: number;

  @Column teachingStaffQuantity: number;

  @Column labsQuantity: number;

  @Column nonTeachingStaffQty: number;

  @Column studentsQuantity: number;

  @Column playGround: boolean;

  @Column swimmingPool: boolean;

  @Column healthClinic: boolean;

  @Column mosque: boolean;

  @Column cafeteria: boolean;

  @Column transport: boolean;

  @Column library: boolean;

  @Column bankBranch: boolean;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  // @BelongsTo(() => Campus, {
  //   foreignKey: 'campusId'
  // })
  // campus: Campus;
}
