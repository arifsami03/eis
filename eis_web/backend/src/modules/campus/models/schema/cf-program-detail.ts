import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
  HasMany
} from 'sequelize-typescript';
import { ProgramDetail } from '../../../institute';
import { CampFaculty } from '../../index';

@Table({ timestamps: true })
export class CFProgramDetail extends Model<CFProgramDetail> {
  @ForeignKey(() => CampFaculty)  
  @Column campusFacultyId: number;

 @ForeignKey(() => ProgramDetail)
  @Column
  programDetailId: number;

  @Column createdBy: number;

  @Column updatedBy: number;

  @HasMany(() => ProgramDetail, {
    foreignKey: 'programDetailId'
  })
  programDetail: ProgramDetail;
}
