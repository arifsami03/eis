import {
  Table,
  Column,
  Model,
  
} from 'sequelize-typescript';

//import { User } from '../../../security/index';

@Table({ timestamps: true })
export class CampusAddress extends Model<CampusAddress> {
  @Column campusId: number;

  @Column address: string;
  @Column latitude: number;
  @Column longitude: number;
  @Column buildingAvailableType: boolean;
  


  @Column createdBy: number;

  @Column updatedBy: number;

}
