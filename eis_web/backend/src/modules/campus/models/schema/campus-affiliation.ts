import {
  Table,
  Column,
  Model,
  
} from 'sequelize-typescript';

//import { User } from '../../../security/index';

@Table({ timestamps: true })
export class CampusAffiliation extends Model<CampusAffiliation> {
  @Column campusId: number;

  @Column affiliation: string;


  @Column createdBy: number;

  @Column updatedBy: number;

}
