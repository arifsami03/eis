import {
  Table,
  Column,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
  HasOne
} from 'sequelize-typescript';

import { CampusOwnerReference, Campus } from './../../';
import { MetaConf } from '../../../configuration';
import { User } from '../../../security/index';

@Table({ timestamps: true })
export class CampusOwner extends Model<CampusOwner> {
  @Column fullName: string;

  @Column cnic: string;

  @Column mobileNumber: string;

  @Column email: string;

  @Column applicationType: string;

  @Column ntn: string;

  @Column countryId: number;

  @Column provinceId: number;

  @ForeignKey(() => MetaConf)
  @Column
  cityId: number;

  @Column tehsilId: number;

  @Column natureOfWorkId: number;

  @Column approxMonthlyIncome: number;

  @Column nearestBankId: number;

  @Column address: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relationships
   */

  // Considering CampusOwner Has Only One Campus
  @HasOne(() => Campus, { foreignKey: 'campusOwnerId' })
  campus: Campus;

  @BelongsTo(() => User, { foreignKey: 'id' })
  user: User;
  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => MetaConf, {
    foreignKey: 'cityId'
  })
  city: MetaConf;
}
