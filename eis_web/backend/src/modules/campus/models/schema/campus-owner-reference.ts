import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo
} from 'sequelize-typescript';

import { CampusOwner } from './../../';

@Table({ timestamps: true })
export class CampusOwnerReference extends Model<CampusOwnerReference> {
  @ForeignKey(() => CampusOwner)
  @Column
  campusOwnerId: number;

  @Column fullName: string;

  @Column mobileNumber: string;

  @Column address: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CampusOwner, {
    foreignKey: 'campusOwnerId'
  })
  campusOwner: CampusOwner;
}
