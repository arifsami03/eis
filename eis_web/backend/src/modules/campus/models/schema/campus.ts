import {
  Table,
  Column,
  Model,
  ForeignKey,
  HasMany,
  BelongsTo
} from 'sequelize-typescript';
import { MetaConf } from '../../../configuration';

import {
  CampusAvailableBuilding,
  CampusTargettedLocations,
  CampusOwner
} from './../../';

@Table({ timestamps: true })
export class Campus extends Model<Campus> {
  @ForeignKey(() => CampusOwner)
  @Column
  campusOwnerId: number;

  @Column campusName: string;

  @Column levelOfEducation: string;

  @Column tentativeSessionStart: Date;
  @Column applicationType: string;

  @Column campusLocation: string;

  @Column website: string;
  @Column officialEmail: string;
  @Column establishedSince: string;

  @Column noOfCampusTransfer: number;


  @Column buildingAvailable: boolean;

  @ForeignKey(() => MetaConf)
  @Column
  cityId: number;

  @Column status: string;

  @Column createdBy: number;

  @Column updatedBy: number;

  /**
   * HasMany Relationships
   */
  @HasMany(() => CampusAvailableBuilding, { foreignKey: 'campusOwnerId' })
  campusAvailableBuildings: CampusAvailableBuilding[];

  @HasMany(() => CampusTargettedLocations, { foreignKey: 'campusOwnerId' })
  campusTargettedLocations: CampusTargettedLocations[];

  /**
   * BelongsTo relationships
   */
  @BelongsTo(() => CampusOwner, {
    foreignKey: 'campusOwnerId'
  })
  campusOwner: CampusOwner;

  @BelongsTo(() => MetaConf, {
    foreignKey: 'cityId'
  })
  city: MetaConf;
}
