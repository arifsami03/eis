import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { CampusOwnerReference } from '../';
import { UserModel } from '../../security/index';


export class CampusOwnerReferenceModel extends BaseModel {
  constructor() {
    super(CampusOwnerReference);
  }
}
