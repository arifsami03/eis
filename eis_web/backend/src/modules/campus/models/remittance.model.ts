import { Promise } from 'bluebird';
import { Sequelize } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { Remittance, CampusOwner, Campus } from '..';
import { User } from '../../security';

export class RemittanceModel extends BaseModel {
  constructor() {
    super(Remittance);
  }

  /**
   * Create
   * @param _data
   */
  save(_data: any) {
    return new Promise((resolve, reject) => {
      const userId = _data.userId;
      let remittance = _data.remittance;
      this.openConnection();
      User.findOne({
        where: { id: userId }
      }).then(response => {
        Campus.findOne({
          where: { campusOwnerId: response.campusOwnerId },
          attributes: ['id']
        }).then(_campus => {
          super.findByCondition(['id'], { id: _campus.id }).then(
            res => {
              if (res) {
                return resolve(super.update(res.id, remittance));
              } else {
                remittance['id'] = _campus.id;
                return resolve(this.sequelizeModel.create(remittance));
              }
            },
            error => {
              return reject(error);
            }
          );
        });
      });
    });
  }

  view(_userId: number) {
    return new Promise((resolve, reject) => {
      const userId = _userId;
      this.openConnection();
      User.findOne({
        where: { id: userId }
      }).then(response => {
        Campus.findOne({
          where: { campusOwnerId: response.campusOwnerId },
          attributes: ['id']
        }).then(_campus => {
          super.findByCondition(null, { id: _campus.id }).then(
            res => {
              return resolve(res);
            },
            error => {
              return reject(error);
            }
          );
        });
      });
    });
  }
}
