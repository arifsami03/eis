import * as nodemailer from 'nodemailer';
import { MetaConf } from './../../configuration';
import { Model } from 'sequelize-typescript';
import { BaseModel } from '../../base';
import { Campus, CampusOwner, CampusOwnerModel } from '../';
import { User } from '../../security';
import { Promise, reject } from 'bluebird';

export class CampusModel extends BaseModel {
  constructor() {
    super(Campus);
  }

  /**
   * create a new record for pre regigitration
   * @param item
   */
  public preRegistration(item) {
    let _campus = item.campus;
    let _campusOwner = item.campusOwner;
    let _user = item.user;

    this.openConnection();

    return this.sequelizeModel.sequelize.transaction(t => {
      return CampusOwner.create(_campusOwner, {
        transaction: t
      }).then(_cmpOwn => {
        _campus['campusOwnerId'] = _cmpOwn['id'];
        _campus['status'] = 'new';
        return Campus.create(_campus, {
          validate: true,
          transaction: t
        }).then(() => {
          _user['campusOwnerId'] = _cmpOwn['id'];
          return User.create(_user, {
            validate: true,
            transaction: t
          });
        });
      });
    });
  }

  /**
   * preRegistrationDetail finding the list of pre registration form
   */
  public preRegistrationList(_status) {
    this.openConnection();
    return this.sequelizeModel.findAll({
      where: { status: _status },
      include: [
        {
          model: MetaConf,
          as: 'city',
          attributes: ['id', 'metaValue']
        },
        {
          model: CampusOwner,
          as: 'campusOwner',
          attributes: ['id', 'fullName', 'email', 'cityId', 'applicationType']
        }
      ]
    });
  }

  /**
   * preRegistrationDetail finding the one record of pre regitsrtaion form on basis of id
   * @param _id
   */
  public preRegistrationDetailOne(_id: number) {
    this.openConnection();
    return this.sequelizeModel.findOne({
      where: { id: _id },
      include: [
        {
          model: MetaConf,
          as: 'city',
          attributes: ['id', 'metaValue']
        },
        {
          model: CampusOwner,
          as: 'campusOwner'
        }
      ]
    });
  }
  /**
   * changeStatus will change the status of pre registration form
   * @param id
   * @param item
   */
  public changeStatus(id: number, item) {
    this.openConnection();
    return super.update(id, item).then(_campus => {
      //TODO:high: remove hardcoded statuses
      //Finding campusOwner from
      return this.sequelizeModel
        .findOne({
          where: { id: id },
          include: [
            {
              model: CampusOwner,
              as: 'campusOwner'
            }
          ]
        })
        .then(_cmp => {
          // Change the status of user isActive for login access
          if (item.status == 'approve') {
            return User.update(
              { isActive: true },
              { where: { campusOwnerId: _cmp.campusOwnerId } }
            ).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.campusOwner.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>Congratulations!</h1>
                  <p>Your campus pre-registration application is approved.Here are your credential for login.</p>
                  <p>Username : ${_cmp.campusOwner.email}</p>
                  <p>Password : pass2word</p>
                  <a href='http://localhost:4200'>
                  Click Here to Login 
                  </a>`
              );
            });
          } else if (item.status == 'reject') {
            return User.update(
              { isActive: false },
              { where: { campusOwnerId: _cmp.campusOwnerId } }
            ).then(_user => {
              return this.sendEmail(
                'developer.muhammadumair@gmail.com',
                'Programming4Fun',
                _cmp.campusOwner.email,
                'Approval for Campus Pre-Registration',
                '',
                `<h1>News!</h1>
                  <p> Your campus pre-registration application is rejected.</p>
                  `
              );
            });
          }
        });
    });
  }

  /**
   *
   * @param _from sender email address
   * @param _fromPassword sender password
   * @param _to receiver email adresses
   * @param _subject subject of email
   * @param _text text of email
   * @param _html html in email
   */
  private sendEmail(
    _from: string,
    _fromPassword: string,
    _to: string,
    _subject: string,
    _text: string,
    _html: string
  ) {
    return new Promise((resolve, reject) => {
      nodemailer.createTestAccount((err, account) => {
        let transporter = nodemailer.createTransport({
          from: 'noreply@gmail.com',
          host: 'smtp.gmail.com', // hostname
          secureConnection: false, // use SSL
          // port: 465, // port for secure SMTP
          transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
          auth: {
            user: 'developer.muhammadumair@gmail.com',
            pass: 'Programming4Fun'
          }
        });
        let mailOptions = {
          from: _from, // sender address
          to: _to, // list of receivers
          subject: _subject, // Subject line
          text: _text, // plain text body
          html: _html
        };

        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            reject({ error: 'Something went wrong.', detail: error });
          } else {
            resolve({ success: 'Sent', detail: info });
          }
        });
      });
    });
  }

  /**
   * validateUniqueEmail
   * @param item
   */
  validateUniqueEmail(item: any) {
    //TODO:low can check also in the user but I am running transaction on adding pre reg form so i think there is no posibility to add duplicate entry
    this.openConnection();
    let response = [];
    return CampusOwner.findOne({
      where: {
        email: item.email
      }
    });
  }

  /**
   *
   * @param _id
   */
  submitPostRegistration(_id: number) {
    this.openConnection();
    // finding campusOwner and Campus  from User
    return User.findOne({
      where: { id: _id },
      include: [
        {
          model: CampusOwner,
          as: 'campusOwner',
          include: [
            {
              model: Campus,
              as: 'campus'
            }
          ]
        }
      ]
    }).then(_user => {
      // Updating status in campus
      //TODO:high remove hard coded statuses
      return this.update(_user.campusOwner.campus.id, {
        status: 'submit'
      }).then(_res => {
        // Finding admin
        return User.findOne({
          where: { isSuperUser: true }
        }).then(res => {
          return this.sendEmail(
            'developer.muhammadumair@gmail.com',
            'Programming4Fun',
            res.username,
            'Post Registration submission',
            '',
            `<h1>Post Registration Submitted by Applicant</h1>
              <p>Applicant has sucessfully submitted all the inforamtion reqwuired for campus registration.<br> You can check it in submitted list of applicant </p>
              <h2> Applicant Information </h2>
              <p><label>Name : </label> ${_user.campusOwner.fullName}</p>
              <p><label>Email : </label> ${_user.campusOwner.email}</p>
              <p><label> Mobile Number  : </label> ${
                _user.campusOwner.mobileNumber
              }</p>
              `
          );
        });
      });
    });
  }

  /**
   *
   * @param _id
   */
  getRegistrationStatus(_id: number) {
    this.openConnection();
    // finding campusOwner and Campus  from User
    return User.findOne({
      where: { id: _id },
      include: [
        {
          model: CampusOwner,
          as: 'campusOwner',
          include: [
            {
              model: Campus,
              as: 'campus'
            }
          ]
        }
      ]
    }).then(_user => {
      return _user.campusOwner.campus.status;
    });
  }
}
