/**
 *  Models
 */
export * from './models/campus.model';
export * from './models/campus-owner.model';
export * from './models/campus-owner-reference.model';
export * from './models/campus-info.model';
export * from './models/remittance.model';
export * from './models/campus-address.model';
export * from './models/campus-faculty.model';
export * from './models/campus-address.model';
export * from './models/campus-available-building.model';
export * from './models/cf-program-detail.model';

/**
 * Schema Models
 */
export * from './models/schema/campus-available-building';
export * from './models/schema/campus-owner-reference';
export * from './models/schema/campus-owner';
export * from './models/schema/campus-targetted-loacation';
export * from './models/schema/campus';
export * from './models/schema/campus-affiliation';
export * from './models/campus-affiliation.model';
export * from './models/schema/campus-address';
export * from './models/schema/cf-program-detail';

export * from './models/schema/campus-faculty';


export * from './models/schema/remittance';

/**
 * Controllers
 */
export * from './controllers/campus-registration.controller';
export * from './controllers/campus-owner.controller';
export * from './controllers/campus-info.controller';
export * from './controllers/remittances.controller';

/**
 * Routes
 */

export * from './routes/base/campus.base.route';
export * from './routes/campus.route';
export * from './routes/campus-owner.route';
export * from './routes/campus-info.route';
export * from './routes/remittances.route';
