import { Request, Response, Router } from 'express';

import { CampusRegistrationController } from '../';

/**
 * / route
 *
 * @class CampusRoute
 */
export class CampusRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusRegistrationController();

    this.router
      .route('/campus/preRegistration')
      .post(controller.preRegistration);
    this.router
      .route('/campus/preRegistration/list/:status')
      .get(controller.preRegistrationList);
    this.router
      .route('/campus/preRegistration/detail/:id')
      .get(controller.preRegistrationDetailOne);
    this.router
      .route('/campus/preRegistration/status/:id')
      .put(controller.changeStatus);

    this.router
      .route('/campus/preRegistration/validateUniqueEmail')
      .post(controller.validateUniqueEmail);

    this.router
      .route('/campus/postRegistration/submitPostRegistration/:id')
      .get(controller.submitPostRegistration);

    this.router
      .route('/campus/postRegistration/getRegistrationStatus/:id')
      .get(controller.getRegistrationStatus);
  }
}
