import { Request, Response, Router } from 'express';

import { CampusOwnerController } from '../';

/**
 * / route
 *
 * @class CampusRoute
 */
export class CampusOwnerRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class CampusOwnerRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class CampusOwnerRoute
   * @method create
   *
   */
  public create() {
    let controller = new CampusOwnerController();

    this.router
      .route('/campusOwner/registration/findPersonalInfo/:userId')
      .get(controller.findPersonalInfo);

      this.router
      .route('/campusOwner/registration/update/:userId')
      .put(controller.update);


  }
}
