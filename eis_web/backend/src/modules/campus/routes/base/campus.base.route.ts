import { NextFunction, Request, Response, Router } from 'express';
import {
  CampusRoute,
  CampusOwnerRoute,
  CampusInfoRoute,
  RemittancesRoute
} from '../../index';

/**
 *
 *
 * @class ConfigurationBaseRoute
 */
export class CampusBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new CampusRoute(this.router);
    new CampusOwnerRoute(this.router);
    new CampusInfoRoute(this.router);
    new RemittancesRoute(this.router);
  }
}
