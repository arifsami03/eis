import { Request, Response, Router } from 'express';

import { RemittancesController } from '../';

/**
 * / route
 *
 * @class CampusRoute
 */
export class RemittancesRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class RemittancesRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class RemittancesRoute
   * @method create
   *
   */
  public create() {
    let controller = new RemittancesController();
    this.router.route('/campus/remittances/save').post(controller.save);
    this.router.route('/campus/remittances/view/:id').get(controller.view);
  }
}
