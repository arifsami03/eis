import { NextFunction, Request, Response, Router } from 'express';

import {
  MetaConfRoute, BanksRoute, OutGoingMailServersRoute, EmailTemplatesRoute, WorkFlowsRoute, WFStateRoute,
  WFStateFlowRoute, EvaluationSheetsRoute, ESSectionsRoute
} from '../../index';


/**
 *
 *
 * @class ConfigurationBaseRoute
 */
export class ConfigurationBaseRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class BaseRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.initAll();
  }

  /**
   * init all routes
   */
  public initAll() {
    new MetaConfRoute(this.router);
    new BanksRoute(this.router);
    new OutGoingMailServersRoute(this.router);
    new EmailTemplatesRoute(this.router);
    new WorkFlowsRoute(this.router);
    new WFStateRoute(this.router);
    new WFStateFlowRoute(this.router);
    new EvaluationSheetsRoute(this.router);
    new ESSectionsRoute(this.router);
  }
}
