import { Router, Request, Response } from 'express';
import { WFStatesController } from '../index';

export class WFStateRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WFStatesController();

    this.router.route('/configuration/wfStates/index').get(controller.index);
    this.router.route('/configuration/wfStates/list/:id').get(controller.find);
    this.router.route('/configuration/wfStates/findById/:id').get(controller.findById);
    
    this.router.route('/configuration/wfStates/checkRecordExists/:id').put(controller.checkRecordExists);
    this.router.route('/configuration/wfStates/create').post(controller.create);
    this.router.route('/configuration/wfStates/update/:id').put(controller.update);
   this.router.route('/configuration/wfStates/delete/:id').delete(controller.delete);
  }
}
