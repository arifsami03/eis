import { Router, Request, Response } from 'express';
import { WFStateFlowsController } from '../index';

export class WFStateFlowRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WFStateFlowsController();

    this.router.route('/configuration/wfStateFlows/index').get(controller.index);
    this.router.route('/configuration/wfStateFlows/list/:id').get(controller.find);
    this.router.route('/configuration/wfStateFlows/findById/:id').get(controller.findById);
    this.router.route('/configuration/wfStateFlows/getFlowByWFId/:id').get(controller.getFlowByWFId);
    
    this.router.route('/configuration/wfStateFlows/create').post(controller.create);
    this.router.route('/configuration/wfStateFlows/update/:id').put(controller.update);
    this.router.route('/configuration/wfStateFlows/delete/:id').delete(controller.delete);
  }
}
