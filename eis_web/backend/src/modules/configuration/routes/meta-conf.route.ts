import { Request, Response, Router } from 'express';

import { MetaConfController } from '../';

/**
 * / route
 *
 * @class MetaConfRoute
 */
export class MetaConfRoute {
  router: Router;

  /**
   * Constructor
   *
   * @class MetaConfRoute
   * @constructor
   */
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  /**
   * Create the routes.
   *
   * @class MetaConfRoute
   * @method create
   *
   */
  public create() {
    let controller = new MetaConfController();

    this.router
      .route('/configuration/metaconf/index/:metaKey')
      .get(controller.index);

    this.router
      .route('/configuration/metaconf/city')
      .get(controller.findCities);
    this.router.route('/configuration/metaconf/bank').get(controller.findBanks);

    this.router.route('/configuration/metaconf/view/:id').get(controller.find);

    this.router
      .route('/configuration/metaconf/update/:id')
      .put(controller.update);

    this.router.route('/configuration/metaconf/create').post(controller.create);

    this.router
      .route('/configuration/metaconf/delete/:id')
      .delete(controller.delete);
  }
}
