import { Router } from 'express';
import { ESSectionsController } from '../index';

export class ESSectionsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new ESSectionsController();

    this.router.route('/configuration/esSection/index').get(controller.index);
    this.router.route('/configuration/esSection/find/:id').get(controller.find);
    this.router.route('/configuration/esSection/create').post(controller.create);
    this.router.route('/configuration/esSection/update/:id').put(controller.update);
    this.router.route('/configuration/esSection/delete/:id').delete(controller.delete);
    this.router.route('/configuration/esSection/findAttributesList').get(controller.findAttributesList);
    this.router.route('/configuration/esSection/findAllESSections/:ESId').get(controller.findAllESSections);
  }
}
