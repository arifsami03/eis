import { Router } from 'express';
import { EvaluationSheetsController } from '../index';

export class EvaluationSheetsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EvaluationSheetsController();

    this.router.route('/configuration/evaluationSheets/index').get(controller.index);
    this.router.route('/configuration/evaluationSheets/find/:id').get(controller.find);
    this.router.route('/configuration/evaluationSheets/create').post(controller.create);
    this.router.route('/configuration/evaluationSheets/update/:id').put(controller.update);
    this.router.route('/configuration/evaluationSheets/delete/:id').delete(controller.delete);
    this.router.route('/configuration/evaluationSheets/findAttributesList').get(controller.findAttributesList);
  }
}
