import { Router } from 'express';
import { BanksController } from '../index';

export class BanksRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new BanksController();

    this.router.route('/configuration/banks/index').get(controller.index);
    this.router.route('/configuration/banks/find/:id').get(controller.find);
    this.router.route('/configuration/banks/create').post(controller.create);
    this.router.route('/configuration/banks/update/:id').put(controller.update);
    this.router.route('/configuration/banks/delete/:id').delete(controller.delete);
    this.router.route('/configuration/banks/findAttributesList').get(controller.findAttributesList);
  }
}
