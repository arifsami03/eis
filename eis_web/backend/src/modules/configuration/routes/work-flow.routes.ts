import { Router, Request, Response } from 'express';
import { WorkFlowsController } from '../index';

export class WorkFlowsRoute {
  router: Router;
  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new WorkFlowsController();

    this.router.route('/configuration/workFlows/index').get(controller.index);
    this.router.route('/configuration/workFlows/find/:id').get(controller.find);
    this.router.route('/configuration/workFlows/checkRecordExists/:id').get(controller.checkRecordExists);
    
    this.router.route('/configuration/workFlows/create').post(controller.create);
    this.router.route('/configuration/workFlows/update/:id').put(controller.update);
    this.router.route('/configuration/workFlows/delete/:id').delete(controller.delete);
  }
}
