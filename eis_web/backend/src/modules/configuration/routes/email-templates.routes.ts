import { Router } from 'express';

import { EmailTemplatesController } from '../index';

export class EmailTemplatesRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new EmailTemplatesController();

    this.router
      .route('/configuration/emailTemplates/index')
      .get(controller.index);
    this.router
      .route('/configuration/emailTemplates/find/:id')
      .get(controller.find);
    this.router
      .route('/configuration/emailTemplates/create')
      .post(controller.create);
    this.router
      .route('/configuration/emailTemplates/update/:id')
      .put(controller.update);
    this.router
      .route('/configuration/emailTemplates/delete/:id')
      .delete(controller.delete);
  }
}
