import { Router } from 'express';

import { OutGoingMailServersController } from '../index';

export class OutGoingMailServersRoute {
  router: Router;

  constructor(router: Router) {
    this.router = router;
    this.create();
  }

  public create() {
    let controller = new OutGoingMailServersController();

    this.router.route('/configuration/outGoingMailServers/index').get(controller.index);
    this.router.route('/configuration/outGoingMailServers/find/:id').get(controller.find);
    this.router.route('/configuration/outGoingMailServers/create').post(controller.create);
    this.router.route('/configuration/outGoingMailServers/update/:id').put(controller.update);
    this.router.route('/configuration/outGoingMailServers/delete/:id').delete(controller.delete);
    this.router.route('/configuration/outGoingMailServers/findAttributesList').get(controller.findAttributesList);
  }
}
