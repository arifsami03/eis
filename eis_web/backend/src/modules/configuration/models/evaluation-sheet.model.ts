import { BaseModel } from '../../base';
import { EvaluationSheet, ESSectionModel } from '../';

export class EvaluationSheetModel extends BaseModel {
  constructor() {
    super(EvaluationSheet);
  }

}
