import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class MetaConf extends Model<MetaConf> {
  
  @Column metaKey: string;

  @Column metaValue: string;

  @Column createdBy: number;

  @Column updatedBy: number;

}
