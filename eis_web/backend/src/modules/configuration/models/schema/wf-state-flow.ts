import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { WorkFlow,WFState } from '../..';

@Table({ timestamps: true })
export class WFStateFlow extends Model<WFStateFlow> {
  @Column description: string;
  @ForeignKey(() => WorkFlow)
  @Column
  WFId: string;

  @ForeignKey(() => WFState)
  @Column
  fromId: number;

  @ForeignKey(() => WFState)
  @Column
  toId: number;
  @Column createdBy: number;
  @Column updatedBy: number;
}
