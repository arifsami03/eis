import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class Bank extends Model<Bank> {
  @Column name: string;
  @Column abbreviation: string;
  @Column createdBy: number;
  @Column updatedBy: number;
}
