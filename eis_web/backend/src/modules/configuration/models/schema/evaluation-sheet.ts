import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class EvaluationSheet extends Model<EvaluationSheet> {
  @Column title: string;
  @Column createdBy: number;
  @Column updatedBy: number;
}
