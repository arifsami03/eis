import { Table, Column, Model, HasMany } from 'sequelize-typescript';
import { EmailTemplate } from '../..';

@Table({ timestamps: true })
export class OutGoingMailServer extends Model<OutGoingMailServer> {
  @Column name: string;
  @Column description: string;
  @Column isDefault: boolean;
  @Column smtpServer: string;
  @Column smtpPort: number;
  @Column connectionSecurity: string;
  @Column ownerId: number;
  @Column username: string;
  @Column password: string;
  @Column createdBy: number;
  @Column updatedBy: number;

  @HasMany(() => EmailTemplate, { foreignKey: 'outGoingMailServerId' })
  outGoingMailServers: EmailTemplate[];
}
