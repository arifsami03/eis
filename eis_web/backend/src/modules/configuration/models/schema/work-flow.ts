import { Table, Column, Model } from 'sequelize-typescript';

@Table({ timestamps: true })
export class WorkFlow extends Model<WorkFlow> {
  @Column WFId: string;
  // @Column id: {
  //   type: string,
  //   primaryKey: true
  // }
  @Column title: string;
  @Column description: string;

  @Column createdBy: number;
  @Column updatedBy: number;
}
