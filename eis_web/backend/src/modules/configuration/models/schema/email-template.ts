import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { OutGoingMailServer } from '../..';

@Table({ timestamps: true })
export class EmailTemplate extends Model<EmailTemplate> {
  @Column name: string;
  @Column subject: string;
  @Column emailBody: boolean;
  @ForeignKey(() => OutGoingMailServer)
  @Column
  outGoingMailServerId: number;
  @Column createdBy: number;
  @Column updatedBy: number;
}
