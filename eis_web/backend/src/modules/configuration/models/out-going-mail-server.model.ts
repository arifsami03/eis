import { BaseModel } from '../../base';
import { OutGoingMailServer } from '../';

export class OutGoingMailServerModel extends BaseModel {
  constructor() {
    super(OutGoingMailServer);
  }
}
