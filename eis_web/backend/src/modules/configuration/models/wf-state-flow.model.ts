import { BaseModel } from '../../base';
import { WFStateFlow } from '../';
import { WFStateModel } from '../'
import { Promise } from 'bluebird';

export class WFStateFlowModel extends BaseModel {
  constructor() {
    super(WFStateFlow);
  }

  findWFStateFlow(id: string) {
    
    return this.findAllByConditions(['id', 'fromId', 'toId', 'description'], { WFId: id }).then(flows => {
      var myflows = [];
      return Promise.each(flows, (flowItem) => {
        
        return new WFStateModel().findByCondition(['state'], { id: flowItem['fromId'] }).then(fromFlow => {
          return new WFStateModel().findByCondition(['state'], { id: flowItem['toId'] }).then(result => {
            myflows.push({
              id:flowItem['id'],
              fromId: flowItem['fromId'],
              toId: flowItem['toId'],
              from: fromFlow['state'],
              to: result['state'],
              description: flowItem['description'],

            });
          })

        })

      }).then(() => {

        return myflows;
      });
    })
  }
  // checkRecordExists(id:string){
  //   return this.findAllByConditions(['id','title','state','description'],{state:id});
  // }

  findById(id:string){
    return this.findByCondition(['id','fromId','toId','description'],{id:id});
  }

  getFlowByWFId(id:string){
    return this.findAllByConditions(['id', 'fromId', 'toId', 'description'], { WFId: id }).then(flows => {
      var myflows = [];
      return Promise.each(flows, (flowItem) => {
        
        return new WFStateModel().findByCondition(['state'], { id: flowItem['fromId'] }).then(fromFlow => {
          return new WFStateModel().findByCondition(['state'], { id: flowItem['toId'] }).then(result => {
            myflows.push({
              id:flowItem['id'],
              fromId: flowItem['fromId'],
              toId: flowItem['toId'],
              from: fromFlow['state'],
              to: result['state'],
              description: flowItem['description'],

            });
          })

        })

      }).then(() => {

        return myflows;
      });
    })
  }


}
