import { BaseModel } from '../../base';
import { WorkFlow, WFStateFlowModel, WFStateModel } from '../';

export class WorkFlowModel extends BaseModel {
  constructor() {
    super(WorkFlow);
  }

  findWorkFlow(id: string) {
    return this.findAllByConditions(['WFId', 'title', 'description'], { WFId: id })
  }

  delete(id: string) {
    return new WFStateFlowModel().deleteByConditions({ WFId: id }).then(() => {
      return new WFStateModel().deleteByConditions({ WFId: id }).then(() => {
        return super.deleteByConditions({WFId:id});
      })

    })
  }


}
