import { BaseModel } from '../../base';
import { WFState,WFStateFlowModel } from '../';
import { Sequelize } from 'sequelize-typescript/lib/models/Sequelize';


export class WFStateModel extends BaseModel {
  constructor() {
    super(WFState);
  }

  findWFState(id:string){
    return this.findAllByConditions(['id','title','state','description'],{WFId:id});
  }
  checkRecordExists(id:string,WFId:string){
    return this.findAllByConditions(['id','title','state','description'],{state:id,WFId:WFId});
  }

  findById(id:string){
    return this.findByCondition(['id','title','state','description'],{id:id});
  }

  delete(id){
    return new WFStateFlowModel().deleteByConditions({[Sequelize.Op.or]: [{fromId: id}, {toId: id}] }).then(()=>{
      return super.delete(id);
    })


  }

}
