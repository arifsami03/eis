import { BaseModel } from '../../base';
import { MetaConf } from '../';

export class MetaConfModel extends BaseModel {
  constructor() {
    super(MetaConf);
  }
}
