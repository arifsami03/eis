import { BaseModel } from '../../base';
import { EmailTemplate } from '../';

export class EmailTemplateModel extends BaseModel {
  constructor() {
    super(EmailTemplate);
  }
}
