import { BaseModel } from '../../base';
import { ESSection } from '../';

export class ESSectionModel extends BaseModel {
  constructor() {
    super(ESSection);
  }

  findAllESSections(ESId: number) {
    return this.findAllByConditions(['id', 'title'], { ESId: ESId });
  }
}
