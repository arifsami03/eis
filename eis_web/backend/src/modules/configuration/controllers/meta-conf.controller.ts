import * as express from 'express';
import { Request, Response } from 'express';
import { ErrorHandler } from '../../base/conf/error-handler';

import { MetaConfModel } from '../';

export class MetaConfController {
  constructor() {}

  /**
   * Index
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  index(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let metaKey = req.params.metaKey;

    // new MetaConfModel().findAll(['id', 'metaKey', 'metaValue']).then(result => {
    new MetaConfModel()
      .findAllByConditions(['id', 'metaKey', 'metaValue'], { metaKey: metaKey })
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }

  /**
   * Find One
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  find(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;

    new MetaConfModel()
      .find(id)
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }

  /**
   * findCities
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findCities(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new MetaConfModel()
      .findAll(['id', 'metaValue'], { metaKey: 'city' })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }

  /**
   * getBanks
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  findBanks(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new MetaConfModel()
      .findAll(['id', 'metaValue'], { metaKey: 'bank' })
      .then(result => {
        if (result) {
          res.json(result);
        } else {
          res.status(ErrorHandler.recordNotFound.status).send(ErrorHandler.recordNotFound);
        }
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }

  /**
   * Update
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  update(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    let id = req.params.id;
    let item = req.body;

    new MetaConfModel()
      .update(id, item)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }

  /**
   * Create
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  create(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new MetaConfModel()
      .create(req.body)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: 'User did not added' });
      });
  }

  /**
   * Delete
   *
   * @param req express.Request
   * @param res express.Response
   * @param next express.NextFunction
   */
  delete(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
  ) {
    new MetaConfModel()
      .delete(req.params.id)
      .then(result => {
        res.json(result);
      })
      .catch(_error => {
        next(_error);
        // res.send({ error: true, message: _error });
      });
  }
}
