'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ProgramDetail', null, {}).then(() => {
      return queryInterface.bulkDelete('Program', null, {}).then(() => {
        return queryInterface.bulkDelete('EducationLevel', null, {}).then(() => {
          return queryInterface.addColumn('EducationLevel', 'instituteTypeId', {
            type: Sequelize.INTEGER, allowNull: false
          }).then(() => {
            return queryInterface.addConstraint('EducationLevel', ['instituteTypeId'], {
              type: 'foreign key',
              name: 'FK_EducationLevel_InstituteType',
              references: {
                table: 'InstituteType',
                field: 'id'
              }
            });
          });
        });
      });
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('EducationLevel', 'FK_EducationLevel_InstituteType').then(() => {
        return queryInterface.removeColumn('EducationLevel', 'instituteTypeId');
      });
  }
};
