'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .addColumn('Campus', 'applicationType', {
      type: Sequelize.STRING
    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'applicationType')
    
   
  }
};
