'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .removeConstraint(
      'Program',
      'FK_Program_Faculty'
    );
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('Program', ['facultyId'], {
      type: 'foreign key',
      name: 'FK_Program_Faculty',
      references: {
        table: 'Faculty',
        field: 'id'
      }
    });
  }
};
