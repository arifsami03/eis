'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('LicenseFee', 'FK_LicenseFee_EducationLevel').then(() => {
        return queryInterface.addConstraint('LicenseFee', ['educationLevelId'], {
          type: 'foreign key',
          name: 'FK_LicenseFee_EducationLevel',
          references: {
            table: 'EducationLevel',
            field: 'id'
          },
          onDelete: 'cascade',
          onUpdate: 'cascade'
        })
      }).then(() => {
        return queryInterface
          .removeConstraint('EducationLevel', 'FK_EducationLevel_InstituteType').then(() => {
            return queryInterface.addConstraint('EducationLevel', ['instituteTypeId'], {
              type: 'foreign key',
              name: 'FK_EducationLevel_InstituteType',
              references: {
                table: 'InstituteType',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            })
          })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('LicenseFee', 'FK_LicenseFee_EducationLevel').then(() => {
        return queryInterface.addConstraint('LicenseFee', ['educationLevelId'], {
          type: 'foreign key',
          name: 'FK_LicenseFee_EducationLevel',
          references: {
            table: 'EducationLevel',
            field: 'id'
          }
        })
      }).then(() => {
        return queryInterface
          .removeConstraint('EducationLevel', 'FK_EducationLevel_InstituteType').then(() => {
            return queryInterface.addConstraint('EducationLevel', ['instituteTypeId'], {
              type: 'foreign key',
              name: 'FK_EducationLevel_InstituteType',
              references: {
                table: 'InstituteType',
                field: 'id'
              }
            })
          })
      });
  }
};