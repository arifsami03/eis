'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .addColumn('campusAddress', 'buildingAvailableType', {
      type: Sequelize.BOOLEAN
    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('campusAddress', 'buildingAvailableType')
    
   
  }
};
