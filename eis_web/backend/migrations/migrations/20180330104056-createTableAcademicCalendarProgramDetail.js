'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('AcademicCalendarProgramDetail', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        academicCalendarId: {
          type: Sequelize.INTEGER
        },
        programDetailId: {
          type: Sequelize.INTEGER
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('AcademicCalendarProgramDetail', ['academicCalendarId'], {
            type: 'foreign key',
            name: 'FK_AcademicCalendarProgramDetail_AcademicCalendar',
            references: {
              table: 'AcademicCalendar',
              field: 'id'
            }
          })
          .then(() => {
            return queryInterface.addConstraint('AcademicCalendarProgramDetail', ['programDetailId'], {
              type: 'foreign key',
              name: 'FK_AcademicCalendarProgramDetail_ProgramDetail',
              references: {
                table: 'ProgramDetail',
                field: 'id'
              }
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AcademicCalendarProgramDetail');
  }
};
