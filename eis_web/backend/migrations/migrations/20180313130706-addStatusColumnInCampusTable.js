'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Campus', 'status', {
      type: Sequelize.STRING(20)
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'status');
  }
};
