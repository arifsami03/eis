'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('ESQuestionOption', {
      id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
      ESQeustionId: { type: Sequelize.INTEGER, allowNull: false },
      optionString: { type: Sequelize.STRING, allowNull: false },
      createdAt: { type: Sequelize.DATE, allowNull: false },
      updatedAt: { type: Sequelize.DATE, allowNull: false },
      createdBy: { type: Sequelize.INTEGER },
      updatedBy: { type: Sequelize.INTEGER }
    }).then(() => {
      return queryInterface.addConstraint('ESQuestionOption', ['ESQeustionId'], {
        type: 'foreign key', name: 'FK_ESQuestionOption_ESQeustion',
        references: { table: 'ESQuestion', field: 'id' }
      })
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint('ESQuestionOption', 'FK_ESQuestionOption_ESQeustion').then(() => {
      return queryInterface.dropTable('ESQuestionOption');
    });
  }
};