'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CampusOwner', ['email'], {
      type: 'unique',
      name: 'Unique_CampusOwner_Email'
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeConstraint(
      'CampusOwner',
      'Unique_CampusOwner_Email'
    );
  }
};
