'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('OutgoingMailServer', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        name: {
          type: Sequelize.STRING(100),
          allowNull: false,
          defaultValue: false
        },
        description: { type: Sequelize.TEXT, allowNull: true },
        isDefault: { type: Sequelize.BOOLEAN, allowNull: true },
        smtpServer: { type: Sequelize.STRING(100), allowNull: false },
        smtpPort: { type: Sequelize.INTEGER(10), allowNull: false },
        connectionSecurity: { type: Sequelize.STRING(100), allowNull: false },
        ownerId: { type: Sequelize.INTEGER, allowNull: true },
        username: { type: Sequelize.STRING(100), allowNull: false },
        password: { type: Sequelize.STRING, allowNull: false },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('OutgoingMailServer', ['ownerId'], {
          type: 'foreign key',
          name: 'FK_OutgoingMailServer_User',
          references: {
            table: 'User',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('OutgoingMailServer', 'FK_OutgoingMailServer_User')
      .then(() => {
        return queryInterface.dropTable('OutgoingMailServer');
      });
  }
};
