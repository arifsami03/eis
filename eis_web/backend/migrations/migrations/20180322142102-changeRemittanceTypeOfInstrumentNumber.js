'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Remittance', 'instrumentNumber', {
      type: Sequelize.STRING
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Remittance', 'instrumentNumber', {
      type: Sequelize.INTEGER
    });
  }
};
