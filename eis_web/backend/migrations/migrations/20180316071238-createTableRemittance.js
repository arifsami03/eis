'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Remittance', {
        id: { type: Sequelize.INTEGER, primaryKey: true },
        instrumentCategory: {
          type: Sequelize.STRING
        },
        currentDate: {
          type: Sequelize.DATE
        },
        bankId: {
          type: Sequelize.INTEGER
        },
        branchName: {
          type: Sequelize.STRING
        },
        instrumentDate: {
          type: Sequelize.DATE
        },
        instrumentNumber: {
          type: Sequelize.INTEGER
        },
        amount: {
          type: Sequelize.FLOAT
        },
        purpose: {
          type: Sequelize.STRING
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('Remittance', ['id'], {
          type: 'foreign key',
          name: 'FK_Remittance_Campus',
          references: {
            table: 'Campus',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Remittance', 'FK_Remittance_Campus')
      .then(() => {
        return queryInterface.dropTable('Remittance');
      });
  }
};
