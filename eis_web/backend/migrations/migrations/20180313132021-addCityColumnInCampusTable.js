'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Campus', 'cityId', {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: false
      })
      .then(() => {
        return queryInterface.addConstraint('Campus', ['cityId'], {
          type: 'foreign key',
          name: 'FK_Campus_MetaConf',
          references: {
            table: 'MetaConf',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Campus', 'FK_Campus_MetaConf')
      .then(() => {
        return queryInterface.removeColumn('Campus', 'cityId');
      });
  }
};
