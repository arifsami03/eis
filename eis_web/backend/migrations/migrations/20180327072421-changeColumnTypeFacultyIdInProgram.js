'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Program','facultyId',{
      type: Sequelize.INTEGER,
      allowNull: true,
    })
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('Program','facultyId',{
      type: Sequelize.INTEGER,
      allowNull: false
    })
  }
};
