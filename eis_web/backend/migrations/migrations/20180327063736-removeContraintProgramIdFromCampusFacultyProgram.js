'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
    .removeConstraint(
      'CampFacultyProgram',
      'FK_CampFacultyProgram_Faculty'
    );
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addConstraint('CampFacultyProgram', ['programId'], {
      type: 'foreign key',
      name: 'FK_CampFacultyProgram_Faculty',
      references: {
        table: 'Program',
        field: 'id'
      }
    });
  }
};
