'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CampusOwnerReference', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        campusOwnerId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        fullName: {
          type: Sequelize.STRING(55),
          allowNull: false,
          defaultValue: false
        },
        mobileNumber: {
          type: Sequelize.STRING(20),
          allowNull: false,
          defaultValue: false
        },
        address: {
          type: Sequelize.STRING,
          allowNull: true
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'CampusOwnerReference',
          ['campusOwnerId'],
          {
            type: 'foreign key',
            name: 'FK_CampusOwnerReference_CampusOwner',
            references: {
              table: 'CampusOwner',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint(
        'CampusOwnerReference',
        'FK_CampusOwnerReference_CampusOwner'
      )
      .then(() => {
        return queryInterface.dropTable('CampusOwnerReference');
      });
  }
};
