'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Institute', [
      { 
        name: 'Education Information System',
        logo: '',
        abbreviation: 'EIS',
        website: '', 
        officeEmail: '', 
        headOfficeLocation: '', 
        latitude: '', 
        longitude: '', 
        establishedScince:'', 
        contactNo: '', 
        affiliation: '', 
        focalPerson: '',
        Description: '', 
        createdAt: new Date(), 
        updatedAt: new Date() }
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Institute', null, {});
  }
};
