'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Campus', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        campusOwnerId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        campusName: {
          type: Sequelize.STRING(55),
          allowNull: false,
          defaultValue: false
        },
        levelOfEducation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        tentativeSessionStart: {
          type: Sequelize.STRING,
          allowNull: true
        },
        campusLocation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        buildingAvailable: {
          type: Sequelize.BOOLEAN, // BOOLEAN VALUE
          allowNull: true
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('Campus', ['campusOwnerId'], {
          type: 'foreign key',
          name: 'FK_Campus_CampusOwner',
          references: {
            table: 'CampusOwner',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Campus', 'FK_Campus_CampusOwner')
      .then(() => {
        return queryInterface.dropTable('Campus');
      });
  }
};
