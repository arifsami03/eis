'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CampusAvailableBuilding', 'address').then(() => {
      return queryInterface.removeColumn('CampusAvailableBuilding', 'latitude').then(() => {
        return queryInterface.removeColumn('CampusAvailableBuilding', 'longitude');
      });
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CampusAvailableBuilding', 'address', {
        type: Sequelize.STRING
      })
      .then(() => {
        return queryInterface.addColumn('CampusAvailableBuilding', 'latitude', {
          type: Sequelize.STRING
        });
      }).then(() => {
        return queryInterface.addColumn('CampusAvailableBuilding', 'longitude', {
          type: Sequelize.STRING
        });
      });
  }
};