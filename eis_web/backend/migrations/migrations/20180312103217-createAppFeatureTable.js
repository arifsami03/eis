'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('AppFeature', {
        id: { type: Sequelize.STRING, allowNull: false },
        parentId: { type: Sequelize.STRING },
        title: {
          type: Sequelize.STRING,
          allowNull: true
        },
        isVisible: {
          type: Sequelize.BOOLEAN,
          allowNull: true,
          defaultValue: false
        },
        weight: { type: Sequelize.INTEGER, allowNull: true },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('AppFeature', ['id', 'parentId'], {
          type: 'unique',
          name: 'Unique_AppFeature_Keys'
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('AppFeature');
  }
};
