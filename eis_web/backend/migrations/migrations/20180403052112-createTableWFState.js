'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('WFState', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        state: {
          type: Sequelize.STRING,
          allowNull: false          
        },
        title: {
          type: Sequelize.STRING
        },
        description: {
          type: Sequelize.TEXT
        },
        WFId: {
          type: Sequelize.STRING,
          allowNull: false,
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('WFState', ['WFId'], {
          type: 'foreign key',
          name: 'FK_WFState_WorkFlow',
          references: {
            table: 'WorkFlow',
            field: 'WFId'
          }
        })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('WFState', 'FK_WFState_WorkFlow').then(() => {
        return queryInterface.dropTable('WFState');
      });

  }
};