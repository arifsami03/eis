'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CampFaculty', {
        id: {
          type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
        },
        campusId: {
          type: Sequelize.INTEGER
        },
        facultyId: {
          type: Sequelize.INTEGER
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('CampFaculty', ['campusId'], {
          type: 'foreign key',
          name: 'FK_CampFaculty_Campus',
          references: {
            table: 'Campus',
            field: 'id'
          }
        }).then(() => {
          return queryInterface.addConstraint('CampFaculty', ['facultyId'], {
            type: 'foreign key',
            name: 'FK_CampFaculty_Faculty',
            references: {
              table: 'Faculty',
              field: 'id'
            }
          })
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CampFaculty', 'FK_CampFaculty_Campus').then(() => {
        return queryInterface
          .removeConstraint('CampFaculty', 'FK_CampFaculty_Faculty').then(() => {
            return queryInterface.dropTable('CampFaculty');
          });
      });

  }
};