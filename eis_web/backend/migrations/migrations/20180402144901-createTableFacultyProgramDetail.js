'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('FacultyProgramDetail', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        facultyId: {
          type: Sequelize.INTEGER, allowNull: false
        },
        programDetailId: {
          type: Sequelize.INTEGER, allowNull: false
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER },
        updatedBy: { type: Sequelize.INTEGER }
      })
      .then(() => {
        return queryInterface
          .addConstraint('FacultyProgramDetail', ['facultyId'], {
            type: 'foreign key',
            name: 'FK_FacultyProgramDetail_Faculty',
            references: {
              table: 'Faculty',
              field: 'id'
            },
            onDelete: 'cascade',
            onUpdate: 'cascade'
          })
          .then(() => {
            return queryInterface.addConstraint('FacultyProgramDetail', ['programDetailId'], {
              type: 'foreign key',
              name: 'FK_FacultyProgramDetail_ProgramDetail',
              references: {
                table: 'ProgramDetail',
                field: 'id'
              },
              onDelete: 'cascade',
              onUpdate: 'cascade'
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('FacultyProgramDetail');
  }
};
