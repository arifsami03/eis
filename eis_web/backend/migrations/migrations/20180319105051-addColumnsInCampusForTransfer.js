'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('Campus', 'website', {
      type: Sequelize.STRING
    }).then(() => {
      return queryInterface.addColumn('Campus', 'officialEmail', {
        type: Sequelize.STRING
      }).then(() => {
        return queryInterface.addColumn('Campus', 'establishedSince', {
          type: Sequelize.STRING
        })
      }).then(() => {
        return queryInterface.addColumn('Campus', 'noOfCampusTransfer', {
          type: Sequelize.INTEGER
        })
      })
    })

  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'establishedSince').then(() => {
      return queryInterface.removeColumn('Campus', 'officialEmail').then(() => {
        return queryInterface.removeColumn('Campus', 'website').then(() => {
          return queryInterface.removeColumn('Campus', 'noOfCampusTransfer')
        })
      })
    })


  }
};