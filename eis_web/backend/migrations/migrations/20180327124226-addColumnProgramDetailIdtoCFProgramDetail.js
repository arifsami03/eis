'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('CFProgramDetail').then(() => {
      return queryInterface
        .addColumn('CFProgramDetail', 'programDetailId', {
          type: Sequelize.INTEGER,
          allowNull: false
        }).then(() => {
          return queryInterface.addConstraint('CFProgramDetail', ['programDetailId'], {
            type: 'foreign key',
            name: 'FK_CFProgramDetail_ProgramDetail',
            references: {
              table: 'ProgramDetail',
              field: 'id'
            }
          });
        });
    })


  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CFProgramDetail', 'FK_CFProgramDetail_ProgramDetail')
      .then(() => {
        return queryInterface.removeColumn('CFProgramDetail', 'programDetailId')
      });
  }
};
