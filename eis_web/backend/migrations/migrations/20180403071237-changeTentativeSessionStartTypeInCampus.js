'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.removeColumn('Campus', 'tentativeSessionStart').then(() => {
      
      return queryInterface.addColumn('Campus', 'tentativeSessionStart', {
        type: Sequelize.DATE,
        allowNull: true
      })

    })
    
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Campus', 'tentativeSessionStart').then(() => {
      
      return queryInterface.addColumn('Campus', 'tentativeSessionStart', {
        type: Sequelize.STRING,
        allowNull: true
      })

    })
  }
};