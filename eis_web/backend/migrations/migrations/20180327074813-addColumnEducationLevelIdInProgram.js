'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Program').then(() => {
      return queryInterface
        .addColumn('Program', 'educationLevelId', {
          type: Sequelize.INTEGER,
          allowNull: false
        }).then(() => {
          return queryInterface.addConstraint('Program', ['educationLevelId'], {
            type: 'foreign key',
            name: 'FK_Program_EducationLevel',
            references: {
              table: 'EducationLevel',
              field: 'id'
            }
          });
        });
    })


  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Program', 'FK_Program_EducationLevel')
      .then(() => {
        return queryInterface.removeColumn('Program', 'educationLevelId')
      });
  }
};