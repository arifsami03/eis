'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CampFacultyProgram', {
        id: {
          type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true
        },
        campusFacultyId: {
          type: Sequelize.INTEGER
        },
        programId: {
          type: Sequelize.INTEGER
        },

        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('CampFacultyProgram', ['campusFacultyId'], {
          type: 'foreign key',
          name: 'FK_CampFacultyProgram_CampFaculty',
          references: {
            table: 'CampFaculty',
            field: 'id'
          }
        }).then(() => {
          return queryInterface.addConstraint('CampFacultyProgram', ['programId'], {
            type: 'foreign key',
            name: 'FK_CampFacultyProgram_Faculty',
            references: {
              table: 'Program',
              field: 'id'
            }
          })
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('CampFacultyProgram', 'FK_CampFacultyProgram_CampFaculty').then(() => {
        return queryInterface
          .removeConstraint('CampFacultyProgram', 'FK_CampFacultyProgram_Program').then(() => {
            return queryInterface.dropTable('CampFacultyProgram');
          });
      });

  }
};
