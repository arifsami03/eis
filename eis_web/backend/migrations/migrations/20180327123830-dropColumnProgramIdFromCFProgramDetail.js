'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CFProgramDetail', 'programId');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('CFProgramDetail', 'programId', {
        type: Sequelize.INTEGER
      });
      
  }
};