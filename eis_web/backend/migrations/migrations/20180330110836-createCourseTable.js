'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Course', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        title: {
          type: Sequelize.STRING,
          allowNull: false
        },
        abbreviation: {
          type: Sequelize.STRING,
          allowNull: false
        },
        description: {
          type: Sequelize.TEXT,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Course');
  }
};