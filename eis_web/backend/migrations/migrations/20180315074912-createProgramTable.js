'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('Program', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        facultyId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint('Program', ['facultyId'], {
          type: 'foreign key',
          name: 'FK_Program_Faculty',
          references: {
            table: 'Faculty',
            field: 'id'
          }
        });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('Program', 'FK_Program_Faculty')
      .then(() => {
        return queryInterface.dropTable('Program');
      });
  }
};
