'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('Program', 'facultyId');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .addColumn('Program', 'facultyId', {
        type: Sequelize.INTEGER
      });
      
  }
};
