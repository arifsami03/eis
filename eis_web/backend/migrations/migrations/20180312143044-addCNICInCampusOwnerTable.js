'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('CampusOwner', 'cnic', {
      type: Sequelize.STRING(20),
      allowNull: false
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('CampusOwner', 'cnic');
  }
};
