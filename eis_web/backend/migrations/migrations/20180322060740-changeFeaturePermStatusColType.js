'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn('FeaturePermission', 'status').then(() => {

      return queryInterface.addColumn(
        'FeaturePermission',
        'status',
        Sequelize.BOOLEAN,
      );

    });

  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.removeColumn('FeaturePermission', 'status').then(() => {

      return queryInterface.addColumn(
        'FeaturePermission',
        'status',
        Sequelize.STRING,
      );
      
    });
  }
};
