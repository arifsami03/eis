'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('LicenseFee', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        educationLevelId: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        amount: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updatedAt: {
          type: Sequelize.DATE,
          allowNull: false
        },
        createdBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        updatedBy: {
          type: Sequelize.INTEGER,
          allowNull: true
        }
      })
      .then(() => {
        return queryInterface.addConstraint('LicenseFee', ['educationLevelId'], {
          type: 'foreign key',
          name: 'FK_LicenseFee_EducationLevel',
          references: {
            table: 'EducationLevel',
            field: 'id'
          }
        })
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('LicenseFee', 'FK_LicenseFee_EducationLevel').then(() => {
        return queryInterface.dropTable('LicenseFee');
      });
  }
};