'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('CampusTargettedLocations', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        campusId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false
        },
        address: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: false
        },
        latitude: { type: Sequelize.FLOAT, allowNull: true },
        longitude: { type: Sequelize.FLOAT, allowNull: true },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface.addConstraint(
          'CampusTargettedLocations',
          ['campusId'],
          {
            type: 'foreign key',
            name: 'FK_CampusTargettedLocations_Campus',
            references: {
              table: 'Campus',
              field: 'id'
            }
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint(
        'CampusTargettedLocations',
        'FK_CampusTargettedLocations_Campus'
      )
      .then(() => {
        return queryInterface.dropTable('CampusTargettedLocations');
      });
  }
};
