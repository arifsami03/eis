'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable('UserGroup', {
        id: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
        userId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
          references: {
            model: 'User',
            key: 'id'
          }
        },
        groupId: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: false,
          references: {
            model: 'Group',
            key: 'id'
          }
        },
        createdAt: { type: Sequelize.DATE, allowNull: false },
        updatedAt: { type: Sequelize.DATE, allowNull: false },
        createdBy: { type: Sequelize.INTEGER, allowNull: true },
        updatedBy: { type: Sequelize.INTEGER, allowNull: true }
      })
      .then(() => {
        return queryInterface
          .addConstraint('UserGroup', ['userId'], {
            type: 'foreign key',
            name: 'FK_UserGroup_User',
            references: {
              table: 'User',
              field: 'id'
            }
          })
          .then(() => {
            return queryInterface.addConstraint('UserGroup', ['groupId'], {
              type: 'foreign key',
              name: 'FK_UserGroup_Group',
              references: {
                table: 'Group',
                field: 'id'
              }
            });
          });
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface
      .removeConstraint('UserGroup', 'FK_UserGroup_User')
      .then(() => {
        return queryInterface
          .removeConstraint('UserGroup', 'FK_UserGroup_Group')
          .then(() => {
            return queryInterface.dropTable('UserGroup');
          });
      });
  }
};
