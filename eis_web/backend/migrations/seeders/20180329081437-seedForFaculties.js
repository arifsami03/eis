'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'faculties.*', parentId: 'institute.*', title: 'Manage Faculties', isVisible: true, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.create', parentId: 'faculties.*', title: 'Create Faculty', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.index', parentId: 'faculties.create', title: 'View Faculties', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.update', parentId: 'faculties.*', title: 'Update Faculty', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.index', parentId: 'faculties.update', title: 'View Faculties', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.delete', parentId: 'faculties.*', title: 'Delete Faculty', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.index', parentId: 'faculties.delete', title: 'View Faculties', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'faculties.find', parentId: 'faculties.index', title: 'View Faculty', isVisible: false, createdAt: new Date(), updatedAt: new Date() }], {});
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'faculties.*',
        'faculties.create',
        'faculties.index',
        'faculties.update',
        'faculties.delete',
        'faculties.find'
      ]
    }, {});
  }
};