'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'AppFeature',
      [
        { id: 'institute.*', parentId: null, title: 'Institute', isVisible: true, weight: 0, createdAt: new Date(), updatedAt: new Date() },

        //InstituteType Management Seeds
        { id: 'instituteTypes.*', parentId: 'institute.*', title: 'Manage Institute types', isVisible: true, weight: 1, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.create', parentId: 'instituteTypes.*', title: 'Create Institute type', isVisible: false, weight: 2, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.create', title: 'View Institute types', isVisible: false, weight: 3, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.update', parentId: 'instituteTypes.*', title: 'Update Institute type', isVisible: false, weight: 4, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.update', title: 'View Institute types', isVisible: false, weight: 5, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.delete', parentId: 'instituteTypes.*', title: 'Delete Institute type', isVisible: false, weight: 6, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.index', parentId: 'instituteTypes.delete', title: 'View Institute types', isVisible: false, weight: 7, createdAt: new Date(), updatedAt: new Date() },
        { id: 'instituteTypes.find', parentId: 'instituteTypes.index', title: 'View Institute type', isVisible: false, weight: 8, createdAt: new Date(), updatedAt: new Date() }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'AppFeature',
      {
        id: ['institute.*', 'instituteTypes.*', 'instituteTypes.create', 'instituteTypes.update', 'instituteTypes.delete', 'instituteTypes.index', 'instituteTypes.find']
      },
      {}
    );
  }
};
