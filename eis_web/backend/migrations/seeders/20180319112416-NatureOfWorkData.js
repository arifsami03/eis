'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf',
      [
        {
          metaKey: 'natureOfWork',
          metaValue: 'Businessman',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'natureOfWork',
          metaValue: 'Private Job',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'natureOfWork',
          metaValue: 'Government Job',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'MetaConf',
      { metaKey: ['natureOfWork'] },
      {}
    );
  }
};
