'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'facultyProgramDetails.index', parentId: 'faculties.find', title: 'View Faculty Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'facultyProgramDetails.update', parentId: 'faculties.create', title: 'Update Faculty Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'facultyProgramDetails.update', parentId: 'faculties.update', title: 'Update Faculty Program Details', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
      { id: 'facultyProgramDetails.findAllProgramDetails', parentId: 'facultyProgramDetails.update', title: 'View Faculty Program Details List for Update', isVisible: false, createdAt: new Date(), updatedAt: new Date() },
    ], {});
  },

  down: (queryInterface, Sequelize) => {

    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'facultyProgramDetails.index',
        'facultyProgramDetails.update',
        'facultyProgramDetails.findAllProgramDetails',
      ]
    }, {});
  }
};