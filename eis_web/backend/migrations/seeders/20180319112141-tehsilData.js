'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf',
      [
        {
          metaKey: 'tehsil',
          metaValue: 'Lahore Cantt',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Model Town',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Raiwind',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Johar Town',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Lahore City',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Model Town',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'tehsil',
          metaValue: 'Shalamar',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MetaConf', { metaKey: ['tehsil'] }, {});
  }
};
