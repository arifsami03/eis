'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf',
      [
        {
          metaKey: 'instrumentType',
          metaValue: 'Businessman',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'instrumentType',
          metaValue: 'Private Job',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'instrumentType',
          metaValue: 'Government Job',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete(
      'MetaConf',
      { metaKey: ['instrumentType'] },
      {}
    );
  }
};
