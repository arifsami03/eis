'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf',
      [
        {
          metaKey: 'city',
          metaValue: 'Lahore',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Gujrat',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Sahiwal',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Pattoki',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Karachi',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Islamabad',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'city',
          metaValue: 'Faisalabad',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MetaConf', { metaKey: ['city'] }, {});
  }
};
