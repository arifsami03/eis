'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('AppFeature',
      [
        // Update Role Permissions Seeds
        { id: 'featurePermissions.updateRolePermissions', parentId: 'roles.update', title: 'Update Permissions', isVisible: false, createdAt: new Date(), updatedAt: new Date() },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', { id: ['featurePermissions.updateRolePermissions'] },
      {});
  }
};
