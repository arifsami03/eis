'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf', [{
          metaKey: 'country',
          metaValue: 'Pakistan',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'India',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'America',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'United Kingdom',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'Italy',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'Sri Lanka',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'country',
          metaValue: 'Australia',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ], {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MetaConf', {
      metaKey: ['country']
    }, {});
  }
};