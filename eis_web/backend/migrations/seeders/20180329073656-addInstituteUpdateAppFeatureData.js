'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature',
      [
        //InstituteType Management Seeds
        { id: 'institutes.update', parentId: 'institute.*', title: 'Update Institute Information', isVisible: true, weight: 9, createdAt: new Date(), updatedAt: new Date() },

      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: ['institutes.update']
    }, {});
  }
};
