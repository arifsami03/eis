'use strict';

module.exports = {

  up: (queryInterface, Sequelize) => {

    return queryInterface.bulkInsert('AppFeature', [
      { id: 'instituteTypes.findAttributesList', parentId: 'educationLevels.find', title: 'Get Institute Type List', isVisible: false, createdAt: new Date(), updatedAt: new Date() }
    ],
      {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('AppFeature', {
      id: [
        'instituteTypes.findAttributesList']
    }, {});
  }
};