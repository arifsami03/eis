'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'MetaConf',
      [
        {
          metaKey: 'province',
          metaValue: 'Punjab',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'province',
          metaValue: 'Sindh',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'province',
          metaValue: 'KPK',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'province',
          metaValue: 'Balochistan',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          metaKey: 'province',
          metaValue: 'Gilgit',
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('MetaConf', { metaKey: ['province'] }, {});
  }
};
