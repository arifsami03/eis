/**
 * Import angular, material and other related modules here
 */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';

/**
 * Import Self created components, model, services etc here.
 */
import {
  InstituteService,
  FacultyService,
  FacultyProgramDetailsService,
  ProgramService,
  EducationLevelService,
  ProgramDetailsService,
  InstituteTypeService,
  ClassesService,
  AcademicCalendarService,
  LicenseFeeService,
  CourseService,
  RoadMapService
} from './services';
import {
  InstituteComponent,
  InstituteViewComponent,
  MapComponent,
  FacultyListComponent,
  FacultyFormComponent,
  FacultyViewComponent,
  ProgramListComponent,
  ProgramFormComponent,
  ProgramDetailsListComponent,
  ProgramDetailsFormComponent,
  EducationLevelFormComponent,
  EducationLevelListComponent,
  InstituteTypeFormComponent,
  InstituteTypeListComponent,
  LicenseFeeFormComponent,
  LicenseFeeListComponent,
  CourseFormComponent,
  CourseListComponent,
  ClassesListComponent,
  ClassesFormComponent,
  AcademicCalendarFormComponent,
  AcademicCalendarListComponent,
  RoadMapFormComponent,
  RoadMapListComponent
} from './components';

/**
 * Export consts here.
 */
export const __IMPORTS = [
  CommonModule,
  HttpModule,
  HttpClientModule,
  MatInputModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  FormsModule,
  ReactiveFormsModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatFormFieldModule,
  FlexLayoutModule,
  MatTabsModule,
  TextMaskModule,
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE'
  })
];

export const __DECLARATIONS = [
  InstituteComponent,
  InstituteViewComponent,
  MapComponent,
  FacultyListComponent,
  FacultyFormComponent,
  FacultyViewComponent,
  ProgramListComponent,
  ProgramFormComponent,
  ProgramDetailsListComponent,
  EducationLevelFormComponent,
  ProgramDetailsFormComponent,
  EducationLevelListComponent,
  InstituteTypeFormComponent,
  InstituteTypeListComponent,
  LicenseFeeFormComponent,
  LicenseFeeListComponent,
  CourseFormComponent,
  CourseListComponent,
  ClassesListComponent,
  ClassesFormComponent,
  AcademicCalendarFormComponent,
  AcademicCalendarListComponent,
  RoadMapFormComponent,
  RoadMapListComponent
];

export const __PROVIDERS = [
  InstituteService,
  FacultyService,
  FacultyProgramDetailsService,
  ProgramService,
  EducationLevelService,
  ProgramDetailsService,
  InstituteTypeService,
  ClassesService,
  AcademicCalendarService,
  LicenseFeeService,
  CourseService,
  RoadMapService
];

export const __ENTRY_COMPONENTS = [MapComponent];
