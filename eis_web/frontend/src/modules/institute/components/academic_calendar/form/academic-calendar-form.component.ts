import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { GLOBALS } from 'modules/app/config/globals';
import { AcademicCalendarService, ProgramDetailsService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';
import { AcademicCalendarModel, ProgramDetailsModel } from 'modules/institute/models';

@Component({
  selector: 'institute-academic-calendar-form',
  templateUrl: './academic-calendar-form.component.html',
  styleUrls: ['./academic-calendar-form.component.css']
})
export class AcademicCalendarFormComponent implements OnInit {
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public academicCalendar: AcademicCalendarModel;
  public options: Observable<string[]>;
  public componentLabels = AcademicCalendarModel.attributesLabels;

  public acProgramDetailIds: number[] = [];

  /**
   * for Program details list
   */
  public data: ProgramDetailsModel[] = [new ProgramDetailsModel()];

  public attrLabels = ProgramDetailsModel.attributesLabels;

  displayedColumns = ['programId', 'name', , 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private academicCalendarService: AcademicCalendarService,
    private programDetailService: ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findAttributesList();
  }

  /**
   * get all records for program details
   */
  findAttributesList(): void {
    this.programDetailService.findAttributesList().subscribe(
      response => {
        this.data = response;
        console.log('PD Data : ', this.data)
        // select existing id on check
        this.isCheckBoxesChecked();
        this.dataSource = new MatTableDataSource<ProgramDetailsModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new AcademicCalendarModel().validationRules());

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.academicCalendarService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.academicCalendar = response;
            this.fg.patchValue(this.academicCalendar);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              // pushing ids to acProgramDetailIds

              let academicCalendarProgramDetailsIds: any = this.academicCalendar['academicCalendarProgramDetails'];
              academicCalendarProgramDetailsIds.forEach(element => {
                this.acProgramDetailIds.push(element['programDetailId']);
              });

              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => { }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Academic Calendar: Create' });

    this.academicCalendar = new AcademicCalendarModel();

    this.fg.enable();
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Academic Calendar: ' + this.academicCalendar.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Academic Calendar ',
          url: '/institute/academicCalendars'
        },
        { label: this.academicCalendar.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Academic Calendar: ' + this.academicCalendar.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Academic Calendar',
          url: '/institute/academicCalendars'
        },
        {
          label: this.academicCalendar.title,
          url: `/institute/academicCalendars/view/${this.academicCalendar.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: AcademicCalendarModel) {
    item['programDetailId'] = this.acProgramDetailIds;

    if (this.pageAct === this.pageActions.create) {
      this.academicCalendarService.create(item).subscribe(
        response => {
          this.router.navigate([`/institute/academicCalendars/view/${response.id}`]);
        },
        error => {
          console.log(error);
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.academicCalendarService.update(this.academicCalendar.id, item).subscribe(
        response => {
          this.router.navigate([`/institute/academicCalendars/view/${this.academicCalendar.id}`]);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  checkboxClicked(event: any, programDetailId) {
    if (event.checked) {
      this.acProgramDetailIds.push(programDetailId);
    } else {
      const index = this.acProgramDetailIds.indexOf(programDetailId);

      if (index > -1) {
        this.acProgramDetailIds.splice(index, 1);
      }
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.academicCalendarService.delete(id).subscribe(
            response => {
              this.router.navigate(['/institute/academicCalendars']);
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }
  isCheckBoxesChecked() {
    if (this.data) {
      if (this.academicCalendar) {
        let academicCalendarProgramDetailsArray: any = this.academicCalendar['academicCalendarProgramDetails'];
        if (this.academicCalendar['academicCalendarProgramDetails']) {
          for (let i = 0; i < this.data.length; i++) {
            academicCalendarProgramDetailsArray.forEach(acpd_element => {
              if (this.data[i].id === acpd_element.programDetailId) {
                this.data[i]['status'] = true;
              }
            });
          }
        }
      }
    }
  }
  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
}
