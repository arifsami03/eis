import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { AcademicCalendarService } from 'modules/institute/services';
import { AcademicCalendarModel } from 'modules/institute/models';

@Component({
  selector: 'institute-academic-calendar-list',
  templateUrl: './academic-calendar-list.component.html',
  styleUrls: ['./academic-calendar-list.component.css']
})
export class AcademicCalendarListComponent implements OnInit {
  public loaded: boolean = false;

  public data: AcademicCalendarModel[] = [new AcademicCalendarModel()];

  public attrLabels = AcademicCalendarModel.attributesLabels;

  displayedColumns = ['title', 'abbreviation', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(private academicCalendarService: AcademicCalendarService, public dialog: MatDialog, public matDialog: MatDialog, private router: Router, public layoutService: LayoutService) {}

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Academic Calendar' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {
    this.academicCalendarService.index().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<AcademicCalendarModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  delete(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.academicCalendarService.delete(id).subscribe(response => {
            this.getRecords();
            setTimeout((router: Router) => {
              this.success = false;
            }, 1000);
          });
        }
      });
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
