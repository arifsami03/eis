import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { ProgramDetailsService,ClassesService } from 'modules/institute/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { ProgramDetailsModel,ClassesModel } from '../../../models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'classes-list',
  templateUrl: './classes-list.component.html',
  styleUrls: ['./classes-list.component.css']
})
export class ClassesListComponent implements OnInit {
  public loaded: boolean = false;
  public data: ClassesModel[] = [new ClassesModel()];
  public attrLabels = ClassesModel.attributesLabels;

  displayedColumns = ['educationLevel','program','programDetail','name',  'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private programDetailService: ProgramDetailsService,
    private classesService: ClassesService,    
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) {}

  /**
   * initialize the page of programs list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Classes' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.classesService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<ClassesModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  deleteProgram(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.classesService.delete(id).subscribe(response => {
          if (response) {
            setTimeout((router: Router) => {
              this.success = false;
              this.getRecords();
            }, 1000);
          } else {
            // Show error message
            this.error = true;
            this.errorMessage = response.error;
          }
        }
        ,error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
      }
    });
  }
}
