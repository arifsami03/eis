import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { ProgramDetailsService,ClassesService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { ProgramDetailsModel,ClassesModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

/**
 * This component is being used for three purposes.
 *  1: Add new Classes
 *  2: View existing Class Members
 *  3: Update existing Classes Members
 *
 */

@Component({
  selector: 'classes-form',
  templateUrl: './classes-form.component.html',
  styleUrls: ['./classes-form.component.css']
})
export class ClassesFormComponent implements OnInit {
  public loaded: boolean = false;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  public programDetails: ProgramDetailsModel[];
  public classes: ClassesModel;
  public fg: FormGroup;
  public pageAct: string;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ClassesModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programDetailsService:ProgramDetailsService,
    private classesService:ClassesService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ClassesModel().validationRules());
    this.findProgramDetails();
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.classesService.find(+params['id']).subscribe(
        response => {
          this.classes = response;
          console.log(this.classes);
          if (response) {
            // // display of Succes and Error Message
            // this.success = true;
            // this.successMessage = response.success;
            this.fg.patchValue(this.classes);
          } else {
            
          }
          if (!this.error) {
            if (this.pageAct === 'view') {
              this.initViewPage();
            } else if (this.pageAct === 'update') {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  findProgramDetails() {
    this.programDetailsService.getProgramDetails().subscribe(
      response => {
        this.programDetails = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }



  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Courses' });
    this.classes = new ClassesModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Classes: ' + this.classes.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Class', url: '/institute/Classes' },
        { label: this.classes.name }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Classes: ' + this.classes.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Class', url: '/institute/classes' },
        {
          label: this.classes.name,
          url: `/institute/classes/view/${this.classes.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update faculty when save button is clicked
   */
  public saveData(item: ClassesModel) {
    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.classesService.create(item).subscribe(
        response => {
          if (response) {
            id = response.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([`/institute/classes/view/${id}`]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    } else if (this.pageAct === 'update') {
      this.classesService.update(this.classes.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([
              `/institute/classes/view/${this.classes.id}`
            ]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    }
  }

  /**
   * delete the selected program
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.classesService.delete(id).subscribe(response => {
          if (response) {
            
            this.router.navigate(['institute/classes']);
          } else {
            // Show error message
            this.error = true;
            this.errorMessage = response.error;
          }
        }
        ,
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
      }
    });
  }
}
