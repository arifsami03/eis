import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

import {
  ConfirmDialogComponent,
  AlertDialogComponent
} from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { FacultyService } from 'modules/institute/services';
import { FacultyModel } from 'modules/institute/models/index';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-faculty-list',
  templateUrl: './faculty-list.component.html',
  styleUrls: ['./faculty-list.component.css']
})
export class FacultyListComponent implements OnInit {
  public loaded: boolean = false;
  public data: FacultyModel[] = [new FacultyModel()];
  public attrLabels = FacultyModel.attributesLabels;

  displayedColumns = ['name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public error: Boolean;

  constructor(
    private facultyService: FacultyService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Faculties' });
    this.getRecords();
  }

  /**
   * Get records 
   */
  getRecords() {
    this.facultyService.index().subscribe(
      response => {
        if (response) {
          this.data = response;
          this.dataSource = new MatTableDataSource<FacultyModel>(this.data);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  deleteFaculty(id: number) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.facultyService.delete(id).subscribe(response => {
            this.getRecords();
          },
            error => {
              // let dialogRef = this.dialog
              //   .open(AlertDialogComponent, {
              //     height: '200px',
              //     width: '400px',
              //     data: { message: GLOBALS.deleteDialog.alertMessage }
              //   })
              //   .afterClosed()
              //   .subscribe((accept: boolean) => {
              //     this.router.navigate([`institute/faculties/view/${id}`]);
              //     // this.getRecords();
              //   });
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
}
