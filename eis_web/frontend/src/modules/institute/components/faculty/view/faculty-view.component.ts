import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FacultyService } from 'modules/institute/services';
import { FacultyProgramDetailsService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { FacultyModel } from 'modules/institute/models';
import { FacultyProgramDetailsModel } from 'modules/institute/models';
import { ProgramDetailsModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';


@Component({
  selector: 'institute-faculty-view',
  templateUrl: './faculty-view.component.html',
  styleUrls: ['./faculty-view.component.css']
})
export class FacultyViewComponent implements OnInit {
  public loaded: boolean = false;
  public facultyProgramDetailsloaded: boolean = false;
  public pageActions = GLOBALS.pageActions;
  public editMode: boolean = false;

  public facultyProgramDetailLabels = FacultyProgramDetailsModel.attributesLabels;

  // data to be sent backend to update program-details for faculty
  private data = {
    insertedIds: [],
    deletedIds: []
  }

  public fg: FormGroup;
  public faculty: FacultyModel;
  public facultyProgramDetails: FacultyProgramDetailsModel[];

  public programDetailsList: ProgramDetailsModel[];

  public componentLabels = FacultyModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private facultyService: FacultyService,
    private facultyProgramDetailsService: FacultyProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
  }

  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new FacultyModel().validationRules());
    this.getData();
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.facultyService.find(params['id']).subscribe(
        response => {
          this.faculty = response;

          this.fg.patchValue(this.faculty);
          this.initViewPage();

        },
        error => {
          console.log(error);
        },
        () => {
          this.loaded = true;
          this.getProgramDetails(this.faculty.id);
        }
      );
    });
  }

  private getProgramDetails(id: number) {
    this.facultyProgramDetailsService.index(id).subscribe(
      response => {
        this.facultyProgramDetails = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.facultyProgramDetailsloaded = true;
      }
    );
  }

  public enableEditMode() {
    this.facultyProgramDetailsService.findAllProgramDetails().subscribe(
      response => {
        this.programDetailsList = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.programDetailsList.forEach((item, index) => {
          let assignedProgramDetail = this.facultyProgramDetails.find(element => {
            return element.programDetailId == item.id;
          });
          if (assignedProgramDetail) {
            this.programDetailsList[index]['status'] = true;
          }
        });
        this.editMode = true;
      }
    );

  }

  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Faculty: ' + this.faculty.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Faculty ', url: '/institute/faculties' },
        { label: this.faculty.name }
      ]
    });
  }

  public change(event, id) {
    if (event.checked) {
      // check if program detail does not exist in already assiged program detail or in data.insertedIds then
      // insert it to data.insertedIds array

      let item = this.facultyProgramDetails.find(element => {
        return element.programDetailId == id;
      });

      let obj = this.data.insertedIds.find(element => {
        return element == id;
      });

      if (!item && !obj) {
        this.data.insertedIds.push(id)
      }

      // and if this program detail exist in data.deletedIds then remove it 

      let indexInDeleted = this.data.deletedIds.indexOf(id);

      if (indexInDeleted != -1) {
        this.data.deletedIds.splice(indexInDeleted, 1);
      }

    } else {
      // check if this program detail is not already assigned and exist in data.deletedId then isert it to data.deletedIds array

      let item = this.facultyProgramDetails.find(element => {
        return element.programDetailId == id;
      });

      let obj = this.data.deletedIds.find(element => {
        return element == id;
      });

      if (item && !obj) {
        this.data.deletedIds.push(id)
      }

      // if this program detail exist in data.insertedIds then remove it 

      let indexInInserted = this.data.insertedIds.indexOf(id);
      if (indexInInserted != -1) {
        this.data.insertedIds.splice(indexInInserted, 1);
      }

    }
  }

  public update() {

    if (this.data.insertedIds.length > 0 || this.data.deletedIds.length > 0) {
      this.facultyProgramDetailsService.update(this.faculty.id, this.data).subscribe(
        response => {
          console.log(response);

        },
        error => {
          console.log(error);
        },
        () => {
          this.getProgramDetails(this.faculty.id);
          this.editMode = false;

          // sets data to be null
          this.data.insertedIds = [];
          this.data.deletedIds = [];
        }
      );
    } else {
      this.editMode = false;
    }

  }

  public cancel() {
    this.editMode = false;
  }

  public deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.facultyService.delete(id).subscribe(response => {
          this.router.navigate(['institute/faculties']);
        },
          error => {
            console.log(error);
            this.loaded = true;
          }
        );
      }
    });
  }

}
