import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { FacultyService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { FacultyModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

/**
 * This component is being used for two purposes.
 *  1: Add new Facutly Members
 *  3: Update existing Facutly Members
 */

@Component({
  selector: 'institute-faculty-form',
  templateUrl: './faculty-form.component.html',
  styleUrls: ['./faculty-form.component.css']
})
export class FacultyFormComponent implements OnInit {
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public faculty: FacultyModel;

  public componentLabels = FacultyModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private facultyService: FacultyService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new FacultyModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.facultyService.find(params['id']).subscribe(
        response => {
          this.faculty = response;

          this.fg.patchValue(this.faculty);

          this.initUpdatePage();
        },
        error => {
          console.log(error);
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Faculty: Create' });
    this.faculty = new FacultyModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Faculty: ' + this.faculty.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Faculty ', url: '/institute/faculties' },
        { label: this.faculty.name }
      ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Faculty: ' + this.faculty.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Faculty', url: '/institute/faculties' },
        {
          label: this.faculty.name,
          url: `/institute/faculties/view/${this.faculty.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: FacultyModel) {
    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.facultyService.create(item).subscribe(
        response => {
          this.router.navigate([`institute/faculties/view/${response.id}`]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.facultyService.update(this.faculty.id, item).subscribe(
        response => {
          this.router.navigate([
            `institute/faculties/view/${this.faculty.id}`
          ]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    }
  }

  /**
   * Delete record
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.facultyService.delete(id).subscribe(response => {
          this.router.navigate(['institute/faculties']);
        },
          error => {
            console.log(error);
            this.loaded = true;
          }
        );
      }
    });
  }
}
