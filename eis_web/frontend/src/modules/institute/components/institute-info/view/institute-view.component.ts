import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { AgmCoreModule, MouseEvent } from '@agm/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LayoutService } from 'modules/layout/services';
import { InstituteModel } from 'modules/institute/models';
import { InstituteService } from 'modules/institute/services';
@Component({
  selector: 'institute-view',
  templateUrl: './institute-view.component.html',
  styleUrls: ['./institute-view.component.css']
})
export class InstituteViewComponent implements OnInit {

  public loaded: boolean = false;
  public pageAct: string;
  public pageTitle: string = 'Institute Information';
  public institute = new InstituteModel();
  public title: string = 'My first AGM project';
  public show: string = 'Less';
  public less: string;
  public more: string;
  zoom: number = 8;

  // initial center position for the map
  lat: number;// = 51.673858;
  lng: number;// = 7.815982;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private layoutService: LayoutService,
    private instituteEditService: InstituteService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */

  ngOnInit() {

    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();



  }

  private initializePage() {
    this.getInstituteRecord();


  }

  getInstituteRecord(): void {

    this.instituteEditService.getAll().subscribe(response => {
      this.institute = response;
      this.more = this.institute.Description;
      this.showDescription();
      this.lat = this.institute.latitude;
      this.lng = this.institute.longitude;
      //this.loaded = false;
    },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  showDescription() {
    if (this.show === 'More') {
      this.show = 'Less';
      this.institute.Description = this.more;
    }
    else if (this.show === 'Less') {
      this.show = 'More';
      this.less = this.institute.Description;
      let arr = this.less.split(' ');
      arr = arr.slice(0, 80);
      let str = arr.toString();
      let result = str.replace(/,/g, " ");
      this.institute.Description = result;
    }
  }

  mapClicked($event: MouseEvent) {
    this.lat = $event.coords.lat;
    this.lng = $event.coords.lng;

  }



}


