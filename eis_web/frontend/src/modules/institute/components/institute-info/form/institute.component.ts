import { Observable } from 'rxjs/Observable';
import { Component, OnInit } from '@angular/core';
import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { LayoutService } from 'modules/layout/services';
import { InstituteModel } from 'modules/institute/models';
import { InstituteService } from 'modules/institute/services';
import { MapComponent } from 'modules/institute/components';
@Component({
  selector: 'institute',
  templateUrl: './institute.component.html',
  styleUrls: ['./institute.component.css']
})
export class InstituteComponent implements OnInit {
  public loaded: boolean = false;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Institute Information';
  public institute = new InstituteModel();
  public establishedScince = [1990, 1991, 1997, 1998];
  public id: number;
  // public contactNumbers:  any[] = [];
  // public affiliation: any[] = [];
  public deletedContact: number[] = [];
  public deletedAffiliationIds: number[] = [];

  public mobileMask = [
    /[0-9]/,
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];

  public componentLabels = InstituteModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    public dialog: MatDialog,
    private instituteEditService: InstituteService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();
  }

  /**
   *
   * @param equalControlName
   */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {


      let controlMatch = 1850;
      var d = new Date();
      var currentYear = d.getFullYear();

      return (controlMatch <= control.value && currentYear >= control.value)
        ? null
        : {
          equalTo: true
        };
    };
  }

  private initializePage() {
    // this.fg = this.fb.group(new InstituteModel().validationRules());

    this.fg = this.fb.group({
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ]),
      officeEmail: ['', [<any>Validators.required, Validators.email]],
      logo: [''],
      abbreviation: [''],
      website: new FormControl('', [<any>Validators.required, Validators.maxLength(25),
      Validators.pattern('^(http:\/\/|https:\/\/)?(www.)?([0-9A-Za-z]+([0-9A-Za-z])*)([.][A-Za-z]([A-Za-z]+))+')]),
      headOfficeLocation: [''],
      latitude: [''],
      longitude: [''],
      establishedScince: ['', [this.equalTo('establishedScince')]],
      affiliation: this.fb.array([]),
      focalPerson: [''],
      Description: [''],
      contactNumbers: this.fb.array([])
    });
    this.getInstituteRecord();
  }

  createContactItem(): FormGroup {
    return this.fb.group({
      contact: '',
      id: ''
    });
  }
  createAffiliationItem(): FormGroup {
    return this.fb.group({
      affiliationa: '',
      id: ''
    });
  }

  addContactClick(): void {
    let contactNumbers = this.fg.get('contactNumbers') as FormArray;
    contactNumbers.push(this.createContactItem());
  }
  removeLink(i: number, item) {
    this.deletedContact.push(item.value.id);
    const control = <FormArray>this.fg.controls['contactNumbers'];
    control.removeAt(i);
  }

  addAffiliationClick(): void {
    let affiliation = this.fg.get('affiliation') as FormArray;
    affiliation.push(this.createAffiliationItem());
  }
  removeAffiliationLink(i: number, item) {
    this.deletedAffiliationIds.push(item.value.id);
    const control1 = <FormArray>this.fg.controls['affiliation'];
    control1.removeAt(i);
  }

  getInstituteRecord(): void {
    this.instituteEditService.getAll().subscribe(
      response => {
        this.institute = response;

        this.id = this.institute.id;

        if (this.institute.contactNumbers.length == 0) {
          this.addContactClick();
        }
        for (var i = 0; i < this.institute.contactNumbers.length; i++) {
          this.addContactClick();
        }
        if (this.institute.affiliation.length == 0) {
          this.addAffiliationClick();
        }
        for (var i = 0; i < this.institute.affiliation.length; i++) {
          this.addAffiliationClick();
        }
        this.fg.patchValue(this.institute);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('officeEmail').hasError('required')
      ? 'Email is Required'
      : this.fg.get('officeEmail').hasError('officeEmail')
        ? 'Not a valid email'
        : '';
  }

  public saveData(item: InstituteModel) {
    this.loaded = false;
    let postData = {
      institute: item,
      deletedContactIds: this.deletedContact,
      deletedAffiliationIds: this.deletedAffiliationIds
    };
    this.instituteEditService.update(this.id, postData).subscribe(
      response => {

        this.router.navigate(['institute/dashboard']);
        this.loaded = true;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Open Popup to add Location
   */
  addLocationClick() {
    let dialogRef = this.dialog.open(MapComponent, {
      width: '800px',
      height: '400px',
      data: { lat: this.institute.latitude, lng: this.institute.longitude }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.institute.latitude = result.lat;
        this.institute.longitude = result.lng;
        this.fg.patchValue({
          latitude: this.institute.latitude,
          longitude: this.institute.longitude
        });
      }
    });
  }
}
