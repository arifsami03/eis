import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { LicenseFeeService, EducationLevelService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { LicenseFeeModel, EducationLevelModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

/**
 * This component is being used for three purposes.
 *  1: Add new LicenseFee Members
 *  2: View existing LicenseFee Members
 *  3: Update existing LicenseFee Members
 *
 */

@Component({
  selector: 'institute-license-fee-form',
  templateUrl: './license-fee-form.component.html',
  styleUrls: ['./license-fee-form.component.css']
})
export class LicenseFeeFormComponent implements OnInit {
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;
  public error: Boolean;
  public educationLevel: EducationLevelModel[];
  public fg: FormGroup;
  public pageAct: string;
  public licenseFee: LicenseFeeModel;
  public educationName;

  public componentLabels = LicenseFeeModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private licenseFeeService: LicenseFeeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private educationLevelService: EducationLevelService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new LicenseFeeModel().validationRules());
    this.getEducationLevelList();
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.licenseFeeService.find(params['id']).subscribe(
        response => {
          this.licenseFee = response;
          this.educationName = response;
          this.fg.patchValue(this.licenseFee);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Get Education Level List
   */
  getEducationLevelList() {
    this.educationLevelService.findAttributesList().subscribe(
      response => {
        this.educationLevel = response;
      },
      error => console.log(error),
      () => { }
    );
  }

  /**
   * Initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'License Fee: Create' });
    this.licenseFee = new LicenseFeeModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'License Fee: ' + this.educationName.educationLevel.education,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'License Fee ', url: '/institute/licenseFees' },
        { label: this.licenseFee.amount }
      ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'License Fee: ' + this.educationName.educationLevel.education,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'License Fee', url: '/institute/licenseFees' },
        {
          label: this.licenseFee.amount,
          url: `/institute/licenseFees/view/${this.licenseFee.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or Update when save button is clicked
   */
  public saveData(item: LicenseFeeModel) {
    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.licenseFeeService.create(item).subscribe(
        response => {
          if (response) {
            this.router.navigate([`/institute/licenseFees/view/${response.id}`]);
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.licenseFeeService.update(this.licenseFee.id, item).subscribe(
        response => {
          if (response) {
            this.router.navigate([
              `/institute/licenseFees/view/${this.licenseFee.id}`
            ]);
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    }
  }

  /**
   * Delete
   */
  delete(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.licenseFeeService.delete(id).subscribe(response => {
          this.router.navigate(['institute/licenseFees']);
        }
          ,
          error => {
            console.log(error);
            this.loaded = true;
          }, () => {
            this.loaded = true;
          }
        );
      }
    });
  }
}
