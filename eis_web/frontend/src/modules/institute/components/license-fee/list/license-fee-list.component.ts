import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { LicenseFeeService } from 'modules/institute/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { LicenseFeeModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-license-fee-list',
  templateUrl: './license-fee-list.component.html',
  styleUrls: ['./license-fee-list.component.css']
})
export class LicenseFeeListComponent implements OnInit {
  public loaded: boolean = false;
  public data: LicenseFeeModel[] = [new LicenseFeeModel()];
  public attrLabels = LicenseFeeModel.attributesLabels;

  displayedColumns = ['educationLevelId', 'amount', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;

  constructor(
    private licenseFeeService: LicenseFeeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'License Fees' });
    this.getRecords();
  }

  /**
   * Get all records
   */
  getRecords(): void {
    this.licenseFeeService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<LicenseFeeModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }

    );
  }

  /**
   * search
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.licenseFeeService.delete(id).subscribe(response => {
          this.getRecords();
        },
          error => {
            console.log(error);
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }
}
