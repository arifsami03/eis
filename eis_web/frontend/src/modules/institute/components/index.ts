export * from './institute-info/form/institute.component';
export * from './institute-info/view/institute-view.component';
export * from './institute-info/form/map/map.component';
export * from './faculty/form/faculty-form.component';
export * from './faculty/view/faculty-view.component';
export * from './faculty/list/faculty-list.component';
export * from './programs/form/program-form.component';
export * from './programs/list/program-list.component';
//TODO:low directory name should be education_level instead of education-level
export * from './education-level/form/education-level-form.component';
export * from './education-level/list/education-level-list.component';

export * from './program-details/form/program-details-form.component';
export * from './program-details/list/program-details-list.component';
export * from './institute-type/form/institute-type-form.component';
export * from './institute-type/list/institute-type-list.component';
export * from './license-fee/form/license-fee-form.component';
export * from './license-fee/list/license-fee-list.component';
export * from './course/form/course-form.component';
export * from './course/list/course-list.component';

export * from './classes/list/classes-list.component';
export * from './classes/form/classes-form.component';
export * from './academic_calendar/list/academic-calendar-list.component';
export * from './academic_calendar/form/academic-calendar-form.component';

/**
 * Roadmap
 */
export * from './roadmap/form/roadmap-form.component';
export * from './roadmap/list/roadmap-list.component';
