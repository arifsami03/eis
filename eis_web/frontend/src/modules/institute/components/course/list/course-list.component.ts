import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { CourseService } from 'modules/institute/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { CourseModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-course-list',
  templateUrl: './course-list.component.html',
  styleUrls: ['./course-list.component.css']
})
export class CourseListComponent implements OnInit {
  public loaded: boolean = false;
  public data: CourseModel[] = [new CourseModel()];
  public attrLabels = CourseModel.attributesLabels;

  displayedColumns = ['title', 'abbreviation', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public success: Boolean;

  constructor(
    private courseService: CourseService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Courses' });
    this.getRecords();
  }

  /**
   * Get all records
   */
  getRecords(): void {
    this.courseService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<CourseModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }

    );
  }

  /**
   * Search in Grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete data against the id given
   */
  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.courseService.delete(id).subscribe(response => {
          this.getRecords();
        },
          error => {
            console.log(error);
            this.loaded = true;
          },
          () => {
            //TODO
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }
}
