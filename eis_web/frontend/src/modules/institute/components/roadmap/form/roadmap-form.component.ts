import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { GLOBALS } from 'modules/app/config/globals';

import {
  RoadMapService,
  ProgramDetailsService,
  AcademicCalendarService,
  ClassesService,
  CourseService
} from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import {
  RoadMapModel,
  ProgramDetailsModel,
  AcademicCalendarModel,
  ClassesModel,
  CourseModel
} from 'modules/institute/models';

@Component({
  selector: 'institute-roadmap-form',
  templateUrl: './roadmap-form.component.html',
  styleUrls: ['./roadmap-form.component.css']
})
export class RoadMapFormComponent implements OnInit {
  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;
  public error: Boolean;
  public fg: FormGroup;
  public pageAct: string;
  public roadMap: RoadMapModel;
  public options: Observable<string[]>;
  public componentLabels = RoadMapModel.attributesLabels;

  public programDetails: ProgramDetailsModel[] = [];
  public academicCalendars: AcademicCalendarModel[] = [];
  public classes: ClassesModel[] = [];
  public courses: CourseModel[] = [];
  public courseAbbr: string;
  public natureOfCourses: any[] = [GLOBALS.natureOfCourses.cumpulsory, GLOBALS.natureOfCourses.elective];
  public courseTypes: any[] = [GLOBALS.courseTypes.theoratical, GLOBALS.courseTypes.practical];
  public creditHours: any[] = GLOBALS.creditHours;

  public deletedCourses: number[] = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private roadMapService: RoadMapService,
    private programDetailsService: ProgramDetailsService,
    private academicCalendarService: AcademicCalendarService,
    private classesService: ClassesService,
    private courseService: CourseService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.findProgramDetailsAttributesList();
    this.findAcademicCalendarAttributesList();
    this.findCourseAttributesList();
    this.findClassesAttributesList();
  }

  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new RoadMapModel().validationRules(), { preReqCoursesList: this.fb.array([]) });

    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get record
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.roadMapService.find(params['id']).subscribe(
        response => {
          if (response) {
            this.roadMap = response;
            for (let i = 0; i < this.roadMap['preReqCoursesList'].length; i++) {
              this.addCoursesClick();
            }
            this.fg.patchValue(this.roadMap);
            //TODO:low need to check why we are dowing this after fetching data
            if (this.pageAct == this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct == this.pageActions.update) {
              this.initUpdatePage();
            }
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Roadmap: Create' });

    this.roadMap = new RoadMapModel();

    this.fg.enable();
    this.addCoursesClick();

    this.loaded = true;
  }

  /**
   * initialize the view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Roadmap: ' + this.roadMap.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Roadmap ',
          url: '/institute/roadMaps'
        },
        { label: this.roadMap.title }
      ]
    });
  }

  /**
   * initialize the update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Roadmap: ' + this.roadMap.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roadmap', url: '/institute/roadMaps' },
        {
          label: this.roadMap.title,
          url: `/institute/roadMaps/view/${this.roadMap.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   *
   */
  public saveData(item: RoadMapModel) {
    if (this.pageAct === this.pageActions.create) {
      this.roadMapService.create(item).subscribe(
        response => {
          this.router.navigate([`/institute/roadMaps/view/${response.id}`]);
        },
        error => {
          console.log(error);
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.roadMapService.update(this.roadMap.id, item).subscribe(
        response => {
          this.router.navigate([`/institute/roadMaps/view/${this.roadMap.id}`]);
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  /**
   * Delete record
   *
   */
  delete(id: number) {
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message: GLOBALS.deleteDialog.message }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.roadMapService.delete(id).subscribe(
            response => {
              this.router.navigate(['/institute/roadMaps']);
            },
            error => {
              console.log(error);
            }
          );
        }
      });
  }

  findProgramDetailsAttributesList() {
    this.programDetailsService.findAttributesList().subscribe(
      response => {
        this.programDetails = response;
      },
      error => console.log(error),
      () => {}
    );
  }
  findAcademicCalendarAttributesList() {
    this.academicCalendarService.findAttributesList().subscribe(
      response => {
        this.academicCalendars = response;
      },
      error => console.log(error),
      () => {}
    );
  }
  findClassesAttributesList() {
    this.classesService.findAttributesList().subscribe(
      response => {
        this.classes = response;
      },
      error => console.log(error),
      () => {}
    );
  }
  findCourseAttributesList() {
    this.courseService.findAttributesList().subscribe(
      response => {
        this.courses = response;
      },
      error => console.log(error),
      () => {}
    );
  }
  eventCourseSelection(eventValue: any) {
    if (this.courses) {
      this.courses.forEach(element => {
        if (element['id'] == eventValue) {
          this.courseAbbr = element['abbreviation'];
        }
      });
    }
  }
  createCourseItem(): FormGroup {
    return this.fb.group({
      courseId: ''
    });
  }
  addCoursesClick(): void {
    let preReqCoursesList = this.fg.get('preReqCoursesList') as FormArray;
    preReqCoursesList.push(this.createCourseItem());
  }
  removeCourses(i: number, item) {
    this.deletedCourses.push(item.value.courseId);
    const control = <FormArray>this.fg.controls['preReqCoursesList'];
    control.removeAt(i);
  }
}
