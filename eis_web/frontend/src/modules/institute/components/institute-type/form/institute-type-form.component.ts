import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { InstituteTypeService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { InstituteTypeModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-institute-type-form',
  templateUrl: './institute-type-form.component.html',
  styleUrls: ['./institute-type-form.component.css']
})
export class InstituteTypeFormComponent implements OnInit {

  public pageActions = GLOBALS.pageActions;
  public loaded: boolean = false;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public instituteType: InstituteTypeModel;

  public componentLabels = InstituteTypeModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private instituteTypeService: InstituteTypeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  /**
   * Initialize page
   */
  private initializePage() {

    this.fg = this.fb.group(new InstituteTypeModel().validationRules());

    if (this.pageAct === this.pageActions.create) {

      this.initCreatePage();

    } else {

      this.getData();

    }

  }

  /**
   * Get record
   */
  private getData() {

    this.route.params.subscribe(params => {

      this.instituteTypeService.find(params['id']).subscribe(response => {

        if (response) {
          this.instituteType = response;

          this.fg.patchValue(this.instituteType);

          //TODO:low need to check why we are dowing this after fetching data
          if (this.pageAct == this.pageActions.view) {
            this.initViewPage();
          } else if (this.pageAct == this.pageActions.update) {
            this.initUpdatePage();
          }
        }

      },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Init create page
   */
  private initCreatePage() {

    this.layoutService.setPageTitle({ title: 'Institute Type: Create' });

    this.instituteType = new InstituteTypeModel();

    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize the view page
   */
  private initViewPage() {

    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Institute Type: ' + this.instituteType.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        {
          label: 'Institute Type ',
          url: '/institute/instituteTypes'
        },
        { label: this.instituteType.name }
      ]
    });
  }

  /**
   * Initialize the update page
   */
  private initUpdatePage() {

    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Institute Type: ' + this.instituteType.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Institute Type', url: '/institute/instituteTypes' },
        {
          label: this.instituteType.name,
          url: `/institute/instituteTypes/view/${this.instituteType.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Create or Update record in database when save button is clicked
   * 
   */
  public saveData(item: InstituteTypeModel) {

    if (this.pageAct === this.pageActions.create) {

      this.instituteTypeService.create(item).subscribe(response => {

        this.router.navigate([`/institute/instituteTypes/view/${response.id}`]);

      },
        error => {
          console.log(error);
        }
      );
    }
    else if (this.pageAct === this.pageActions.update) {

      this.instituteTypeService.update(this.instituteType.id, item).subscribe(response => {

        this.router.navigate([`/institute/instituteTypes/view/${this.instituteType.id}`]);
      },
        error => {
          console.log(error);
        }
      );
    }
  }

  /**
   * Delete record
   * 
   */
  delete(id: number) {

    this.matDialog.open(ConfirmDialogComponent, {

      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }

    }).afterClosed().subscribe((accept: boolean) => {

      if (accept) {

        this.loaded = false;

        this.instituteTypeService.delete(id).subscribe(response => {

          this.router.navigate(['/institute/instituteTypes']);
        }, error => {
          console.log(error);
        });
      }
    });
  }
}
