import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog, MatSort } from '@angular/material';
import { Router } from '@angular/router';
import { GLOBALS } from 'modules/app/config/globals';
import { ConfirmDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { InstituteTypeService } from 'modules/institute/services';
import { InstituteTypeModel } from 'modules/institute/models';


@Component({
  selector: 'institute-institute-type-list',
  templateUrl: './institute-type-list.component.html',
  styleUrls: ['./institute-type-list.component.css']
})
export class InstituteTypeListComponent implements OnInit {

  public loaded: boolean = false;

  public data: InstituteTypeModel[] = [new InstituteTypeModel()];

  public attrLabels = InstituteTypeModel.attributesLabels;

  displayedColumns = ['name', 'options'];

  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private instituteTypeService: InstituteTypeService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Institute Type' });

    this.getRecords();
  }

  /**
   * Get data/records from backend
   */
  getRecords() {

    this.instituteTypeService.index().subscribe(response => {

      this.data = response;

      this.dataSource = new MatTableDataSource<InstituteTypeModel>(this.data);

      this.dataSource.paginator = this.paginator;

      this.dataSource.sort = this.sort;

    },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Apply filter and search in data grid
   */
  applyFilter(filterValue: string) {

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;

  }

  /**
   * Delete record
   */
  delete(id: number) {

    // Confirm dialog
    this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          //TODO:high: server is sending hardcoded sucess message and error
          // It should return true if delete and error responce in case of any problem
          // and our base servers errorHandler will handel that error.
          this.instituteTypeService.delete(id).subscribe(response => {
            this.getRecords();
          },
            error => {
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
}
