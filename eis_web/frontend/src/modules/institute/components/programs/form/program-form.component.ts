import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { ProgramService, FacultyService,EducationLevelService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { ProgramModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';
import { FacultyModel,EducationLevelModel } from 'modules/institute/models/index';

/**
 * This component is being used for three purposes.
 *  1: Add new Program Members
 *  2: View existing Program Members
 *  3: Update existing Program Members
 *
 */

@Component({
  selector: 'institute-program-form',
  templateUrl: './program-form.component.html',
  styleUrls: ['./program-form.component.css']
})
export class ProgramFormComponent implements OnInit {
  public loaded: boolean = false;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  private mNumber: ProgramModel;
  public faculties: FacultyModel[];
  public educationLevel:EducationLevelModel[];
  public fg: FormGroup;
  public pageAct: string;
  public program: ProgramModel;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ProgramModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programService: ProgramService,
    private facultyService: FacultyService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
    private educationLevelService:EducationLevelService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ProgramModel().validationRules());
    this.findFaculties();
    this.getEducationLevelList();
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.programService.find(+params['id']).subscribe(
        response => {
          this.program = response['data'];
          if (response) {
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            console.log(this.program);
            this.fg.patchValue(this.program);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
          if (!this.error) {
            if (this.pageAct === 'view') {
              this.initViewPage();
            } else if (this.pageAct === 'update') {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },

        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  findFaculties() {
    this.facultyService.index().subscribe(
      response => {
        this.faculties = response;
      },
      error => console.log(error),
      () => {}
    );
  }

  getEducationLevelList(){
    this.educationLevelService.index().subscribe(
      response => {
        this.educationLevel = response;
      },
      error => console.log(error),
      () => {}
    );
  }

  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Program' });
    this.program = new ProgramModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Program: ' + this.program.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program ', url: '/institute/programs' },
        { label: this.program.name }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Program: ' + this.program.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program', url: '/institute/programs' },
        {
          label: this.program.name,
          url: `/institute/programs/view/${this.program.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update faculty when save button is clicked
   */
  public saveData(item: ProgramModel) {
    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.programService.create(item).subscribe(
        response => {
          if (response) {
            id = response.data.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([`/institute/programs/view/${id}`]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    } else if (this.pageAct === 'update') {
      this.programService.update(this.program.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([
              `/institute/programs/view/${this.program.id}`
            ]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    }
  }

  /**
   * delete the selected program
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.programService.delete(id).subscribe(response => {
          // if (response) {
          //   this.success = true;
          //   this.successMessage = response.success;
            this.router.navigate(['institute/programs']);
          // } else {
          //   // Show error message
          //   this.error = true;
          //   this.errorMessage = response.error;
          // }
        }
        ,
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
      }
    });
  }
}
