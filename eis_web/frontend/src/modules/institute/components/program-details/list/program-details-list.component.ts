import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { ProgramDetailsService } from 'modules/institute/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { ProgramDetailsModel } from '../../../models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'institute-program-list',
  templateUrl: './program-details-list.component.html',
  styleUrls: ['./program-details-list.component.css']
})
export class ProgramDetailsListComponent implements OnInit {
  public loaded: boolean = false;
  public data: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public attrLabels = ProgramDetailsModel.attributesLabels;

  displayedColumns = ['name', 'programId', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private programDetailService: ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) {}

  /**
   * initialize the page of programs list
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Program Details' });
    this.getRecords();
  }

  /**
   * get all records
   */
  getRecords(): void {
    this.programDetailService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<ProgramDetailsModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * change alphabets into lowercase
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  /**
   * delete data against the id given
   */
  deleteProgram(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.programDetailService.delete(id).subscribe(response => {
          if (response) {
            setTimeout((router: Router) => {
              this.success = false;
              this.getRecords();
            }, 1000);
          } else {
            // Show error message
            this.error = true;
            this.errorMessage = response.error;
          }
        }
        ,error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
      }
    });
  }
}
