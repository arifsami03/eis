import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { ProgramService,ProgramDetailsService } from 'modules/institute/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { ProgramModel,ProgramDetailsModel } from 'modules/institute/models';
import { GLOBALS } from 'modules/app/config/globals';
import { FacultyModel,EducationLevelModel } from 'modules/institute/models/index';

/**
 * This component is being used for three purposes.
 *  1: Add new Program Members
 *  2: View existing Program Members
 *  3: Update existing Program Members
 *
 */

@Component({
  selector: 'institute-program-details-form',
  templateUrl: './program-details-form.component.html',
  styleUrls: ['./program-details-form.component.css']
})
export class ProgramDetailsFormComponent implements OnInit {
  public loaded: boolean = false;

  // success and Error
  public success: Boolean;
  public successMessage: String;
  public errorMessage: String;
  public error: Boolean;
  public programDetail: ProgramDetailsModel;
  public programs: ProgramModel[];
  public fg: FormGroup;
  public pageAct: string;
  public options: Observable<string[]>;
  public check = true;

  public componentLabels = ProgramDetailsModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private programService: ProgramService,
    private programDetailsService:ProgramDetailsService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService,
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * it will initialize the program form page
   */
  private initializePage() {
    this.fg = this.fb.group(new ProgramDetailsModel().validationRules());
    this.findPrograms();
    if (this.pageAct === 'add') {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get Data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.programDetailsService.find(+params['id']).subscribe(
        response => {
          this.programDetail = response['data'];
          if (response) {
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.fg.patchValue(this.programDetail);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
          if (!this.error) {
            if (this.pageAct === 'view') {
              this.initViewPage();
            } else if (this.pageAct === 'update') {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    });
  }

  findPrograms() {
    this.programService.getProgramsList().subscribe(
      response => {
        this.programs = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      }, () => {
        this.loaded = true;
      }
    );
  }



  /**
   * initialize page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Create Program Details' });
    this.programDetail = new ProgramDetailsModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * initialize view page
   */
  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Program Detail: ' + this.programDetail.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program Detail', url: '/institute/programDetails' },
        { label: this.programDetail.name }
      ]
    });
  }

  /**
   * initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Program Detail: ' + this.programDetail.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Program Detail', url: '/institute/programDetails' },
        {
          label: this.programDetail.name,
          url: `/institute/programDetails/view/${this.programDetail.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * add or update faculty when save button is clicked
   */
  public saveData(item: ProgramModel) {
    let id;
    if (this.pageAct === 'add') {
      item.id = null;
      this.programDetailsService.create(item).subscribe(
        response => {
          if (response) {
            id = response.data.id;
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([`/institute/programDetails/view/${id}`]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    } else if (this.pageAct === 'update') {
      this.programDetailsService.update(this.programDetail.id, item).subscribe(
        response => {
          if (response) {
            this.success = true;
            this.successMessage = response.success;
            this.router.navigate([
              `/institute/programDetails/view/${this.programDetail.id}`
            ]);
          } else {
            this.error = true;
            this.errorMessage = response.error;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
    }
  }

  /**
   * delete the selected program
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.programDetailsService.delete(id).subscribe(response => {
          if (response) {
            
            this.router.navigate(['institute/programDetails']);
          } else {
            // Show error message
            this.error = true;
            this.errorMessage = response.error;
          }
        }
        ,
        error => {
          console.log(error);
          this.loaded = true;
        }, () => {
          this.loaded = true;
        }
      );
      }
    });
  }
}
