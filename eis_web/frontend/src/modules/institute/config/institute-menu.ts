export const InstituteMenu = [
  {
    heading: 'Institute',
    menuItems: [
      { title: 'Dashboard', url: ['/institute/dashboard'], icon: 'dashboard', perm: 'institutes.index' },
      {
        title: 'Academic Calendar',
        url: ['/institute/academicCalendars'],
        icon: 'dashboard',
        perm: 'academicCalendars.index'
      },
      { title: 'Roadmap', url: ['/institute/roadMaps'], icon: 'dashboard', perm: 'roadMaps.index' }
    ]
  },
  {
    heading: 'Programs',
    menuItems: [
      { title: 'Institute Type', url: ['/institute/instituteType'], icon: 'view_list', perm: 'instituteTypes.index' },
      {
        title: 'Level of Education',
        url: ['/institute/educationLevel'],
        icon: 'view_list',
        perm: 'educationLevels.index'
      },
      { title: 'Programs', url: ['/institute/programs'], icon: 'list', perm: 'programs.index' },
      { title: 'Program Details', url: ['/institute/programDetails'], icon: 'list', perm: 'programDetails.index' },
      { title: 'Faculties', url: ['/institute/faculties'], icon: 'list', perm: 'faculties.index' },
      { title: 'License Fee', url: ['/institute/licenseFees'], icon: 'list', perm: 'licenseFees.index' },
      { title: 'Course', url: ['/institute/courses'], icon: 'list', perm: 'courses.index' },
      { title: 'Classes', url: ['/institute/classes'], icon: 'list', perm: 'classes.index' }
    ]
  }
];
