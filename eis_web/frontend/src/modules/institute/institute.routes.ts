import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  InstituteComponent,
  InstituteViewComponent,
  FacultyListComponent,
  FacultyFormComponent,
  FacultyViewComponent,
  ProgramListComponent,
  ProgramFormComponent,
  ProgramDetailsFormComponent,
  ProgramDetailsListComponent,
  EducationLevelListComponent,
  EducationLevelFormComponent,
  InstituteTypeFormComponent,
  InstituteTypeListComponent,
  LicenseFeeFormComponent,
  LicenseFeeListComponent,
  CourseFormComponent,
  CourseListComponent,
  ClassesListComponent,
  ClassesFormComponent,
  AcademicCalendarFormComponent,
  AcademicCalendarListComponent,
  RoadMapFormComponent,
  RoadMapListComponent
} from './components';
import { GLOBALS } from '../app/config/globals';
/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'dashboard',
    data: { breadcrumb: { title: 'Institute', display: true } },
    children: [
      {
        path: '',
        component: InstituteViewComponent,
        data: { breadcrumb: { title: 'Dashboard', display: false } }
      },
      {
        path: 'update',
        component: InstituteComponent,
        data: { breadcrumb: { title: 'Update', display: true } }
      }
    ]
  },
  {
    path: 'academicCalendars',
    data: { breadcrumb: { title: 'Academic Calendar', display: true } },
    children: [
      {
        path: '',
        component: AcademicCalendarListComponent,
        data: { breadcrumb: { title: 'Academic Calendar', display: false } }
      },
      {
        path: 'create',
        component: AcademicCalendarFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: AcademicCalendarFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: AcademicCalendarFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'roadMaps',
    data: { breadcrumb: { title: 'Roadmap', display: true } },
    children: [
      {
        path: '',
        component: RoadMapListComponent,
        data: { breadcrumb: { title: 'Roadmap', display: false } }
      },
      {
        path: 'create',
        component: RoadMapFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: RoadMapFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: RoadMapFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'faculties',
    data: { breadcrumb: { title: 'Faculties', display: true } },
    children: [
      {
        path: '',
        component: FacultyListComponent,
        data: { breadcrumb: { title: 'Faculties', display: false } }
      },
      {
        path: 'create',
        component: FacultyFormComponent,
        data: {
          act: 'create',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: FacultyViewComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: FacultyFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'programs',
    data: { breadcrumb: { title: 'Programs', display: true } },
    children: [
      {
        path: '',
        component: ProgramListComponent,
        data: { breadcrumb: { title: 'Programs', display: false } }
      },
      {
        path: 'add',
        component: ProgramFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: ProgramFormComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: ProgramFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'programDetails',
    data: { breadcrumb: { title: 'Program Details', display: true } },
    children: [
      {
        path: '',
        component: ProgramDetailsListComponent,
        data: { breadcrumb: { title: 'Programs', display: false } }
      },
      {
        path: 'add',
        component: ProgramDetailsFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: ProgramDetailsFormComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: ProgramDetailsFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'classes',
    data: { breadcrumb: { title: 'Classes', display: true } },
    children: [
      {
        path: '',
        component: ClassesListComponent,
        data: { breadcrumb: { title: 'Classes', display: false } }
      },
      {
        path: 'add',
        component: ClassesFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: ClassesFormComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: ClassesFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'educationLevels',
    data: { breadcrumb: { title: 'Level of Education', display: true } },
    children: [
      {
        path: '',
        component: EducationLevelListComponent,
        data: { breadcrumb: { title: 'Level of Education', display: false } }
      },
      {
        path: 'create',
        component: EducationLevelFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: EducationLevelFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: EducationLevelFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'instituteTypes',
    data: { breadcrumb: { title: 'Institute Type', display: true } },
    children: [
      {
        path: '',
        component: InstituteTypeListComponent,
        data: { breadcrumb: { title: 'Institute Type', display: false } }
      },
      {
        path: 'create',
        component: InstituteTypeFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: InstituteTypeFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: InstituteTypeFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'licenseFees',
    data: { breadcrumb: { title: 'License Fee', display: true } },
    children: [
      {
        path: '',
        component: LicenseFeeListComponent,
        data: { breadcrumb: { title: 'License Fee', display: false } }
      },
      {
        path: 'create',
        component: LicenseFeeFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: LicenseFeeFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: LicenseFeeFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'courses',
    data: { breadcrumb: { title: 'Course', display: true } },
    children: [
      {
        path: '',
        component: CourseListComponent,
        data: { breadcrumb: { title: 'Course', display: false } }
      },
      {
        path: 'create',
        component: CourseFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: CourseFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: CourseFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InstituteRoutes { }
