import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from 'modules/shared/services';
import { FacultyModel } from '../models/faculty';

@Injectable()
export class FacultyService extends BaseService {
  private routeURL: String = 'institute/faculties';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<FacultyModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <FacultyModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<FacultyModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <FacultyModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(faculty: FacultyModel) {
    return this.__post(`${this.routeURL}/create`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, faculty: FacultyModel) {
    return this.__put(`${this.routeURL}/update/${id}`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Get All Data
   */
  getFaculties(ids) {
    return this.__post(`${this.routeURL}/getFaculties`, ids).map(data => {
      return data.json();
    });
  }

  /**
   * Get All Programs With Faculties
   */
  getFacultiesWithPrograms() {
    return this.__get(`${this.routeURL}/getFacultiesWithPrograms`).map(data => {
      return data.json();
    });
  }

  getFacultiesWithProgramDetails() {
    return this.__get(`${this.routeURL}/getFacultiesWithProgramDetails`).map(data => {
      return data.json();
    });
  }
}
