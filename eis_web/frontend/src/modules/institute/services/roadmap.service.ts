import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from 'modules/shared/services';
import { RoadMapModel } from 'modules/institute/models';

@Injectable()
export class RoadMapService extends BaseService {
  private routeURL: String = 'institute/roadMaps';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<RoadMapModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <RoadMapModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<RoadMapModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <RoadMapModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: RoadMapModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: RoadMapModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  /**
   * find record
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
}
