import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'modules/shared/services';
import { EducationLevelModel } from 'modules/institute/models';

@Injectable()
export class EducationLevelService extends BaseService {

  private routeURL: String = 'institute/educationLevels';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<EducationLevelModel[]> {

    return this.__get(`${this.routeURL}/index`).map(data => {
      return <EducationLevelModel[]>data.json();
    });

  }

  /**
   * Get single record
   */
  find(id: number): Observable<EducationLevelModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <EducationLevelModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(item: EducationLevelModel) {
    return this.__post(`${this.routeURL}/create`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item: EducationLevelModel) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }
  /**
     * find record
     */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }
}
