import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { LicenseFeeModel } from '../models/';
import { BaseService } from 'modules/shared/services';

@Injectable()
export class LicenseFeeService extends BaseService {
  private routeURL: String = 'institute/licenseFees';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * return data
   */
  index(): Observable<LicenseFeeModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <LicenseFeeModel[]>data.json();
    });
  }

  /**
   * Return single Data
   */
  find(id: number): Observable<LicenseFeeModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <LicenseFeeModel>data.json();
    });
  }

  /**
   * Delete Record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create Record
   */
  create(licenseFee: LicenseFeeModel) {
    return this.__post(`${this.routeURL}/create`, licenseFee).map(data => {
      return data.json();
    });
  }

  /**
   * Update Record
   */
  update(id: number, licenseFee: LicenseFeeModel) {
    return this.__put(`${this.routeURL}/update/${id}`, licenseFee).map(data => {
      return data.json();
    });
  }
}
