import { Injectable, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { environment } from '../../../environments/environment';

import { ProgramModel } from '../models/';
import { BaseService } from 'modules/shared/services';

@Injectable()
export class ProgramDetailsService extends BaseService {
  private routeURL: String = 'institute/programDetails';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * it will return all the data of programs
   */
  index() {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return data.json();
    });
  }
  /**
   * it will return all the data of programs
   */
  findAttributesList() {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return data.json();
    });
  }

  /**
   * it will return the data of a program against the id
   */
  find(id: number) {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will delete the selected program record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * it will create a new program entry
   */
  create(faculty: ProgramModel) {
    return this.__post(`${this.routeURL}/create`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * it will update the selected program record
   */
  update(id: number, faculty: ProgramModel) {
    return this.__put(`${this.routeURL}/update/${id}`, faculty).map(data => {
      return data.json();
    });
  }
  getPrograms(id: number) {
    return this.__get(`${this.routeURL}/getPrograms/${id}`).map(data => {
      return data.json();
    });
  }

  getFacultyProgramDetails(id: number) {
    return this.__get(`${this.routeURL}/getFacultyProgramDetails/${id}`).map(data => {
      return data.json();
    });
  }
  getProgramDetails() {
    return this.__get(`${this.routeURL}/getProgramDetails`).map(data => {
      return data.json();
    });
  }
}
