// Services
export * from './institute.service';
export * from './faculty.service';
export * from './program.service';
export * from './education-level.service';
export * from './program-details.service';
export * from './institute-type.service';
export * from './license-fee.service';
export * from './course.service';
export * from './classes.service';
export * from './academic-calendar.service';
export * from './faculty-program-details.service';
export * from './roadmap.service';
