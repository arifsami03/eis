import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';
import { BaseService } from 'modules/shared/services';
import { FacultyProgramDetailsModel } from 'modules/institute/models';
import { ProgramDetailsModel } from 'modules/institute/models';

@Injectable()
export class FacultyProgramDetailsService extends BaseService {
  private routeURL: String = 'institute/facultyProgramDetails';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(id: number): Observable<FacultyProgramDetailsModel[]> {
    return this.__get(`${this.routeURL}/index/${id}`).map(data => {
      return <FacultyProgramDetailsModel[]>data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, item) {
    return this.__put(`${this.routeURL}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  /**
   * Get All program details 
   */
  findAllProgramDetails(): Observable<ProgramDetailsModel[]> {
    return this.__get(`${this.routeURL}/findAllProgramDetails`).map(data => {
      return <ProgramDetailsModel[]>data.json();
    });
  }

}
