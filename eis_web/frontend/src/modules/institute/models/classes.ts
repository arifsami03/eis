import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ClassesModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    name: 'Classes',
    programDetail:'Program Details',
    educationLevel:'Level of Education',
    program:'Program',
    
  };

  id?: number;
  programDetailId:number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ]),
      programDetailId: new FormControl('', [<any>Validators.required]),
      
    };
  }
}
