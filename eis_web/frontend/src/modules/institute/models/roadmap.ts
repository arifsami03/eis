import { GLOBALS } from 'modules/app/config/globals';
import { FormControl, Validators, AbstractControl, ValidatorFn, FormArray } from '@angular/forms';

export class RoadMapModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    title: 'Roadmap Title',
    programDetailId: 'Program Details',
    academicCalendarId: 'Academic Calendar for Roadmap',
    courseId: 'Course Name',
    classId: 'Select Semester',
    courseCode: 'Course Code',
    natureOfCourse: 'Nature of Course?',
    courseType: 'Course Type?',
    courseAbbreviation: 'Course',
    courseOrder: 'Course Order',
    creditHours: 'Credit Hours',
    preReqCoursesList: 'Pre Requisite Course'
  };

  id?: number;
  title: string;
  programDetailId: number;
  academicCalendarId: number;
  courseId: number;
  classId: number;
  courseCode: number;
  natureOfCourse: string;
  courseType: string;
  courseOrder: number;
  creditHours: number;
  preReqCoursesList: any[]; //new FormArray([]); //any[]=[]; //TODO:Low Have to remove any;;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
      [key: string]: any;
    } => {
      let controlMatch = 1;

      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
            equalTo: true
          };
    };
  }
  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      title: new FormControl('', [<any>Validators.required]),
      programDetailId: new FormControl('', [<any>Validators.required]),
      academicCalendarId: new FormControl('', [<any>Validators.required]),
      courseId: new FormControl('', [<any>Validators.required]),
      classId: new FormControl('', [<any>Validators.required]),
      courseCode: new FormControl('', [<any>Validators.required]),
      natureOfCourse: new FormControl(GLOBALS.natureOfCourses.cumpulsory.value, [<any>Validators.required]),
      courseType: new FormControl(GLOBALS.courseTypes.theoratical.value, [<any>Validators.required]),
      courseOrder: new FormControl('', this.greaterThan('courseOrder')),
      creditHours: new FormControl(''),
      preReqCoursesList: new FormArray([])
    };
  }
}
