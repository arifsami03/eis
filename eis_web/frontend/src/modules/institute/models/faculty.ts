import { FormControl, Validators } from '@angular/forms';

export class FacultyModel {
  /**
   * set the labels for attributes
   */
  static attributesLabels = {
    name: 'Faculty Name'
  };

  id?: number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ])
    };
  }
}
