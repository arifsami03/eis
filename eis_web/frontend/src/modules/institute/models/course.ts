import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CourseModel {
  /**
   * Set the labels for attributes
   */
  static attributesLabels = {
    title: 'Course Title',
    abbreviation: 'Course Abbreviation',
    description: 'Description'
  };

  id?: number;
  title: string;
  abbreviation: string;
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: new FormControl('', [
        <any>Validators.required
      ]),
      abbreviation: new FormControl('', [<any>Validators.required]),
      description: new FormControl('', [<any>Validators.required])
    };
  }
}
