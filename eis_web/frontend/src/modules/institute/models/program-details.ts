import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ProgramDetailsModel {
  /**
   * it will set the labels for attributes of program
   */
  static attributesLabels = {
    name: 'Program Detail',
    programId: 'Program',
  };

  id?: number;
  programId: number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ]),
      programId: new FormControl('', [<any>Validators.required]),
    };
  }
}
