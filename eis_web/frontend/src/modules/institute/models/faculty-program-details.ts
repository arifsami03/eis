import { ProgramDetailsModel } from 'modules/institute/models';

export class FacultyProgramDetailsModel {

  /**
     * it will set the labels for attributes of Faculty
     */
  static attributesLabels = {
    program: 'Program',
    programDetail: 'Program Detail',
    status: 'Status',
  };


  id?: number;
  facultyId?: number;
  status?: boolean;
  programDetailId?: number;
  programDetail?: ProgramDetailsModel;

  constructor() { }

}
