import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class LicenseFeeModel {
  /**
   * it will set the labels for attributes
   */
  static attributesLabels = {
    amount: 'Amount',
    educationLevelId: 'Level Of Education'
  };

  id?: number;
  educationLevelId: number;
  amount: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      amount: new FormControl('', [
        <any>Validators.required
      ]),
      educationLevelId: new FormControl('', [<any>Validators.required]),
    };
  }
}
