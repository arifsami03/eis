import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class ProgramModel {
  /**
   * it will set the labels for attributes of program
   */
  static attributesLabels = {
    name: 'Program Name',
    facultyId: 'Faculty',
    educationLevelId:'Level Of Education'
  };

  id?: number;
  educationLevelId:number;
  name: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for program
   */
  public validationRules?() {
    return {
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ]),
      educationLevelId: new FormControl('', [<any>Validators.required]),
    };
  }
}
