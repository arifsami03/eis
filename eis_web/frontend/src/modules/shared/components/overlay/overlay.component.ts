import { Component } from '@angular/core';
/**
 * Overlay Component
 *
 * How to use?
 *
 * <shared-overlay *ngIf="!loaded"> </shared-overlay>
 */
@Component({
  selector: 'shared-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.css']
})
export class OverlayComponent {
  constructor() {}
}
