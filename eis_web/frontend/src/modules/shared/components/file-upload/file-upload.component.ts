import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'shared-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
  @ViewChild('fileInput') fileInput;

  fileName: string = '';
  constructor() {}

  ngOnInit() {}
  // TODO: Backend to be implemented
  upload() {
    const fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      this.fileName = fileBrowser.files[0]['name'];
      const formData = new FormData();
      formData.append('image', fileBrowser.files[0]);
      // this.projectService.upload(formData, this.project.id).subscribe(res => {
      //   // do stuff w/my uploaded file
      // });
    }
  }
}
