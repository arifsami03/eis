import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from 'environments/environment';
import { TokenService } from './';

@Injectable()
export class BaseService {

  constructor(protected http: Http) {
    this.setHeaders();
  }

  private setHeaders() {
    let headers = new Headers();
    headers.append('token', localStorage.getItem('token'));
    // this.setRequestOptions(this.__headers);
    return new RequestOptions({ headers: headers })
  }

  // private setRequestOptions(headers: Headers) {
  //   this.__requestOptions = new RequestOptions({ headers: headers });
  // }

  protected __get(url) {
    return this.http.get(
      `${environment.backendAPIURL}/${url}`,
      this.setHeaders()
    );
  }

  __put(url, putBody) {

    return this.http.put(
      `${environment.backendAPIURL}/${url}`,
      putBody,
      this.setHeaders()
    );
  }

  __post(url, postBody) {

    return this.http.post(
      `${environment.backendAPIURL}/${url}`,
      postBody,
      this.setHeaders()
    );
  }

  __delete(url) {

    return this.http.delete(
      `${environment.backendAPIURL}/${url}`,
      this.setHeaders()
    );
  }

  /**
   * handle error
   *
   * @param error Response
   */
  protected handleError(error) {
    console.log('-------------------');
    console.log(error);
    // if (error.json()['message'] == 'jwt expired') {

    //   TokenService.instance1.setTokenMessage('jwt expired');

    // }

    return Observable.throw(
      { message: error.json()['message'], status: error.status } ||
      'Server error'
    );
  }
}
