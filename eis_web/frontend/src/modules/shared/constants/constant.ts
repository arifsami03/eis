export const supervisor = {
    id: 1
  },
  assignment = {
    ASSIGNEDTODEPARTMENT: 'assigned',
    ASSIGNEDTOEMPLOYEE: 'pending',
    TOEMPLOYEE: 'employee',
    TODEPARTMENT: 'department',
    TOMS: 'ms'
  };

export const mobileNumberMask = [
  /[0-9]/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

export const cnicMask = [
  /[0-9]/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/
];
