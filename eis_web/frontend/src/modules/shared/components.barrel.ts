// Import components and services etc here

import {
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatProgressSpinnerModule
} from '@angular/material';

import { DownloadPdfService } from 'modules/shared/services';

import {
  CMatErrorComponent,
  CMatSuccessComponent,
  ConfirmDialogComponent,
  OverlayComponent,
  FileUploadComponent,
  AlertDialogComponent
} from './components';

import { PermissionDirective } from './directives';

import { CapitalizePipe } from './helper';

export const __IMPORTS = [
  MatFormFieldModule,
  MatIconModule,
  MatDialogModule,
  MatButtonModule,
  MatProgressSpinnerModule
];

export const __DECLARATIONS = [
  CMatErrorComponent,
  CMatSuccessComponent,
  ConfirmDialogComponent,
  AlertDialogComponent,
  OverlayComponent,
  CapitalizePipe,
  FileUploadComponent,
  PermissionDirective
];

export const __PROVIDERS = [DownloadPdfService];

export const __ENTRY_COMPONENTS = [
  ConfirmDialogComponent,
  AlertDialogComponent
];
