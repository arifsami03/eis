// import { UserFormComponent } from 'modules/app/security/users/form/user-form.component';
// Import components and services etc here
import { MetaConfService } from 'modules/configuration/services';
import { CampusService } from 'modules/public/services';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import { NgxCarouselModule } from 'ngx-carousel';
import { EligibilityCriteriaComponent } from 'modules/public/components';
import { DownloadPdfService } from 'modules/shared/services';

import {
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatSelectModule,
  MatDatepickerModule,
  MatRadioModule
} from '@angular/material';

import {
  HomeComponent,
  ApplyForRegistrationComponent,
  CampusPreRegistrationComponent,
  ThankyouComponent
} from './components';

export const __IMPORTS = [
  CommonModule,
  FormsModule,
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatButtonModule,
  MatCardModule,
  MatDatepickerModule,
  ReactiveFormsModule,
  TextMaskModule,
  MatSelectModule,
  FlexLayoutModule,
  NgxCarouselModule,
  MatRadioModule
];

export const __DECLARATIONS = [
  HomeComponent,
  ApplyForRegistrationComponent,
  CampusPreRegistrationComponent,
  ThankyouComponent,
  EligibilityCriteriaComponent
];

export const __PROVIDERS = [MetaConfService, CampusService, DownloadPdfService];

export const __ENTRY_COMPONENTS = [];
