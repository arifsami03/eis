import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  HomeComponent,
  ApplyForRegistrationComponent,
  CampusPreRegistrationComponent,
  ThankyouComponent,
  EligibilityCriteriaComponent
} from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'applyForRegistration', component: ApplyForRegistrationComponent },
  { path: 'campusPreRegistration', component: CampusPreRegistrationComponent },
  { path: 'thankyou', component: ThankyouComponent },
  { path: 'eligibilityCriteria', component: EligibilityCriteriaComponent }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutes {}
