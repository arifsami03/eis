import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/services/base.service';

@Injectable()
export class CampusService extends BaseService {
  private _url: String = 'campus';

  constructor(protected http: Http) {
    super(http);
  }

  add(item: any) {
    return this.__post(`${this._url}/preRegistration`, item)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  validateUniqueEmail(_email: any) {
    return this.__post(
      `${this._url}/preRegistration/validateUniqueEmail`,
      _email
    )
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
}
