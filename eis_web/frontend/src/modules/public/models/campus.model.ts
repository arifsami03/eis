import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class Campus {
  id: number;
  campusOwnerId: number;
  campusName: string;
  levelOfEducation: string;
  tentativeSessionStart: Date;
  campusLocation: string;
  applicationType: string;

  buildingAvailable: boolean;
  cityId: number;
  status: string;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    campusOwnerId: 'Campus Owner',
    campusName: 'Campus Name',
    levelOfEducation: 'Level of Education',
    tentativeSessionStart: 'Tentative session start',
    campusLocation: 'Campus Location',
    buildingAvailable: 'Building Available',
    status: 'Status'
  };

  constructor() { }

  public validationRules?() {
    return {
      campusOwnerId: new FormControl('', [Validators.required]),
      campusName: new FormControl('', [
        Validators.required,
        Validators.maxLength(55)
      ]),
      levelOfEducation: new FormControl(''),
      tentativeSessionStart: new FormControl(''),
      campusLocation: new FormControl(''),
      cityId: new FormControl(''),
      buildingAvailable: new FormControl('')
    };
  }
}
