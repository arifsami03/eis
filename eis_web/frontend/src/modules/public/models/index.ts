export * from './campus-pre-registration.model';
export * from './campus-available-building.model';
export * from './campus-owner-reference.model';
export * from './campus-owner.model';
export * from './campus-targetted-loacation.model';
export * from './campus.model';
