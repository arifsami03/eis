import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CampusOwnerReference {
  id: number;
  campusOwnerId: number;
  fullName: string;
  mobileNumber: string;
  address: string;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    campusOwnerId: 'Campus Owner',
    fullName: 'Full Name',
    mobileNumber: 'Mobile Number',
    address: 'Address'
  };

  constructor() {}

  public validationRules?() {
    return {
      campusOwnerId: new FormControl('', [Validators.required]),
      fullName: new FormControl('', [
        Validators.required,
        Validators.maxLength(55)
      ]),
      mobileNumber: new FormControl('', [
        Validators.pattern('([0-9]{4})-([0-9]{7})'),
        Validators.maxLength(20)
      ]),
      address: new FormControl('')
    };
  }
}
