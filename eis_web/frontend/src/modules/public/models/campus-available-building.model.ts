import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CampusAvailableBuilding {
  id: number;
  campusId: number;
  buildingOwn: boolean;
  rentAgreementUpTo: string;
  address: string;
  latitude: number;
  longitude: number;
  coverdArea: number;
  openArea: number;
  totalArea: number;
  roomsQuantity: number;
  washroomsQuantity: number;
  teachingStaffQuantity: number;
  labsQuantity: number;
  nonTeachingStaffQuantity: number;
  studentsQuantity: number;
  playGround: boolean;
  swimmingPool: boolean;
  healthClinic: boolean;
  mosque: boolean;
  cafeteria: boolean;
  transport: boolean;
  library: boolean;
  bankBranch: boolean;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    campusId: 'Campus',
    buildingOwn: 'Own Building',
    rentAgreementUpTo: 'Rent Agrrement To',
    address: 'Address',
    latitude: 'Latitude',
    longitude: 'Longitude',
    coverdArea: 'Coverd Area',
    openArea: 'Open Area',
    totalArea: 'Total Area',
    roomsQuantity: 'Rooms Quanitity',
    washroomsQuantity: 'Washroom Quanitity',
    teachingStaffQuantity: 'Technical Staff Quantity',
    labsQuantity: 'Labs Quanitity',
    nonTeachingStaffQuantity: 'Non Technical Staff Quanitity',
    studentsQuantity: 'Student Quanitity',
    playGround: 'Paly Ground',
    swimmingPool: 'Swimming Pool',
    healthClinic: 'Health Clinic',
    mosque: 'Mosque',
    cafeteria: 'Cafeteria',
    transport: 'Transport',
    library: 'Library',
    bankBranch: 'Bank Branch'
  };

  constructor() {}

  public validationRules?() {
    return {
      campusId: new FormControl('', [Validators.required]),
      buildingOwn: new FormControl('', [Validators.required]),
      rentAgreementUpTo: new FormControl(''),
      address: new FormControl(''),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      coverdArea: new FormControl(''),
      openArea: new FormControl(''),
      totalArea: new FormControl(''),
      roomsQuantity: new FormControl(''),
      washroomsQuantity: new FormControl(''),
      teachingStaffQuantity: new FormControl(''),
      labsQuantity: new FormControl(''),
      nonTeachingStaffQuantity: new FormControl(''),
      studentsQuantity: new FormControl(''),
      playGround: new FormControl(''),
      swimmingPool: new FormControl(''),
      healthClinic: new FormControl(''),
      mosque: new FormControl(''),
      cafeteria: new FormControl(''),
      transport: new FormControl(''),
      library: new FormControl(''),
      bankBranch: new FormControl('')
    };
  }
}
