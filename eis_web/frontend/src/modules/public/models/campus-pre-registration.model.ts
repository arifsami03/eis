import { CampusService } from 'modules/public/services';
import { GLOBALS } from 'modules/app/config/globals';
import { MetaConfModel } from 'modules/configuration/models';
import { CampusOwner } from './campus-owner.model';
import {
  FormControl,
  Validators,
  ValidatorFn,
  FormGroup,
  AbstractControl
} from '@angular/forms';

export class CampusPreRegistration {
  id?: number;
  fullName?: string;
  cnic?: string;
  mobileNumber?: string;
  email?: string;
  applicationType: string;
  campus?: string;
  cityId?: number;
  tentativeSessionStart?: Date;
  password?: string;
  confirmPassword?: string;
  cityName?: string;
  campusOwner?: CampusOwner = new CampusOwner();
  city?: MetaConfModel = new MetaConfModel();
  status?: string;

  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  listLabels = {
    fullName: 'Applicant Name',
    email: 'Email',
    applicationType: 'Application Type',
    city: 'City',
    status: 'Status'
  };

  labels = {
    fullName: 'Full Name',
    cnic: 'CNIC',
    mobileNumber: 'Mobile Number',
    email: 'Email',
    applicationType: 'Application Type',
    cityId: 'City ( Target Campus Location )',
    tentativeSessionStart: 'Tentative Session Start',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    sessionStartDate: 'Session Start Date',
    status: 'Status',
    createdAt: 'Created At'
  };

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName])
        throw new TypeError(
          'Form Control ' + equalControlName + ' does not exists.'
        );

      let controlMatch = control['_parent'].controls[equalControlName];

      return controlMatch.value == control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  public validationRules?() {
    return {
      fullName: new FormControl('', [
        Validators.required,
        Validators.maxLength(30)
      ]),
      cnic: new FormControl('', [
        Validators.required,
        Validators.pattern('([0-9]{5})-([0-9]{7})-([0-9]{1})'),
        Validators.maxLength(16)
      ]),
      mobileNumber: new FormControl('', [
        Validators.required,
        Validators.pattern('([0-9]{4})-([0-9]{7})'),
        Validators.maxLength(13)
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.maxLength(30)
      ]),
      applicationType: new FormControl(
        GLOBALS.campusApplicationType.create.value,
        [Validators.required]
      ),
      cityId: new FormControl('', [Validators.required]),
      tentativeSessionStart: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(30)
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('password')
      ])
    };
  }
}
