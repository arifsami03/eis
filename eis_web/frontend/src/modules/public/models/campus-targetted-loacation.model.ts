import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class CampusTargettedLocations {
  id: number;
  campusId: number;
  address: string;
  latitude: number;
  longitude: number;
  createdBy: number;
  updatedBy: number;
  createdAt: Date;
  updatedAt: Date;

  labels = {
    campusId: 'Campus',
    address: 'Address',
    latitude: 'Latitude',
    longitude: 'Longitude'
  };

  constructor() {}

  public validationRules?() {
    return {
      campusOwnerId: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      latitude: new FormControl(''),
      longitude: new FormControl('')
    };
  }
}
