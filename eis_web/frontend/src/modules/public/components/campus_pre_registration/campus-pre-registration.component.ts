import { Component, OnInit } from '@angular/core';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  FormControl
} from '@angular/forms';
import { Router } from '@angular/router';

import {
  CampusPreRegistration,
  Campus,
  CampusOwner
} from 'modules/public/models';
import { } from 'modules/security/models';

import { CampusService } from 'modules/public/services';
import { MetaConfService } from 'modules/configuration/services';
import { MetaConfModel } from 'modules/configuration/models';
import { mobileNumberMask, cnicMask } from 'modules/shared/constants/constant';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'app-campus-pre-registration',
  templateUrl: './campus-pre-registration.component.html',
  styleUrls: ['./campus-pre-registration.component.css']
})
export class CampusPreRegistrationComponent implements OnInit {
  public loaded: boolean = true;
  formGroup: FormGroup;
  campusPreRegistration: CampusPreRegistration;
  cities: MetaConfModel[];
  applicationTypes: any[] = [
    GLOBALS.campusApplicationType.create,
    GLOBALS.campusApplicationType.transfer
  ];

  componentsLabel = new CampusPreRegistration().labels;

  public _mobileNumberMask = mobileNumberMask;
  public _cnicMask = cnicMask;

  public isValidEmail: boolean;

  campus: Campus;

  /**
   *
   * @param formBuilder for angular reactive forms
   */
  constructor(
    private formBuilder: FormBuilder,
    private metaConfService: MetaConfService,
    private campusService: CampusService,
    private router: Router
  ) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.getCities();
    this.initForm();
  }

  initForm() {
    /**
     * initializing from Group
     */
    this.formGroup = this.formBuilder.group(
      new CampusPreRegistration().validationRules()
    );
    /**
     * for Datepicker
     */

    this.formGroup.enable();
  }

  saveData(data: any) {
    //TODO:low
    // Check for unique email adrress
    this.loaded = false;
    this.campusService
      .validateUniqueEmail({ email: data.email })
      .subscribe(res => {
        if (!res) {
          const _campusOwner: CampusOwner = new CampusOwner();
          _campusOwner.fullName = data.fullName;
          _campusOwner.mobileNumber = data.mobileNumber;
          _campusOwner.cnic = data.cnic;
          _campusOwner.email = data.email;
          _campusOwner.applicationType = data.applicationType;

          let _user: any = {};
          _user['password'] = data.password;
          _user['username'] = data.email;
          //TODO need to change this portal
          _user['portal'] = 'registration';
          _user['isActive'] = false;

          const _campus: Campus = new Campus();
          _campus.tentativeSessionStart = data.tentativeSessionStart;
          _campus.cityId = data.cityId;
          _campus.status = data.status;
          _campus.applicationType = data.applicationType;

          const _campusPreRegistration = {
            campusOwner: _campusOwner,
            campus: _campus,
            user: _user
          };

          this.campusService.add(_campusPreRegistration).subscribe(
            response => {
              this.loaded = true;
              this.router.navigate(['/home/thankyou']);
            },
            error => {
              this.loaded = true;
            }
          );
        } else {
          this.formGroup.controls['email'].setErrors({ exist: true });
          this.loaded = true;
        }
      });
  }
  cancel() {
    window.history.back();
  }

  getCities() {
    this.metaConfService.getCities().subscribe(
      response => {
        this.cities = response;
      },
      error => console.log(error),
      () => { }
    );
  }
}
