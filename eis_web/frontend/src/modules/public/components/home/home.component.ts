import { Component, OnInit, Input } from '@angular/core';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';

import {
  FormControl,
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';

import { LoginService } from 'modules/security/services';
import { Login } from 'modules/security/models';
import 'hammerjs';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [LoginService]
})
export class HomeComponent implements OnInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;

  public fg: FormGroup;

  public error: Boolean = false;

  public attributesLabels = Login.attributesLabels;

  public authError: string;

  constructor(
    private activatedRoute: Router,
    private loginService: LoginService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.fg = this.fb.group(new Login().validationRules());
    this.carouselTileItems = [
      '../../../../assets/images/home5.jpg',
      '../../../../assets/images/home6.jpg',
      '../../../../assets/images/home8.jpeg',
      '../../../../assets/images/home9.jpg',
      '../../../../assets/images/home4.gif',
      '../../../../assets/images/home2.jpg',
      '../../../../assets/images/home7.jpeg',
      '../../../../assets/images/home3.jpg'
    ];

    this.carouselTile = {
      grid: { xs: 1, sm: 2, md: 3, lg: 4, all: 0 },
      slide: 1,
      speed: 400,
      animation: 'lazy',
      point: {
        visible: true
      },
      load: 3,
      touch: true,
      easing: 'ease'
    };
  }
  /**
   * Login If user is valid
   * @param form NgForm
   */
  login(item: Login) {
    this.loginService.login(item).subscribe(
      response => {
        if (response) {
          if (response.toString() === 'registration') {
            this.activatedRoute.navigate([
              '/post-registration/campus/personalinfo'
            ]);
          } else {
            this.activatedRoute.navigate(['/institute/dashboard']);
          }
        }
      },
      error => {
        this.authError = error;
      }
    );
  }
}
