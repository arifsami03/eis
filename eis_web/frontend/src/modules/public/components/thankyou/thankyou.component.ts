import { Component, OnInit } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { CampusPreRegistration } from 'modules/public/models';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  constructor() {}

  /**
   * ngOnInit()
   */
  ngOnInit() {}
}
