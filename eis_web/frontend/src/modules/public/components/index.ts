export * from './home/home.component';
export * from './apply_for_registration/apply-for-registration.component';
export * from './campus_pre_registration/campus-pre-registration.component';
export * from './thankyou/thankyou.component';
export * from './eligibility_criteria/eligibility-criteria.component';
