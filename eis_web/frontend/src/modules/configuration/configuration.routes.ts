import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  MetaConfComponent,
  MetaConfFormComponent,
  BankFormComponent,
  BankListComponent,
  DashboardComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent
} from './components';
import { GLOBALS } from '../app/config/globals';


/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'metaconf',
    data: { breadcrumb: { title: 'Meta Conf', display: true } },
    children: [
      {
        path: 'country',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.country,
          title: 'Countries',
          breadcrumb: { title: 'Countries', display: true }
        }
      },
      {
        path: 'province',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.province,
          title: 'Provinces',
          breadcrumb: { title: 'Provinces', display: true }
        }
      },
      {
        path: 'city',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.city,
          title: 'Cities',
          breadcrumb: { title: 'Cities', display: true }
        }
      },
      {
        path: 'tehsil',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.tehsil,
          title: 'Tehsils',
          breadcrumb: { title: 'Tehsils', display: true }
        }
      },
      {
        path: 'natureofwork',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.natureOfWork,
          title: 'Nature of Work',
          breadcrumb: { title: 'Nature of Work', display: true }
        }
      },
      {
        path: 'instrumentType',
        component: MetaConfComponent,
        data: {
          metaKey: GLOBALS.metaKeys.instrumentType,
          title: 'Instrument Type',
          breadcrumb: { title: 'Instrument Type', display: true }
        }
      },
      {
        path: 'banks',
        data: { breadcrumb: { title: 'Banks', display: true } },
        children: [
          {
            path: '',
            component: BankListComponent,
            data: { breadcrumb: { title: 'Banks', display: false } }
          },
          {
            path: 'create',
            component: BankFormComponent,
            data: {
              act: 'create',
              breadcrumb: { title: 'Create', display: true }
            }
          },
          {
            path: 'view/:id',
            component: BankFormComponent,
            data: { act: 'view' }
          },
          {
            path: 'update/:id',
            component: BankFormComponent,
            data: { act: 'update' }
          }
        ]
      }
    ]
  },
  {
    path: 'outGoingMailServers',
    data: { breadcrumb: { title: 'Out Going Mail Server', display: true } },
    children: [
      {
        path: '',
        component: OutGoingMailServerListComponent,
        data: { breadcrumb: { title: 'Out Going Mail Server', display: false } }
      },
      {
        path: 'create',
        component: OutGoingMailServerFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: OutGoingMailServerFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: OutGoingMailServerFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'workFlow',
    data: { breadcrumb: { title: 'Work Flow', display: true } },
    children: [
      {
        path: '',
        component: WorkFlowListComponent,
        data: { breadcrumb: { title: 'Work Flow List', display: false } }
      },
      {
        path: 'create',
        component: WorkFlowFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:WFId',
        component: WorkFlowViewComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:WFId',
        component: WorkFlowFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'emailTemplates',
    data: { breadcrumb: { title: 'Email Template', display: true } },
    children: [
      {
        path: '',
        component: EmailTemplateListComponent,
        data: { breadcrumb: { title: 'Email Template', display: false } }
      },
      {
        path: 'create',
        component: EmailTemplateFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'view/:id',
        component: EmailTemplateFormComponent,
        data: { act: GLOBALS.pageActions.view }
      },
      {
        path: 'update/:id',
        component: EmailTemplateFormComponent,
        data: { act: GLOBALS.pageActions.update }
      }
    ]
  },
  {
    path: 'evaluationSheets',
    data: { breadcrumb: { title: 'Evaluation Sheet', display: true } },
    children: [
      {
        path: '',
        component: EvaluationListComponent,
        data: { breadcrumb: { title: 'Evaluation Sheet', display: false } }
      },
      {
        path: 'create',
        component: EvaluationFormComponent,
        data: {
          act: GLOBALS.pageActions.create,
          breadcrumb: { title: 'Create', display: true }
        }
      },
      {
        path: 'update/:id',
        component: EvaluationFormComponent,
        data: { act: GLOBALS.pageActions.update }
      },
      {
        path: 'view/:id',
        component: EvaluationViewComponent,
        data: { act: GLOBALS.pageActions.view }
      }

    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { breadcrumb: { title: 'Dashboard', display: true } }
  },
  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutes { }
