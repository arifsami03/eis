import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from 'modules/shared/services';
import { EvaluationSheetModel, ESSectionModel } from 'modules/configuration/models';

@Injectable()
export class EvaluationSheetService extends BaseService {
  private routeURL: String = 'configuration/evaluationSheets';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<EvaluationSheetModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<EvaluationSheetModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <EvaluationSheetModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<EvaluationSheetModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <EvaluationSheetModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(faculty: EvaluationSheetModel) {
    return this.__post(`${this.routeURL}/create`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, faculty: EvaluationSheetModel) {
    return this.__put(`${this.routeURL}/update/${id}`, faculty).map(data => {
      return data.json();
    });
  }

}
