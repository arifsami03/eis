import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/services/base.service';
import { MetaConfModel } from '../models';

@Injectable()
export class MetaConfService extends BaseService {
  private _url: String = 'configuration/metaconf';

  constructor(protected http: Http) {
    super(http);
  }

  getCities(): Observable<MetaConfModel[]> {
    return this.__get(`${this._url}/city`)
      .map(data => {
        return <MetaConfModel[]>data.json();
      })
      .catch(this.handleError);
  }
  getAll(metaKey: string): Observable<MetaConfModel[]> {
    return this.__get(`${this._url}/index/${metaKey}`)
      .map(data => {
        return <MetaConfModel[]>data.json();
      })
      .catch(this.handleError);
  }
  getOneRecord(metaKey: string, id: number): Observable<MetaConfModel> {
    return this.__get(`${this._url}/view/${id}`)
      .map(data => {
        return <MetaConfModel>data.json();
      })
      .catch(this.handleError);
  }
  add(item: MetaConfModel) {
    return this.__post(`${this._url}/create`, item)
      .map(data => {
        return <MetaConfModel[]>data.json();
      })
      .catch(this.handleError);
  }
  update(id: Number, item: MetaConfModel) {
    return this.__put(`${this._url}/update/${id}`, item).map(data => {
      return data.json();
    });
  }

  delete(id: Number) {
    return this.__delete(`${this._url}/delete/${id}`).map(data => {
      return data.json();
    });
  }
}
