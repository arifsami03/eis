import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { BaseService } from 'modules/shared/services';
import { ESSectionModel } from 'modules/configuration/models';

@Injectable()
export class ESSectionService extends BaseService {
  private routeURL: String = 'configuration/esSection';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<ESSectionModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <ESSectionModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<ESSectionModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <ESSectionModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  find(id: number): Observable<ESSectionModel> {
    return this.__get(`${this.routeURL}/find/${id}`).map(data => {
      return <ESSectionModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(faculty: ESSectionModel) {
    return this.__post(`${this.routeURL}/create`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: number, faculty: ESSectionModel) {
    return this.__put(`${this.routeURL}/update/${id}`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Get All ESSections against an Evaluation Sheet
   */
  findAllESSections(ESId: number): Observable<ESSectionModel[]> {
    return this.__get(`${this.routeURL}/findAllESSections/${ESId}`).map(data => {
      return <ESSectionModel[]>data.json();
    });
  }

}
