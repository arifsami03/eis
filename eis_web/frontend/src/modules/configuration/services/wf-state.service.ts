import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable, EventEmitter } from '@angular/core';

import { BaseService } from 'modules/shared/services';
import { WFStateModel } from 'modules/configuration/models';

@Injectable()
export class WFStateService extends BaseService {
  private routeURL: String = 'configuration/wfStates';

  constructor(protected http: Http) {
    super(http);
  }

  /**
   * Get All records
   */
  index(): Observable<WFStateModel[]> {
    return this.__get(`${this.routeURL}/index`).map(data => {
      return <WFStateModel[]>data.json();
    });
  }
  /**
   * Get All records
   */
  findAttributesList(): Observable<WFStateModel[]> {
    return this.__get(`${this.routeURL}/findAttributesList`).map(data => {
      return <WFStateModel[]>data.json();
    });
  }

  /**
   * Get single record
   */
  list(id: string): Observable<WFStateModel[]> {
    return this.__get(`${this.routeURL}/list/${id}`).map(data => {
      return <WFStateModel[]>data.json();
    });
  }

   /**
   * Get single record
   */
  findById(id: string): Observable<WFStateModel> {
    return this.__get(`${this.routeURL}/findById/${id}`).map(data => {
      return <WFStateModel>data.json();
    });
  }

  /**
   * Delete record
   */
  delete(id: number) {
    return this.__delete(`${this.routeURL}/delete/${id}`).map(data => {
      return data.json();
    });
  }

  /**
   * Create record
   */
  create(workFlow: WFStateModel) {
    return this.__post(`${this.routeURL}/create`, workFlow).map(data => {
      return data.json();
    });
  }

  /**
   * Update record
   */
  update(id: string, faculty: WFStateModel) {
    return this.__put(`${this.routeURL}/update/${id}`, faculty).map(data => {
      return data.json();
    });
  }

  /**
   * Check Record Exists or Not
   */

   checkRecordExists(item:WFStateModel){
     console.log('ARif',item);
     var id=item.state;
    return this.__put(`${this.routeURL}/checkRecordExists/${id}`,item).map(data => {
      return data.json();
    });
   }
}
