import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class MetaConfModel {
  id?:number;
  metaKey: string;
  metaValue: string;

  static attributesLabels = {
    metaValue: 'Value',
  };

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      metaValue: new FormControl('', [<any>Validators.required]),
    };
  }
}
