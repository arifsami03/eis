import { FormControl, Validators } from '@angular/forms';

export class EmailTemplateModel {
  /**
   * it will set the labels for attributes of Level of Education
   */
  static attributesLabels = {
    name: 'Name',
    subject: 'Subject',
    emailBody: 'Eamil Body',
    outGoingMailServerId: 'Out Going Mail Server'
  };

  id?: number;
  name: string;
  subject: string;
  emailBody: string;
  outGoingMailServerId: number;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() {}

  /**
   * Form Validation Rules for Level of Education
   */
  public validationRules?() {
    return {
      name: new FormControl('', [
        <any>Validators.required,
        Validators.maxLength(30)
      ]),
      subject: new FormControl('', [<any>Validators.required]),
      emailBody: new FormControl('', [<any>Validators.required]),
      outGoingMailServerId: new FormControl('', [<any>Validators.required])
    };
  }
}
