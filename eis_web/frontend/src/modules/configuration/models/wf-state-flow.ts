import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class WFStateFlowModel {
  /**
   * it will set the labels for attributes of Work Flow
   */
  static attributesLabels = {
    fromId: 'From',
    toId: 'To',
    description: 'Description'
  };

  fromId: number;
  toId:number;
  from:string;
  to:string;
  WFId:string;
  
  description: string;
  createdBy?: number;
  updatedBy?: number;
  createdAT?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules for Faculty
   */
  public validationRules?() {
    return {
      fromId: new FormControl('', [<any>Validators.required]),
      toId: new FormControl('', [<any>Validators.required]),
      description:['']
    };
  }
}
