import { FormControl, Validators } from '@angular/forms';

export class EvaluationSheetModel {
  /**
   * Set Labels of attributes
   */
  static attributesLabels = {
    title: 'Title'
  };

  id?: number;
  title: string;
  createdBy?: number;
  updatedBy?: number;
  createdAt?: Date;
  updatedAt?: Date;

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      title: new FormControl('', [Validators.required]),
    };
  }
}
