// Import angular and material modules here
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatSelectModule,
  MatDividerModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatTooltipModule,
  MatCardModule,
  MatCheckboxModule,
  MatSortModule
} from '@angular/material';
import {
  MetaConfService,
  BankService,
  OutGoingMailServerService,
  EmailTemplateService,
  WorkFlowService,
  WFStateService,
  WFStateFlowService,
  EvaluationSheetService,
  ESSectionService
} from './services';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MetaConfComponent,
  MetaConfFormComponent,
  DashboardComponent,
  BankFormComponent,
  BankListComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  WorkFlowStateComponent,
  WFStateFormComponent,
  WFStateFlowComponent,
  WFStateFlowFormComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent,
  ESSectionViewComponent,
  ESSectionFormComponent
} from './components';
// Import components services here

export const __IMPORTS = [
  MatFormFieldModule,
  MatInputModule,
  FormsModule,
  ReactiveFormsModule,
  MatListModule,
  MatSelectModule,
  MatDividerModule,
  MatIconModule,
  MatTableModule,
  MatPaginatorModule,
  MatButtonModule,
  MatTooltipModule,
  MatCardModule,
  FlexLayoutModule,
  MatCheckboxModule,
  MatSortModule
];

export const __DECLARATIONS = [
  MetaConfComponent,
  MetaConfFormComponent,
  DashboardComponent,
  BankFormComponent,
  BankListComponent,
  OutGoingMailServerListComponent,
  OutGoingMailServerFormComponent,
  EmailTemplateListComponent,
  EmailTemplateFormComponent,
  WorkFlowFormComponent,
  WorkFlowListComponent,
  WorkFlowViewComponent,
  WorkFlowStateComponent,
  WFStateFormComponent,
  WFStateFlowComponent,
  WFStateFlowFormComponent,
  EvaluationFormComponent,
  EvaluationListComponent,
  EvaluationViewComponent,
  ESSectionViewComponent,
  ESSectionFormComponent
];

export const __PROVIDERS = [
  MetaConfService,
  BankService,
  OutGoingMailServerService,
  EmailTemplateService,
  WorkFlowService,
  WFStateService,
  WFStateFlowService,
  EvaluationSheetService,
  ESSectionService
];

export const __ENTRY_COMPONENTS = [MetaConfFormComponent, WFStateFormComponent, WFStateFlowFormComponent, ESSectionFormComponent];
