export const ConfigurationMenu = [
  {
    heading: 'Configuration',
    menuItems: [
      { title: 'Dashboard', url: ['/configuration/dashboard'], icon: 'view_list', perm: 'n/a' },
      { title: 'Nature Of Work', url: ['/configuration/metaconf/natureofwork'], icon: 'view_list', perm: 'metaconf.index' },
      { title: 'Instrument Type', url: ['/configuration/metaconf/instrumentType'], icon: 'view_list', perm: 'metaconf.index' },
      { title: 'Bank', url: ['/configuration/metaconf/banks'], icon: 'view_list', perm: 'banks.index' },
    ]
  },
  {
    heading: 'Email',
    menuItems: [
      { title: 'Out Going Mail Server', url: ['/configuration/outGoingMailServers'], icon: 'view_list', perm: 'outGoingMailServers.index' },
      { title: 'Email Template', url: ['/configuration/emailTemplates'], icon: 'view_list', perm: 'emailTemplates.index' }
    ]
  },
  {
    heading: 'Locations',
    menuItems: [
      { title: 'Country', url: ['/configuration/metaconf/country'], icon: 'view_list', perm: 'metaconf.index' },
      { title: 'Province', url: ['/configuration/metaconf/province'], icon: 'view_list', perm: 'metaconf.index' },
      { title: 'City', url: ['/configuration/metaconf/city'], icon: 'view_list', perm: 'metaconf.index' },
      { title: 'Tehsil', url: ['/configuration/metaconf/tehsil'], icon: 'view_list', perm: 'metaconf.index' },
    ]
  },
  {
    heading: 'Work Flow',
    menuItems: [
      { title: 'Work Flow', url: ['/configuration/workFlow'], icon: 'view_list', perm: 'workFlows.index' },
    ]
  },
  {
    heading: 'Evaluation Sheet',
    menuItems: [
      { title: 'Evaluation Sheet', url: ['/configuration/evaluationSheets'], icon: 'view_list', perm: 'evaluationSheets.index' },
    ]
  },
];
