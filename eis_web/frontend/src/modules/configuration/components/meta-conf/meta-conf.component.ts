import {
  Component,
  ViewChild,
  OnInit,
  AfterViewInit,
  Injectable
} from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { LayoutService } from 'modules/layout/services';
import { MetaConfService } from 'modules/configuration/services';
import { MetaConfModel } from 'modules/configuration/models';
import { MetaConfFormComponent } from './form/meta-conf-form.component';
import { ConfirmDialogComponent } from 'modules/shared/components';

@Component({
  selector: 'config-meta-conf',
  templateUrl: './meta-conf.component.html',
  styleUrls: ['./meta-conf.component.css']
})
export class MetaConfComponent implements OnInit {
  public loaded: boolean = false;

  public pageTitle: string;

  public metaKey: string;

  displayedColumns = ['metaValue', 'options'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  public componentLabels = MetaConfModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public layoutService: LayoutService,
    public metaService: MetaConfService,
    public dialog: MatDialog
  ) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.metaKey = this.route.snapshot.data['metaKey'];
    this.pageTitle = this.route.snapshot.data['title'];

    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.getRecords();
  }

  getRecords(): void {
    this.metaService.getAll(this.metaKey).subscribe(
      response => {
        this.dataSource = new MatTableDataSource<MetaConfModel>(response);
        this.dataSource.paginator = this.paginator;
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }
  openForm(action: string, value: string) {
    let dialogRef = this.dialog.open(MetaConfFormComponent, {
      width: '500px',
      height: '200px',
      data: { metaKey: this.metaKey, action: action, value: value }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getRecords();
    });
  }
  /**
   * Function to delete item
   * @param id meta Configuration id
   */
  deleteClick(id: number) {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you permanently want to delete?' }
    });

    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.metaService.delete(id).subscribe(
          response => {
            // Display of succes message in case of succesfull deletion
            if (response.success) {
              this.success = true;
              this.successMessage = response.success;
              this.getRecords();
            } else {
              // Error Display
              //  this.error = true;
              this.getRecords();
              this.errorMessage = response.error;
            }
          },
          error => {
            console.log(error);
          }
        );
      }
    });
  }
}
