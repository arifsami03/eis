import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { MetaConfService } from 'modules/configuration/services';

import { MetaConfModel } from 'modules/configuration/models';

@Component({
  selector: 'config-meta-conf-form',
  templateUrl: './meta-conf-form.component.html',
  styleUrls: ['./meta-conf-form.component.css']
})
export class MetaConfFormComponent implements OnInit {
  public loaded: boolean = false;

  public pageTitle: string;
  public fg: FormGroup;
  public metaConf = new MetaConfModel();

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  public componentLabels = MetaConfModel.attributesLabels;

  constructor(
    public metaService: MetaConfService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<MetaConfFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    if (this.data.action == 'Update') {
      this.getOneRecord(this.data.value);
    }
  }

  private initializePage() {
    this.fg = this.fb.group(new MetaConfModel().validationRules());
  }

  getOneRecord(id: number) {
    this.metaService.getOneRecord(this.data.metaKey, id).subscribe(
      response => {
        this.metaConf = response;
        this.fg.patchValue(this.metaConf);

        // this.employee = response;
        // this.employeeCopy = JSON.parse(JSON.stringify(this.employee));
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Function to Save Meta Configuration
   * @param item MetaConfModel
   */

  public saveData(item: MetaConfModel) {
    if (this.data.action == 'Update') {
      this.updateData(item);
    } else {
      item.metaKey = this.data.metaKey;
      this.metaService.add(item).subscribe(
        response => {
          if (response.success) {
            // display of Succes and Error Message
            this.success = true;
            this.successMessage = response.success;
            this.dialogRef.close();
          } else {
            this.error = true;
            this.dialogRef.close();
            this.errorMessage = response.error;
          }
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  updateData(item: MetaConfModel) {
    item.metaKey = this.data.metaKey;
    this.metaService.update(this.data.value, item).subscribe(
      response => {
        if (response.success) {
          // display of succes or error message
          this.success = true;
          this.successMessage = response.success;
          this.dialogRef.close();
        } else {
          this.error = true;
          this.dialogRef.close();
          this.errorMessage = response.error;
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  onNoClick() {
    this.dialogRef.close();
  }
}
