import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BankService } from 'modules/configuration/services';
import { LayoutService } from 'modules/layout/services';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { BankModel } from 'modules/configuration/models';
import { GLOBALS } from 'modules/app/config/globals';

/**
 * This component is being used for three purposes.
 *  1: Add new Bank Members
 *  2: View existing Bank Members
 *  3: Update existing Bank Members
 *
 */

@Component({
  selector: 'configuration-bank-form',
  templateUrl: './bank-form.component.html',
  styleUrls: ['./bank-form.component.css']
})
export class BankFormComponent implements OnInit {
  public loaded: boolean = false;
  public pageActions = GLOBALS.pageActions;

  public error: Boolean;

  public fg: FormGroup;
  public pageAct: string;
  public bank: BankModel;

  public componentLabels = BankModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private bankService: BankService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }
  /**
   * Initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new BankModel().validationRules());
    if (this.pageAct === this.pageActions.create) {
      this.initCreatePage();
    } else {
      this.getData();
    }
  }

  /**
   * Get all data
   */
  private getData() {
    this.route.params.subscribe(params => {
      this.bankService.find(params['id']).subscribe(
        response => {
          this.bank = response;
          this.fg.patchValue(response);
          if (!this.error) {
            if (this.pageAct === this.pageActions.view) {
              this.initViewPage();
            } else if (this.pageAct === this.pageActions.update) {
              this.initUpdatePage();
            }
            this.loaded = true;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    });
  }

  /**
   * Initialize create page
   */
  private initCreatePage() {
    this.layoutService.setPageTitle({ title: 'Bank: Create' });
    this.bank = new BankModel();
    this.fg.enable();

    this.loaded = true;
  }

  /**
   * Initialize view page
   */
  private initViewPage() {
    this.fg.disable();
    this.layoutService.setPageTitle({
      title: 'Bank: ' + this.bank.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Bank ', url: '/configuration/metaconf/banks' },
        { label: this.bank.name }
      ]
    });
  }

  /**
   * Initialize update page
   */
  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Bank: ' + this.bank.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Bank', url: '/configuration/metaconf/banks' },
        {
          label: this.bank.name,
          url: `/configuration/metaconf/banks/view/${this.bank.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: BankModel) {
    let id;
    if (this.pageAct === this.pageActions.create) {
      item.id = null;
      this.bankService.create(item).subscribe(
        response => {
          this.router.navigate([`/configuration/metaconf/banks/view/${response.id}`]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    } else if (this.pageAct === this.pageActions.update) {
      this.bankService.update(this.bank.id, item).subscribe(
        response => {
          this.router.navigate([
            `/configuration/metaconf/banks/view/${this.bank.id}`
          ]);
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    }
  }

  /**
   * Delete record
   */
  deleteRecord(id: number) {
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: GLOBALS.deleteDialog.width,
      data: { message: GLOBALS.deleteDialog.message }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.bankService.delete(id).subscribe(response => {
          this.router.navigate(['/configuration/metaconf/banks']);
        },
          error => {
            console.log(error);
            this.loaded = true;
          }
        );
      }
    });
  }
}
