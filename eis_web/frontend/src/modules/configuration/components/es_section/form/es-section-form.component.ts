import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { ESSectionService } from 'modules/configuration/services';
import { ESSectionModel } from 'modules/configuration/models';

@Component({
  selector: 'configuration-es-section',
  templateUrl: './es-section-form.component.html',
  styleUrls: ['./es-section-form.component.css']
})
export class ESSectionFormComponent {
  public loaded: boolean = false;
  private pageAct: string;
  public pageTitle: string;
  private ESId: number;
  private esSectionId: number;
  public fg: FormGroup;
  public componentLabels = ESSectionModel.attributesLabels;

  public esSection: ESSectionModel;

  constructor(
    private esSectionService: ESSectionService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<ESSectionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  ngOnInit() {
    this.ESId = this.data.ESId;
    this.esSectionId = this.data.esSectionId;
    this.pageAct = this.data.action
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new ESSectionModel().validationRules());
    if (this.pageAct === 'create') {
      this.pageTitle = 'Create Section'
      this.initAddPage();
    } else {
      this.getData(this.esSectionId);
    }
  }

  private getData(esSectionId: number) {
    this.esSectionService.find(esSectionId).subscribe(
      response => {
        this.esSection = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.fg.patchValue(this.esSection);
        this.initUpdatePage();
        this.loaded = true;
      }
    );
  }

  private initAddPage() {
    this.esSection = new ESSectionModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initUpdatePage() {
    this.fg.enable();
    this.pageTitle = `Update: ${this.esSection.title}`;

  }

  public saveData(item: ESSectionModel) {
    this.loaded = false;
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.esSection.id, item);
    }
  }

  private create(item: ESSectionModel) {
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');
    item.ESId = this.ESId;
    this.esSectionService.create(item).subscribe(
      response => {
        this.esSection = response;
        console.log('resposne: ', response);
      },
      error => {
        this.loaded = true;
        console.log('resposne: ', error);
      },
      () => {
        this.loaded = true;
        this.dialogRef.close({ response: true });
      }
    );
  }

  private update(id: number, item: ESSectionModel) {
    this.loaded = false;
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    this.esSectionService.update(id, item).subscribe(
      response => {
        console.log('resposne: ', response);
      },
      error => {
        this.loaded = true;
        console.log('resposne: ', error);
      },
      () => {
        this.loaded = true;
        this.dialogRef.close({ response: true });
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }

  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }


}
