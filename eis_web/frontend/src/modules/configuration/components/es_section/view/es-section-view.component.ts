import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ESSectionService } from 'modules/configuration/services';

import { ESSectionModel } from 'modules/configuration/models';

import { ESSectionFormComponent } from 'modules/configuration/components';

import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'configuration-es-section-view',
  templateUrl: './es-section-view.component.html',
  styleUrls: ['./es-section-view.component.css']
})
export class ESSectionViewComponent implements OnInit {


  public loaded: boolean = false;
  public esSections: ESSectionModel[];

  @Input() ESId: number = 1;

  constructor(private esSectionService: ESSectionService, public matDialog: MatDialog) { }

  ngOnInit() {
    console.log('this.esSections: ', this.esSections);
    this.getRecord(this.ESId);
  }

  getRecord(ESId: number) {
    this.esSectionService.findAllESSections(ESId).subscribe(
      response => {
        console.log('response: ', response);
        this.esSections = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  addESSection() {
    const dialogRef = this.matDialog.open(ESSectionFormComponent, {
      width: '500px',
      data: { ESId: this.ESId, action: GLOBALS.pageActions.create }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecord(this.ESId);
      }

    });
  }

  editESSection(esSectionId: number) {
    const dialogRef = this.matDialog.open(ESSectionFormComponent, {
      width: '500px',
      data: { esSectionId: esSectionId, action: GLOBALS.pageActions.update }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getRecord(this.ESId);
      }

    });
  }

}
