import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { EvaluationSheetService } from 'modules/configuration/services';
import { LayoutService } from 'modules/layout/services';

import { EvaluationSheetModel } from 'modules/configuration/models';

/**
 * This component is being used for two purposes.
 *  1: Add new Users
 *  3: Update existing Users
 *
 */

@Component({
  selector: 'configuration-evaluation-sheet-form',
  templateUrl: './evaluation-sheet-form.component.html',
  styleUrls: ['./evaluation-sheet-form.component.css']
})
export class EvaluationFormComponent implements OnInit {
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public evaluationSheet: EvaluationSheetModel;

  public componentLabels = EvaluationSheetModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private evaluationSheetService: EvaluationSheetService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new EvaluationSheetModel().validationRules());
    if (this.pageAct === 'create') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.evaluationSheetService.find(params['id']).subscribe(
        response => {
          this.evaluationSheet = response;
        },
        error => {
          this.loaded = true;
        },
        () => {

          this.fg.patchValue(this.evaluationSheet);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Evaluation Sheet' });
    this.evaluationSheet = new EvaluationSheetModel();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Evaluation Sheets', url: '/configuration/evaluationSheets' },
        { label: this.evaluationSheet.title }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Evaluation Sheets', url: '/configuration/evaluationSheets' },
        {
          label: this.evaluationSheet.title,
          url: `/configuration/evaluationSheets/view/${this.evaluationSheet.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: EvaluationSheetModel) {
    this.loaded = false;
    if (this.pageAct === 'create') {
      this.create(item);
    } else {
      this.update(this.evaluationSheet.id, item);
    }
  }

  private create(item: EvaluationSheetModel) {
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    let result;
    this.evaluationSheetService.create(item).subscribe(
      response => {
        this.evaluationSheet = response;
        console.log('resposne: ', response);
      },
      error => {
        this.loaded = true;
        console.log('resposne: ', error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/configuration/evaluationSheets/view/${this.evaluationSheet.id}`]);
      }
    );
  }

  private update(id: number, item: EvaluationSheetModel) {
    this.loaded = false;
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    this.evaluationSheetService.update(id, item).subscribe(
      response => {
        console.log('resposne: ', response);
      },
      error => {
        this.loaded = true;
        console.log('resposne: ', error);
      },
      () => {
        this.loaded = true;
        this.router.navigate([`/configuration/evaluationSheets/view/${this.evaluationSheet.id}`]);
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
}
