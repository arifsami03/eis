import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';

import { EvaluationSheetService } from 'modules/configuration/services';
import { LayoutService } from 'modules/layout/services';

import { EvaluationSheetModel } from 'modules/configuration/models';
import { ConfirmDialogComponent } from 'modules/shared/components';

@Component({
  selector: 'configuration-evaluation-sheet-list',
  templateUrl: './evaluation-sheet-list.component.html',
  styleUrls: ['./evaluation-sheet-list.component.css']
})
export class EvaluationListComponent implements OnInit {
  public loaded: boolean = false;

  public evaluationSheets: EvaluationSheetModel[] = [new EvaluationSheetModel()];

  public attrLabels = EvaluationSheetModel.attributesLabels;

  displayedColumns = ['title', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(
    private evaluationSheetService: EvaluationSheetService,
    public layoutService: LayoutService,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Evaluation Sheets' });
    this.getRecords();
  }

  getRecords(): void {
    this.evaluationSheetService.index().subscribe(
      response => {
        this.evaluationSheets = response;
        this.dataSource = new MatTableDataSource<EvaluationSheetModel>(this.evaluationSheets);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.evaluationSheetService.delete(id).subscribe(
          response => {
            console.log(response);
          },
          error => {
            console.log(error);
            this.loaded = true;
          },
          () => {
            this.getRecords();
            this.loaded = true;
          }
        );
      }
    });
  }

}
