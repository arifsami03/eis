import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { EvaluationSheetService } from 'modules/configuration/services';
import { LayoutService } from 'modules/layout/services';

import { EvaluationSheetModel, ESSectionModel } from 'modules/configuration/models';

@Component({
  selector: 'configuration-evaluation-sheet-view',
  templateUrl: './evaluation-sheet-view.component.html',
  styleUrls: ['./evaluation-sheet-view.component.css']
})
export class EvaluationViewComponent implements OnInit {
  public loaded: boolean = false;

  public sectionsloaded: boolean = false;


  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;

  public evaluationSheet: EvaluationSheetModel;

  public componentLabels = EvaluationSheetModel.attributesLabels;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private evaluationSheetService: EvaluationSheetService,
    private layoutService: LayoutService
  ) {
  }

  ngOnInit() {
    this.getData();
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.evaluationSheetService.find(params['id']).subscribe(
        response => {
          this.evaluationSheet = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.initViewPage();
          this.loaded = true;
        }
      );
    });
  }

  private initViewPage() {
    this.fg = this.fb.group(new EvaluationSheetModel().validationRules());
    this.fg.patchValue(this.evaluationSheet);
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Evaluation Sheet: ' + this.evaluationSheet.title,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Evaluation Sheets', url: '/configuration/evaluationSheets' },
        { label: this.evaluationSheet.title }
      ]
    });
  }

}
