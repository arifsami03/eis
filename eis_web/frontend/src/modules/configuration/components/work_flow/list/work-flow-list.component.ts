import { Component, ViewChild, OnInit } from '@angular/core';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

import { ConfirmDialogComponent } from 'modules/shared/components';
import { LayoutService } from 'modules/layout/services';
import { WorkFlowService } from 'modules/configuration/services';
import { WorkFlowModel } from 'modules/configuration/models';
import { GLOBALS } from 'modules/app/config/globals';

@Component({
  selector: 'work-flow-list',
  templateUrl: './work-flow-list.component.html',
  styleUrls: ['./work-flow-list.component.css']
})
export class WorkFlowListComponent implements OnInit {
  public loaded: boolean = false;
  public data: WorkFlowModel[] = [new WorkFlowModel()];
  public attrLabels = WorkFlowModel.attributesLabels;

  displayedColumns = ['WFId', 'title', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public error: Boolean;

  constructor(
    private workFlowService: WorkFlowService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private router: Router,
    public layoutService: LayoutService
  ) { }

  /**
   * Initialize page
   */
  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Work Flow' });
    this.getRecords();
  }

  /**
   * Get records
   */
  getRecords() {
    this.workFlowService.index().subscribe(
      response => {
        this.data = response;
        this.dataSource = new MatTableDataSource<WorkFlowModel>(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
    return this.data;
  }

  /**
   * Apply filter and serch in data grid
   */
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /**
   * Delete record
   */
  deleteWorkFlow(id: string) {
    // Confirm dialog
    this.matDialog
      .open(ConfirmDialogComponent, {
        width: GLOBALS.deleteDialog.width,
        data: { message:'By Deleting this the Association with State Flows also Delete, Are you Sure?' }
      })
      .afterClosed()
      .subscribe((accept: boolean) => {
        if (accept) {
          this.loaded = false;

          this.workFlowService.delete(id).subscribe(response => {
            this.getRecords();
          },
            error => {
              console.log(error);
              this.loaded = true;
            }
          );
        }
      });
  }
}
