import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { WFStateFlowService,WFStateService } from 'modules/configuration/services';
import { WFStateModel,WFStateFlowModel } from 'modules/configuration/models';

@Component({
  selector: 'wf-state-flow-form',
  templateUrl: './wf-state-flow-form.component.html',
  styleUrls: ['./wf-state-flow-form.component.css']
})
export class WFStateFlowFormComponent {
  public loaded: boolean = false;
  public wfState: WFStateModel[];
  public fg: FormGroup;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;
  public state:string;
  public componentLabels = WFStateFlowModel.attributesLabels;

  constructor(
    private wfStateService: WFStateService,
    private wfStateFlowService: WFStateFlowService,
    
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<WFStateFlowFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded=true;
  }

   /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
    this.getWFState();
    if(this.data.type != 'Add Flow'){
      this.getFlowData();
    }
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new WFStateFlowModel().validationRules());

  }

  getWFState(){
    this.wfStateService.list(this.data.WFId).subscribe(
      response => {
        this.wfState=response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getFlowData(){
    console.log(this.data.id);
    this.wfStateFlowService.findById(this.data.id).subscribe(
      response => {
        //this.state=response.state;
        this.fg.patchValue(response);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

 
  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

    /**
   * Add or update data in database when save button is clicked
   */
  public saveData(item: WFStateFlowModel) {
    console.log(item);
    item.WFId=this.data.WFId
    this.saveRecord(item);
  //  this.checkRecordExist(item);
  }
  saveRecord(item: WFStateFlowModel) {
    
    if (this.data.type === 'Add Flow') {
      console.log('asdasd');
      this.wfStateFlowService.create(item).subscribe(
        response => {
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    }
     else  {
      this.wfStateFlowService.update(this.data.id, item).subscribe(
        response => {
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    }
  }

  // public isRecordExists: boolean = false;
  // checkRecordExist(item: WFStateModel) {
  //   this.wfStateService.checkRecordExists(item).subscribe(
  //     response => {
  //       if (response.length != 0 && response[0].state !=this.state ) {
  //         this.fg.controls['state'].setErrors({ exist: true });
  //       }
  //       else {
  //         this.saveRecord(item);
  //       }
  //     },
  //     error => {
  //       console.log(error);
  //       this.loaded = true;
  //     },
  //     () => {
  //       this.loaded = true;
  //     }
  //   );

  // }

 
}
