import { environment } from 'environments/environment';
import { Component, ViewChild, OnInit } from '@angular/core';

import { LayoutService } from 'modules/layout/services';

@Component({
  selector: 'configuration-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(public layoutService: LayoutService) {}

  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Dashboard'
    });
  }
}
