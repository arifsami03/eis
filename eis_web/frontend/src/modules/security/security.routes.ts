import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  UserFormComponent,
  UserViewComponent,
  UserListComponent,
  GroupFormComponent,
  GroupListComponent,
  GroupViewComponent,
  RoleListComponent,
  RoleFormComponent,
  RoleViewComponent,
  DashboardComponent
} from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'users',
    data: { breadcrumb: { title: 'Users', display: true } },
    children: [
      {
        path: '',
        component: UserListComponent,
        data: { breadcrumb: { title: 'Users', display: false } }
      },
      {
        path: 'add',
        component: UserFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: UserViewComponent
      },
      {
        path: 'update/:id',
        component: UserFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'groups',
    data: { breadcrumb: { title: 'Groups', display: true } },
    children: [
      {
        path: '',
        component: GroupListComponent,
        data: { breadcrumb: { title: 'Groups', display: false } }
      },
      {
        path: 'add',
        component: GroupFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: GroupViewComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: GroupFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'roles',
    data: { breadcrumb: { title: 'Roles', display: true } },
    children: [
      {
        path: '',
        component: RoleListComponent,
        data: { breadcrumb: { title: 'Roles', display: false } }
      },
      {
        path: 'add',
        component: RoleFormComponent,
        data: {
          act: 'add',
          breadcrumb: { title: 'Add', display: true }
        }
      },
      {
        path: 'view/:id',
        component: RoleViewComponent,
        data: { act: 'view' }
      },
      {
        path: 'update/:id',
        component: RoleFormComponent,
        data: { act: 'update' }
      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { breadcrumb: { title: 'Dashboard', display: true } }
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SecurityRoutes {}
