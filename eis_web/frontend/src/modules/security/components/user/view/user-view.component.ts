import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';


import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { User, Group, Role } from 'modules/security/models';
import {
  UserService,
  UserGroupService,
  RoleAssignmentService,
  FeaturePermissionService
} from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';
import {
  AssignGroupToUserComponent,
  AssignRoleToUserComponent
} from 'modules/security/components';

@Component({
  selector: 'security-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
  public loaded: boolean = false;

  public showDivBar: boolean = false;

  public pageTitle: string;
  public user: User = new User();
  public userLabels = User.attributesLabels;
  public roleLabels = Role.attributesLabels;
  public fg: FormGroup;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private featurePermissionService: FeaturePermissionService,
    private userGroupService: UserGroupService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog,
    private fb: FormBuilder,
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.user.id = param.id;
    });
    this.getData(this.user.id);

    // initialize formGroup to view data 
    this.fg = this.fb.group(new User().validationRules());
  }

  private getData(id: number) {
    this.userService.find(id).subscribe(
      response => {
        this.user = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
        this.fg.patchValue(this.user);
        this.fg.disable();
        this.modifyPageHeader();
      }
    );
  }

  public assignGroup(userId: number) {
    const dialogRef = this.matDialog.open(AssignGroupToUserComponent, {
      width: '700px',
      data: { userId: userId, userName: this.user.username }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(userId);
      }
    });
  }
  public assignRole(userId: number) {
    const dialogRef = this.matDialog.open(AssignRoleToUserComponent, {
      width: '700px',
      data: { userId: userId, userName: this.user.username }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(userId);
      }
    });
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.userService.delete(id).subscribe(
          response => {
            this.displayMessage('success', 'User has been deleted successfully', '/security/users');
          },
          error => {
            this.loaded = true;

            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
          },

          () => {
            this.loaded = true;
          }
        );
      }
    });
  }
  public deleteFromGroup(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        let result;
        this.userGroupService.revoke(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }
  public deleteFromRole(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        { label: this.user.username }
      ]
    });
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
      this.getData(this.user.id);
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route?) {
    this.success = true;
    this.successMessage = message;
    if (route) {
      setTimeout((router: Router) => {
        this.router.navigate([route]);
      }, 1000);
    } else {
      // Hide success message after one sec
      setTimeout(() => {
        this.success = false;
        this.getData(this.user.id);
      }, 1000);
    }
  }
}
