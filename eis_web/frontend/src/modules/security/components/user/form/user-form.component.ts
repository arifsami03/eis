import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { User } from 'modules/security/models';
import { UserService } from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
/**
 * This component is being used for three purposes.
 *  1: Add new Users
 *  2: View existing Users
 *  3: Update existing Users
 *
 */

@Component({
  selector: 'security-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public user: User;
  public options: Observable<string[]>;

  public userStatus = [{ name: 'Active', value: true }, { name: 'Inactive', value: false }]

  public userLabels = User.attributesLabels;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private userService: UserService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new User().validationRules());
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.userService.find(params['id']).subscribe(
        response => {
          this.user = response;
        },
        error => {
          this.loaded = true;
        },
        () => {
          this.fg.patchValue(this.user);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add User' });
    this.user = new User();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        { label: this.user.username }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'User: ' + this.user.username,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Users', url: '/security/users' },
        {
          label: this.user.username,
          url: `/security/users/view/${this.user.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: User) {

    this.loaded = false;
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.user.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.userService.delete(id).subscribe(
          response => {
            this.displaySuccessMessage('User has been deleted successfully', '/security/users');
          },
          error => {
            this.loaded = false;
            this.displayErrorMessage(
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            this.loaded = false;
          }
        );
      }
    });
  }

  private create(item: User) {
    this.loaded = false;

    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    this.userService.create(item).subscribe(
      response => {
        this.displaySuccessMessage('User has been added successfully', `/security/users/view/${response.id}`);
      },
      error => {
        this.loaded = true;
        this.displayErrorMessage('Something went wrong, please try again.');
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private update(id: number, item: User) {
    this.loaded = false;

    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    this.userService.update(id, item).subscribe(
      response => {
        this.displaySuccessMessage('User has been updated successfully', `/security/users/view/${id}`);
      },
      error => {
        this.loaded = true;
        this.displayErrorMessage('Something went wrong, please try again.');
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  // private displayMessage(messageType, message, route?) {
  //   this.loaded = true;
  //   if (messageType === 'error') {
  //     this.displayErrorMessage(message);
  //   } else {
  //     this.displaySuccessMessage(message, route);
  //   }
  // }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route?) {
    this.success = true;
    this.successMessage = message;

    setTimeout((router: Router) => {

      if (route) {
        this.router.navigate([route]);
      } else {
        this.success = false;
      }
    }, 1000);
  }
}
