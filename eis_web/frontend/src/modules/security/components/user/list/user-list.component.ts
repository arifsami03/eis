import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { LayoutService } from 'modules/layout/services';
import { UserService } from 'modules/security/services';
import { ConfirmDialogComponent } from 'modules/shared/components';

import { User } from 'modules/security/models';

@Component({
  selector: 'security-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public loaded: boolean = false;

  public users: User[] = [new User()];

  public attrLabels = User.attributesLabels;

  displayedColumns = ['username', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    public layoutService: LayoutService,
    private userService: UserService,
    public dialog: MatDialog,
    public matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Users' });
    this.getRecords();
  }

  getRecords(): void {
    this.userService.index().subscribe(
      response => {
        this.users = response;
        this.dataSource = new MatTableDataSource<User>(this.users);
        this.dataSource.paginator = this.paginator;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;

        // hide popup message if displayed
        if (this.success) {
          this.success = false;
        }
      }

    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        this.userService.delete(id).subscribe(
          response => {
            this.loaded = true;
            if (response.success) {
              this.successMessage = response.success;
              setTimeout(() => {
                this.getRecords();
              }, 1000);
            } else {
              // Error Display
              this.error = true;
              this.errorMessage = response.error;
              // Hide error message after one sec
              setTimeout(() => {
                this.error = false;
              }, 1000);
            }
          },
          error => {
            this.loaded = true;
          },
          () => {
            this.loaded = true;
          }
        );
      }
    });
  }
}
