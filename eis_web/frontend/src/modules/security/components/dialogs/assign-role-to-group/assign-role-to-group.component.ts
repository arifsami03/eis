import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { GroupService, RoleAssignmentService } from 'modules/security/services';
import { Role, RoleAssignment } from 'modules/security/models';

@Component({
  selector: 'security-assign-role',
  templateUrl: './assign-role-to-group.component.html',
  styleUrls: ['./assign-role-to-group.component.css']
})
export class AssignRoleToGroupComponent {
  public loaded: boolean = false;
  public roleAssignment: RoleAssignment;
  public roles: Role[];

  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;

  constructor(
    private groupService: GroupService,
    private roleAssignmentService: RoleAssignmentService,
    public dialogRef: MatDialogRef<AssignRoleToGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RoleAssignment
  ) {
    this.roleAssignment = data;
    this.findAllRoles(this.roleAssignment.groupId);
  }

  /**
   * submit a note against complaint suggestion or appreciation
   */
  submit() {
    // get id of loggedIn userId
    this.roleAssignment.createdBy = +this.getLoggedInUserInfo('id');

    let result;
    this.roleAssignmentService.assignToGroup(this.roleAssignment).subscribe(
      response => {
        result = response;
      },
      error => {
        this.displayMessage('error', 'Something went wrong, please try again.');
        // TODO: low reset form data if error occurs
      },
      () => {
        if (result.success) {
          this.displayMessage('success', result.message, '/security/users');
        } else {
          this.displayMessage(
            'error',
            'Something went wrong, please try again.'
          );
          // TODO: low reset form data if error occurs
        }
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }

  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    this.loaded = true;
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route) {
    this.success = true;
    this.successMessage = message;
    setTimeout(() => {
      this.dialogRef.close({ response: true });
    }, 1000);
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  private findAllRoles(groupId: number) {
    let data;
    this.groupService.findAllRoles(groupId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.roles = data;
        this.loaded = true;
      }
    );
  }
}
