import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { GroupService, UserGroupService } from 'modules/security/services';
import { UserGroup, User } from 'modules/security/models';

@Component({
  selector: 'security-assign-group',
  templateUrl: './assign-user-to-group.component.html',
  styleUrls: ['./assign-user-to-group.component.css']
})
export class AssignUserToGroupComponent {
  public loaded: boolean = false;
  public userGroup: UserGroup;
  public users: User[];

  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;

  constructor(
    private groupService: GroupService,
    private userGroupService: UserGroupService,
    public dialogRef: MatDialogRef<AssignUserToGroupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: UserGroup
  ) {
    this.userGroup = data;
    this.findAllUsers(this.userGroup.groupId);
  }

  /**
   * submit a note against complaint suggestion or appreciation
   */
  submit() {
    // get id of loggedIn userId
    this.userGroup.createdBy = +this.getLoggedInUserInfo('id');

    let result;
    this.userGroupService.assign(this.userGroup).subscribe(
      response => {
        result = response;
      },
      error => {
        this.displayMessage('error', 'Something went wrong, please try again.');
        // TODO: low reset form data if error occurs
      },
      () => {
        if (result.success) {
          this.displayMessage('success', result.message, '/security/users');
        } else {
          this.displayMessage(
            'error',
            'Something went wrong, please try again.'
          );
          // TODO: low reset form data if error occurs
        }
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }

  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    this.loaded = true;
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route) {
    this.success = true;
    this.successMessage = message;
    setTimeout(() => {
      this.dialogRef.close({ response: true });
    }, 1000);
  }

  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  private findAllUsers(groupId) {
    let data;
    this.groupService.findAllUsers(groupId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.users = data;
        this.loaded = true;
      }
    );
  }
}
