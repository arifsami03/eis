import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { Role } from 'modules/security/models';
import { RoleService } from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
/**
 * This component is being used for three purposes.
 *  1: Add new Roles
 *  2: View existing Roles
 *  3: Update existing Roles
 *
 */

@Component({
  selector: 'security-role-form',
  templateUrl: './role-form.component.html',
  styleUrls: ['./role-form.component.css']
})
export class RoleFormComponent implements OnInit {
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public role: Role;
  public options: Observable<string[]>;

  public componentLabels = Role.attributesLabels;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private roleService: RoleService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new Role().validationRules());
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      this.roleService.find(params['id']).subscribe(
        response => {
          this.role = response;
        },
        error => {
          this.loaded = true;
        },
        () => {

          this.fg.patchValue(this.role);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Role' });
    this.role = new Role();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        { label: this.role.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        {
          label: this.role.name,
          url: `/security/roles/view/${this.role.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: Role) {
    this.loaded = false;
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.role.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleService.delete(id).subscribe(
          response => {
            this.displayMessage('success', 'Role has been deleted', '/security/roles');
          },
          error => {
            this.loaded = true;
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            this.loaded = true;
          }
        );
      }
    });
  }

  private create(item: Role) {
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    let result;
    this.roleService.create(item).subscribe(
      response => {
        this.displayMessage('success', 'Role has been created successfully', `/security/roles/view/${response.id}`);
      },
      error => {
        this.loaded = true;
        this.displayMessage('error', 'Something went wrong, please try again.');
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private update(id: number, item: Role) {
    this.loaded = false;
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    this.roleService.update(id, item).subscribe(
      response => {
        this.displayMessage('success', 'Role has been saved successfully', `/security/roles/view/${id}`);
      },
      error => {
        this.loaded = true;
        this.displayMessage('error', 'Something went wrong, please try again.');
      },
      () => {
        this.loaded = true;
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    this.loaded = true;
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route) {
    this.success = true;
    this.successMessage = message;
    setTimeout((router: Router) => {
      this.router.navigate([route]);
    }, 1000);
  }
}
