import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  MatPaginator,
  MatTableDataSource,
  MatDialog,
  MatSort
} from '@angular/material';
import { WFStateFlowService, WFStateService, WorkFlowService, } from 'modules/configuration/services';
import { RoleWFStateFlowService } from 'modules/security/services';
import { WFStateModel, WFStateFlowModel, WorkFlowModel } from 'modules/configuration/models';

import { RoleWFStateFlowModel } from 'modules/security/models';


@Component({
  selector: 'role-wf-state-flow-form',
  templateUrl: './role-wf-state-flow-form.component.html',
  styleUrls: ['./role-wf-state-flow-form.component.css']
})
export class RoleWFStateFlowFormComponent {
  public loaded: boolean = false;
  public wfState: WFStateModel[];
  public wfStateFlow: WFStateFlowModel[];
  public workFlow: WorkFlowModel[];
  displayedColumns = ['from', 'to', 'option'];
  public roleWFStateFlow: RoleWFStateFlowModel;
  public fg: FormGroup;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;
  public success: Boolean;
  public state: string;
  public displayFlows: boolean = false;
  public selectedFlow = [];

  public flowsList = [];

  dataSource: any;
  listOfFlows: any = [];
  public componentLabels = RoleWFStateFlowModel.attributesLabels;

  constructor(
    private wfStateService: WFStateService,
    private wfStateFlowService: WFStateFlowService,
    private wfService: WorkFlowService,
    private roleWFStateFlowService: RoleWFStateFlowService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<RoleWFStateFlowFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loaded = true;
  }

  /**
  * ngOnInit
  */
  ngOnInit() {
    this.initializePage();
    this.getWorkFlow();

    this.getWFStateFlowByRole();


    //this.getWFState();
    if (this.data.type != 'Add Work Flow Permission') {
      this.fg.controls['WFId'].setValue(this.data.WFId);

      this.allStatesOfWF();

    }
  }
  /**
   * initialize page
   */
  private initializePage() {
    this.fg = this.fb.group(new RoleWFStateFlowModel().validationRules());

  }

  getWorkFlow() {
    this.wfService.index().subscribe(
      response => {
        this.workFlow = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getWFStateFlowByRole() {

    this.roleWFStateFlowService.getWFStateFlowByRole(this.data.roleId).subscribe(
      response => {
        if (response) {
          for (var i = 0; i < response.length; i++) {
            this.listOfFlows.push(response[i].WFStateFlowId)
          }
        }

        //this.listOfFlows = response;


      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {

        this.loaded = true;
        // for (var i = 0; this.listOfFlows.length; i++) {
        //   this.selectedFlow.push({ roleId: this.data.roleId, WFStateFlowId: this.listOfFlows[i] });
        // }
      }
    );
  }

  wfChange(event) {
    this.flowsList = [];
    this.wfStateFlowService.getFlowByWFId(event.value).subscribe(
      response => {
        this.wfStateFlow = response;

        for (var i = 0; i < response.length; i++) {

          if (this.listOfFlows.includes(response[i]['id'])) {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: true
              }
            )
          }
          else {
            this.flowsList.push(
              {
                id: response[i]['id'],
                from: response[i].from,
                to: response[i].to,
                fromId: response[i].fromId,
                toId: response[i].toId,
                checked: false
              }
            )
            //}
          }

        }

        this.dataSource = new MatTableDataSource<WFStateFlowModel>(this.flowsList);
        this.displayFlows = true;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  allStatesOfWF() {
    this.flowsList = [];
    this.wfStateFlowService.getFlowByWFId(this.data.WFId).subscribe(
      response => {
        this.wfStateFlow = response;

        for (var i = 0; i < response.length; i++) {
          for (var j = 0; j < this.listOfFlows.length; j++) {
            if (response[i]['id'] == this.listOfFlows[j]['WFStateFlowId']) {
              this.flowsList.push(
                {
                  id: response[i]['id'],
                  from: response[i].from,
                  to: response[i].to,
                  fromId: response[i].fromId,
                  toId: response[i].toId,
                  checked: true
                }
              )
            }
            else {
              this.flowsList.push(
                {
                  id: response[i]['id'],
                  from: response[i].from,
                  to: response[i].to,
                  fromId: response[i].fromId,
                  toId: response[i].toId,
                  checked: false
                }
              )
            }
          }

        }

        this.dataSource = new MatTableDataSource<WFStateFlowModel>(this.flowsList);
        this.displayFlows = true;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
  deletedWorkFlows = [];
  checkChange(event, id) {
    if (event.checked) {
      this.selectedFlow.push({
        roleId: this.data.roleId,
        WFStateFlowId: id
      });

      for(var i=0;i < this.deletedWorkFlows.length;i++){
        if(id == this.deletedWorkFlows[i].WFStateFlowId){
          this.deletedWorkFlows.splice(i, 1);
        }
      }


    }
    else {
      for (var i = 0; i < this.selectedFlow.length; i++) {
        if (id == this.selectedFlow[i]['WFStateFlowId']) {
          this.selectedFlow.splice(i, 1);
        }

      }
      this.deletedWorkFlows.push({WFStateFlowId:id,roleId:this.data.roleId});


    }

  }




  /**
   * cancel form and go back
   */
  onNoClick(): void {
    this.dialogRef.close({ response: false });
  }

  /**
 * Add or update data in database when save button is clicked
 */
  public saveData() {
    var postData={
      data:this.selectedFlow,
      deletedIds:this.deletedWorkFlows
    }
    console.log(this.deletedWorkFlows);
    if (this.data.type === 'Add Work Flow Permission') {
      this.roleWFStateFlowService.create(postData).subscribe(
        response => {
          this.dialogRef.close({ response: false });
        },
        error => {
          console.log(error);
          this.loaded = true;
        }
      );
    }
    else {
      console.log(this.selectedFlow)
      // this.wfStateFlowService.update(this.data.id, item).subscribe(
      //   response => {
      //     this.dialogRef.close({ response: false });
      //   },
      //   error => {
      //     console.log(error);
      //     this.loaded = true;
      //   }
      // );
    }
  }



}
