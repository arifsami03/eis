import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { User, Group, Role, FeaturePermission } from 'modules/security/models';
import {
  RoleService,
  RoleAssignmentService,
  FeaturePermissionService
} from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';
import {
  AssignGroupToRoleComponent,
  AssignUserToRoleComponent
} from 'modules/security/components';

@Component({
  selector: 'security-role-view',
  templateUrl: './role-view.component.html',
  styleUrls: ['./role-view.component.css']
})
export class RoleViewComponent implements OnInit {
  public loaded: boolean = false;
  public enableClass: boolean = true;

  // check for permissions are succesfully received
  public showDivBar: boolean = false;

  public fg: FormGroup;
  public pageTitle: string;
  public role: Role = new Role();
  public userLabels = User.attributesLabels;
  public groupLabels = Group.attributesLabels;
  public roleLabels = Role.attributesLabels;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private roleService: RoleService,
    private featurePermissionService: FeaturePermissionService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog,
    private fb: FormBuilder,
  ) { }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.role.id = param.id;
    });
    this.getData(this.role.id);
    this.findAllFeatures(this.role.id);
  }

  private getData(id: number) {
    this.roleService.find(id).subscribe(
      response => {
        this.role = response;
      },
      error => {
        this.loaded = true;
      },
      () => {
        this.loaded = true;
        // initialize formGroup to view data 
        this.fg = this.fb.group(new Role().validationRules());
        this.fg.patchValue(this.role);
        this.fg.disable();
        this.findAllFeatures(this.role.id);
        this.modifyPageHeader();
      }
    );
  }

  private findAllFeatures(roleId: number) {
    this.roleService.findAllFeatures(roleId).subscribe(
      response => {
        this.role.assignedPermissions = response;
      },
      error => { },
      () => { }
    );
  }

  public assignGroup(id: number) {
    const dialogRef = this.matDialog.open(AssignGroupToRoleComponent, {
      width: '700px',
      data: { roleId: id, roleName: this.role.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(this.role.id);
      }
    });
  }

  public deleteGroup(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        let result;
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  public assignUser(id: number) {
    const dialogRef = this.matDialog.open(AssignUserToRoleComponent, {
      width: '700px',
      data: { roleId: id, roleName: this.role.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(this.role.id);
      }
    });
  }

  public deleteUser(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  public changePermission(roleId: number) {
    this.showDivBar = true;

    let result;

    this.featurePermissionService
      .updateRolePermissions(roleId, this.role.assignedPermissions)
      .subscribe(
        response => {
          result = response;
        },
        error => {
          this.showDivBar = false;
          console.log(error);
          this.error = true;
          this.errorMessage = 'Something went wrong, please try again.';
          setTimeout(() => {
            this.error = false;
          }, 1000);
        },

        () => {
          this.showDivBar = false;
          if (result.success) {
            this.success = true;
            this.successMessage = result.message;
          } else {
            this.error = true;
            this.errorMessage = 'Something went wrong, please try again.';
          }

          setTimeout(() => {
            this.success = false;
            this.error = false;
          }, 1000);
        }
      );
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message, '/security/roles');
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'Role: ' + this.role.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Roles', url: '/security/roles' },
        { label: this.role.name }
      ]
    });
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
      this.getData(this.role.id);
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route?) {
    this.success = true;
    this.successMessage = message;
    if (route) {
      setTimeout((router: Router) => {
        this.router.navigate([route]);
      }, 1000);
    } else {
      // Hide success message after one sec
      setTimeout(() => {
        this.success = false;
        this.getData(this.role.id);
      }, 1000);
    }
  }

  public enableEditMode() {
    this.enableClass = false;
  }
}
