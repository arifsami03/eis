import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';

import { Group } from 'modules/security/models';
import { GroupService } from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';
/**
 * This component is being used for three purposes.
 *  1: Add new Groups
 *  2: View existing Groups
 *  3: Update existing Groups
 *
 */

@Component({
  selector: 'security-group-form',
  templateUrl: './group-form.component.html',
  styleUrls: ['./group-form.component.css']
})
export class GroupFormComponent implements OnInit {
  public loaded: boolean = false;

  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string;
  public group: Group;
  public options: Observable<string[]>;

  public componentLabels = Group.attributesLabels;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private groupService: GroupService,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.initializePage();
  }

  private initializePage() {
    this.fg = this.fb.group(new Group().validationRules());
    if (this.pageAct === 'add') {
      this.initAddPage();
    } else {
      this.getData();
    }
  }

  private getData() {
    this.route.params.subscribe(params => {
      let data;
      this.groupService.find(params['id']).subscribe(
        response => {
          data = response;
        },
        error => console.log(error),
        () => {
          this.group = data;
          this.fg.patchValue(this.group);
          if (this.pageAct === 'view') {
            this.initViewPage();
          } else if (this.pageAct === 'update') {
            this.initUpdatePage();
          }
          this.loaded = true;
        }
      );
    });
  }

  private initAddPage() {
    this.layoutService.setPageTitle({ title: 'Add Group' });
    this.group = new Group();
    this.fg.enable();

    this.loaded = true;
  }

  private initViewPage() {
    this.fg.disable();

    this.layoutService.setPageTitle({
      title: 'Group: ' + this.group.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Groups', url: '/security/groups' },
        { label: this.group.name }
      ]
    });
  }

  private initUpdatePage() {
    this.fg.enable();

    this.layoutService.setPageTitle({
      title: 'Group: ' + this.group.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Groups', url: '/security/groups' },
        {
          label: this.group.name,
          url: `/security/groups/view/${this.group.id}`
        },
        { label: 'Update' }
      ]
    });
  }

  public saveData(item: Group) {
    this.loaded = false;
    if (this.pageAct === 'add') {
      this.create(item);
    } else {
      this.update(this.group.id, item);
    }
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.groupService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage(
                'success',
                result.message,
                '/security/groups'
              );
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  private create(item: Group) {
    // get id of loggedIn userId
    item.createdBy = +this.getLoggedInUserInfo('id');

    let result;
    this.groupService.create(item).subscribe(
      response => {
        result = response;
      },
      error => {
        this.displayMessage('error', 'Something went wrong, please try again.');
        // TODO: low reset form data if error occurs
      },
      () => {
        if (result.success) {
          this.displayMessage('success', result.message, '/security/groups');
        } else {
          this.displayMessage(
            'error',
            'Something went wrong, please try again.'
          );
          // TODO: low reset form data if error occurs
        }
      }
    );
  }

  private update(id: number, item: Group) {
    // get id of loggedIn userId
    item.updatedBy = +this.getLoggedInUserInfo('id');

    let result;
    this.groupService.update(id, item).subscribe(
      response => {
        result = response;
      },
      error => {
        this.displayMessage('error', 'Something went wrong, please try again.');
        // TODO: low reset form data if error occurs
      },
      () => {
        if (result.success) {
          this.displayMessage('success', result.message, '/security/groups');
        } else {
          this.displayMessage(
            'error',
            'Something went wrong, please try again.'
          );
          // TODO: low reset form data if error occurs
        }
      }
    );
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    this.loaded = true;
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route) {
    this.success = true;
    this.successMessage = message;
    setTimeout((router: Router) => {
      this.router.navigate([route]);
    }, 1000);
  }
}
