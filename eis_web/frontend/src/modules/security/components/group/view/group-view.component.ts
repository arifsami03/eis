import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material';

import { Observable } from 'rxjs/Observable';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { User, Group, Role } from 'modules/security/models';
import {
  GroupService,
  UserGroupService,
  RoleAssignmentService,
  FeaturePermissionService
} from 'modules/security/services';
import { LayoutService } from 'modules/layout/services';
import { ConfirmDialogComponent } from 'modules/shared/components';
import {
  AssignUserToGroupComponent,
  AssignRoleToGroupComponent
} from 'modules/security/components';
import {} from 'modules/security/components';

@Component({
  selector: 'security-group-view',
  templateUrl: './group-view.component.html',
  styleUrls: ['./group-view.component.css']
})
export class GroupViewComponent implements OnInit {
  public loaded: boolean = false;

  public showDivBar: boolean = false;

  public pageTitle: string;
  public group: Group = new Group();
  public userLabels = User.attributesLabels;
  public groupLabels = Group.attributesLabels;
  public roleLabels = Role.attributesLabels;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private groupService: GroupService,
    private featurePermissionService: FeaturePermissionService,
    private userGroupService: UserGroupService,
    private roleAssignmentService: RoleAssignmentService,
    private layoutService: LayoutService,
    private matDialog: MatDialog
  ) {}

  /**
   * ngOnInit
   */
  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.group.id = param.id;
    });
    this.getData(this.group.id);
    this.findAllPermissions(this.group.id);
  }

  private getData(id: number) {
    let data;
    this.groupService.findDetail(id).subscribe(
      response => {
        data = response;
      },
      error => console.log(error),
      () => {
        this.group = data;
        this.loaded = true;
        this.modifyPageHeader();
      }
    );
  }

  private findAllPermissions(groupId: number) {
    let data;
    this.groupService.findAllPermissions(groupId).subscribe(
      response => {
        data = response;
      },
      error => {
        console.log(error);
      },
      () => {
        this.group.assignedPermissions = data;
        this.showDivBar = false;
      }
    );
  }

  public addUser(groupId: number) {
    const dialogRef = this.matDialog.open(AssignUserToGroupComponent, {
      width: '70%',
      data: { groupId: groupId, groupName: this.group.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(this.group.id);
        this.findAllPermissions(this.group.id);
      }
    });
  }

  public deleteUser(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        // this.loaded = false;
        let result;
        this.userGroupService.revoke(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  public assignRole(id: number) {
    const dialogRef = this.matDialog.open(AssignRoleToGroupComponent, {
      width: '70%',
      data: { groupId: id, groupName: this.group.name }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.response) {
        this.getData(this.group.id);
        this.findAllPermissions(this.group.id);
      }
    });
  }

  public deleteRole(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.roleAssignmentService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message);
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }
  public changePermission(roleId: number) {
    this.showDivBar = true;
    let result;
    this.featurePermissionService
      .updateGroupPermissions(roleId, this.group.assignedPermissions)
      .subscribe(
        response => {
          result = response;
        },
        error => {
          this.showDivBar = false;
          this.error = true;
          this.errorMessage = 'Something went wrong, please try again.';
          setTimeout(() => {
            this.error = false;
          }, 1000);
        },

        () => {
          this.showDivBar = false;
          if (result.success) {
            this.success = true;
            this.successMessage = result.message;
          } else {
            this.error = true;
            this.errorMessage = 'Something went wrong, please try again.';
          }

          setTimeout(() => {
            this.success = false;
            this.error = false;
          }, 1000);
        }
      );
  }

  public delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.groupService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => {
            this.displayMessage(
              'error',
              'Something went wrong, please try again.'
            );
            // TODO: low reset form data if error occurs
          },

          () => {
            if (result.success) {
              this.displayMessage('success', result.message, '/security/users');
            } else {
              this.displayMessage(
                'error',
                'Something went wrong, please try again.'
              );
            }
          }
        );
      }
    });
  }

  private modifyPageHeader() {
    this.layoutService.setPageTitle({
      title: 'Group: ' + this.group.name,
      breadCrumbs: [
        { label: 'Home', url: '/home/dashboard' },
        { label: 'Groups', url: '/security/groups' },
        { label: this.group.name }
      ]
    });
  }

  private getLoggedInUserInfo(parameter: string): string {
    return localStorage.getItem(parameter);
  }
  /**
   * Display flash message success | error
   * @param messageType
   * @param message
   * @param route
   */
  private displayMessage(messageType, message, route?) {
    if (messageType === 'error') {
      this.displayErrorMessage(message);
    } else {
      this.displaySuccessMessage(message, route);
    }
  }

  /**
   * Dispaly error message as flash and hide it after 1 sec
   * @param message
   */
  private displayErrorMessage(message) {
    this.error = true;
    this.errorMessage = message;
    // Hide error message after one sec
    setTimeout(() => {
      this.error = false;
      this.getData(this.group.id);
      this.findAllPermissions(this.group.id);
    }, 1000);
  }

  /**
   * Display success message and navigate to given route string
   * @param message
   * @param route
   */
  private displaySuccessMessage(message, route?) {
    this.success = true;
    this.successMessage = message;
    if (route) {
      setTimeout((router: Router) => {
        this.router.navigate([route]);
      }, 1000);
    } else {
      // Hide success message after one sec
      setTimeout(() => {
        this.success = false;
        this.getData(this.group.id);
        this.findAllPermissions(this.group.id);
      }, 1000);
    }
  }
}
