import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { LayoutService } from 'modules/layout/services';
import { GroupService } from 'modules/security/services';
import { ConfirmDialogComponent } from 'modules/shared/components';

import { Group } from 'modules/security/models';

@Component({
  selector: 'security-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.css']
})
export class GroupListComponent implements OnInit {
  public loaded: boolean = false;

  public attrLabels = Group.attributesLabels;

  displayedColumns = ['name', 'options'];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    public layoutService: LayoutService,
    private groupService: GroupService,
    public dialog: MatDialog,
    public matDialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.layoutService.setPageTitle({ title: 'Groups' });
    this.getRecords();
  }

  getRecords(): void {
    let data;
    this.groupService.getAll().subscribe(
      response => {
        data = response;
      },
      error => console.log(error),
      () => {
        this.dataSource = new MatTableDataSource<Group>(data);
        this.dataSource.paginator = this.paginator;
        this.loaded = true;
        if (this.success) {
          this.success = false;
        }
      }
    );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); // TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }

  delete(id: number) {
    // Confirm dialog
    const dialogRef = this.matDialog.open(ConfirmDialogComponent, {
      width: '350px',
      data: { message: 'Do you want to delete it permanently?' }
    });
    dialogRef.afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.loaded = false;
        let result;
        this.groupService.delete(id).subscribe(
          response => {
            result = response;
          },
          error => console.log(error),
          () => {
            this.loaded = true;
            if (result.success) {
              this.success = true;
              this.successMessage = result.message;
              setTimeout(() => {
                this.getRecords();
              }, 1000);
            } else {
              // Error Display
              this.error = true;
              this.errorMessage = result.error;
              // Hide error message after one sec
              setTimeout(() => {
                this.error = false;
              }, 1000);
            }
          }
        );
      }
    });
  }
}
