import { FormControl, Validators, ValidatorFn } from '@angular/forms';

export class RoleWFStateFlowModel {
  static attributesLabels = {
    roleId: 'Name',
    wfStateFlowID: 'State',
    from:'From',
    to:'To',
    WFId:'Work Flow'
  };

  public id?: number;
  public roleId: number;
  public wfStateFlowID: number;
  public from: string;
  public to: string;
  public WFId: string;
  

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
    
      WFId: new FormControl('', [Validators.required]),
    
      
    };
  }
}
