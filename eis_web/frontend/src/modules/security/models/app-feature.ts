export class AppFeature {
  public id: number;
  public name: string;
  public title: string;
  public parentId: number;
  public status: boolean;

  // following properites must be deleted after implementing permissions screen succesfully

  public featureId: number;
  public type: string;
  public check: boolean = true;
  public createdBy?: number;
  public updatedBy?: number;

  constructor() {}
}
