import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { UserGroup, RoleAssignment, FeaturePermission } from '.';

export class Group {
  static attributesLabels = {
    name: 'Name',
    createdBy: 'Created by',
    updatedBy: 'Updated by'
  };

  public id?: number;
  public name: string;
  public userGroups?: UserGroup[];
  public roleAssignments?: RoleAssignment[];
  public assignedPermissions: FeaturePermission[] = [new FeaturePermission()];
  public createdBy?: number;
  public updatedBy?: number;

  constructor() {}

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      name: new FormControl('', [<any>Validators.required])
    };
  }
}
