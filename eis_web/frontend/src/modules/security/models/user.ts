import { FormControl, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';
import { Group, UserGroup, RoleAssignment, FeaturePermission } from './';

export class User {
  static attributesLabels = {
    username: 'Username',
    password: 'Password',
    confirmPassword: 'Confirm Password',
    isSuperUser: 'Super User',
    isActive: 'Active',
    portal: 'Portal',
    createdBy: 'Created by',
    updatedBy: 'Updated by'
  };

  public id?: number;
  public name?: string;
  public username: string;
  public password?: string;
  public confirmPassword?: string;
  public isSuperUser: string;
  public isActive: string;
  public portal: string;
  public userGroups?: UserGroup[];
  public roleAssignments?: RoleAssignment[];
  public assignedPermissions: FeaturePermission[] = [new FeaturePermission()];
  public createdBy?: number;
  public updatedBy?: number;

  constructor() { }

  /**
     *
     * @param equalControlName
     */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      if (!control['_parent']) return null;

      if (!control['_parent'].controls[equalControlName])
        throw new TypeError(
          'Form Control ' + equalControlName + ' does not exists.'
        );

      let controlMatch = control['_parent'].controls[equalControlName];

      return controlMatch.value == control.value
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      id: [''],
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [
        Validators.required,
        Validators.maxLength(30)
      ]),
      confirmPassword: new FormControl('', [
        Validators.required,
        Validators.maxLength(30),
        this.equalTo('password')
      ]),
      isActive: new FormControl(''),
      isSuperUser: new FormControl('')
    };
  }
}
