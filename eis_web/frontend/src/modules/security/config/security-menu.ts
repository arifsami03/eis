export const SecurityMenu = [
  {
    heading: 'Security',
    menuItems: [
      { title: 'Dashboard', url: ['/security/dashboard'], icon: 'dashboard', perm: 'n/a' },
      { title: 'Users', url: ['/security/users'], icon: 'vpn_key', perm: 'users.index' },
      { title: 'Roles', url: ['/security/roles'], icon: 'vpn_key', perm: 'roles.index' }
      // { title: 'Groups', url: ['/security/groups'], icon: 'vpn_key' }
    ]
  }
];
