import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router/src/router';

import { LayoutService } from 'modules/layout/services';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  public loaded: boolean = true;


  lat: number;// = 51.678418;
  lng: number;// = 7.809007;

  constructor(private layoutService: LayoutService) {
  }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.layoutService.setPageTitle({
      title: 'Dashboard'
    });

  }


}
