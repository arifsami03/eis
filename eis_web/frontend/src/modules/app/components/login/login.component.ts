import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import {
  FormControl,
  FormBuilder,
  Validators,
  FormGroup
} from '@angular/forms';

import { LoginService } from 'modules/security/services';
import { Login } from 'modules/security/models';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService]
})
export class LoginComponent implements OnInit {
  public fg: FormGroup;

  public error: Boolean = false;

  public attributesLabels = Login.attributesLabels;

  public authError: string;

  constructor(
    private activatedRoute: Router,
    private loginService: LoginService,
    private fb: FormBuilder
  ) { }

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.fg = this.fb.group(new Login().validationRules());
  }

  /**
   * Login If user is valid
   * @param form NgForm
   */
  login(item: Login) {
    this.loginService.login(item).subscribe(
      response => {
        if (response) {

          if (response.toString() === 'registration') {
            this.activatedRoute.navigate([
              '/post-registration/campus/personalinfo'
            ]);
          } else {
            this.activatedRoute.navigate(['/institute/dashboard']);
          }
        }
      },
      error => {
        this.authError = error;
      }
    );
  }
}
