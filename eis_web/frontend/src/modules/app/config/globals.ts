export const GLOBALS = {
  appName: 'Education Information System',
  pageActions: { create: 'create', view: 'view', update: 'update' },
  deleteDialog: {
    width: '350px',
    message: 'Do you want to delete it permanently?',
    alertMessage: 'This Record cannot be deleted'
  },
  masks: {
    mobile: [/[0-9]/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    zip: [/[0-9]/, /\d/, /\d/, /\d/, /\d/]
  },
  metaKeys: {
    appName: 'appName',
    city: 'city',
    tehsil: 'tehsil',
    province: 'province',
    country: 'country',
    natureOfWork: 'natureOfWork',
    instrumentType: 'instrumentType',
    bank: 'bank'
  },
  campusApplicationType: {
    create: { title: 'Create Campus', value: 'create' },
    transfer: { title: 'Transfer Campus', value: 'transfer' }
  },
  campusApplicationStatus: {
    approve: { title: 'Approve', value: 'approve' },
    reject: { title: 'Reject', value: 'reject' },
    new: { title: 'New', value: 'new' },
    submit: { title: 'Submit', value: 'submit' }
  },
  session: {
    spring: { title: 'Spring', value: 'spring' },
    summer: { title: 'Summer', value: 'summer' },
    fall: { title: 'Fall', value: 'fall' }
  },

  //for Road Map Form
  natureOfCourses: {
    cumpulsory: { title: 'Compulsory', value: 'compulsory' },
    elective: { title: 'Elective', value: 'elective' }
  },
  courseTypes: {
    theoratical: { title: 'Theoratical', value: 'theoratical' },
    practical: { title: 'Practical', value: 'practical' }
  },
  creditHours: [
    { id: 1, value: '1' },
    { id: 2, value: '2' },
    { id: 3, value: '3' },
    { id: 4, value: '4' },
    { id: 5, value: '5' },
    { id: 6, value: '6' }
  ]
};
