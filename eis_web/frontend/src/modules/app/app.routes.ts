import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import {
  AppComponent,
  LoginComponent,
  DashboardComponent
} from 'modules/app/components';
import { AuthGuardService as AuthGuard } from 'modules/app/services';

import {
  LayoutComponent,
  PublicLayoutComponent,
  PostRegistrationLayoutComponent
} from 'modules/layout/components';
import { PersonalInformationComponent } from '../campus/components';

/**
 * We need to check for root url that where should it go regarding logged in user's portal
 *
 * If portal is university got to /institute/dashboard
 * If portal is registration got to /post-registration/campus/personalinfo
 * If portal is campus got to 'not implemented yet'.
 *
 */
let rootRedirectUrl = '';

if (localStorage.getItem('portal') == 'university') {
  rootRedirectUrl = '/institute/dashboard';
} else if (localStorage.getItem('portal') == 'registration') {
  rootRedirectUrl = '/post-registration/campus/personalinfo';
} else if (localStorage.getItem('portal') == 'campus') {
  rootRedirectUrl = 'n/a';
}

/**
 * Available routing paths
 */
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'home',
    component: PublicLayoutComponent,
    children: [
      {
        path: '',
        loadChildren: 'modules/public/public.module#PublicModule'
      }
    ]
  },
  {
    path: 'post-registration',
    canActivate: [AuthGuard],
    component: PostRegistrationLayoutComponent,
    children: [
      {
        path: 'campus',
        loadChildren: 'modules/campus/campus.module#CampusModule',
        data: { breadcrumb: { display: false } }
      }
    ]
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: LayoutComponent,
    data: { breadcrumb: { title: 'Home', display: true } },
    children: [
      {
        path: '',
        redirectTo: rootRedirectUrl,
        pathMatch: 'full'
      },
      {
        path: 'institute',
        loadChildren: 'modules/institute/institute.module#InstituteModule',
        data: { breadcrumb: { title: 'Institute', display: false } }
      },

      {
        path: 'campus',
        loadChildren: 'modules/campus/campus.module#CampusModule',
        data: { breadcrumb: { title: 'Campus', display: true, disable: true } }
      },
      {
        path: 'configuration',
        loadChildren:
          'modules/configuration/configuration.module#ConfigurationModule',
        data: {
          breadcrumb: { title: 'Configuration', display: true, disable: true }
        }
      },
      {
        path: 'security',
        loadChildren: 'modules/security/security.module#SecurityModule',
        data: {
          breadcrumb: { title: 'Security', display: true, disable: true }
        }
      }
    ]
  },

  { path: '**', redirectTo: '' }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutes {}
