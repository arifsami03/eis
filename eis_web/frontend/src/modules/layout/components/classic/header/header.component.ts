import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { ModuleMenu } from '../../../config/module-menu';

@Component({
  selector: 'layout-classic-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class ClassicHeaderComponent implements OnInit {
  public moduleMenu = ModuleMenu;

  public username: string;

  constructor(private router: Router, private layoutService: LayoutService) {}
  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.username = localStorage.getItem('username');
  }
  /**
   * logout
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['home']);
  }

  public navigateAndLoadMenu(moduleName: string) {
    this.layoutService.initModuleNav(moduleName);
  }
}
