import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LayoutService } from '../../../services';
import { Subscription } from 'rxjs';

import { InstituteMenu } from 'modules/institute/config/institute-menu';
import { CampusMenu } from 'modules/campus/config/campus-menu';
import { SecurityMenu } from 'modules/security/config/security-menu';
import { ConfigurationMenu } from 'modules/configuration/config/configuration-menu';

@Component({
  selector: 'layout-classic-left-nav',
  templateUrl: './left-nav.component.html',
  styleUrls: ['./left-nav.component.css']
})
export class ClassicLeftNavComponent implements OnInit {

  public loaded = false;

  public clicked: string = 'dashboard';

  public layoutSubscription: Subscription;

  public moduleMenuItems: any[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private layoutService: LayoutService
  ) {
    this.layoutSubscription = this.layoutService.initModuleNav$.subscribe(
      moduleName => {
        if (moduleName) {
          this.assignModuleMenu(moduleName);
        }
      }
    );
  }

  /**
   * ngOnInit()
   */
  ngOnInit() {

    this.clicked = this.router.url;
  }

  private assignModuleMenu(moduleName) {

    this.loaded = false;

    if (moduleName == 'institute') {
      this.moduleMenuItems = InstituteMenu;
    } else if (moduleName == 'campus') {
      this.moduleMenuItems = CampusMenu;
    } else if (moduleName == 'security') {
      this.moduleMenuItems = SecurityMenu;
    } else if (moduleName == 'configuration') {
      this.moduleMenuItems = ConfigurationMenu;
    }

    this.sortPermission();

    if (this.moduleMenuItems.length) {
      this.navigateToUrl(this.moduleMenuItems[0]['menuItems'][0]);
      this.loaded = true;
    } else {
      console.log('This module has no menu item with perm attribute');
    }
  }


  public sortPermission() {

    let permissionsAry = JSON.parse(localStorage.getItem('permissions'));

    let spliceIndexes = [];

    for (let c1 = 0; c1 < this.moduleMenuItems.length; c1++) {

      let MMItem = this.moduleMenuItems[c1];

      let found = false;

      for (let c2 = 0; c2 < MMItem['menuItems'].length; c2++) {

        let permissionName = MMItem['menuItems'][c2]['perm'];

        if (permissionName && permissionName != 'undefined') {

          let foundItem = permissionsAry.find(o => o['name'].toLowerCase() == permissionName.toLowerCase());

          if (!foundItem || (foundItem && foundItem['status'] == true)) {
            found = true;
          }
        }
      }

      if (found == false) {
        spliceIndexes.push(c1);
      }
    }

    for(let c3 = 0 ; c3 < spliceIndexes.length; c3++){
      this.moduleMenuItems.splice(spliceIndexes[c3]);
    }

  }
  /**
   * route links for main navigations
   * @param link String
   */
  public navigateToUrl(item) {
    this.clicked = item.title;
    this.router.navigate(item.url);
  }
}
