import { Subscription } from 'rxjs/Subscription';
import { LayoutService } from 'modules/layout/services';
import { Component, OnDestroy } from '@angular/core';

@Component({
  selector: 'layout-classic',
  templateUrl: './classic-layout.component.html',
  styleUrls: ['./classic-layout.component.css']
})
export class ClassicLayoutComponent implements OnDestroy {
  
  public subNavSub: Subscription;

  public subNavActive: boolean = false;

  constructor(private layoutService: LayoutService) {
    this.subNavSub = this.layoutService.subNav$.subscribe(response => {
      this.subNavActive = response;
    });
  }

  ngOnDestroy() {
    this.subNavSub.unsubscribe();
  }
}
