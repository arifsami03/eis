import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'layout-conf-nav',
  templateUrl: './conf-nav.component.html',
  styleUrls: ['./conf-nav.component.css']
})
export class ConfNavComponent implements OnInit {
  public clicked: string;
  configLinks = [
    {
      text: 'Designation',
      link: '/configurations/designation',
      icon: 'view_list'
    },
    {
      text: 'Department',
      link: '/configurations/department',
      icon: 'view_list'
    },
    { text: 'Gender', link: '/configurations/gender', icon: 'view_list' },
    {
      text: 'Application Title',
      link: '/configurations/appTitle',
      icon: 'view_list'
    },
    { text: 'Feedback', link: '/configurations/feedback', icon: 'view_list' },
    {
      text: 'Marital Status',
      link: '/configurations/maritalStatus',
      icon: 'view_list'
    },
    { text: 'MS', link: '/configurations/ms', icon: 'person' }
    // { text: 'Video', link: 'uploadVideo' }
  ];

  constructor(private router: Router, private route: ActivatedRoute) {}

  /**
   * ngOnInit()
   */
  ngOnInit() {
    this.clicked = this.router.url;
  }

  /**
   * route links for configurations
   * @param link String
   */
  getConfigType(link: string) {
    this.clicked = link;
    this.router.navigate([link]);
  }
}
