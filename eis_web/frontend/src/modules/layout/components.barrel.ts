// Import components and services etc here
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutService } from 'modules/layout/services/layout.service';

import {
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  SubHeaderComponent,
  ConfNavComponent,
  PublicLayoutComponent,
  PublicHeaderComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  PostRegistrationHeaderComponent,
  PostRegistrationLayoutComponent
} from './components';

export const __IMPORTS = [
  CommonModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatListModule,
  MatButtonModule,
  MatTooltipModule,
  FlexLayoutModule
];

export const __DECLARATIONS = [
  LayoutComponent,
  DefaultLayoutComponent,
  HeaderComponent,
  FooterComponent,
  LeftNavComponent,
  ConfNavComponent,
  SubHeaderComponent,
  PublicHeaderComponent,
  PublicLayoutComponent,
  ClassicLayoutComponent,
  ClassicHeaderComponent,
  ClassicSubHeaderComponent,
  ClassicLeftNavComponent,
  ClassicFooterComponent,
  PostRegistrationHeaderComponent,
  PostRegistrationLayoutComponent
];

export const __PROVIDERS = [LayoutService];

export const __ENTRY_COMPONENTS = [];
