export const ModuleMenu = [
  { name: 'institute', title: 'Institute', url: [], icon: '' },
  { name: 'campus', title: 'Campus', url: [], icon: '' },
  { name: 'security', title: 'Security', url: [], icon: '' },
  {
    name: 'configuration',
    title: 'Configuration',
    url: ['/configuration'],
    icon: ''
  }
];
