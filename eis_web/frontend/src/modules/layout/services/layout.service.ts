import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class LayoutService {
  
  /**
   * App title
   */
  private setTitleSource = new Subject<string>();
  setTitle$ = this.setTitleSource.asObservable();

  /**
   * For page title / sub-header title and breadcrumbs
   */
  private setPageTitleSource = new Subject<object>();
  setPageTitle$ = this.setPageTitleSource.asObservable();

  /**
   * For displaying or hiding Sub navigation (i.e. configuration)
   */
  private subNavSource = new Subject<boolean>();
  subNav$ = this.subNavSource.asObservable();

  /**
   * Classic Layout Module Navigation
   */
  private initModuleNavSource = new Subject<string>();
  initModuleNav$ = this.initModuleNavSource.asObservable();

  constructor() {}

  /**
   * Set Application Title
   * @param titleData string
   */
  setTitle(titleData: string) {
    this.setTitleSource.next(titleData);
  }

  /**
   * Set Page Title / Sub-Heading with breadcrumbs
   * @param titleData object
   */
  setPageTitle(titleData: object) {
    this.setPageTitleSource.next(titleData);
  }

  /**
   * Display / hide sub navigation i.e. configuration sub nav
   * @param active boolean
   */
  subNav(active: boolean) {
    this.subNavSource.next(active);
  }

  /**
   * initialize module navigation
   * 
   * @param moduleName string
   */
  initModuleNav(moduleName: string) {
    this.initModuleNavSource.next(moduleName);
  }
}
