import { GLOBALS } from 'modules/app/config/globals';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { FormBuilder } from '@angular/forms';

import { LayoutService } from 'modules/layout/services';
import { CampusPreRegistrationService } from 'modules/campus/services';

import { CampusPreRegistration } from 'modules/public/models';
//  import { Nfp_Data } from 'modules/security/data/nfp-data';

@Component({
  selector: 'campus-pre-registration-list',
  templateUrl: './pre-registration-list.component.html',
  styleUrls: ['./pre-registration-list.component.css']
})
export class PreRegistrationListComponent implements OnInit {
  public loaded: boolean = false;
  public data: CampusPreRegistration[] = [new CampusPreRegistration()];
  public status: string;
  public listLabels = new CampusPreRegistration().listLabels;

  public campusApplicationType = GLOBALS.campusApplicationType;
  public campusApplicationStatus = GLOBALS.campusApplicationStatus;
  public session = GLOBALS.session;

  displayedColumns = [
    'fullName',
    'email',
    'applicationType',
    'cityName',
    'status',
    'options'
  ];
  dataSource: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  // Success or Error message variables
  public success: Boolean;
  public successMessage: string;
  public errorMessage: string;
  public error: Boolean;

  constructor(
    private campusPreRegistrationService: CampusPreRegistrationService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.status = this.router.url.substring(
      this.router.url.lastIndexOf('/') + 1,
      this.router.url.length
    );
    const _title = this.router.url.substring(
      this.router.url.lastIndexOf('/') + 1,
      this.router.url.length
    );
    if (_title === 'new') {
      this.layoutService.setPageTitle({ title: 'Awaiting Applications' });
    } else if (_title === 'approve') {
      this.layoutService.setPageTitle({ title: 'Approved Applications' });
    } else if (_title === 'reject') {
      this.layoutService.setPageTitle({ title: 'Rejected Applications' });
    } else if (_title === 'submit') {
      this.layoutService.setPageTitle({ title: 'Submitted Applications' });
    }
    this.getRecords();
  }

  /**
   * Getting pre registartion list
   */
  getRecords(): void {
    this.campusPreRegistrationService
      .preRegistrationList(this.status)
      .subscribe(
        response => {
          this.data = response;
          this.data.map(_data => {
            _data.fullName = _data.campusOwner.fullName;
            _data.email = _data.campusOwner.email;
            _data.applicationType = _data.campusOwner.applicationType;
            _data.cityName = _data.city.metaValue;
          });
          this.dataSource = new MatTableDataSource<CampusPreRegistration>(
            this.data
          );
        },
        error => {},
        () => {
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.loaded = true;
        }
      );
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase(); //TODO: need to check what it do.
    this.dataSource.filter = filterValue;
  }
  sortData() {
    this.dataSource.sort = this.sort;
  }
}
