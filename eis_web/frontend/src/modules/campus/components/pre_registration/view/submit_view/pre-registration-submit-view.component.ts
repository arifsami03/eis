import { GLOBALS } from 'modules/app/config/globals';
import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { LayoutService } from 'modules/layout/services';
import { CampusPreRegistrationService } from 'modules/campus/services';

import { CampusPreRegistration } from 'modules/public/models';

import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'campus-pre-registration-submit-view',
  templateUrl: './pre-registration-submit-view.component.html',
  styleUrls: ['./pre-registration-submit-view.component.css']
})
export class PreRegistrationSubmitViewComponent implements OnInit {
  public loaded: boolean = false;
  public isEmailSending = false;
  public campusApplicationType = GLOBALS.campusApplicationType;
  public campusApplicationStatus = GLOBALS.campusApplicationStatus;
  public session = GLOBALS.session;
  campusPreRegistration: CampusPreRegistration = new CampusPreRegistration();
  public labels = new CampusPreRegistration().labels;
  public preRegStatus: string;
  public selectedButton:string='personal';

  campusId: number;
  constructor(
    private campusPreRegistrationService: CampusPreRegistrationService,
    public layoutService: LayoutService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.layoutService.setPageTitle({
      title: 'Campus Pre Resgistrations Submitted Form'
    });
    this.getParams();
  }

  getParams(): void {
    this.activatedRoute.params.subscribe(params => {
      this.campusId = +params['id'];
      // this.getRecords(this.campusId);
    });
  }

  getRecords(_id: number): void {
    this.campusPreRegistrationService.preRegistrationDetailOne(_id).subscribe(
      response => {
        this.campusPreRegistration = response;
        this.preRegStatus = this.campusPreRegistration.status;
      },
      error => {},
      () => {
        this.loaded = true;
      }
    );
  }
  changeStatus(_status: string) {
    this.isEmailSending = true;
    this.campusPreRegistrationService
      .changeStatus(this.campusId, { status: _status })
      .subscribe(
        response => {
          this.snackBar.open('Email has been sent.', _status.toUpperCase(), {
            duration: 2000
          });
          this.preRegStatus = _status;
          this.campusPreRegistration.status = _status;
          this.isEmailSending = false;
        },
        error => {
          this.snackBar.open('Email is not sent.', 'ERROR', {
            duration: 2000
          });
          this.isEmailSending = false;
        },
        () => {}
      );
  }

  tabChange(type:string){
    this.selectedButton=type;
  }
}
