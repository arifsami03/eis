import { Component, OnInit, Input } from '@angular/core';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';

import { CampusInformationModel } from 'modules/campus/models';
@Component({
  selector: 'create-campus',
  templateUrl: './create-campus.component.html',
  styleUrls: ['./create-campus.component.css']
})
export class CreateCampusComponent implements OnInit {
  public buildingType: string = 'available';

  @Input() fg: FormGroup;
  @Input() camModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  constructor() {}
  ngOnInit() {}
}
