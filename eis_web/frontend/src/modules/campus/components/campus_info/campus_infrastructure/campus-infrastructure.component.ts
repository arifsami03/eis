import { Component, OnInit, Input } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'campus-infrastructure',
  templateUrl: './campus-infrastructure.component.html',
  styleUrls: ['./campus-infrastructure.component.css']
})
export class CampusInfrastructureComponent implements OnInit {

  @Input() fg:FormGroup;
  public ownRentedCondition:boolean=false;
  constructor(  ) {

  }
  ngOnInit() {
  }
  ownRentedChange(event){
    if(event.value){
      this.ownRentedCondition=true;
    }
    else{
      this.ownRentedCondition=false;
      
    }


  }

}
