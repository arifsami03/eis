import { Component, OnInit, Input } from '@angular/core';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { CampusInformationModel } from 'modules/campus/models';

@Component({
  selector: 'campus-transfer',
  templateUrl: './campus-transfer.component.html',
  styleUrls: ['./campus-transfer.component.css']
})
export class CampusTransferComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() camInfoModel: CampusInformationModel;
  @Input() isSubmit: boolean = false;

  @Input() camModel: CampusInformationModel;
  public deletedAffiliationIds: number[] = [];

  constructor(private fb: FormBuilder) {}
  ngOnInit() {
    this.fg.controls['affiliations'] = this.fb.array([]);
    if (this.camInfoModel['affiliations'].length == 0) {
      this.addAffiliationClick();
    } else {
      for (var i = 0; i < this.camInfoModel['affiliations'].length; i++) {
        this.addAffiliationClick();
      }
      this.fg.patchValue(this.camInfoModel);
    }
  }

  createAffiliationItem(): FormGroup {
    return this.fb.group({
      affiliation: '',
      id: ''
    });
  }
  deleteAffiliationItem(id): FormGroup {
    return this.fb.group({
      id: id
    });
  }

  addAffiliationClick(): void {
    let affiliation = this.fg.get('affiliations') as FormArray;
    affiliation.push(this.createAffiliationItem());
  }
  removeAffiliationLink(i: number, item) {
    let deletedIds = this.fg.get('deletedAffiliationIds') as FormArray;
    deletedIds.push(this.deleteAffiliationItem(item.value.id));
    const control1 = <FormArray>this.fg.controls['affiliations'];
    control1.removeAt(i);
  }
}
