import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

import { FacultyService, ProgramDetailsService } from 'modules/institute/services';

import { FacultyModel } from 'modules/institute/models/index';
import { ProgramDetailsModel } from 'modules/institute/models';

import { FacultyProgramService } from 'modules/campus/services';

import { CampusInformationModel } from 'modules/campus/models';
@Component({
  selector: 'faculty-program',
  templateUrl: './faculty-program.component.html',
  styleUrls: ['./faculty-program.component.css']
})
export class FacultyProgramComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() campusInfo: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  public faculties: FacultyModel[] = [new FacultyModel()];
  public programs: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public selectedFaculties = [];
  public facultiesWithPrograms = [];
  public selectedPrograms = [];
  public loaded: boolean = false;

  public addButton: number = 0;
  public multiple: any = [];
  public facultyPrograms: any = [];

  constructor(
    private fb: FormBuilder,
    private facultyService: FacultyService,
    private programDetailService: ProgramDetailsService,
    private facultyProgramService: FacultyProgramService
  ) { }

  ngOnInit() {
    
    this.campusInfo['facultyProgram'].forEach(element => {
      this.selectedFaculties.push(element['facultyId']);
    });
    for (var i = 0; i < this.campusInfo['facultyProgram'].length; i++) {
      for (var j = 0;j < this.campusInfo['facultyProgram'][i]['programs'].length;j++) {
        this.selectedPrograms.push(
          this.campusInfo['facultyProgram'][i]['programs'][j].programDetailId
        );
      }
   
    }
    this.facultyService.getFacultiesWithPrograms().subscribe(res => {
      this.facultiesWithPrograms = res;
      this.getFacultyList();
    });
  }
  getFacultyList() {
    this.facultyService.getFaculties(this.selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.selectedFaculties.length == 0) {
          this.multiple.push({ faculty: [], facultyId: '', programs: [] });
          for (var i = 0; i < this.faculties.length; i++) {
            this.multiple[this.addButton]['faculty'].push({
              id: this.faculties[i].id,
              name: this.faculties[i].name
            });
          }
        } else {
          for (var i = 0; i < this.facultiesWithPrograms.length; i++) {
            if (
              this.selectedFaculties.includes(
                this.facultiesWithPrograms[i].facultyId
              )
            ) {
              this.multiple.push({ faculty: [], facultyId: '', programs: [] });
              this.multiple[this.addButton][
                'facultyId'
              ] = this.facultiesWithPrograms[i].facultyId;
              for (var j = 0; j < this.facultiesWithPrograms.length; j++) {
                this.multiple[this.addButton]['faculty'].push({
                  id: this.facultiesWithPrograms[j].facultyId,
                  name: this.facultiesWithPrograms[j].facultyName
                });
              }
              for (var k = 0;k < this.facultiesWithPrograms[i]['programs'].length;k++) {
                var checked = true;
                if (
                  this.selectedPrograms.includes(
                    this.facultiesWithPrograms[i]['programs'][k].id
                  )
                ) {
                  this.editChange(true, this.facultiesWithPrograms[i].facultyId,this.facultiesWithPrograms[i]['programs'][k].id);
                } else {
                  checked = false;
                  this.editChange(
                    false,
                    this.facultiesWithPrograms[i].facultyId,
                    this.facultiesWithPrograms[i]['programs'][k].id
                  );
                }
                this.multiple[this.addButton]['programs'].push({
                  id: this.facultiesWithPrograms[i]['programs'][k].id,
                  name: this.facultiesWithPrograms[i]['programs'][k].name,
                  checked: checked
                });
              }
              this.addButton = this.addButton + 1;
            }
          }

          if (
            this.facultiesWithPrograms.length != this.selectedFaculties.length
          ) {
            this.multiple.push({ faculty: [], facultyId: '', programs: [] });
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
    return this.faculties;
  }

  facultyChange(event) {
    this.selectedFaculties.push(event.value);
    this.programDetailService.getPrograms(event.value).subscribe(
      response => {
        this.programs = response;
        for (var i = 0; i < this.programs.length; i++) {
          this.multiple[this.addButton]['facultyId'] = event.value;
          this.multiple[this.addButton]['programs'].push({
            id: this.programs[i].id,
            name: this.programs[i].name
          });
        }
        this.addMultiple(event.value);
      },
      error => console.log(error),
      () => { }
    );
  }

  addMultiple(facultyId) {
    this.addButton = this.addButton + 1;
    this.facultyService.getFaculties(this.selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.faculties.length != 0) {
          this.multiple.push({ faculty: [], facultyId: '', programs: [] });
          for (var i = 0; i < this.faculties.length; i++) {
            this.multiple[this.addButton]['faculty'].push({
              id: this.faculties[i].id,
              name: this.faculties[i].name
            });
          }
        }
      },
      error => console.log(error),
      () => {
        if (this.isSubmit) {
        }
      }
    );
  }
  deleteFaculty(facultyId) {
    this.addButton = this.addButton - 1;
    for (var i = 0; i < this.multiple.length; i++) {
      if (this.multiple[i].facultyId == facultyId) {
        this.multiple.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selectedFaculties.length; i++) {
      if (this.selectedFaculties[i] == facultyId) {
        this.selectedFaculties.splice(i, 1);
      }
    }
    for (var j = 0; j < this.facultyPrograms.length; j++) {
      if (this.facultyPrograms[j]['facultyId'] == facultyId) {
        this.facultyPrograms.splice(j, 1);
      }
    }
    this.facultyService.getFaculties(this.selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.faculties.length != 0) {
          if (this.multiple[this.multiple.length - 1]['facultyId'] == '') {
            // this.multiple.push(
            //   { faculty: [], facultyId: '', programs: [] }
            // )
            this.multiple[this.multiple.length - 1]['faculty'] = [];
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          } else {
            this.multiple.push({ faculty: [], facultyId: '', programs: [] });
            //this.multiple[this.multiple.length -1]['faculty']=[];
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          }
        }
        // this.multiple[this.multiple.length - 1]['faculty'] = [];
        // if (this.faculties.length != 0) {

        //   for (var i = 0; i < this.faculties.length; i++) {
        //     this.multiple[this.multiple.length - 1]['faculty'].push({
        //       id: this.faculties[i].id,
        //       name: this.faculties[i].name
        //     })
        //   }
        // }
      },
      error => console.log(error),
      () => { }
    );
  }
  checkChange(event, facultyId, proId) {
    var trueCondition = true;
    if (event.checked == true) {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          this.facultyPrograms[i]['programs'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.facultyPrograms.push({
          facultyId: facultyId,
          programs: []
        });
        for (var i = 0; i < this.facultyPrograms.length; i++) {
          if (this.facultyPrograms[i]['facultyId'] == facultyId) {
            this.facultyPrograms[i]['programs'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          for (var j = 0; j < this.facultyPrograms[i]['programs'].length; j++) {
            if (this.facultyPrograms[i]['programs'][j] == proId) {
              this.facultyPrograms[i]['programs'].splice(j, 1);
            }
          }
          if (this.facultyPrograms[i]['programs'].length != 0) {
          } else {
            this.facultyPrograms.splice(i, 1);
          }
        }
      }
    }
    this.facultyProgramService.sendFacultyProgram(this.facultyPrograms);
  }

  editChange(event, facultyId, proId) {
    var trueCondition = true;
    if (event == true) {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          this.facultyPrograms[i]['programs'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.facultyPrograms.push({
          facultyId: facultyId,
          programs: []
        });
        for (var i = 0; i < this.facultyPrograms.length; i++) {
          if (this.facultyPrograms[i]['facultyId'] == facultyId) {
            this.facultyPrograms[i]['programs'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          if (this.facultyPrograms[i]['programs'].length != 0) {
            for (
              var j = 0;
              j < this.facultyPrograms[i]['programs'].length;
              j++
            ) {
              if (this.facultyPrograms[i]['programs'][j] == proId) {
                this.facultyPrograms[i]['programs'].splice(j, 1);
              }
            }
          } else {
            this.facultyPrograms.splice(i, 1);
          }
        }
      }
    }
    this.facultyProgramService.sendFacultyProgram(this.facultyPrograms);
  }
}
