import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { FacultyService, ProgramDetailsService } from 'modules/institute/services';
import { FacultyProgramService } from 'modules/campus/services';

import { FacultyModel, ProgramDetailsModel } from 'modules/institute/models/index';
import { CampusInformationModel } from 'modules/campus/models';

@Component({
  selector: 'faculty-program-details',
  templateUrl: './faculty-program-details.component.html',
  styleUrls: ['./faculty-program-details.component.css']
})
export class FacultyProgramDetailsComponent implements OnInit {
  @Input() fg: FormGroup;
  @Input() campusInfo: CampusInformationModel;
  @Input() isSubmit: boolean = false;
  public faculties: FacultyModel[] = [new FacultyModel()];
  public programDetails: ProgramDetailsModel[] = [new ProgramDetailsModel()];
  public facultiesWithProgramDetails = [];
  public loaded: boolean = false;

  public selected = [];

  public addButton: number = 0;
  public multiple: any = [];
  public facultyPrograms: any = [];

  constructor(
    private fb: FormBuilder,
    private facultyService: FacultyService,
    private programDetailService: ProgramDetailsService,
    private facultyProgramService: FacultyProgramService
  ) { }

  ngOnInit() {

    this.campusInfo['facultyProgramDetails'].forEach(element => {

      let facultyId = element['facultyId'];

      let programs = [];
      element.programDetails.forEach(programDetail => {
        programs.push(programDetail.programDetailId);
      });

      this.selected.push({ facultyId: facultyId, programs: programs });

    });

    this.facultyService.getFacultiesWithProgramDetails().subscribe(res => {
      this.facultiesWithProgramDetails = res;
      this.getFacultyList();
    });
  }
  getFacultyList() {
    let selectedFaculties = [];

    this.selected.forEach(element => {
      selectedFaculties.push(element.facultyId);
    });

    this.facultyService.getFaculties(selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.selected.length == 0) {
          this.multiple.push({ faculty: [], facultyId: '', programDetails: [] });
          for (var i = 0; i < this.faculties.length; i++) {
            this.multiple[this.addButton]['faculty'].push({
              id: this.faculties[i].id,
              name: this.faculties[i].name
            });
          }
        } else {
          for (var i = 0; i < this.facultiesWithProgramDetails.length; i++) {
            let item = this.selected.find(element => {
              return element.facultyId == this.facultiesWithProgramDetails[i].facultyId;
            })
            if (item) {
              this.multiple.push({ faculty: [], facultyId: '', programDetails: [] });
              this.multiple[this.addButton][
                'facultyId'
              ] = this.facultiesWithProgramDetails[i].facultyId;
              for (var j = 0; j < this.facultiesWithProgramDetails.length; j++) {
                this.multiple[this.addButton]['faculty'].push({
                  id: this.facultiesWithProgramDetails[j].facultyId,
                  name: this.facultiesWithProgramDetails[j].facultyName
                });
              }
              for (var k = 0; k < this.facultiesWithProgramDetails[i]['programDetails'].length; k++) {
                var checked = true;

                if (
                  item.programs.includes(
                    this.facultiesWithProgramDetails[i]['programDetails'][k].id
                  )
                ) {
                  this.editChange(true, this.facultiesWithProgramDetails[i].facultyId, this.facultiesWithProgramDetails[i]['programDetails'][k].id);
                } else {
                  checked = false;
                  this.editChange(
                    false,
                    this.facultiesWithProgramDetails[i].facultyId,
                    this.facultiesWithProgramDetails[i]['programDetails'][k].id
                  );
                }
                this.multiple[this.addButton]['programDetails'].push({
                  id: this.facultiesWithProgramDetails[i]['programDetails'][k].id,
                  name: this.facultiesWithProgramDetails[i]['programDetails'][k].name,
                  checked: checked
                });
              }
              this.addButton = this.addButton + 1;
            }
          }

          if (
            this.facultiesWithProgramDetails.length != this.selected.length
          ) {
            this.multiple.push({ faculty: [], facultyId: '', programDetails: [] });
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
    return this.faculties;
  }

  facultyChange(event) {
    let facultyId = event.value;
    this.selected.push({ facultyId: facultyId, programs: [] });
    this.programDetailService.getFacultyProgramDetails(event.value).subscribe(
      response => {
        this.programDetails = response;
        for (var i = 0; i < this.programDetails.length; i++) {
          this.multiple[this.addButton]['facultyId'] = event.value;
          this.multiple[this.addButton]['programDetails'].push({
            id: this.programDetails[i].id,
            name: this.programDetails[i].name
          });
        }
        this.addMultiple(event.value);
      },
      error => console.log(error),
      () => { }
    );
  }

  addMultiple(facultyId) {
    this.addButton = this.addButton + 1;
    let selectedFaculties = [];

    this.selected.forEach(element => {
      selectedFaculties.push(element.facultyId);
    });
    this.facultyService.getFaculties(selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.faculties.length != 0) {
          this.multiple.push({ faculty: [], facultyId: '', programDetails: [] });
          for (var i = 0; i < this.faculties.length; i++) {
            this.multiple[this.addButton]['faculty'].push({
              id: this.faculties[i].id,
              name: this.faculties[i].name
            });
          }
        }
      },
      error => console.log(error),
      () => {
        if (this.isSubmit) {
        }
      }
    );
  }
  deleteFaculty(facultyId) {
    this.addButton = this.addButton - 1;
    for (var i = 0; i < this.multiple.length; i++) {
      if (this.multiple[i].facultyId == facultyId) {
        this.multiple.splice(i, 1);
      }
    }
    for (var i = 0; i < this.selected.length; i++) {
      if (this.selected[i]['facultyId'] == facultyId) {
        this.selected.splice(i, 1);
      }
    }
    for (var j = 0; j < this.facultyPrograms.length; j++) {
      if (this.facultyPrograms[j]['facultyId'] == facultyId) {
        this.facultyPrograms.splice(j, 1);
      }
    }
    let selectedFaculties = [];

    this.selected.forEach(element => {
      selectedFaculties.push(element.facultyId);
    });

    this.facultyService.getFaculties(selectedFaculties).subscribe(
      response => {
        this.faculties = response;
        if (this.faculties.length != 0) {
          if (this.multiple[this.multiple.length - 1]['facultyId'] == '') {
            this.multiple[this.multiple.length - 1]['faculty'] = [];
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          } else {
            this.multiple.push({ faculty: [], facultyId: '', programDetails: [] });
            for (var i = 0; i < this.faculties.length; i++) {
              this.multiple[this.addButton]['faculty'].push({
                id: this.faculties[i].id,
                name: this.faculties[i].name
              });
            }
          }
        }
      },
      error => console.log(error),
      () => { }
    );
  }
  checkChange(event, facultyId, proId) {
    var trueCondition = true;
    if (event.checked == true) {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          this.facultyPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.facultyPrograms.push({
          facultyId: facultyId,
          programDetails: []
        });
        for (var i = 0; i < this.facultyPrograms.length; i++) {
          if (this.facultyPrograms[i]['facultyId'] == facultyId) {
            this.facultyPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          for (var j = 0; j < this.facultyPrograms[i]['programDetails'].length; j++) {
            if (this.facultyPrograms[i]['programDetails'][j] == proId) {
              this.facultyPrograms[i]['programDetails'].splice(j, 1);
            }
          }
          if (this.facultyPrograms[i]['programDetails'].length != 0) {
          } else {
            this.facultyPrograms.splice(i, 1);
          }
        }
      }
    }
    this.facultyProgramService.sendFacultyProgram(this.facultyPrograms);
  }

  editChange(event, facultyId, proId) {
    var trueCondition = true;
    if (event == true) {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          this.facultyPrograms[i]['programDetails'].push(proId);
          trueCondition = false;
        }
      }
      if (trueCondition) {
        this.facultyPrograms.push({
          facultyId: facultyId,
          programDetails: []
        });
        for (var i = 0; i < this.facultyPrograms.length; i++) {
          if (this.facultyPrograms[i]['facultyId'] == facultyId) {
            this.facultyPrograms[i]['programDetails'].push(proId);
          }
        }
      }
    } else {
      for (var i = 0; i < this.facultyPrograms.length; i++) {
        if (this.facultyPrograms[i]['facultyId'] == facultyId) {
          if (this.facultyPrograms[i]['programDetails'].length != 0) {
            for (
              var j = 0;
              j < this.facultyPrograms[i]['programDetails'].length;
              j++
            ) {
              if (this.facultyPrograms[i]['programDetails'][j] == proId) {
                this.facultyPrograms[i]['programDetails'].splice(j, 1);
              }
            }
          } else {
            this.facultyPrograms.splice(i, 1);
          }
        }
      }
    }
    this.facultyProgramService.sendFacultyProgram(this.facultyPrograms);
  }
}
