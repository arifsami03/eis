import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NgControl,
  FormArray,
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import { MatRadioChange } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';

import { CampusInformationModel } from 'modules/campus/models';
import { LayoutService } from 'modules/layout/services';
import { CampusInfoService, FacultyProgramService, CampusPostRegistrationService } from 'modules/campus/services';



@Component({
  selector: 'campus-info',
  templateUrl: './campus-info.component.html',
  styleUrls: ['./campus-info.component.css']
})
export class CampusInfoComponent implements OnInit {
  public campusType: string = 'create';
  public subscription: Subscription;
  public loaded: boolean = false;
  public id: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Campus Information';
  public campusInfo: CampusInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public isSubmit: boolean = false;
  @Input() campusApplicantId: number;
  public mobileMask = [
    /[0-9]/,
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/
  ];

  public cnicMask = [
    /[0-9]/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    /\d/,
    '-',
    /\d/
  ];

  public componentLabels = CampusInformationModel.attributesLabels;
  public userId: number;
  public facultyPrograms = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService,
    private campusInfoService: CampusInfoService,
    private facultyProgramService: FacultyProgramService,
    private campusPostRegistrationService: CampusPostRegistrationService
  ) {
    this.subscription = this.facultyProgramService.getFacultyPrograms().subscribe(message => {
      this.facultyPrograms = message.facultyProgram;
    });
  }



  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();
    this.getLoginUserId();
    this.getCampusInfo();
  }
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  private initializePage() {
    this.fg = this.fb.group(new CampusInformationModel().validationRules());
  }

  public resetForm(event: MatRadioChange) {
    // this.fg = this.fb.group(new CampusInformationModel().validationRules());
    // this.fg.patchValue(this.campusInfo)
  }

  /**
   * Function to get Login user id from Local Storage;
   */
  getLoginUserId() {
    this.userId = Number(localStorage.getItem('id'));
  }

  /**
   * Function to get Campus owner id from user Table
   */
  getCampusInfo() {
    this.campusApplicantId = (this.campusApplicantId ? this.campusApplicantId : this.userId);
    this.campusInfoService.findCampusInfo(this.campusApplicantId).subscribe(response => {
      this.campusInfo = response;

      this.id = response['id'];


      this.fg.patchValue(this.campusInfo)
      if (this.campusInfo.campusName == '0') {
        this.fg.controls['campusName'].setValue('');
      }

      if (response['applicationType'] == 'create') {

        this.fg.controls['campusType'].setValue(true);
      }
      else {
        this.fg.controls['campusType'].setValue(false);
      }

      if (response['buildingAvailable'] == null) {

        this.fg.controls['buildingAvailable'].setValue(true);
      }
      this.getRegistrationStatus();
    },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('email').hasError('required')
      ? 'Email is Required'
      : this.fg.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  public saveData(item: CampusInformationModel) {
    if (item.campusType) {
      item['applicationType'] = 'create';
    }
    else {
      item['applicationType'] = 'transfer';
    }


    item['facultyProgram'] = this.facultyPrograms;
    this.campusInfoService.update(this.id, item).subscribe(response => {

      this.router.navigate(['/post-registration/campus/remittance']);
    },
      error => {
        this.router.navigate(['/post-registration/campus/remittance']);
      }

    );
  }
  getRegistrationStatus() {
    this.campusApplicantId = (this.campusApplicantId ? this.campusApplicantId : this.userId);
    this.campusPostRegistrationService
      .getRegistrationStatus(this.campusApplicantId)
      .subscribe(
        res => {
          if (res === 'submit') {
            this.isSubmit = true;
            this.fg.disable();
          } else {
            this.fg.enable();
            this.isSubmit = false;
          }
        },
        error => {
          console.log(error);
          this.loaded = true;
        },
        () => {
          this.loaded = true;
        }
      );
  }
}
