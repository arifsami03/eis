import { Observable } from 'rxjs/Observable';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { startWith } from 'rxjs/operators/startWith';
import { map } from 'rxjs/operators/map';

import { PersonalInformationModel } from 'modules/campus/models';
import { LayoutService } from 'modules/layout/services';
import { MetaConfService, BankService } from 'modules/configuration/services';
import { CampusOwnerService, CampusPostRegistrationService } from 'modules/campus/services';

import { ConfirmDialogComponent } from 'modules/shared/components';

import { MetaConfModel, BankModel } from 'modules/configuration/models';
import { GLOBALS } from 'modules/app/config/globals';
import { CampusPreRegistration } from 'modules/public/models';

@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.css'],
  providers: [MetaConfService, CampusOwnerService]
})
export class PersonalInformationComponent implements OnInit {
  @Input() campusApplicantId: number;
  public loaded: boolean = false;
  public id: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Personal Information';
  public personalInfo: PersonalInformationModel;
  public states: string[] = ['Punjab', 'Sindh', 'Balochistan', 'KPK', 'Gilgit'];
  public options: Observable<string[]>;
  public deletedReferenceIds: number[] = [];
  public isSubmit: boolean = false;
  public mobileMask = [/[0-9]/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  public cnicMask = [/[0-9]/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/];

  public componentLabels = PersonalInformationModel.attributesLabels;
  public countries: MetaConfModel[];
  public province: MetaConfModel[];
  public cities: MetaConfModel[];
  public tehsils: MetaConfModel[];
  public natureOfwork: MetaConfModel[];
  public bank: BankModel[];
  public userId: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public matDialog: MatDialog,
    public metaService: MetaConfService,
    public bankService: BankService,
    public campusOwnerService: CampusOwnerService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private layoutService: LayoutService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.initializePage();
    this.getCountryList();
    this.getProvinceList();
    this.getCitiesList();
    this.getTehsilsList();
    this.getNatureOfWorkList();
    this.getBankList();
    this.getLoginUserId();
    this.getCampusOwnerInfo();
  }

  private initializePage() {
    //this.fg = this.fb.group(new PersonalInformationModel().validationRules());
    this.fg = this.fb.group({
      fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      // last_name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: ['', [<any>Validators.required, Validators.email]],
      mobileNumber: ['', [<any>Validators.maxLength(12)]],
      address: ['', [<any>Validators.maxLength(100)]],
      cnic: ['', [<any>Validators.required]],
      countryId: [''],
      cityId: [''],
      provinceId: [''],
      tehsilId: [''],
      state: [''],
      zip: [''],
      country: [''],
      description: [''],
      nearestBankId: [''],
      approxMonthlyIncome: [''],
      natureOfWorkId: [''],
      references: this.fb.array([])
    });
  }

  createReferenceItem(): FormGroup {
    return this.fb.group({
      id: '',
      refName: [''],
      refMobileNumber: ['', [<any>Validators.maxLength(12)]],
      refAddress: ['']
    });
  }

  addReferenceClick() {
    let references = this.fg.get('references') as FormArray;
    references.push(this.createReferenceItem());
    // disabling on submit
    this.getRegistrationStatus();
  }

  /**
   * Function to get Login user id from Local Storage;
   */
  getLoginUserId() {
    this.userId = Number(localStorage.getItem('id'));
  }

  /**
   * Function to get Campus owner id from user Table
   */
  getCampusOwnerInfo() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.userId;

    this.campusOwnerService.findPersonInfo(this.campusApplicantId).subscribe(
      response => {
        //this.province = response;
        this.personalInfo = response;

        this.id = response['id'];
        if (this.personalInfo['references'].length == 0) {
          this.addReferenceClick();
        }
        for (var i = 0; i < this.personalInfo['references'].length; i++) {
          this.addReferenceClick();
        }
        this.fg.patchValue(this.personalInfo);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  getEmailErrorMessage() {
    return this.fg.get('email').hasError('required') ? 'Email is Required' : this.fg.get('email').hasError('email') ? 'Not a valid email' : '';
  }

  public saveData(item: PersonalInformationModel) {
    let postData = {
      personalInfo: item,
      deleteRefernceIds: this.deletedReferenceIds
    };

    this.campusOwnerService.update(this.id, postData).subscribe(
      response => {
        this.router.navigate(['/post-registration/campus/campusinfo']);
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Countries
   */
  getCountryList() {
    this.metaService.getAll(GLOBALS.metaKeys.country).subscribe(
      response => {
        this.countries = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Provinces
   */
  getProvinceList() {
    this.metaService.getAll(GLOBALS.metaKeys.province).subscribe(
      response => {
        this.province = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Cities
   */
  getCitiesList() {
    this.metaService.getAll(GLOBALS.metaKeys.city).subscribe(
      response => {
        this.cities = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Tehsils
   */
  getTehsilsList() {
    this.metaService.getAll(GLOBALS.metaKeys.tehsil).subscribe(
      response => {
        this.tehsils = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Nature of Work
   */
  getNatureOfWorkList() {
    this.metaService.getAll(GLOBALS.metaKeys.natureOfWork).subscribe(
      response => {
        this.natureOfwork = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Get List of Banks
   */
  getBankList() {
    this.bankService.findAttributesList().subscribe(
      response => {
        this.bank = response;
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }

  removeReference(i: number, item) {
    this.deletedReferenceIds.push(item.value.id);
    const control = <FormArray>this.fg.controls['references'];
    control.removeAt(i);
  }
  getRegistrationStatus() {
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : this.userId;
    this.campusPostRegistrationService.getRegistrationStatus(this.campusApplicantId).subscribe(
      res => {
        if (res === 'submit') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {
        console.log(error);
        this.loaded = true;
      },
      () => {
        this.loaded = true;
      }
    );
  }
}
