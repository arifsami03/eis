import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'campus-registration',
  templateUrl: './campus-registration.component.html',
  styleUrls: ['./campus-registration.component.css']
})
export class CampusRegistrationComponent implements OnInit {
  @Input() selectedButton:any;
  public registration:boolean=true;
  constructor() {}
  ngOnInit() {
    var type = (localStorage.getItem('portal'));
    if(type=='registration'){
      this.registration=true;
    }
    else{
      this.registration=false;
    }

  }
}
