import { GLOBALS } from 'modules/app/config/globals';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { CampusPostRegistrationService, CampusOwnerService, RemittanceService, CampusInfoService } from 'modules/campus/services';
import { PersonalInformationModel, RemittanceModel, CampusInformationModel } from 'modules/campus/models';

@Component({
  selector: 'campus-post-registration-submit',
  templateUrl: './submit.component.html',
  styleUrls: ['./submit.component.css']
})
export class SubmitComponent implements OnInit {
  public loaded: boolean = false;
  public isEmailSending = false;
  public isSubmit: boolean = false;
  public userId: number;

  public personalInfo: PersonalInformationModel;
  public campusInfo: CampusInformationModel;
  public remittance: RemittanceModel;

  public personalInfoErrors: string[] = [];
  public remittanceErrors: string[] = [];
  public campusInfoErrors: string[] = [];

  constructor(
    private campusPostRegistrationService: CampusPostRegistrationService,
    private campusOwnerService: CampusOwnerService,
    private remittanceService: RemittanceService,
    private campusInfoService: CampusInfoService,
    public snackBar: MatSnackBar
  ) {}
  ngOnInit() {
    this.getRegistrationStatus();
    this.getLoginUserId();
    this.findPersonInfo();
    this.viewRemittance();
    this.findCampusInfo();
  }
  submit() {
    this.isEmailSending = true;
    this.loaded = false;
    this.campusPostRegistrationService.submitPostRegistration(+localStorage.getItem('id')).subscribe(
      response => {
        this.snackBar.open('Email has been sent.', 'Submitted', {
          duration: 2000
        });
        this.isEmailSending = false;
        this.isSubmit = true;
        this.loaded = true;
      },
      error => {
        this.snackBar.open('Email is not sent.', 'Not Submitted', {
          duration: 2000
        });
        this.isEmailSending = false;
        this.isSubmit = false;
        this.loaded = true;
      },
      () => {}
    );
  }
  getRegistrationStatus() {
    this.campusPostRegistrationService.getRegistrationStatus(+localStorage.getItem('id')).subscribe(
      res => {
        this.loaded = true;
        if (res === 'submit') {
          this.isSubmit = true;
        } else {
          this.isSubmit = false;
        }
      },
      error => {},
      () => {}
    );
  }
  /**
   * Function to get Login user id from Local Storage;
   */
  getLoginUserId() {
    this.userId = Number(localStorage.getItem('id'));
  }

  /**
   * Function to get Campus owner id from user Table
   */
  findPersonInfo() {
    this.campusOwnerService.findPersonInfo(this.userId).subscribe(
      response => {
        this.personalInfo = response;
        this.validatePersonalInfo();
      },
      error => console.log(error),
      () => {
        this.loaded = true;
      }
    );
  }

  /**
   * Pushing personal info form errors into array
   */
  validatePersonalInfo() {
    if (this.personalInfo) {
      if (!this.personalInfo.fullName) {
        this.personalInfoErrors.push('Full name is required.');
      }
      if (!this.personalInfo.mobileNumber) {
        this.personalInfoErrors.push('Mobile Number is required.');
      }
      if (!this.personalInfo.email) {
        this.personalInfoErrors.push('Email is required.');
      }
      if (!this.personalInfo.address) {
        this.personalInfoErrors.push('Adress is required.');
      }
      if (!this.personalInfo.cnic) {
        this.personalInfoErrors.push('CNIC is required.');
      }
      if (!this.personalInfo.countryId) {
        this.personalInfoErrors.push('Country is required.');
      }
      if (!this.personalInfo.cityId) {
        this.personalInfoErrors.push('City is required.');
      }
      if (!this.personalInfo.provinceId) {
        this.personalInfoErrors.push('Province is required.');
      }
      if (!this.personalInfo.tehsilId) {
        this.personalInfoErrors.push('Tehsil is required.');
      }
      if (!this.personalInfo.natureOfWorkId) {
        this.personalInfoErrors.push('Nature of work is required.');
      }
      if (!this.personalInfo.nearestBankId) {
        this.personalInfoErrors.push('Bank is required.');
      }
      if (this.personalInfo['references'].length > 0) {
        this.personalInfo['references'].forEach(element => {
          if (!element['refName']) {
            this.personalInfoErrors.push('Reference Name is required.');
          }
          if (!element['refMobileNumber']) {
            this.personalInfoErrors.push('Reference Mobile Number is required.');
          }
          if (!element['refAddress']) {
            this.personalInfoErrors.push('Reference Address is required.');
          }
        });
      }
      if (this.personalInfo['references'].length === 0) {
        this.personalInfoErrors.push('References are required.');
      }
    } else {
      this.personalInfoErrors.push('Personal Info');
    }
  }
  viewRemittance() {
    this.remittanceService.view(this.userId).subscribe(
      response => {
        this.remittance = response;
        this.validateRemittance();
      },
      error => {},
      () => {}
    );
  }
  validateRemittance() {
    if (this.remittance) {
      if (!this.remittance.instrumentCategory) {
        this.remittanceErrors.push('Instrument Category is required.');
      }
      if (!this.remittance.currentDate) {
        this.remittanceErrors.push('Current date is required.');
      }
      if (!this.remittance.bankId) {
        this.remittanceErrors.push('Bank is required.');
      }
      if (!this.remittance.branchName) {
        this.remittanceErrors.push('Branch Name is required.');
      }
      if (!this.remittance.instrumentDate) {
        this.remittanceErrors.push('Instrument date is required.');
      }
      if (!this.remittance.instrumentNumber) {
        this.remittanceErrors.push('Instrument Number is required.');
      }
      if (this.remittance.amount === 0 || !this.remittance.amount) {
        this.remittanceErrors.push('Amount is required.');
      }
      if (!this.remittance.purpose) {
        this.remittanceErrors.push('Purpose is required.');
      }
    } else {
      this.remittanceErrors.push('Remittance information is required.');
    }
  }

  findCampusInfo() {
    this.campusInfoService.findCampusInfo(this.userId).subscribe(
      response => {
        this.campusInfo = response;
        this.validateCampusInfo();
      },
      error => {},
      () => {}
    );
  }
  validateCampusInfo() {
    if (this.campusInfo) {
      // check for campus type
      if (this.campusInfo['applicationType'] === GLOBALS.campusApplicationType.create.value) {
        if (!this.campusInfo.campusName) {
          this.campusInfoErrors.push('Campus Name is required.');
        }
        if (!this.campusInfo.levelOfEducation) {
          this.campusInfoErrors.push('Level of Education is required.');
        }
        if (!this.campusInfo.tentativeSessionStart) {
          this.campusInfoErrors.push('Tentative session start is required.');
        }
        // check for faculty program
        // this.validateFacultyPrograms();

        // check for building available
        if (!this.campusInfo.buildingAvailable) {
          if (this.campusInfo['addresses'].length > 0) {
            this.campusInfo['addresses'].forEach(element => {
              if (!element['address']) {
                this.campusInfoErrors.push('Building Address is required.');
              }
              if (!element['latitude'] || !element['longitude']) {
                this.campusInfoErrors.push('Building Location is required.');
              }
            });
          } else {
            this.campusInfoErrors.push('Building Information is required.');
          }
        }
      } else {
        if (this.campusInfo.noOfCampusTransfer === 0 || !this.campusInfo.noOfCampusTransfer) {
          this.campusInfoErrors.push('Number of campuses is required.');
        }
        if (!this.campusInfo.campusName) {
          this.campusInfoErrors.push('Campus Name is required.');
        }
        if (!this.campusInfo.website) {
          this.campusInfoErrors.push('Campus Website is required.');
        }
        if (!this.campusInfo.officialEmail) {
          this.campusInfoErrors.push('Office Email is required.');
        }
        if (!this.campusInfo.establishedSince) {
          this.campusInfoErrors.push('Established yaer is required.');
        }
        if (!this.campusInfo.levelOfEducation) {
          this.campusInfoErrors.push('Level of Education is required.');
        }
        if (this.campusInfo['affiliations'].length > 0) {
          this.campusInfo['affiliations'].forEach(element => {
            if (!element['affiliation']) {
              this.campusInfoErrors.push('Affiliation is required.');
            }
          });
        } else {
          this.campusInfoErrors.push('Affiliations are required.');
        }
        // check for faculty programs
        // this.validateFacultyPrograms();
        // check for address
        if (this.campusInfo['addresses'].length > 0) {
          this.campusInfo['addresses'].forEach(element => {
            if (!element['address']) {
              this.campusInfoErrors.push('Campus Address is required.');
            }
            if (!element['latitude'] || !element['longitude']) {
              this.campusInfoErrors.push('Campus Location is required.');
            }
          });
        } else {
          this.campusInfoErrors.push('Campus Address is required.');
        }

        if (!this.campusInfo.buildingOwn) {
          if (!this.campusInfo.rentAgreementUpTo) {
            this.campusInfoErrors.push('Campus rent agreement is required.');
          }
          this.validateBuildingInfrastructure();
        } else {
          this.validateBuildingInfrastructure();
        }
      }
    } else {
      this.campusInfoErrors.push('Campus information is required.');
    }
  }

  validateFacultyPrograms() {
    if (this.campusInfo['facultyProgram'].length > 0) {
      this.campusInfo['facultyProgram'].forEach(faculty => {
        if (faculty['programs'].length > 0) {
          // check for faculkty program
          faculty['programs'].forEach(program => {
            if (!program['programId']) {
              this.campusInfoErrors.push('Faculty program is required.');
            }
          });
        } else {
          this.campusInfoErrors.push('Faculty programs are required.');
        }
      });
    } else {
      this.campusInfoErrors.push('Faculty and programs are required.');
    }
  }
  validateBuildingInfrastructure() {
    if (this.campusInfo.coverdArea === 0 || !this.campusInfo.coverdArea) {
      this.campusInfoErrors.push('Covered area is required.');
    }
    if (this.campusInfo.openArea === 0 || !this.campusInfo.openArea) {
      this.campusInfoErrors.push('Open area is required');
    }
    if (this.campusInfo.roomsQuantity === 0 || !this.campusInfo.roomsQuantity) {
      this.campusInfoErrors.push('Rooms are required.');
    }
    if (this.campusInfo.teachingStaffQuantity === 0 || !this.campusInfo.teachingStaffQuantity) {
      this.campusInfoErrors.push('Teaching Staff is required.');
    }
    if (this.campusInfo.nonTeachingStaffQty === 0 || !this.campusInfo.nonTeachingStaffQty) {
      this.campusInfoErrors.push('Non Teaching Staff is required.');
    }
    if (this.campusInfo.washroomsQuantity === 0 || !this.campusInfo.washroomsQuantity) {
      this.campusInfoErrors.push('Number of washrooms are required.');
    }
    if (this.campusInfo.labsQuantity === 0 || !this.campusInfo.labsQuantity) {
      this.campusInfoErrors.push('Number of labs are required.');
    }
    if (this.campusInfo.studentsQuantity === 0 || !this.campusInfo.studentsQuantity) {
      this.campusInfoErrors.push('Number of students are required.');
    }
  }
}
