import { MetaConfModel, BankModel } from 'modules/configuration/models';
import { Campus } from './../../../../../../../backend/src/modules/campus/models/schema/campus';
import { Observable } from 'rxjs/Observable';
import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgControl, FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { MatDialog, MatDialogRef } from '@angular/material';
import { RemittanceModel } from '../../../models';
import { LayoutService } from 'modules/layout/services';
import { MetaConfService, BankService } from 'modules/configuration/services';
import { RemittanceService, CampusPostRegistrationService } from '../../../services';

declare const tinymce: any;
@Component({
  selector: 'remittance',
  templateUrl: './remittance.component.html',
  styleUrls: ['./remittance.component.css']
})
export class RemittanceComponent implements OnInit, AfterViewInit {
  // public loaded: boolean = false;
  public loaded: boolean = false;
  @Input() campusApplicantId: number;
  public fg: FormGroup;
  public pageAct: string;
  public pageTitle: string = 'Remittence';
  public instrumentCategory = ['Bank Draft', 'Cheque'];
  public bankName = ['AlliedBank', 'Alfalah Bank', 'Habib Bank', 'Meezan Bank'];
  public purposes = ['Application Form', 'Registraion Form', 'Cancel Form'];
  public id: number;
  public banks: BankModel[];
  public componentLabels = RemittanceModel.attributesLabels;
  public isSubmit: boolean = false;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private layoutService: LayoutService, //public dialog: MatDialog // private instituteEditService: InstituteService
    private remittanceService: RemittanceService,
    private campusPostRegistrationService: CampusPostRegistrationService,
    private banksService: BankService
  ) {
    this.pageAct = route.snapshot.data['act'];
  }

  /**
   * ngOnInit
   */

  ngOnInit() {
    this.layoutService.setPageTitle({ title: this.pageTitle });
    this.findBanks();
    this.initializePage();
    this.getRegistrationStatus();
  }

  ngAfterViewInit() {
    // tinymce.init({
    //   selector: 'editor', // change this value according to your HTML
    //   toolbar: 'image',
    //   plugins: 'image imagetools',
    //   imagetools_cors_hosts: ['mydomain.com', 'otherdomain.com']
    // });
  }
  private initializePage() {
    this.fg = this.fb.group(new RemittanceModel().validationRules());
    var _userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : _userId;
    this.remittanceService.view(this.campusApplicantId).subscribe(
      response => {
        this.loaded = true;
        if (response) {
          this.fg.patchValue(response);
        }
      },
      error => {},
      () => {}
    );
  }

  public saveData(item: RemittanceModel) {
    var _userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : _userId;
    const _data = {
      userId: this.campusApplicantId,
      remittance: item
    };
    this.remittanceService.save(_data).subscribe(
      response => {},
      error => {},
      () => {
        this.router.navigate(['/post-registration/campus/submit']);
      }
    );
  }

  findBanks() {
    this.loaded = false;

    this.banksService.findAttributesList().subscribe(
      response => {
        this.loaded = true;

        this.banks = response;
      },
      error => {},
      () => {}
    );
  }

  getRegistrationStatus() {
    var _userId = Number(localStorage.getItem('id'));
    this.campusApplicantId = this.campusApplicantId ? this.campusApplicantId : _userId;
    this.loaded = false;
    this.campusPostRegistrationService.getRegistrationStatus(this.campusApplicantId).subscribe(
      res => {
        this.loaded = true;
        if (res === 'submit') {
          this.isSubmit = true;
          this.fg.disable();
        } else {
          this.fg.enable();
          this.isSubmit = false;
        }
      },
      error => {},
      () => {}
    );
  }
}
