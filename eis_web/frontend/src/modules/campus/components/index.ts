export * from './pre_registration/list/pre-registration-list.component';
export * from './pre_registration/view/pre-registration-view.component';
export * from './campus-post-registration/campus-registration.component';
export * from './campus-post-registration/personal_information/personal-information.component';
export * from './campus_info/campus-info.component';
export * from './campus_info/create_campus/create-campus.component';
export * from './campus_info/campus_address/campus-address.component';
export * from './campus_info/campus_infrastructure/campus-infrastructure.component';
export * from './campus_info/campus_transfer/campus-transfer.component';
export * from './map/map.component';
export * from './campus-post-registration/remittance/remittance.component';
export * from './campus_info/faculty-program/faculty-program.component';
export * from './campus_info/faculty-program-details/faculty-program-details.component';

export * from './dashboard/dashboard.component';
export * from './campus-post-registration/submit/submit.component';
export * from './pre_registration/view/submit_view/pre-registration-submit-view.component';

