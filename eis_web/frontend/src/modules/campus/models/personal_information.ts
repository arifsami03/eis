import { FormControl, Validators, ValidatorFn } from '@angular/forms';
import { MinLengthValidator } from '@angular/forms/src/directives/validators';

export class PersonalInformationModel {
  id?: number;
  fullName: string;
  email: string;
  mobileNumber: string;
  address: string;
  cnic: string;
  countryId: number;
  provinceId: number;
  tehsilId: number;
  cityId: number;
  natureOfWorkId: number;
  approxMonthlyIncome: number;
  nearestBankId: number;
  refName: string;
  refMobileNumber: string;
  refAddress: string;

  static attributesLabels = {
    fullName: 'Full Name',
    email: 'Email',
    mobileNumber: 'Mobile No.',
    address: 'Address',
    cnic: 'CNIC',
    countryId: 'Country',
    provinceId: 'Province',
    tehsilId: 'Tehsil',
    cityId: 'City',
    natureOfWorkId: 'Nature Of Work',
    approxMonthlyIncome: 'Approx. Monthly Income',
    nearestBankId: 'Nearest Bank',
    refName: 'Name',
    refMobileNumber: 'Mobile No.',
    refAddress: 'Address'
  };

  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      fullName: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      // last_name: new FormControl('', [<any>Validators.required, Validators.maxLength(30)]),
      email: ['', [<any>Validators.required, Validators.email]],
      mobileNumber: ['', [<any>Validators.maxLength(12)]],
      address: ['', [<any>Validators.maxLength(100)]],
      cnic: ['', [<any>Validators.required]],
      countryId: [''],
      cityId: [''],
      provinceId: [''],
      tehsilId: [''],
      state: [''],
      zip: [''],
      country: [''],
      description: [''],
      nearestBankId: [''],
      approxMonthlyIncome: [''],
      natureOfWorkId: [''],
      refName: [''],
      refMobileNumber: ['', [<any>Validators.maxLength(12)]],
      refAddress: [''],
      
    };
  }
}
