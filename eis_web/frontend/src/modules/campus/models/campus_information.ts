import { FormControl, Validators, ValidatorFn, FormBuilder, AbstractControl } from '@angular/forms';
import { MinLengthValidator } from '@angular/forms/src/directives/validators';

export class CampusInformationModel {
  id?: number;
  campusType: boolean;
  campusName: string;
  levelOfEducation: string;
  tentativeSessionStart: Date;
  buildingAvailable: boolean;
  buildingOwn: boolean;
  rentAgreementUpTo: string;
  coverdArea: number;
  openArea: number;
  totalArea: number;
  roomsQuantity: number;
  washroomsQuantity: number;
  teachingStaffQuantity: number;
  labsQuantity: number;
  nonTeachingStaffQty: number;
  studentsQuantity: number;
  playGround: boolean;
  swimmingPool: boolean;
  healthClinic: boolean;
  mosque: boolean;
  cafeteria: boolean;
  transport: boolean;
  library: boolean;
  bankBranch: boolean;
  website: string;
  officialEmail: string;
  establishedSince: string;
  noOfCampusTransfer: number;



  static attributesLabels = {
    // fullName: 'Full Name',
    // email: 'Email',
    // mobileNumber: 'Mobile No.',
    // address: 'Address',
    // cnic: 'CNIC',
    // countryId: 'Country',
    // provinceId: 'Province',
    // tehsilId: 'Tehsil',
    // cityId: 'City',
    // natureOfWorkId: 'Nature Of Work',
    // approxMonthlyIncome: 'Approx. Monthly Income',
    // nearestBankId: 'Nearest Bank',
    // refName: 'Name',
    // refMobileNumber: 'Mobile No.',
    // refAddress: 'Address'
  };

  constructor() { }

  /**
   *
   * @param equalControlName
   */
  public equalTo?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      var d = new Date();
      var currentYear = d.getFullYear();

      let controlMatch = 1850;

      return (controlMatch <= control.value && currentYear >= control.value)
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {


      let controlMatch = 0;

      return (controlMatch <= control.value && 9999999999 >= control.value)
        ? null
        : {
          equalTo: true
        };
    };
  }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    let fb = new FormBuilder();
    return {

      campusType: [true],
      campusName: new FormControl('', [<any>Validators.required, Validators.maxLength(50)]),
      levelOfEducation: [''],
      tentativeSessionStart: [''],
      buildingAvailable: [true],
      buildingOwn: [false],
      rentAgreementUpTo: [''],
      coverdArea: ['', [this.greaterThan('coverdArea')]],
      openArea: ['', [this.greaterThan('openArea')]],
      totalArea: ['', [this.greaterThan('totalArea')]],
      roomsQuantity: ['', [this.greaterThan('roomsQuantity')]],
      washroomsQuantity: ['', [this.greaterThan('washroomsQuantity')]],
      teachingStaffQuantity: ['', [this.greaterThan('teachingStaffQuantity')]],
      labsQuantity: ['', [this.greaterThan('labsQuantity')]],
      nonTeachingStaffQty: ['', [this.greaterThan('nonTeachingStaffQty')]],
      studentsQuantity: ['', [this.greaterThan('studentsQuantity')]],
      playGround: [false],
      swimmingPool: [false],
      healthClinic: [false],
      mosque: [false],
      cafeteria: [false],
      transport: [false],
      library: [false],
      bankBranch: [false],
      website: [''],
      officialEmail: ['', [<any>Validators.email]],
      addresses: fb.array([]),
      officailEmail: [''],
      establishedSince: ['', [this.equalTo('establishedSince')]],
      noOfCampusTransfer: ['', [this.greaterThan('noOfCampusTransfer')]],
      affiliations: fb.array([]),
      deletedAffiliationIds: fb.array([]),
    };


  }

}
