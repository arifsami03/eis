import {
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';
// import { MinLengthValidator } from '@angular/forms/src/directives/validators';

export class RemittanceModel {
  id?: number;
  instrumentCategory: string;
  currentDate: Date;
  bankId: string;
  branchName: string;
  instrumentDate: Date;
  instrumentNumber: string;
  amount: number;
  purpose: string;
  descriptionEditor: string;

  static attributesLabels = {
    instrumentCategory: 'Instrument Category',
    currentDate: 'Choose Date',
    bankId: 'Bank Name',
    branchName: 'Branch Name',
    instrumentDate: 'Instrument Date',
    instrumentNumber: 'Instrument Number',
    amount: 'Amount',
    purpose: 'Purpose',
    descriptionEditor: 'Description'
  };
  /**
   *
   * @param equalControlName
   */
  public greaterThan?(equalControlName): ValidatorFn {
    return (
      control: AbstractControl
    ): {
        [key: string]: any;
      } => {
      let controlMatch = 0;

      return controlMatch <= control.value && 9999999999 >= control.value
        ? null
        : {
          equalTo: true
        };
    };
  }
  constructor() { }

  /**
   * Form Validation Rules
   */
  public validationRules?() {
    return {
      instrumentCategory: [''],
      currentDate: [null],
      bankId: [''],
      branchName: [''],
      instrumentDate: [null],
      instrumentNumber: [''],
      amount: ['', this.greaterThan('amount')],
      purpose: ['']
    };
  }
}
