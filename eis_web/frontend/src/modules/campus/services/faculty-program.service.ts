import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class FacultyProgramService {

  // Observable string sources
  private facultyPrograms = new Subject<any>();

  sendFacultyProgram(facultyProgram: any) {
    this.facultyPrograms.next({ facultyProgram: facultyProgram });
  }


  // Service message commands
  getFacultyPrograms(): Observable<any> {
    return this.facultyPrograms.asObservable();

  }

}