import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/services/base.service';

@Injectable()
export class CampusOwnerService extends BaseService {
  private _url: String = 'campusOwner';

  constructor(protected http: Http) {
    super(http);
  }

  findPersonInfo(userId:number){
    return this.__get(`${this._url}/registration/findPersonalInfo/${userId}`)
    .map(data => {
      return <any[]>data.json();
    })
    .catch(this.handleError);
  }
  update(id: Number, item) {
    return this.__put(`${this._url}/registration/update/${id}`, item).map(data => {
      return data.json();
    });
  }
}
