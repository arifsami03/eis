import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/services/base.service';

@Injectable()
export class CampusPostRegistrationService extends BaseService {
  private _url: String = 'campus';

  constructor(protected http: Http) {
    super(http);
  }

  submitPostRegistration(_id: number) {
    return this.__get(
      `${this._url}/postRegistration/submitPostRegistration/${_id}`
    )
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  getRegistrationStatus(_id: number) {
    return this.__get(
      `${this._url}/postRegistration/getRegistrationStatus/${_id}`
    )
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
}
