import 'rxjs/add/operator/map';
import { Injectable, EventEmitter } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../../environments/environment';
import { BaseService } from '../../shared/services/base.service';

@Injectable()
export class CampusPreRegistrationService extends BaseService {
  private _url: String = 'campus';

  constructor(protected http: Http) {
    super(http);
  }

  preRegistrationList(_status) {
    return this.__get(`${this._url}/preRegistration/list/${_status}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  preRegistrationDetailOne(_id: number) {
    return this.__get(`${this._url}/preRegistration/detail/${_id}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
  changeStatus(_id: number, _status: any) {
    return this.__put(`${this._url}/preRegistration/status/${_id}`, _status)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }

  findPersonInfo(userId: number) {
    return this.__get(`${this._url}/registration/findPersonalInfo/${userId}`)
      .map(data => {
        return <any[]>data.json();
      })
      .catch(this.handleError);
  }
}
