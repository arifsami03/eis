import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  RemittanceComponent,
  DashboardComponent,
  PreRegistrationSubmitViewComponent,
  SubmitComponent
} from './components';

/**
 * Available routing paths
 */
const routes: Routes = [
  {
    path: 'pre-registrations',
    data: {
      breadcrumb: { title: 'Pre Registration', display: true, disable: true }
    },
    children: [
      {
        path: 'new',
        data: {
          breadcrumb: { title: 'Awaiting Applications', display: true }
        },
        children: [
          {
            path: '',
            component: PreRegistrationListComponent,
            data: {
              breadcrumb: { title: 'Awaiting Applications', display: false }
            }
          },
          {
            path: ':id',
            component: PreRegistrationViewComponent,
            data: { breadcrumb: { title: 'Detail', display: true } }
          }
        ]
      },
      {
        path: 'approve',
        data: {
          breadcrumb: { title: 'Approved Applications', display: true }
        },
        children: [
          {
            path: '',
            component: PreRegistrationListComponent,
            data: {
              breadcrumb: { title: 'Approved Applications', display: false }
            }
          },
          {
            path: ':id',
            component: PreRegistrationViewComponent,
            data: { breadcrumb: { title: 'Detail', display: true } }
          }
        ]
      },
      {
        path: 'reject',
        data: {
          breadcrumb: { title: 'Rejected Applications', display: true }
        },
        children: [
          {
            path: '',
            component: PreRegistrationListComponent,
            data: {
              breadcrumb: { title: 'Rejected Applications', display: false }
            }
          },
          {
            path: ':id',
            component: PreRegistrationViewComponent,
            data: { breadcrumb: { title: 'Detail', display: true } }
          }
        ]
      },
      {
        path: 'submit',
        data: {
          breadcrumb: { title: 'Submitted Applications', display: true }
        },
        children: [
          {
            path: '',
            component: PreRegistrationListComponent,
            data: {
              breadcrumb: { title: 'Submitted Applications', display: false }
            }
          },
          {
            path: ':id',
            component: PreRegistrationSubmitViewComponent,
            data: { breadcrumb: { title: 'Detail', display: true } }
          }
        ]
      }
    ]
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { breadcrumb: { title: 'Dashboard', display: true } }
  },
  {
    path: 'personalinfo',
    component: PersonalInformationComponent
  },
  {
    path: 'campusinfo',
    component: CampusInfoComponent
  },
  {
    path: 'remittance',
    component: RemittanceComponent
  },
  {
    path: 'submit',
    component: SubmitComponent
  }
];

/**
 * NgModule
 */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampusRoutes {}
