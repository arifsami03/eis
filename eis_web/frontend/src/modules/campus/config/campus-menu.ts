export const CampusMenu = [
  {
    heading: 'Campus',
    menuItems: [
      { title: 'Dashboard', url: ['/campus/dashboard'], icon: 'dashboard', perm: 'n/a' }
    ]
  },
  {
    heading: 'Pre-Registrations',
    menuItems: [
      {
        title: 'Awaiting',
        url: ['/campus/pre-registrations/new'],
        icon: 'list', perm: 'n/a'
      },
      {
        title: 'Approved',
        url: ['/campus/pre-registrations/approve'],
        icon: 'list', perm: 'n/a'
      },
      {
        title: 'Rejected',
        url: ['/campus/pre-registrations/reject'],
        icon: 'list', perm: 'n/a'
      },
      {
        title: 'Submitted',
        url: ['/campus/pre-registrations/submit'],
        icon: 'list', perm: 'n/a'
      }
      // { title: 'Qualifications', url: ['/campus'], icon: 'list' },
      // { title: 'Awaiting Approvals', url: ['/campus/pre-registrations'], icon: 'list' },
      // { title: 'Approved', url: ['/campus'], icon: 'list' },
      // { title: 'Rejected', url: ['/campus'], icon: 'list' },
      // { title: 'Post Registration', url: ['/campus/personalinfo'], icon: 'list' }
    ]
  }
];
