/**
 * Import angular, material and other related modules here
 */
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule,
  MatSnackBarModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TextMaskModule } from 'angular2-text-mask';
import 'hammerjs';
import { AgmCoreModule } from '@agm/core';
/**
 * Import (self created) Services, Components, models etc here.
 */

/**
 * Services
 */
import {
  CampusPreRegistrationService,
  CampusPostRegistrationService,
  CampusInfoService,
  RemittanceService,
  FacultyProgramService,
  CampusOwnerService
} from './services';

import { FacultyService, ProgramService, ProgramDetailsService } from 'modules/institute/services';
import { BankService } from 'modules/configuration/services';

/**
 * Components
 */
import {
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  CampusRegistrationComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  CreateCampusComponent,
  CampusAddressComponent,
  CampusInfrastructureComponent,
  CampusTransferComponent,
  MapComponent,
  RemittanceComponent,
  FacultyProgramComponent,
  FacultyProgramDetailsComponent,
  DashboardComponent,
  PreRegistrationSubmitViewComponent,
  SubmitComponent
} from './components';

/**
 * Export consts here.
 */
export const __IMPORTS = [
  MatFormFieldModule,
  MatInputModule,
  MatButtonModule,
  MatToolbarModule,
  MatSidenavModule,
  MatTableModule,
  MatSelectModule,
  MatMenuModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatCardModule,
  MatRadioModule,
  MatIconModule,
  MatListModule,
  MatCheckboxModule,
  MatDialogModule,
  MatTooltipModule,
  MatPaginatorModule,
  MatTabsModule,
  FormsModule,
  ReactiveFormsModule,
  TextMaskModule,
  FlexLayoutModule,
  AgmCoreModule.forRoot({
    apiKey: 'AIzaSyAaYDoRGc-X4Q5DPpCBxqrbgx5XOp8YisE'
  }),
  MatSnackBarModule
];

export const __DECLARATIONS = [
  PreRegistrationListComponent,
  PreRegistrationViewComponent,
  CampusRegistrationComponent,
  PersonalInformationComponent,
  CampusInfoComponent,
  CreateCampusComponent,
  CampusAddressComponent,
  MapComponent,
  CampusInfrastructureComponent,
  CampusTransferComponent,
  RemittanceComponent,
  FacultyProgramComponent,
  FacultyProgramDetailsComponent,
  DashboardComponent,
  PreRegistrationSubmitViewComponent,
  SubmitComponent
];

export const __PROVIDERS = [
  CampusPreRegistrationService,
  CampusPostRegistrationService,
  CampusInfoService,
  RemittanceService,
  CampusOwnerService,
  FacultyService,
  ProgramService,
  FacultyProgramService,
  BankService,
  ProgramDetailsService
];

export const __ENTRY_COMPONENTS = [MapComponent];
